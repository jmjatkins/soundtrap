// SoundTrap Software v1.0
//
// Copyright (C) 2011-2014, John Atkins and Mark Johnson
//
// This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
//
// This file is part of the SoundTrap software. SoundTrap is an acoustic
// recording system intended for underwater acoustic measurements. This
// component of the SoundTrap project is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or any later version.
//
// The SoundTrap software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this code. If not, see <http://www.gnu.org/licenses/>.


#include <serialFlash.h>
#include <spi.h>
#include <stdio.h>
#include <tistdtypes.h>
#include "soc.h"
#include "csl_pll.h"
#include "csl_intc.h"
#include "csl_general.h"
#include "csl_pll.h"

#include "d3std.h"
#include "flash.h"
#include "usb.h"
#include "messageProcessor.h"
#include "tick.h"
#include "i2c.h"
#include "sysControl.h"
#include "gpio.h"
#include "csl_dma.h"
#include "crc.h"
#include "mspif.h"
#include "config.h"
#include "uart.h"
#include "timer.h"
#include "audioTest.h"
#include "dma.h"
#include "commands.h"
#include "sar.h"
#include "hid.h"
#include "ioExpander.h"
#include "readMcpAdc.h"
#include "irq.h"
#include "protect.h"


PLL_Obj pllObj;
const Uint32  sysclk = 73728000;

extern void VECSTART(void);
MSP_Sensors sensorData;
Uint16 flashState = 10;

Uint32 getRtcTime()
{
	MSP_Time t;
	if(msp_gettime(&t,NULL,NULL)==MSP_FAIL)
		return(0ul);
	return(t.rtime);
}

void pllInit(Uint32 clk)
{
    PLL_Config configInfo;
    Uint16 d;
    Uint16 m;

	//asm("	BCLR CLKOFF,ST3_55"); //enable clk out

    switch(clk) {
		case 25000000:
			m = 1526;
			d = 2;
			break;
		case 50000000:
			m = 1526;
			d = 1;
			break;
		case 73728000:
			m = 2249; //due to hardware bug 2249 gives multiple of 2250 - see TI forum  'C5535 PLL multiplier anomaly' 20 Aug '14
			d = 1;
			break;
		case 100000000:
			m = 3052;
			d = 1;
			break;
	}

	PLL_init(&pllObj,0);
    
    configInfo.PLLCNTL1 = (m-4) & 0x0FFF; //CGCR1
    configInfo.PLLINCNTL = 0x8000;	//CGCR2
    configInfo.PLLOUTCNTL = d > 1 ? 0x0200 | (d-1) : 0x0000; //CRCR4
    configInfo.PLLCNTL2 = 0x0806; //CGCR3
    PLL_bypass(&pllObj);
    PLL_reset(&pllObj);
    PLL_config(&pllObj, &configInfo);
 	PLL_enable(&pllObj);
}

void ledOff()
{
	gpioSetVal( GPIO_BIT_GREEN_LED, 1);
}

void blink()
{
	flashState--;
	if(flashState > 10) {
		gpioSetVal( GPIO_BIT_GREEN_LED, 0);
		tickRunOnce(&ledOff, 50);
	}
	else if(!flashState) {
		flashState = 10;
		gpioSetVal( GPIO_BIT_GREEN_LED, 0);
		tickRunOnce(&ledOff, 50);
	}

}

void sensorScan()
{
	msp_getSensorData(&sensorData);
}

void main(void)
{

	int	*ier0 = (int *)0 ;
	int	*ier1 = (int *)0x45 ; //JMJA Checked OK

	// Disable all interrupts and clear any pending flags.
	// IFR0 and IFR1 are at the next address up from IER0 and IER1.
	*ier0 = 0x0000 ;
	*(ier0+1) = 0x0000 ;
	*ier1 = 0x0000 ;
	*(ier1+1) = 0x0000 ;

	IRQ_globalDisable() ;		// disable all interrupts

	sysControlInit();
	sysControlResetPeripheral(0xFFFF);
	IRQ_setVecs((Uint32)(&VECSTART)) ;

	pllInit(sysclk);

	gpioInit();

	spiInit(sysclk);
	serialFlashInit();

	timerInit();
	tickInit(sysclk);


	i2cInit(sysclk);
	sarInit();
	END_PROTECT ;       // enable global interrupts
	END_ATOMIC ;
	IRQ_globalEnable(); //interrupts required for hidInit

	hidInit();

	if(hid < ST600) {
		uartInit(sysclk, 115200);
		uartEnableTx(FALSE);
		uartSelectTransceiver(FALSE);
	}

	ioeInit(AUDIO_IO_EXPANDER_DEVICE_ADDRESS, 0xFFC0, 0x0000);
	gpioSetVal(GPIO_BIT_GREEN_LED, TRUE);

	if ((hid == ST4300) || (hid == ST500)) {
		ioeInit(AUDIO_IO_EXPANDER_DEVICE_ADDRESS_MC, 0x00C0, 0x0007);
	}

	if(hid == ST500) {
		ioeWritePU(AUDIO_IO_EXPANDER_DEVICE_ADDRESS, 0x38);
	}

	tickSetUnixTime(getRtcTime());

	dmaInterruptHandlerInit();
	audioTestInit();

	flashInit();
	
	messageProcessorInit();
	usbInit();


	tickRunEvery(&blink, 100);

	msp_getFirmwareVersion();

	tickRunEvery(&sensorScan, 30000);
	if(hid >= ST600) tickRunEvery(&readMcp, 200);

	configInit();

	sensorScan();

	while(1) {
		parseHeader();
		usbServiceDataOut();
	    tickProcessCallbacks();
	}
}
