################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
ASM_SRCS += \
../src/csl_irqplug.asm \
../src/vectors.asm 

C_SRCS += \
../src/csl_audioClass.c \
../src/csl_cdc.c \
../src/csl_dat.c \
../src/csl_dma.c \
../src/csl_gpio.c \
../src/csl_gpt.c \
../src/csl_i2c.c \
../src/csl_i2s.c \
../src/csl_intc.c \
../src/csl_lcdc.c \
../src/csl_mem.c \
../src/csl_mmcsd.c \
../src/csl_mmcsd_ataIf.c \
../src/csl_msc.c \
../src/csl_nand.c \
../src/csl_pll.c \
../src/csl_rtc.c \
../src/csl_sar.c \
../src/csl_sdio.c \
../src/csl_spi.c \
../src/csl_uart.c \
../src/csl_usb.c \
../src/csl_wdt.c 

OBJS += \
./csl_audioClass.obj \
./csl_cdc.obj \
./csl_dat.obj \
./csl_dma.obj \
./csl_gpio.obj \
./csl_gpt.obj \
./csl_i2c.obj \
./csl_i2s.obj \
./csl_intc.obj \
./csl_irqplug.obj \
./csl_lcdc.obj \
./csl_mem.obj \
./csl_mmcsd.obj \
./csl_mmcsd_ataIf.obj \
./csl_msc.obj \
./csl_nand.obj \
./csl_pll.obj \
./csl_rtc.obj \
./csl_sar.obj \
./csl_sdio.obj \
./csl_spi.obj \
./csl_uart.obj \
./csl_usb.obj \
./csl_wdt.obj \
./vectors.obj 

ASM_DEPS += \
./src/csl_irqplug.pp \
./src/vectors.pp 

C_DEPS += \
./src/csl_audioClass.pp \
./src/csl_cdc.pp \
./src/csl_dat.pp \
./src/csl_dma.pp \
./src/csl_gpio.pp \
./src/csl_gpt.pp \
./src/csl_i2c.pp \
./src/csl_i2s.pp \
./src/csl_intc.pp \
./src/csl_lcdc.pp \
./src/csl_mem.pp \
./src/csl_mmcsd.pp \
./src/csl_mmcsd_ataIf.pp \
./src/csl_msc.pp \
./src/csl_nand.pp \
./src/csl_pll.pp \
./src/csl_rtc.pp \
./src/csl_sar.pp \
./src/csl_sdio.pp \
./src/csl_spi.pp \
./src/csl_uart.pp \
./src/csl_usb.pp \
./src/csl_wdt.pp 

C_DEPS__QUOTED += \
"src\csl_audioClass.pp" \
"src\csl_cdc.pp" \
"src\csl_dat.pp" \
"src\csl_dma.pp" \
"src\csl_gpio.pp" \
"src\csl_gpt.pp" \
"src\csl_i2c.pp" \
"src\csl_i2s.pp" \
"src\csl_intc.pp" \
"src\csl_lcdc.pp" \
"src\csl_mem.pp" \
"src\csl_mmcsd.pp" \
"src\csl_mmcsd_ataIf.pp" \
"src\csl_msc.pp" \
"src\csl_nand.pp" \
"src\csl_pll.pp" \
"src\csl_rtc.pp" \
"src\csl_sar.pp" \
"src\csl_sdio.pp" \
"src\csl_spi.pp" \
"src\csl_uart.pp" \
"src\csl_usb.pp" \
"src\csl_wdt.pp" 

OBJS__QUOTED += \
"csl_audioClass.obj" \
"csl_cdc.obj" \
"csl_dat.obj" \
"csl_dma.obj" \
"csl_gpio.obj" \
"csl_gpt.obj" \
"csl_i2c.obj" \
"csl_i2s.obj" \
"csl_intc.obj" \
"csl_irqplug.obj" \
"csl_lcdc.obj" \
"csl_mem.obj" \
"csl_mmcsd.obj" \
"csl_mmcsd_ataIf.obj" \
"csl_msc.obj" \
"csl_nand.obj" \
"csl_pll.obj" \
"csl_rtc.obj" \
"csl_sar.obj" \
"csl_sdio.obj" \
"csl_spi.obj" \
"csl_uart.obj" \
"csl_usb.obj" \
"csl_wdt.obj" \
"vectors.obj" 

ASM_DEPS__QUOTED += \
"src\csl_irqplug.pp" \
"src\vectors.pp" 

C_SRCS__QUOTED += \
"../src/csl_audioClass.c" \
"../src/csl_cdc.c" \
"../src/csl_dat.c" \
"../src/csl_dma.c" \
"../src/csl_gpio.c" \
"../src/csl_gpt.c" \
"../src/csl_i2c.c" \
"../src/csl_i2s.c" \
"../src/csl_intc.c" \
"../src/csl_lcdc.c" \
"../src/csl_mem.c" \
"../src/csl_mmcsd.c" \
"../src/csl_mmcsd_ataIf.c" \
"../src/csl_msc.c" \
"../src/csl_nand.c" \
"../src/csl_pll.c" \
"../src/csl_rtc.c" \
"../src/csl_sar.c" \
"../src/csl_sdio.c" \
"../src/csl_spi.c" \
"../src/csl_uart.c" \
"../src/csl_usb.c" \
"../src/csl_wdt.c" 

ASM_SRCS__QUOTED += \
"../src/csl_irqplug.asm" \
"../src/vectors.asm" 


