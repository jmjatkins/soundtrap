/*
 * readMcpAdc.h
 *
 *  Created on: 12/11/2020
 *      Author: John
 */

#ifndef INC_READMCPADC_H_
#define INC_READMCPADC_H_

extern Int32 adcData[];
void readMcpSetCallBack(void (*callBack)() );
void readMcp();

#endif /* INC_READMCPADC_H_ */
