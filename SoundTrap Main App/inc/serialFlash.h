/*
 * serialFlash.h
 *
 *  Created on: 23/04/2020
 *      Author: John
 */

#ifndef INC_SERIALFLASH_H_
#define INC_SERIALFLASH_H_

#include <tistdtypes.h>

Uint16 serialFlashReadID(Uint16 *buf, Uint16 count);
Uint16 serialFlashWrite(Uint32 address, Uint16 *buf, Uint16 count);
Uint16 serialFlashRead(Uint32 address, Uint16 *buf, Uint16 count);
Uint16 serialFlashWriteEnable(Uint16 enable);
Uint16 serialFlashWritePage(Uint32 address, Uint16 *buf, Uint16 count);
Uint16 serialFlashEraseBlock(Uint32 address);
Uint16 serialFlashEraseSector(Uint32 address);
Uint16 serialFlashChipErase();

int serialFlashInit();

#define ID_SFLASH_ADDRESS 0x1FA000
#define CONFIG_SFLASH_ADDRESS 0x1FC000



#endif /* INC_SERIALFLASH_H_ */
