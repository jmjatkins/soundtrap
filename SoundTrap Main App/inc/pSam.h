/*
 * psam.h
 *
 *  Created on: 7/09/2016
 *      Author: jatk009
 */

#ifndef PSAM_H_
#define PSAM_H_


int	pSamClose(int id);
int	pSamOpen(JOB_Fxn outputJob);


#endif /* PSAM_H_ */
