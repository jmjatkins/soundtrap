/*
 * serialMem.h
 *
 *  Created on: 12/11/2020
 *      Author: John
 */

#ifndef INC_SERIALMEM_H_
#define INC_SERIALMEM_H_


int serialMemGetFirmwareDetails(char *ver, Uint16 maxCount, Uint16 *count);



#endif /* INC_SERIALMEM_H_ */
