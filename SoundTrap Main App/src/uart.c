// SoundTrap Software v1.0
//
// Copyright (C) 2011-2014, John Atkins and Mark Johnson
//
// This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
//
// This file is part of the SoundTrap software. SoundTrap is an acoustic
// recording system intended for underwater acoustic measurements. This
// component of the SoundTrap project is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or any later version.
//
// The SoundTrap software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this code. If not, see <http://www.gnu.org/licenses/>.

#include <tistdtypes.h>
#include "d3defs.h"
#include "d3std.h"
#include "protect.h"
#include "gpio.h"
#include "irq.h"
#include "uart.h"
#include "dma.h"

typedef struct  {
    volatile Uint16 RBR;
    volatile Uint16 RSVD0;
    volatile Uint16 IER;
    volatile Uint16 RSVD1;
    volatile Uint16 IIR;
    volatile Uint16 RSVD2;
    volatile Uint16 LCR;
    volatile Uint16 RSVD3;
    volatile Uint16 MCR;
    volatile Uint16 RSVD4;
    volatile Uint16 LSR;
    volatile Uint16 RSVD5;
    volatile Uint16 MSR;
    volatile Uint16 RSVD6;
    volatile Uint16 SCR;
    volatile Uint16 RSVD7;
    volatile Uint16 DLL;
    volatile Uint16 RSVD8;
    volatile Uint16 DLH;
    volatile Uint16 RSVD9;
    volatile Uint16 PID1;
    volatile Uint16 PID2;
    volatile Uint16 RSVD10[2];
    volatile Uint16 PWREMU_MGMT;
    volatile Uint16 RSVD11;
    volatile Uint16 MDR;
} UartRegs;

volatile ioport UartRegs * uartRegs;

CSL_DMA_Handle uartDma;
CSL_DMA_ChannelObj uartDmaChan;
Fxn uartTxCompleteHandler;

UartRxCallbackHandler urxcb = NULL;

int uartTx(Uint16 *data, Uint16 count)
{
	Uint16 i = 0;
	while(i < count) {
		while(!(uartRegs->LSR & 0x40)) {};
		uartRegs->RBR = data[i++];
	}
	return 0;
}

int uartRx(Uint16 *data, Uint16 count)
{
	Uint16 i = 0;
	while(i < count) {
		while(!(uartRegs->LSR & 0x01)) {};
		data[i++] = uartRegs->RBR;
	}
	return 0;
}

int uartEnableTx(Bool enable)
{
	//gpioSetVal(GPIO_BIT_UART_TX_EN, enable);
	return 0;
}

int uartSelectTransceiver(Bool enable485)
{
	//gpioSetVal(GPIO_BIT_RS485, enable485);
	return 0;
}

int uartClearRx()
{
	int temp =  uartRegs->RBR;
	return 0;
}

int uartEnableRxInterrupt(int enable)
{
	if(enable) {
		uartRegs->IER |= 0x01;
	}
	else {
		uartRegs->IER &= ~0x01;
	}
	return 0;
}

interrupt void uartIsr(void)
{
	Uint16 data;
	PROTECT;
	data = uartRegs->RBR;
	if(urxcb != NULL) urxcb(data);
	END_PROTECT ;
}

void uartRegRxCallbackHandler( UartRxCallbackHandler cb )
{
	urxcb = cb;
}

int uartTxDma(Uint32 *data, Uint16 count)
{
    CSL_DMA_Config cfg;
	DMA_reset(uartDma);
	cfg.dmaEvt = CSL_DMA_EVT_UART_TX;
	cfg.dmaInt = CSL_DMA_INTERRUPT_ENABLE;
	cfg.autoMode = CSL_DMA_AUTORELOAD_DISABLE;
	cfg.burstLen = CSL_DMA_TXBURST_1WORD;
	cfg.chanDir = CSL_DMA_WRITE;
	cfg.dataLen = (count -1) * 4; //in bytes
	cfg.pingPongMode = CSL_DMA_PING_PONG_DISABLE;
	cfg.trfType = CSL_DMA_TRANSFER_IO_MEMORY;
	cfg.trigger = CSL_DMA_EVENT_TRIGGER;
	cfg.srcAddr = (Uint32) (data + 1);
	cfg.destAddr = (Uint32) (0x1B00);
	DMA_config(uartDma, &cfg);
	DMA_start(uartDma);
	uartRegs->RBR = *data;
    return OK;
}

void uartdmaWriteIsr(int i)
{
	if(uartTxCompleteHandler != NULL) {
		uartTxCompleteHandler(0);
	}
}

int uartSetTxCompleteHandler( void (*handler)(int status) )
{
	uartTxCompleteHandler = handler;
	return OK;
}

int uartDmaInit()
{
    CSL_Status status;
    uartDma = DMA_open(UART_DMA_WRITE_CHANNEL, &uartDmaChan, &status);
    if (status != CSL_SOK) {
        return FAIL;
    }
    dmaInterruptHandlerRegister(UART_DMA_WRITE_CHANNEL, uartdmaWriteIsr);
    return OK;
}

int uartInit(Uint32 sysClk, Uint32 baud)
{
	Uint16 divider;
	
	uartRegs = (volatile ioport UartRegs *)0x1B00;
	
	//gpioSetDir(GPIO_BIT_RS485, GPIO_DIR_OUT);
	//gpioSetDir(GPIO_BIT_UART_TX_EN, GPIO_DIR_OUT);
	
	//gpioSetVal(GPIO_BIT_RS485, FALSE);
	//gpioSetVal(GPIO_BIT_UART_TX_EN, FALSE);
	
	divider = sysClk / 16 / baud;
	
	uartRegs->PWREMU_MGMT = 0x6000; //enable
	uartRegs->DLH = divider >> 8; 
	uartRegs->DLL = divider & 0x00ff; 
	uartRegs->IIR = 0x0000; //disable fifo mode (IIR & FCR reg share same address)
	uartRegs->IER = 0x0001; //enable rx interrupt
	uartRegs->IIR = 0x0001;
	uartRegs->LCR = 0x0003; //8-bit
	uartRegs->IIR = 0x000F;
	irqClear(UART_EVENT);
	irqPlug (UART_EVENT, &uartIsr);
	irqEnable(UART_EVENT);

	return 0;
}
