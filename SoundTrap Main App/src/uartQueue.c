// SoundTrap Software v1.0
//
// Copyright (C) 2011-2014, John Atkins and Mark Johnson
//
// This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
//
// This file is part of the SoundTrap software. SoundTrap is an acoustic
// recording system intended for underwater acoustic measurements. This
// component of the SoundTrap project is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or any later version.
//
// The SoundTrap software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this code. If not, see <http://www.gnu.org/licenses/>.

#include <tistdtypes.h>
#include "d3std.h"
#include "d3defs.h"
#include "protect.h"
#include "gpio.h"
#include "irq.h"
#include "uart.h"
#include "dma.h"
#include "data.h"
#include "dmem.h"
#include "uart.h"
#include "job.h"
#include "cfg.h"

// write queue is a linked list of FLSH_Job objects
typedef struct uartQueueJob {
	DATA_Obj	*data ;			   // data object currently being read / written
	struct uartQueueJob *next ;	// next link in the list
	struct uartQueueJob *previous ;	// next link in the list
} uartQueueJob;

static uartQueueJob	*uartQueTail = NULL;
static uartQueueJob	*uartQueHead = NULL;
static uartQueueJob *uartQueCurrent = NULL;

volatile int busy = 0;
volatile int curBufPos = 0;

#define UQ_BUF_SIZE 256

Uint32 uartQueueBuff[(UQ_BUF_SIZE * 2) + 24];

void uartQueueRun()
{
	int j = 0;
	int i = 0;
	int wordsToSend;
	int wordsRemaining;
	Uint16 *p;

	busy = 1;

	if(uartQueCurrent != NULL) {
		wordsRemaining = uartQueCurrent->data->size - curBufPos;
		if(wordsRemaining == 0) {
			DATA_free(uartQueCurrent->data);
			DMEM_free(uartQueCurrent);
			uartQueCurrent = NULL;
		}
	}

	if(uartQueCurrent == NULL) {
		GO_ATOMIC ;
		uartQueCurrent = uartQueHead;
		curBufPos = 0;
		if( uartQueCurrent!=NULL ) {
			uartQueHead = uartQueCurrent->next ;
			if(uartQueHead == NULL)	// if there is no next job
				uartQueTail = NULL ;	// null the queue
		}
		END_ATOMIC ;
	}

	if(uartQueCurrent != NULL)
	{
		wordsRemaining = uartQueCurrent->data->size - curBufPos;
		wordsToSend = MIN(wordsRemaining, UQ_BUF_SIZE);

		if(curBufPos== 0) {
			uartQueueBuff[j++] = 'S'; //pre-amble
			uartQueueBuff[j++] = 'T';

			uartQueueBuff[j++] =  0;  //packet type
			uartQueueBuff[j++] =  1;

			uartQueueBuff[j++] = (uartQueCurrent->data->size >> 8) & 0x00ff;
			uartQueueBuff[j++] = uartQueCurrent->data->size & 0x00ff;
			uartQueueBuff[j++] = (uartQueCurrent->data->nsamps >> 8) & 0x00ff;
			uartQueueBuff[j++] = uartQueCurrent->data->nsamps & 0x00ff;

			uartQueueBuff[j++] = (uartQueCurrent->data->rtime >> 24) & 0x000000ff;
			uartQueueBuff[j++] = (uartQueCurrent->data->rtime >> 16) & 0x000000ff;
			uartQueueBuff[j++] = (uartQueCurrent->data->rtime >> 8) & 0x000000ff;
			uartQueueBuff[j++] = uartQueCurrent->data->rtime & 0x000000ff;

			uartQueueBuff[j++] = (uartQueCurrent->data->mticks >> 24) & 0x000000ff;
			uartQueueBuff[j++] = (uartQueCurrent->data->mticks >> 16) & 0x000000ff;
			uartQueueBuff[j++] = (uartQueCurrent->data->mticks >> 8) & 0x000000ff;
			uartQueueBuff[j++] = uartQueCurrent->data->mticks & 0x000000ff;

			uartQueueBuff[j++] = '?';
			uartQueueBuff[j++] = '?';
			uartQueueBuff[j++] =  (uartQueCurrent->data->fs >> 8) & 0x000000ff;
			uartQueueBuff[j++] =  uartQueCurrent->data->fs & 0x000000ff;
			uartQueueBuff[j++] = '?';
			uartQueueBuff[j++] = '?';
			uartQueueBuff[j++] = '?'; //TODO CRC
			uartQueueBuff[j++] = '?'; //TODO CRC
		}

		p = (Uint16 *)(uartQueCurrent->data->p) + curBufPos;
		for(i=0; i<wordsToSend; i++) {
			uartQueueBuff[j++] = p[i] & 0x00ff;
			uartQueueBuff[j++] = (p[i]>> 8) & 0x00ff;
		}

		uartTxDma(uartQueueBuff, j);

		curBufPos += wordsToSend;
		wordsRemaining -= wordsToSend;
	} else busy = 0;

}

void uartQueueOnTxCompleteHandler(int status)
{
	uartQueueRun();
}

int uartQueueProc(Ptr pt, DATA_Obj *d)
{
	uartQueueJob	*j ;

	if( d->nsamps==0 ) {
		return(OK) ;
	}

	// create a uart job
	if((j=(uartQueueJob *)DMEM_alloc(sizeof(uartQueueJob)))==(uartQueueJob *)NULL) {
		//run out of dmem. Will have to drop this packet
		DATA_free(d);
		return(OK);
	}
	j->data = d;
	j->next = NULL;
	GO_ATOMIC;
	if(uartQueHead == NULL) {
		uartQueHead = uartQueTail = j;
	}
	else {
		uartQueTail->next = j;
		uartQueTail = j;
	}
	END_ATOMIC;

	if(!busy) {
		uartQueueRun(); //kick off the tx process
	}

	return(OK);
}


int	uartQueueOpen()
{
	int id;
	uartSetTxCompleteHandler(uartQueueOnTxCompleteHandler);
	id = CFG_register((JOB_Fxn) uartQueueProc, NULL, NULL);
	return id;
}



