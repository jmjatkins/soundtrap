// SoundTrap Software v1.0
//
// Copyright (C) 2011-2014, John Atkins and Mark Johnson
//
// This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
//
// This file is part of the SoundTrap software. SoundTrap is an acoustic
// recording system intended for underwater acoustic measurements. This
// component of the SoundTrap project is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or any later version.
//
// The SoundTrap software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this code. If not, see <http://www.gnu.org/licenses/>.

/*
 * Implements MCP3424 plus Mux hardware interface.
 *
 * JMJA
 *
 */

#include <tistdtypes.h>
#include "soc.h"
#include "csl_intc.h"
#include "csl_general.h"

#include "d3std.h"
#include "tick.h"
#include "i2c.h"
#include "mspif.h"

#define ADC_CHANS 4
#define ADC_MUX_CHAN 4

Uint16 chan = 0;
Uint16 mux = 0;

Int16 adcData[ADC_CHANS*ADC_MUX_CHAN];

i2cTrans readMcpTrans;
i2cTrans setMuxTrans;
int readMcpInbuf[2];
int readMcpOutbuf[3];
void readMcpAdcCallBack(Ptr state);

void readMcpAdc()
{
	//i2c read of Mcp3424 in 12 bit mode
	readMcpOutbuf[0] = ((chan << 5) & 0x60)  | 0x0080;//  1000 0000 12 bit

	readMcpTrans.slaveAddress = 0x0068;
	readMcpTrans.dataOut = readMcpOutbuf;
	readMcpTrans.lengthOut = 1;
	readMcpTrans.dataIn = readMcpInbuf;
	readMcpTrans.lengthIn = 2;
	readMcpTrans.callback = readMcpAdcCallBack;
	readMcpTrans.state = NULL;
	readMcpTrans.result = -1;
	i2cPostTrans(&readMcpTrans);
}

void setMux()
{
	//set new Mux channel via MSP
	readMcpOutbuf[0] = 18;
	readMcpOutbuf[1] = mux + 1;
	readMcpOutbuf[2] = 0;

	setMuxTrans.slaveAddress = 0x0060;
	setMuxTrans.dataOut = readMcpOutbuf;
	setMuxTrans.lengthOut = 3;
	setMuxTrans.dataIn = readMcpInbuf;
	setMuxTrans.lengthIn = 0;
	setMuxTrans.callback = NULL;//readMcpAdc;
	setMuxTrans.state = NULL;
	setMuxTrans.result = -1;
	i2cPostTrans(&setMuxTrans);
}

void readMcpAdcCallBack(Ptr state)
{
	//store 12 bit adc result
	adcData[chan + (mux*ADC_MUX_CHAN)] = (readMcpInbuf[0] << 8) + readMcpInbuf[1];
	if(++chan >= ADC_CHANS) {
		chan = 0; //switch to next mux
		if(++mux >= ADC_MUX_CHAN) {
			mux = 0;
		}
	}
	setMux(); //set mux so its ready for next time
}

void readMcp()
{
	readMcpAdc();
}

