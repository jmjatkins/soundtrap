/*
 * serialFlash.c
 *
 *  Created on: 23/04/2020
 *      Author: John
 */

#include <tistdtypes.h>
#include <string.h>
#include "d3std.h"
#include "timr.h"
//#include "tick.h"
#include "spi.h"



#define SLAVE_ADDRESS 0x54
#define TIMEOUT 0xFFF0
#define TIMEOUT_MS 500
#define FLASH_PAGE_LENGTH 256 //128 words
#define FLASH_CHIP_SELECT 0

Uint16 serialFlashBuf[FLASH_PAGE_LENGTH+2];

Uint16 serialFlashTestBusy()
{
	Uint16 command[1];
	Uint16 status;
	command[0] = 0x0005; //read status reg
	spiWriteRead(FLASH_CHIP_SELECT, command, &status, 1, 1);
	return ( status & 0x0001 );
}

Uint16 serialFlashWaitForReady()
{
	Uint32 startTime;
	startTime = TIMR_GetTickCount();
	while( serialFlashTestBusy() ) {
		if(TIMR_GetTickCount() - startTime > TIMEOUT_MS) {
			return FAIL;
		}
	}
	return OK;
}



Uint16 serialFlashDeepPowerDown()
{
	Uint16 command[1];
	command[0] = 0x00B9; //deep power down
	return spiWrite(FLASH_CHIP_SELECT, command, 1);
}




Uint16 serialFlashWriteEnable(Uint16 enable)
{
	Uint16 command[1];
	command[0] = enable ? 0x0006 : 0x0004; //write enable/disable
	return spiWrite(FLASH_CHIP_SELECT, command, 1);
}

Uint16 serialFlashChipErase()
{
	Uint16 command[1];

	serialFlashWriteEnable(1);

	if( serialFlashWaitForReady() != OK ) return FAIL;
	if( serialFlashWriteEnable(1) != OK ) return FAIL;
	command[0] = 0x00C7; //chip erase
	return spiWrite(FLASH_CHIP_SELECT, command, 1);
}


Uint16 serialFlashReadID(Uint16 *buf, Uint16 count)
{
	Uint16 command[1];
	command[0] = 0x009F; //JEDEC-ID Read - SST26VF016BEUI should return 3 bytes = BF 26 41
	//return spiWrite(FLASH_CHIP_SELECT, command, 1);
	return spiWriteRead(FLASH_CHIP_SELECT, command, buf, 1, count);
}


Uint16 serialFlashEraseSector(Uint32 address)
{
	Uint16 command[4];

	command[0] = 0x0020; //sector erase
	command[1] = (address >> 16) & 0x00ff;
	command[2] = (address >> 8) & 0x00ff;
	command[3] = address & 0x00ff;

	return spiWrite(FLASH_CHIP_SELECT, command, 4);
}

Uint16 serialFlashEraseBlock(Uint32 address)
{
	Uint16 command[4];

	if( serialFlashWaitForReady() != OK ) return FAIL;
	if( serialFlashWriteEnable(1) != OK ) return FAIL;

	//serialFlashWriteEnable(1);

	command[0] = 0x00D8; //block erase
	command[1] = (address >> 16) & 0x00ff;
	command[2] = (address >> 8) & 0x00ff;
	command[3] = address & 0x00ff;

	return spiWrite(FLASH_CHIP_SELECT, command, 4);
}

Uint16 serialFlashWritePage(Uint32 address, Uint16 *buf, Uint16 count)
{
	Uint16 i;
	Uint16 command[4];

	if( serialFlashWaitForReady() != OK ) return FAIL;
	if( serialFlashWriteEnable(1) != OK ) return FAIL;

	if( count > FLASH_PAGE_LENGTH/2) count = FLASH_PAGE_LENGTH/2;

	command[0] = 0x0002; //page program
	command[1] = (address >> 16) & 0x00ff;
	command[2] = (address >> 8) & 0x00ff;
	command[3] = address & 0x00ff;

	for(i=0;i<count;i++) {
		serialFlashBuf[i*2] = buf[i] & 0x00ff;
		serialFlashBuf[(i*2)+1] = (buf[i] >> 8) & 0x00ff;
	}
	return spiWriteWithHeader(FLASH_CHIP_SELECT, command, serialFlashBuf, 4, count*2);
}

Uint16 serialFlashRead(Uint32 address, Uint16 *buf, Uint16 count)
{
	int i;
	Uint16 command[4];

	if( count > FLASH_PAGE_LENGTH/2) count = FLASH_PAGE_LENGTH/2;

	command[0] = 0x0003; //read
	command[1] = (address >> 16) & 0x00ff;
	command[2] = (address >> 8) & 0x00ff;
	command[3] = address & 0x00ff;

	if( serialFlashWaitForReady() != OK ) return FAIL;

	if( spiWriteRead(FLASH_CHIP_SELECT, command, serialFlashBuf, 4, count*2) != OK ) {
		return FAIL;
	}

	for(i=0;i<count;i++) {
		buf[i] = (serialFlashBuf[(i*2)+1] << 8) + serialFlashBuf[i*2];
	}
	return OK;
}

void serialFlashInit()
{
	Uint16 command[2];
	command[0] = 0x35; //write enable/disable
	command[1] = 0x02;
	spiWrite(FLASH_CHIP_SELECT, command, 2);

	//serialFlashWriteEnable(1);
	//command[0] = 0x98; //global protect unlock
	//spiWrite(FLASH_CHIP_SELECT, command, 1);
}


