// SoundTrap Software v1.0
//
// Copyright (C) 2011-2014, John Atkins and Mark Johnson
//
// This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
//
// This file is part of the SoundTrap software. SoundTrap is an acoustic
// recording system intended for underwater acoustic measurements. This
// component of the SoundTrap project is free software: you can redistribute
// it and/or modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the License,
// or any later version.
//
// The SoundTrap software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this code. If not, see <http://www.gnu.org/licenses/>.

/*
 * Implements Device ID (ie serial number)
 *
 * JMJA
 *
 */

#include "d3defs.h"
#include "d3std.h"
#include <tistdtypes.h>
#include "sysControl.h"
#include "serialEeprom.h"
#include "serialFlash.h"

Uint16 DeviceId[4];

int idInit()
{
	Uint16 buf[4];
	Uint32 *p = (Uint32 *)buf;

	serialEepromRead(DEVICE_ID_SFLASH_ADDRESS, buf, 4);
	if( p[0] == ~p[1]) {
		//we have a valie ID in EEPROM, so use it...
		DeviceId[0] = buf[1];
		DeviceId[1] = buf[0];
		DeviceId[2] = 0;
		DeviceId[3] = 0;
		return OK;
	}

	if( serialFlashRead(ID_SFLASH_ADDRESS, buf, 4) == OK) {
		if( p[0] == ~p[1]) {
			//we have a valid ID in EEPROM, so use it...
			DeviceId[0] = buf[1];
			DeviceId[1] = buf[0];
			DeviceId[2] = 0;
			DeviceId[3] = 0;
			return OK;
		}
	}

	sysControlGetId(DeviceId); //otherwise use DSP hardware ID

	return OK;
}
