﻿/* 
* 	SoundTrap Software v1.0
*  
*	Copyright (C) 2011-2014, John Atkins and Mark Johnson
*
*	This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
*
*	This file is part of the SoundTrap software. SoundTrap is an acoustic recording
*	system intended for underwater acoustic measurements. This component of the 
*	SoundTrap project is free software: you can redistribute it and/or modify it 
*	under the terms of the GNU General Public License as published by the Free Software 
*	Foundation, either version 3 of the License, or any later version.
*
*	The SoundTrap software is distributed in the hope that it will be useful, but 
*	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
*	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License along with this 
*	code. If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Text;
using System.Xml;

namespace SudarHost.Calibration
{
	/// <summary>
	/// Description of calibrationControl.
	/// </summary>
	public partial class calibrationControl : UserControl
	{
		public calibrationControl()
		{
			InitializeComponent();
		}
		
		void TextBox2TextChanged(object sender, EventArgs e)
		{
			
		}
		
		void Button2Click(object sender, EventArgs e)
		{
			XmlDocument doc = new XmlDocument();
			doc.AppendChild(doc.CreateXmlDeclaration("1.0", null, null));// Create the root element
			doc.AppendChild(doc.CreateElement("Calibration"));
			XmlElement cal = doc.CreateElement("cal");
			cal.SetAttribute("Date", DateTime.Now.ToShortDateString());
			cal.SetAttribute("RLH", textBox1.Text);
			cal.SetAttribute("RLL", textBox2.Text);
			XmlElement title = doc.CreateElement("Title");
			cal.AppendChild(title);
			richTextBox1.Text = doc.ToString();
			
		}
		
		void Button1Click(object sender, EventArgs e)
		{
			ParentForm.Close();
		}
	}
}
