﻿/* 
* 	SoundTrap Software v1.0
*  
*	Copyright (C) 2011-2014, John Atkins and Mark Johnson
*
*	This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
*
*	This file is part of the SoundTrap software. SoundTrap is an acoustic recording
*	system intended for underwater acoustic measurements. This component of the 
*	SoundTrap project is free software: you can redistribute it and/or modify it 
*	under the terms of the GNU General Public License as published by the Free Software 
*	Foundation, either version 3 of the License, or any later version.
*
*	The SoundTrap software is distributed in the hope that it will be useful, but 
*	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
*	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License along with this 
*	code. If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.IO;

namespace SudarHost
{
	/// <summary>
	/// Description of BitStream2.
	/// </summary>
	public class BitStream2
	{
		
		UInt16[] buffer;
		int bufPos = 0;
		int bitPos = 0;
		
		
		public BitStream2()
		{
			// Initialise the bit buffer with 1 UInt32
			buffer = new UInt16[1];
		}


		
		private byte[] streamToArray(Stream buf, int count)
		{
			byte[] result = new byte[count];
			for(int i=0;i<count;i++) {
				int j = buf.ReadByte();
				if(j==-1) throw new EndOfStreamException();
				result[i] = (byte)j;
			}
			return result;
		}

		public BitStream2(byte[] bits) : this()
		{
			buffer = new UInt16 [bits.Length / 2];
			for(int i=0; i<buffer.Length;i++) {
				//buffer[i] =  BitConverter.ToUInt16(bits.SubArray(i*2,2), 0);
				buffer[i] =  (UInt16)((256 * bits[(i*2)+1]) + bits[(i*2)]);
			}
		}

		public BitStream2(Stream bits) : this()
		{
			bits.Seek(0,SeekOrigin.Begin);
			buffer = new UInt16 [bits.Length / 2];
			for(int i=0; i<buffer.Length;i++) {
				byte[] b = streamToArray(bits, 2);
				buffer[i] =  BitConverter.ToUInt16(b, 0);
			}
		}
		
		void updateIndex(int bits)
		{
			bitPos += bits;
			if(bitPos  == 16) {
				bufPos++;
				bitPos=0;
			}
			if(bitPos > 16) {
				throw new Exception();
			}
			
		}
		
		public int ReadIntLargerThan(long lev, out long outVal) {
			
			int bits = 0;
			long val = 0;
			while(true) {
				bits++;
				if(bufPos > (buffer.Length-1)) throw new EndOfStreamException();
				
				if((buffer[bufPos] & (0x8000>>bitPos)) != 0) val++;
				
				bitPos++;
				if(bitPos == 16) {
					bufPos++;
					bitPos=0;
				}
				
				if(val >= lev) {
					outVal = val;
					return bits;
				}
				val <<= 1;
			}
		}
		

		public UInt16 ReadInt(int bitsToRead) {
			
			if(bitsToRead == 0) return 0;

			if(bufPos > (buffer.Length-1)) throw new EndOfStreamException();
			
			int bitsToReadNow = Math.Min(bitsToRead, 16 - bitPos);
			
			int shift = 16 - (bitPos + bitsToReadNow);
			UInt32 mask = (UInt32)(0x00000001 << (bitsToReadNow)) - 1;
			UInt32 val = (UInt32)(buffer[bufPos] >> shift) & mask;

			updateIndex(bitsToReadNow);

			int bitsremaining = bitsToRead - bitsToReadNow;
			if(bitsremaining > 0) {
				val = val << bitsremaining;
				val |= ReadInt( bitsremaining);
			}
			return (UInt16)val;
		}
	}
}
