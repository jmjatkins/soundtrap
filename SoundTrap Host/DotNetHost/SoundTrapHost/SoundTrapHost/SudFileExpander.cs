﻿/* 
 * 	SoundTrap Software v1.0
 *
 *	Copyright (C) 2011-2014, John Atkins and Mark Johnson
 *
 *	This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
 *
 *	This file is part of the SoundTrap software. SoundTrap is an acoustic recording
 *	system intended for underwater acoustic measurements. This component of the
 *	SoundTrap project is free software: you can redistribute it and/or modify it
 *	under the terms of the GNU General Public License as published by the Free Software
 *	Foundation, either version 3 of the License, or any later version.
 *
 *	The SoundTrap software is distributed in the hope that it will be useful, but
 *	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License along with this
 *	code. If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.IO;
using System.Text;
using Crc;
using System.Collections.Generic;
using System.Xml;

namespace SudarHost
{
	/// <summary>
	/// Description of SudarFileReader.
	/// </summary>
	public class SudFileExpander
	{
		
		//SudarDataHandlers handlers = new SudarDataHandlers();
		
		public SudFileExpander() {;}
		
		public enum SudFileExpanderResult{ Warning, OK, Error, Cancelled};
		
		public Dictionary<int, DataConfig> configs = new Dictionary<int, DataConfig>();
		
		public SudFileExpanderResult ProcessFile(string filePath, SudarFile.ProgressCallback progressCallback)
		{
			int progress, lastprogress = 0;
			DateTime t = DateTime.Now;
			bool previousChunkIdFailed;
			FileStream fs;
			SudarFile.SudarOffloadFileHeader FileHeader;
			LogFileStream log;

			SudFileExpanderResult result = SudFileExpanderResult.OK;

			string logFileName = Path.GetDirectoryName(filePath) + Path.DirectorySeparatorChar + Path.GetFileNameWithoutExtension(filePath) + ".log.xml";
			log = new LogFileStream( logFileName, FileMode.Create);
			fs = new FileStream(filePath, FileMode.Open, FileAccess.Read);



			DataConfig d = new DataConfig();
			d.id = 0;
			d.handler = new XmlFileHandler(filePath, configs);
			d.handler.Init(log, "", 0);
			
			configs.Clear();
			configs.Add(d.id, d);
			
			try {
				FileHeader = SudarFile.SudarOffloadFileHeader.Deserialise(fs);
				if(FileHeader == null) throw new Exception("Input File Is Missing A Valid Header");

				SudarFile.ChunkHeader ch;
				previousChunkIdFailed = false;
				while(fs.Position != fs.Length)
				{
					ch = SudarFile.ChunkHeader.Deserialise(fs);//new MemoryStream(ReadBuf( SudarFile.ChunkHeader.Length )));
					if(! ch.CheckId() ) {
						previousChunkIdFailed = true;
						continue;
					}
					
					if(previousChunkIdFailed) {
						log.Write(-1, "SudarFileReader", "Warning", "Chunk Decode CheckID Failed");
						result = SudFileExpanderResult.Warning;
						previousChunkIdFailed = false;
					}

					if(! ch.CheckCrc() ) {
						log.Write(-1, "SudarFileReader", "Error", "Chunk Decode CRC Failed");
						result = SudFileExpanderResult.Warning;
						continue;
					}
					
					byte[] buf = new byte[ch.DataLength];
					if ( fs.Read(buf, 0, ch.DataLength) != ch.DataLength) {
						throw new EndOfStreamException();
					}
					
					if(crc.calc(buf, buf.Length) != ch.DataCrc) {
						log.Write(-1, "SudarFileReader", "Error", "Chunk Data CRC Failed");
						result = SudFileExpanderResult.Warning;
						continue;
					}
					
					try {
						//if(ch.ChunkId == 0)
						//	xmlFileHandler.ProcessChunk(ch, ref buf);
						//else
						ProcessChunk(ch.ChunkId, ch, ref buf);
					}
					catch (Exception e)
					{
						log.Write(-1, "SudarFileReader", "Error", "ProcessChunk Raised Exception: " + e.Message);
						result = SudFileExpanderResult.Warning;
						continue;
					}
					
					
					if(progressCallback != null) {
						TimeSpan ts = DateTime.Now - t;
						double rate = fs.Position / (ts.TotalSeconds + 0.1) / 1000000;
						
						progress = (int)(fs.Position / (fs.Length / 100));
						if(progress >= lastprogress + 10) {
							lastprogress = progress;
							if ( progressCallback(String.Format("Decompressing, MBs = {0:0.0}", rate), progress) ) {
								result = SudFileExpanderResult.Cancelled;
								break;
							}
						}
						//lastPosition = fs.Position;
						//t = DateTime.Now;
					}
				}

			}
			catch (EndOfStreamException) {
				//log.Write("SudarFileReader", "Warning", "Logging Ended Ungracefully");
				//TODO Investigate why this is always true.
				//result = SudFileExpanderResult.Warning;
			}
			catch (Exception) {
				result = SudFileExpanderResult.Error;
			}
			finally {
				foreach(KeyValuePair<int, DataConfig> kvp in configs) {
					kvp.Value.handler.Close();
				}
				fs.Close();
				log.Close();
			}
			return result;
		}
		
		
		void ProcessChunk(int chunkId, SudarFile.ChunkHeader ch, ref byte[] buf)
		{
			if(configs[chunkId].srcId > 0)
				if( configs.ContainsKey( configs[chunkId].srcId ))
					ProcessChunk(configs[chunkId].srcId, ch, ref buf); //recursive
			configs[chunkId].handler.ProcessChunk(ch, ref buf);
		}
		
	}
}
