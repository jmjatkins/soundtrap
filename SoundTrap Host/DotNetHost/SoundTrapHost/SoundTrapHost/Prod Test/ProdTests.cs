﻿/* 
 * 	SoundTrap Software v1.0
 *
 *	Copyright (C) 2011-2014, John Atkins and Mark Johnson
 *
 *	This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
 *
 *	This file is part of the SoundTrap software. SoundTrap is an acoustic recording
 *	system intended for underwater acoustic measurements. This component of the
 *	SoundTrap project is free software: you can redistribute it and/or modify it
 *	under the terms of the GNU General Public License as published by the Free Software
 *	Foundation, either version 3 of the License, or any later version.
 *
 *	The SoundTrap software is distributed in the hope that it will be useful, but
 *	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License along with this
 *	code. If not, see <http://www.gnu.org/licenses/>.
 */


using System;
using System.Collections.Generic;
using OceanInstruments.ApiProxy2;
using System.Threading;
using System.IO.Ports;

namespace SudarHost
{
	/// <summary>
	/// SoundTrap production testing routines
	/// </summary>


	abstract public partial class ProdTest {
		abstract  public OiApiProxy.TestSubResult DoTest(Sudar sudar, string testName, double low, double high, string param1);
		
		protected double CalcDcOffset(float[] inBuf) {
			double av = 0;
			for(int i=0; i<inBuf.Length;i++){
				av += inBuf[i];
			}
			return av / inBuf.Length;
		}

		protected double CalcDbRms(float[] inBuf) {
			
			double av = CalcDcOffset(inBuf);
			double rms = 0;
			
			for(int i=0; i<inBuf.Length;i++){
				rms +=  Math.Pow(inBuf[i] - av, 2);
			}
			
			rms = Math.Sqrt((rms / inBuf.Length));
			return 20 * Math.Log10(rms);
		}
		
		protected double LevelCheck(Sudar sudar)
		{
			GrabSample(sudar, 4096 * 2); //flush
			GrabSample(sudar, 4096 * 10);
			return Math.Round( CalcDbRms( sudar.AudioBuf ), 2);
		}
		
		protected void GrabSample(Sudar sudar, int count)
		{
			if( !sudar.AudioSample(count) || !(sudar.AudioBuf.Length > 0) ) {
				throw new Exception("Audio Sample Failed");
			}
		}

	}
	

	public class SetChannelTest : ProdTest
	{
		override public OiApiProxy.TestSubResult DoTest(Sudar sudar, string testName, double low, double high, string param1)
		{
			if( !sudar.AudioSetActiveChannel( Convert.ToUInt16( param1 )) ) throw new Exception("Failed to set channel");
			OiApiProxy.TestSubResult str = new OiApiProxy.TestSubResult(testName, 1, low, high, 1);
			return str;
		}
	}

	public class NoiseFloorTest : ProdTest
	{
		override public OiApiProxy.TestSubResult DoTest(Sudar sudar, string testName, double low, double high, string param1)
		{
			if(!sudar.AudioEnable(true) ) throw new Exception("failed to enable audio");
			Thread.Sleep(100);
			if( !sudar.AudioSetDivider(1) ) throw new Exception("Failed to set sample rate");
			Thread.Sleep(100);
			if( !sudar.AudioSetMute(true) ) throw new Exception("Failed to set mute");
			Thread.Sleep(100);
			if( !sudar.AudioSetHighPass(true) ) throw new Exception("Failed to set high pass");
			Thread.Sleep(100);
			if( !sudar.AudioSetGain( param1 =="1" ) ) throw new Exception("Failed to set gain");
			Thread.Sleep(100);
			if( !sudar.AudioEnableCalSig(false) ) throw new Exception("Failed to disable cal sig");
			Thread.Sleep(1000);
			GrabSample(sudar, 4096); //flush
			OiApiProxy.TestSubResult str = new OiApiProxy.TestSubResult(testName, 1, low, high, LevelCheck( sudar ));
			return str;
		}
	}
	
	public class CalLevelTest : ProdTest
	{
		override public OiApiProxy.TestSubResult DoTest(Sudar sudar, string testName, double low, double high, string param1)
		{
			if( !sudar.AudioEnable(true) ) throw new Exception("failed to enable audio");
			if( !sudar.AudioSetDivider(1) ) throw new Exception("Failed to set sample rate");
			if( !sudar.AudioSetMute(true) ) throw new Exception("Failed to set mute");
			if( !sudar.AudioSetGain(  param1 =="1" ) ) throw new Exception("Failed to set gain");
			if( !sudar.AudioSetCalFrequency(1000) ) throw new Exception("Falied to set cal frequency");
			if( !sudar.AudioEnableCalSig(true) ) throw new Exception("Failed to disable cal sig");
			Thread.Sleep(200);
			OiApiProxy.TestSubResult str = new OiApiProxy.TestSubResult(testName, 0, low, high, LevelCheck( sudar ));
			return str;
		}
	}
	

	public class BandPassTest : ProdTest
	{
		override public OiApiProxy.TestSubResult DoTest(Sudar sudar, string testName, double low, double high, string param1)
		{
			uint f = 1000;
			if( !sudar.AudioEnable(true) ) throw new Exception("failed to enable audio");
			if( !sudar.AudioSetDivider(1) ) throw new Exception("Failed to set sample rate");
			if( !sudar.AudioSetMute(true) ) throw new Exception("Failed to set mute");
			if( !sudar.AudioSetGain( param1 =="1" ) ) throw new Exception("Failed to set gain");
			if( !sudar.AudioSetCalFrequency(f) ) throw new Exception("Falied to set cal frequency");
			if( !sudar.AudioEnableCalSig(true) ) throw new Exception("Failed to disable cal sig");
			Thread.Sleep(200);
			
			double refLevel = LevelCheck( sudar );
			double level =  0;
			do {
				f = f  + 4000;
				if( !sudar.AudioSetCalFrequency(f) ) throw new Exception("Falied to set cal frequency");
				level = LevelCheck( sudar );
			} while( (refLevel - level < 3.0) && (f < sudar.AudioSampleRate/2) );
			
			OiApiProxy.TestSubResult str = new OiApiProxy.TestSubResult(testName, 1, low, high, f);
			return str;
		}
	}

	public class BandPassTestNew : ProdTest
	{
		override public OiApiProxy.TestSubResult DoTest(Sudar sudar, string testName, double low, double high, string param1)
		{
			List<float> dat = new List<float>();
			const int bufLength = 2304;
			int fftLength = 2048;
			double f = 10000;
			if( !sudar.AudioEnable(true) ) throw new Exception("failed to enable audio");
			if( !sudar.AudioSetDivider(1) ) throw new Exception("Failed to set sample rate");
			if( !sudar.AudioSetMute(true) ) throw new Exception("Failed to set mute");
			if( !sudar.AudioSetGain( param1 =="1" ) ) throw new Exception("Failed to set gain");
			if( !sudar.AudioSetCalFrequency(0) ) throw new Exception("Falied to set cal frequency");
			if( !sudar.AudioEnableCalSig(true) ) throw new Exception("Failed to disable cal sig");
			Thread.Sleep(200);
			
			while(dat.Count < fftLength*100) {
				sudar.AudioSetCalFrequency(0);
				//Thread.Sleep(20);
				GrabSample(sudar, bufLength);
				dat.AddRange(sudar.AudioBuf);
			}
			
			//GrabSample(sudar, fftLength*10);
			float[] X = sigProc.fft(fftLength, dat.ToArray());
			double binWidth = sudar.AudioSampleRate / fftLength;
			
			double refLevel = X[(int)(f/binWidth)];
			
			double level =  0;
			do {
				f = f  + 2000.0;
				int i = (int)(f/binWidth);
				level = X[i];
			} while( refLevel - level < 3.0 );
			
			OiApiProxy.TestSubResult str = new OiApiProxy.TestSubResult(testName, 0, low, high, f);
			return str;
		}
	}

	
	public class NoiseFloorTestFft : ProdTest
	{
		override public OiApiProxy.TestSubResult DoTest(Sudar sudar, string testName, double low, double high, string param1)
		{
			List<float> dat = new List<float>();
			int fftLength = 4096;
			string[] param = param1.Split(' ');
			bool highGain = param[0] =="1";

			if(!sudar.AudioEnable(true) ) throw new Exception("failed to enable audio");
			if( !sudar.AudioSetDivider(1) ) throw new Exception("Failed to set sample rate");
			if( !sudar.AudioSetMute(true) ) throw new Exception("Failed to set mute");
			if( !sudar.AudioSetHighPass(true) ) throw new Exception("Failed to set high pass");
			if( !sudar.AudioSetGain( highGain ) ) throw new Exception("Failed to set gain");
			if( !sudar.AudioEnableCalSig(false) ) throw new Exception("Failed to disable cal sig");
			Thread.Sleep(2200);

			GrabSample(sudar, fftLength*10);

			float[] X = sigProc.fft(fftLength, sudar.AudioBuf);
			double binWidth = sudar.AudioSampleRate / fftLength / 2;

			double maxAmp = Double.NegativeInfinity;
			double maxf = 0;
			
			int startIndex = (int)( 3000.0 / binWidth);
			for(int i = startIndex; i<X.Length/2; i++)
			{
				if(X[i] > maxAmp) {
					maxAmp = X[i];
					maxf = i * binWidth;
				}
			}

			OiApiProxy.TestSubResult str = new OiApiProxy.TestSubResult(testName, 1, low, high, maxAmp);
			return str;
		}
	}

	
	
	public class DcOffsetTest : ProdTest
	{
		override public OiApiProxy.TestSubResult DoTest(Sudar sudar, string testName, double low, double high, string param1)
		{
			if( !sudar.AudioEnable(true) ) throw new Exception("failed to enable audio");
			if( !sudar.AudioSetDivider(1) ) throw new Exception("Failed to set sample rate");
			if( !sudar.AudioSetMute(true) ) throw new Exception("Failed to set mute");
			if( !sudar.AudioSetGain( param1 =="1" )) throw new Exception("Failed to set gain");
			if( !sudar.AudioEnableCalSig(false) ) throw new Exception("Failed to disable cal sig");
			Thread.Sleep(200);
			GrabSample(sudar, 4096);
			OiApiProxy.TestSubResult str = new OiApiProxy.TestSubResult(testName, 0, low, high, Math.Round(CalcDcOffset( sudar.AudioBuf ), 2));
			return str;
		}
	}
	
	
	public class TemperatureSensorTest : ProdTest
	{
		override public OiApiProxy.TestSubResult DoTest(Sudar sudar, string testName, double low, double high, string param1)
		{
			OiApiProxy.TestSubResult str = new OiApiProxy.TestSubResult(testName, 0, low, high, Math.Round(sudar.LastStatusPacket.temperature / 100.0, 2));
			return str;
		}
	}


	public class BatteryVoltageTest : ProdTest
	{
		override public OiApiProxy.TestSubResult DoTest(Sudar sudar, string testName, double low, double high, string param1)
		{
			OiApiProxy.TestSubResult str = new OiApiProxy.TestSubResult(testName, 0, low, high, Math.Round(sudar.LastStatusPacket.battVoltage / 1000.0, 2));
			return str;
		}
	}

	public class ExtBatteryVoltageTest : ProdTest
	{
		override public OiApiProxy.TestSubResult DoTest(Sudar sudar, string testName, double low, double high, string param1)
		{
			OiApiProxy.TestSubResult str = new OiApiProxy.TestSubResult(testName, 0, low, high, Math.Round(sudar.LastStatusPacket.extBattVoltage / 1000.0, 2));
			return str;
		}
	}

	public class AccelTest : ProdTest
	{
		override public OiApiProxy.TestSubResult DoTest(Sudar sudar, string testName, double low, double high, string param1)
		{
			bool result = sudar.SendCommand(Sudar.SudarCommands.ACCEL_TEST , 100);
			OiApiProxy.TestSubResult str = new OiApiProxy.TestSubResult(testName, 0, low, high, result ? 1:0);
			return str;
		}
	}

	public class UartRxTest : ProdTest
	{
		override public OiApiProxy.TestSubResult DoTest(Sudar sudar, string testName, double low, double high, string param1)
		{
			string[] param = param1.Split(' ');
			string testString = "testing123";
			SerialPort sp = new SerialPort(param[0], 9600);
			try {
				sp.Open();
				
				sudar.SendCommand(Sudar.SudarCommands.UART_SELECT_TRANS, 100, (UInt16)0); //select RS232
				string rxData = sudar.UartRx(); //purge
				
				sp.Write(testString);
				Thread.Sleep(100);
				rxData = sudar.UartRx();
				
				bool result = rxData == testString;
				
				OiApiProxy.TestSubResult str = new OiApiProxy.TestSubResult(testName, 0, low, high, result ? 1:0);
				return str;
			}
			finally {
				sp.Close();
			}
		}
	}

	public class UartTxTest : ProdTest
	{
		override public OiApiProxy.TestSubResult DoTest(Sudar sudar, string testName, double low, double high, string param1)
		{
			string[] param = param1.Split(' ');
			SerialPort sp = new SerialPort(param[0], 9600);
			try {
				sp.ReadTimeout = 1000;
				sp.Open();
				sudar.SendCommand(Sudar.SudarCommands.UART_SELECT_TRANS, 100, (UInt16)0); //select RS232
				sudar.SendCommand(Sudar.SudarCommands.UART_ENABLE_TX, 100, (UInt16)1);
				Thread.Sleep(100);
				
				sp.DiscardInBuffer();
				
				sudar.SendCommand(Sudar.SudarCommands.UART_TX_TEST, 100, 1);
				bool result = false;
				try {
					string rx = sp.ReadTo("\r");
					result = rx == "test";
				}
				catch (System.TimeoutException)
				{
					result = false;
				}
				sudar.SendCommand(Sudar.SudarCommands.UART_ENABLE_TX, 100, (UInt16)0); //disable tx
				OiApiProxy.TestSubResult str = new OiApiProxy.TestSubResult(testName, 0, low, high, result ? 1:0);
				return str;
			}
			finally {
				sp.Close();
			}
		}
	}

	
	public class FlashMemortCapacityTest : ProdTest
	{
		override public OiApiProxy.TestSubResult DoTest(Sudar sudar, string testName, double low, double high, string param1)
		{
			sudar.sdSetCardNo(ushort.Parse( param1 ));
			Thread.Sleep(500);
			OiApiProxy.TestSubResult str = new OiApiProxy.TestSubResult(testName, 0, low, high, sudar.FsParams.CapacityGB);
			return str;
		}
	}

	public class Calibrate : ProdTest
	{
		override public OiApiProxy.TestSubResult DoTest(Sudar sudar, string testName, double low, double high, string param1)
		{
			double x, peakLevel;
			if( !sudar.AudioEnable(true) ) throw new Exception("failed to enable audio");
			if( !sudar.AudioSetDivider(5) ) throw new Exception("Failed to set sample rate");
			if( !sudar.AudioSetMute(false) ) throw new Exception("Failed to set mute");
			if( !sudar.AudioSetGain( param1 =="1" ) ) throw new Exception("Failed to set gain");
			if( !sudar.AudioSetCalFrequency(1000) ) throw new Exception("Falied to set cal frequency");
			if( !sudar.AudioEnableCalSig(true) ) throw new Exception("Failed to disable cal sig");
			Thread.Sleep(200);
			GrabSample(sudar, 4096);
			GrabSample(sudar, 4096);
			Thread.Sleep(200);

			float[] fft = sigProc.fft(4096, sudar.AudioBuf);
			float[] data = sigProc.interpolate(fft,fft.Length/2, 10);
			sigProc.findPeak( data, (float)(250.0 / (sudar.AudioSampleRate / 2)), out x, out peakLevel);

			OiApiProxy.TestSubResult str = new OiApiProxy.TestSubResult(testName, 0, low, high, peakLevel);
			return str;
		}
	}
	
	public class SerialNoTest : ProdTest
	{
		override public OiApiProxy.TestSubResult DoTest(Sudar sudar, string testName, double low, double high, string param1)
		{
			OiApiProxy.TestSubResult str = new OiApiProxy.TestSubResult(testName, 0, low, high, Convert.ToDouble( sudar.SerialNumber));
			return str;
		}
	}

	public class ST600BattTest : ProdTest
	{
		override public OiApiProxy.TestSubResult DoTest(Sudar sudar, string testName, double low, double high, string param1)
		{
			//int noOfbatt = sudar.ST600batChannelsI.Length;
			//var battOK = new bool[noOfbatt];
			int startTime = Environment.TickCount;
			int batt = Convert.ToInt32( param1 );
			//for(int j=0; j<noOfbatt; j++) {
			while(Environment.TickCount - startTime < 20000 ) {
				sudar.PollAdcValues();
				double v = sudar.AdcValue(sudar.ST600batChannelsTestOrder[batt]) * .00312;
				if (v > 1.0) {
					OiApiProxy.TestSubResult str = new OiApiProxy.TestSubResult(testName, 0, low, high, v);
					return str;
//						battOK[j] = true;
					
				}
				Thread.Sleep(100);
			}
			throw new Exception("Failed to detect battery");
		}
	}
	
	
	
	
}



