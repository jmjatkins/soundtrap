﻿/* 
 * 	SoundTrap Software v1.0
 *
 *	Copyright (C) 2011-2014, John Atkins and Mark Johnson
 *
 *	This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
 *
 *	This file is part of the SoundTrap software. SoundTrap is an acoustic recording
 *	system intended for underwater acoustic measurements. This component of the
 *	SoundTrap project is free software: you can redistribute it and/or modify it
 *	under the terms of the GNU General Public License as published by the Free Software
 *	Foundation, either version 3 of the License, or any later version.
 *
 *	The SoundTrap software is distributed in the hope that it will be useful, but
 *	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License along with this
 *	code. If not, see <http://www.gnu.org/licenses/>.
 */


using System;
using System.Collections.Generic;
using OceanInstruments.ApiProxy2;
using System.Threading;
using System.IO.Ports;

namespace SudarHost
{
	/// <summary>
	/// SoundTrap production calibration routines
	/// </summary>
	/// 
	abstract public partial class ProdTest
	{
		
		internal class CalResult
		{
			public double low;
			public double high;
			public double refLevel;
			public double toneLev;
		}

		internal void postCalibration(CalResult calResult, string serial, string hardwareSerial, string model, int calType)
		{
			if(OiApiProxy.Instance.DeviceExists(serial)) {
				//MessageBox.Show("Warning - Device Exists", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}


			OiApiProxy.Instance.PostCalibration(serial, hardwareSerial, model,
			                                    Math.Round(calResult.low, 1),
			                                    Math.Round(calResult.high, 1),
			                                    Math.Round(calResult.refLevel, 1 ),
			                                    Math.Round(calResult.toneLev, 1),
			                                    calType );

		}
		
	}
	public class CalType2 : ProdTest //STHP
	{
		override public OiApiProxy.TestSubResult DoTest(Sudar sudar, string testName, double low, double high, string param1)
		{
			double x;
			float[] fft;
			float[] data;

			OiApiProxy.TestSubResult str;
			var calResult = new CalResult();

			const double st500RefCal = -1.8; //dBFS of 1V on reference hardware
			calResult.refLevel = 120.0;// Convert.ToDouble( param1 ); //use param1 for reference level

			
			if(!sudar.AudioEnable(true) ) throw new Exception("failed to enable audio");
			if( !sudar.AudioSetDivider(4) ) throw new Exception("Failed to set sample rate");
			if( !sudar.AudioSetMute(false) ) throw new Exception("Failed to set mute");
			if( !sudar.AudioEnableCalSig(false) ) throw new Exception("Failed to disable cal sig");
			if( !sudar.AudioSetHighPass(true) ) throw new Exception("Failed to set high pass");
			if( !sudar.AudioSetGain( true ) ) throw new Exception("Failed to set gain");

			sudar.AudioSample(4096); //flush;
			Thread.Sleep(200);
			sudar.AudioSample(4096*10);
			Thread.Sleep(200);
			
			fft = sigProc.fft(4096, sudar.AudioBuf);
			data = sigProc.interpolate(fft,fft.Length/2, 10);
			sigProc.findPeak( data, (float)(250.0 / (sudar.AudioSampleRate / 2)), out x, out calResult.high);
			
			calResult.high = Math.Round((-1.0 *  calResult.high) + calResult.refLevel - st500RefCal, 2);
			calResult.toneLev = 0;
			calResult.low = 0;
			
			str = new OiApiProxy.TestSubResult(testName, 0, low, high, calResult.high);
			
			if(str.testResult == 0) { //if test result = passed
				//if serial starts with 5 then call it a STHP2, otherwise STHP1
				postCalibration(calResult, param1, sudar.HwSerial, param1[0] == '6' ? "STHP2" : "STHP1", 2); //post a type 2 calibration record
			}

			return str;
		}
	}
	
	/* For testing external hydrophones. Same as regular noise floor test except mute=off*/
	public class HPNoiseFloorTest : ProdTest
	{
		override public OiApiProxy.TestSubResult DoTest(Sudar sudar, string testName, double low, double high, string param1)
		{
			if(!sudar.AudioEnable(true) ) throw new Exception("failed to enable audio");
			Thread.Sleep(2000);
			if( !sudar.AudioSetDivider(1) ) throw new Exception("Failed to set sample rate");
			Thread.Sleep(100);
			if( !sudar.AudioSetMute(false) ) throw new Exception("Failed to set mute");
			Thread.Sleep(100);
			if( !sudar.AudioSetGain( param1 =="1" ) ) throw new Exception("Failed to set gain");
			Thread.Sleep(100);
			if( !sudar.AudioEnableCalSig(false) ) throw new Exception("Failed to disable cal sig");
			Thread.Sleep(2200);
			GrabSample(sudar, 4096); //flush
			OiApiProxy.TestSubResult str = new OiApiProxy.TestSubResult(testName, 1, low, high, LevelCheck( sudar ));
			return str;
		}
	}


	public class CalType1 : ProdTest //ST500 or ST4300
	{
		override public OiApiProxy.TestSubResult DoTest(Sudar sudar, string testName, double low, double high, string param1)
		{
			double x;
			float[] fft;
			float[] data;

			OiApiProxy.TestSubResult str;
			var calResult = new CalResult();

			const double st500RefCalLevel = -37.6; //dBV of 13mV internal cal sig
			
			if(!sudar.AudioEnable(true) ) throw new Exception("failed to enable audio");
			if( !sudar.AudioSetDivider(4) ) throw new Exception("Failed to set sample rate");
			if( !sudar.AudioSetMute(true) ) throw new Exception("Failed to set mute");
			if( !sudar.AudioSetCalFrequency(1000) ) throw new Exception("Failed to ser cal f");
			if( !sudar.AudioEnableCalSig(true) ) throw new Exception("Failed to disable cal sig");
			if( !sudar.AudioSetHighPass(true) ) throw new Exception("Failed to set high pass");
			if( !sudar.AudioSetGain( true ) ) throw new Exception("Failed to set gain");

			sudar.AudioSample(4096); //flush;
			Thread.Sleep(200);
			sudar.AudioSample(4096*10);
			Thread.Sleep(200);
			
			fft = sigProc.fft(4096, sudar.AudioBuf);
			data = sigProc.interpolate(fft,fft.Length/2, 10);
			sigProc.findPeak( data, (float)(1000f / (sudar.AudioSampleRate / 2)), out x, out calResult.toneLev);

			calResult.refLevel = st500RefCalLevel;
			calResult.low = 0;
			calResult.high = Math.Round(calResult.toneLev + (-1.0 * calResult.refLevel), 2);
			calResult.toneLev = Math.Round(calResult.toneLev, 2);

			str = new OiApiProxy.TestSubResult(testName, 0, low, high, calResult.high);
			
			if(str.testResult == 0) { //if test result = passed
				postCalibration(calResult, sudar.SerialNumber, sudar.HwSerial, sudar.modelName, 1); //post a type 1 calibration record
			}

			return str;
		}
	}

	
}

