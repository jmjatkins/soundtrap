﻿/*
 * Created by SharpDevelop.
 * User: jatk009
 * Date: 13/09/2012
 * Time: 9:19 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace SudarHost
{
	partial class SudarDeployControl
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the control.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.panel6 = new System.Windows.Forms.Panel();
			this.groupBoxSync = new SudarHost.BetterGroupBox();
			this.cBstream = new System.Windows.Forms.CheckBox();
			this.cblogGps = new System.Windows.Forms.CheckBox();
			this.checkBoxDisCalTones = new System.Windows.Forms.CheckBox();
			this.gbAux = new SudarHost.BetterGroupBox();
			this.numericUpDownLogOnceEvery = new System.Windows.Forms.NumericUpDown();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.checkBoxTemperature = new System.Windows.Forms.CheckBox();
			this.checkBoxAcceleration = new System.Windows.Forms.CheckBox();
			this.groupBox6 = new SudarHost.BetterGroupBox();
			this.gbChanSel = new System.Windows.Forms.GroupBox();
			this.cbCh4 = new System.Windows.Forms.CheckBox();
			this.cbCh3 = new System.Windows.Forms.CheckBox();
			this.cbCh2 = new System.Windows.Forms.CheckBox();
			this.cbCh1 = new System.Windows.Forms.CheckBox();
			this.betterGroupBox1 = new SudarHost.BetterGroupBox();
			this.button1 = new System.Windows.Forms.Button();
			this.betterRadioButton11 = new SudarHost.BetterRadioButton();
			this.betterRadioButton9 = new SudarHost.BetterRadioButton();
			this.groupBoxGain = new SudarHost.BetterGroupBox();
			this.betterRadioButton5 = new SudarHost.BetterRadioButton();
			this.betterRadioButton6 = new SudarHost.BetterRadioButton();
			this.gbHighPass = new SudarHost.BetterGroupBox();
			this.radioButton13 = new SudarHost.BetterRadioButton();
			this.radioButton12 = new SudarHost.BetterRadioButton();
			this.groupBoxSampleRate = new SudarHost.BetterGroupBox();
			this.betterRadioButton12 = new SudarHost.BetterRadioButton();
			this.betterRadioButton1 = new SudarHost.BetterRadioButton();
			this.betterRadioButton10 = new SudarHost.BetterRadioButton();
			this.betterRadioButton3 = new SudarHost.BetterRadioButton();
			this.betterRadioButton4 = new SudarHost.BetterRadioButton();
			this.betterRadioButton8 = new SudarHost.BetterRadioButton();
			this.betterRadioButton7 = new SudarHost.BetterRadioButton();
			this.betterRadioButton2 = new SudarHost.BetterRadioButton();
			this.groupBoxRecordSkedule = new SudarHost.BetterGroupBox();
			this.panelRecordTiming = new System.Windows.Forms.Panel();
			this.label12 = new System.Windows.Forms.Label();
			this.textBoxOnceEvery = new System.Windows.Forms.TextBox();
			this.labelInvalidTimes = new System.Windows.Forms.Label();
			this.comboBoxOnceEvery = new System.Windows.Forms.ComboBox();
			this.label11 = new System.Windows.Forms.Label();
			this.textBoxPeriodOf = new System.Windows.Forms.TextBox();
			this.comboBoxPeriodOf = new System.Windows.Forms.ComboBox();
			this.checkBoxRunCont = new System.Windows.Forms.CheckBox();
			this.groupBoxRecStart = new SudarHost.BetterGroupBox();
			this.radioButton1 = new SudarHost.BetterRadioButton();
			this.radioButton17 = new SudarHost.BetterRadioButton();
			this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
			this.radioButton14 = new SudarHost.BetterRadioButton();
			this.panel6.SuspendLayout();
			this.groupBoxSync.SuspendLayout();
			this.gbAux.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownLogOnceEvery)).BeginInit();
			this.groupBox6.SuspendLayout();
			this.gbChanSel.SuspendLayout();
			this.betterGroupBox1.SuspendLayout();
			this.groupBoxGain.SuspendLayout();
			this.gbHighPass.SuspendLayout();
			this.groupBoxSampleRate.SuspendLayout();
			this.groupBoxRecordSkedule.SuspendLayout();
			this.panelRecordTiming.SuspendLayout();
			this.groupBoxRecStart.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel6
			// 
			this.panel6.Controls.Add(this.groupBoxSync);
			this.panel6.Controls.Add(this.gbAux);
			this.panel6.Controls.Add(this.groupBox6);
			this.panel6.Controls.Add(this.groupBoxRecordSkedule);
			this.panel6.Controls.Add(this.groupBoxRecStart);
			this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel6.Location = new System.Drawing.Point(0, 0);
			this.panel6.Margin = new System.Windows.Forms.Padding(4);
			this.panel6.Name = "panel6";
			this.panel6.Size = new System.Drawing.Size(717, 598);
			this.panel6.TabIndex = 19;
			// 
			// groupBoxSync
			// 
			this.groupBoxSync.Controls.Add(this.cBstream);
			this.groupBoxSync.Controls.Add(this.cblogGps);
			this.groupBoxSync.Controls.Add(this.checkBoxDisCalTones);
			this.groupBoxSync.Dock = System.Windows.Forms.DockStyle.Top;
			this.groupBoxSync.Location = new System.Drawing.Point(0, 542);
			this.groupBoxSync.Margin = new System.Windows.Forms.Padding(4);
			this.groupBoxSync.Name = "groupBoxSync";
			this.groupBoxSync.Padding = new System.Windows.Forms.Padding(4);
			this.groupBoxSync.SelectedRadioButtonIndex = ((uint)(0u));
			this.groupBoxSync.Size = new System.Drawing.Size(717, 51);
			this.groupBoxSync.TabIndex = 1;
			this.groupBoxSync.TabStop = false;
			this.groupBoxSync.Text = "Other";
			// 
			// cBstream
			// 
			this.cBstream.Location = new System.Drawing.Point(403, 11);
			this.cBstream.Margin = new System.Windows.Forms.Padding(4);
			this.cBstream.Name = "cBstream";
			this.cBstream.Size = new System.Drawing.Size(139, 30);
			this.cBstream.TabIndex = 2;
			this.cBstream.Text = "Stream Data";
			this.cBstream.UseVisualStyleBackColor = true;
			// 
			// cblogGps
			// 
			this.cblogGps.Location = new System.Drawing.Point(288, 11);
			this.cblogGps.Margin = new System.Windows.Forms.Padding(4);
			this.cblogGps.Name = "cblogGps";
			this.cblogGps.Size = new System.Drawing.Size(139, 30);
			this.cblogGps.TabIndex = 1;
			this.cblogGps.Text = "Log GPS";
			this.cblogGps.UseVisualStyleBackColor = true;
			// 
			// checkBoxDisCalTones
			// 
			this.checkBoxDisCalTones.Location = new System.Drawing.Point(68, 11);
			this.checkBoxDisCalTones.Margin = new System.Windows.Forms.Padding(4);
			this.checkBoxDisCalTones.Name = "checkBoxDisCalTones";
			this.checkBoxDisCalTones.Size = new System.Drawing.Size(192, 30);
			this.checkBoxDisCalTones.TabIndex = 0;
			this.checkBoxDisCalTones.Text = "Disable calibration tones";
			this.checkBoxDisCalTones.UseVisualStyleBackColor = true;
			// 
			// gbAux
			// 
			this.gbAux.Controls.Add(this.numericUpDownLogOnceEvery);
			this.gbAux.Controls.Add(this.label2);
			this.gbAux.Controls.Add(this.label1);
			this.gbAux.Controls.Add(this.checkBoxTemperature);
			this.gbAux.Controls.Add(this.checkBoxAcceleration);
			this.gbAux.Dock = System.Windows.Forms.DockStyle.Top;
			this.gbAux.Location = new System.Drawing.Point(0, 479);
			this.gbAux.Margin = new System.Windows.Forms.Padding(4);
			this.gbAux.Name = "gbAux";
			this.gbAux.Padding = new System.Windows.Forms.Padding(4);
			this.gbAux.SelectedRadioButtonIndex = ((uint)(0u));
			this.gbAux.Size = new System.Drawing.Size(717, 63);
			this.gbAux.TabIndex = 0;
			this.gbAux.TabStop = false;
			this.gbAux.Text = "Ancillary Sensors";
			// 
			// numericUpDownLogOnceEvery
			// 
			this.numericUpDownLogOnceEvery.Location = new System.Drawing.Point(543, 27);
			this.numericUpDownLogOnceEvery.Margin = new System.Windows.Forms.Padding(4);
			this.numericUpDownLogOnceEvery.Maximum = new decimal(new int[] {
			65000,
			0,
			0,
			0});
			this.numericUpDownLogOnceEvery.Minimum = new decimal(new int[] {
			10,
			0,
			0,
			0});
			this.numericUpDownLogOnceEvery.Name = "numericUpDownLogOnceEvery";
			this.numericUpDownLogOnceEvery.Size = new System.Drawing.Size(81, 22);
			this.numericUpDownLogOnceEvery.TabIndex = 2;
			this.numericUpDownLogOnceEvery.Value = new decimal(new int[] {
			10,
			0,
			0,
			0});
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(632, 30);
			this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(79, 28);
			this.label2.TabIndex = 5;
			this.label2.Text = "seconds";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(425, 30);
			this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(109, 28);
			this.label1.TabIndex = 3;
			this.label1.Text = "Log once every ";
			// 
			// checkBoxTemperature
			// 
			this.checkBoxTemperature.Location = new System.Drawing.Point(178, 23);
			this.checkBoxTemperature.Margin = new System.Windows.Forms.Padding(4);
			this.checkBoxTemperature.Name = "checkBoxTemperature";
			this.checkBoxTemperature.Size = new System.Drawing.Size(139, 30);
			this.checkBoxTemperature.TabIndex = 2;
			this.checkBoxTemperature.Text = "Temperature";
			this.checkBoxTemperature.UseVisualStyleBackColor = true;
			// 
			// checkBoxAcceleration
			// 
			this.checkBoxAcceleration.Location = new System.Drawing.Point(42, 23);
			this.checkBoxAcceleration.Margin = new System.Windows.Forms.Padding(4);
			this.checkBoxAcceleration.Name = "checkBoxAcceleration";
			this.checkBoxAcceleration.Size = new System.Drawing.Size(139, 30);
			this.checkBoxAcceleration.TabIndex = 1;
			this.checkBoxAcceleration.Text = "Acceleration";
			this.checkBoxAcceleration.UseVisualStyleBackColor = true;
			// 
			// groupBox6
			// 
			this.groupBox6.Controls.Add(this.gbChanSel);
			this.groupBox6.Controls.Add(this.betterGroupBox1);
			this.groupBox6.Controls.Add(this.groupBoxGain);
			this.groupBox6.Controls.Add(this.gbHighPass);
			this.groupBox6.Controls.Add(this.groupBoxSampleRate);
			this.groupBox6.Dock = System.Windows.Forms.DockStyle.Top;
			this.groupBox6.Location = new System.Drawing.Point(0, 229);
			this.groupBox6.Margin = new System.Windows.Forms.Padding(4);
			this.groupBox6.Name = "groupBox6";
			this.groupBox6.Padding = new System.Windows.Forms.Padding(4);
			this.groupBox6.SelectedRadioButtonIndex = ((uint)(0u));
			this.groupBox6.Size = new System.Drawing.Size(717, 250);
			this.groupBox6.TabIndex = 2;
			this.groupBox6.TabStop = false;
			this.groupBox6.Text = "Audio Options";
			// 
			// gbChanSel
			// 
			this.gbChanSel.Controls.Add(this.cbCh4);
			this.gbChanSel.Controls.Add(this.cbCh3);
			this.gbChanSel.Controls.Add(this.cbCh2);
			this.gbChanSel.Controls.Add(this.cbCh1);
			this.gbChanSel.Location = new System.Drawing.Point(21, 23);
			this.gbChanSel.Margin = new System.Windows.Forms.Padding(4);
			this.gbChanSel.Name = "gbChanSel";
			this.gbChanSel.Padding = new System.Windows.Forms.Padding(4);
			this.gbChanSel.Size = new System.Drawing.Size(665, 65);
			this.gbChanSel.TabIndex = 2;
			this.gbChanSel.TabStop = false;
			this.gbChanSel.Text = "Channel Selection";
			// 
			// cbCh4
			// 
			this.cbCh4.Location = new System.Drawing.Point(247, 23);
			this.cbCh4.Margin = new System.Windows.Forms.Padding(4);
			this.cbCh4.Name = "cbCh4";
			this.cbCh4.Size = new System.Drawing.Size(139, 30);
			this.cbCh4.TabIndex = 3;
			this.cbCh4.Text = "4";
			this.cbCh4.UseVisualStyleBackColor = true;
			this.cbCh4.CheckedChanged += new System.EventHandler(this.CbChCheckedChanged);
			// 
			// cbCh3
			// 
			this.cbCh3.Location = new System.Drawing.Point(173, 23);
			this.cbCh3.Margin = new System.Windows.Forms.Padding(4);
			this.cbCh3.Name = "cbCh3";
			this.cbCh3.Size = new System.Drawing.Size(139, 30);
			this.cbCh3.TabIndex = 2;
			this.cbCh3.Text = "3";
			this.cbCh3.UseVisualStyleBackColor = true;
			this.cbCh3.CheckedChanged += new System.EventHandler(this.CbChCheckedChanged);
			// 
			// cbCh2
			// 
			this.cbCh2.Location = new System.Drawing.Point(100, 23);
			this.cbCh2.Margin = new System.Windows.Forms.Padding(4);
			this.cbCh2.Name = "cbCh2";
			this.cbCh2.Size = new System.Drawing.Size(139, 30);
			this.cbCh2.TabIndex = 1;
			this.cbCh2.Text = "2";
			this.cbCh2.UseVisualStyleBackColor = true;
			this.cbCh2.CheckedChanged += new System.EventHandler(this.CbChCheckedChanged);
			// 
			// cbCh1
			// 
			this.cbCh1.Location = new System.Drawing.Point(27, 23);
			this.cbCh1.Margin = new System.Windows.Forms.Padding(4);
			this.cbCh1.Name = "cbCh1";
			this.cbCh1.Size = new System.Drawing.Size(139, 30);
			this.cbCh1.TabIndex = 0;
			this.cbCh1.Text = "1";
			this.cbCh1.UseVisualStyleBackColor = true;
			this.cbCh1.CheckedChanged += new System.EventHandler(this.CbChCheckedChanged);
			// 
			// betterGroupBox1
			// 
			this.betterGroupBox1.Controls.Add(this.button1);
			this.betterGroupBox1.Controls.Add(this.betterRadioButton11);
			this.betterGroupBox1.Controls.Add(this.betterRadioButton9);
			this.betterGroupBox1.Location = new System.Drawing.Point(21, 175);
			this.betterGroupBox1.Margin = new System.Windows.Forms.Padding(4);
			this.betterGroupBox1.Name = "betterGroupBox1";
			this.betterGroupBox1.Padding = new System.Windows.Forms.Padding(4);
			this.betterGroupBox1.SelectedRadioButtonIndex = ((uint)(0u));
			this.betterGroupBox1.Size = new System.Drawing.Size(665, 64);
			this.betterGroupBox1.TabIndex = 3;
			this.betterGroupBox1.TabStop = false;
			this.betterGroupBox1.Text = "Detector";
			// 
			// button1
			// 
			this.button1.Enabled = false;
			this.button1.Location = new System.Drawing.Point(544, 24);
			this.button1.Margin = new System.Windows.Forms.Padding(4);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(100, 28);
			this.button1.TabIndex = 2;
			this.button1.Text = "Configure....";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.Button1Click);
			// 
			// betterRadioButton11
			// 
			this.betterRadioButton11.Index = ((uint)(1u));
			this.betterRadioButton11.Location = new System.Drawing.Point(130, 23);
			this.betterRadioButton11.Margin = new System.Windows.Forms.Padding(4);
			this.betterRadioButton11.Name = "betterRadioButton11";
			this.betterRadioButton11.Size = new System.Drawing.Size(139, 30);
			this.betterRadioButton11.TabIndex = 1;
			this.betterRadioButton11.Text = "HF Click";
			this.betterRadioButton11.UseVisualStyleBackColor = true;
			// 
			// betterRadioButton9
			// 
			this.betterRadioButton9.Checked = true;
			this.betterRadioButton9.Index = ((uint)(0u));
			this.betterRadioButton9.Location = new System.Drawing.Point(28, 23);
			this.betterRadioButton9.Margin = new System.Windows.Forms.Padding(4);
			this.betterRadioButton9.Name = "betterRadioButton9";
			this.betterRadioButton9.Size = new System.Drawing.Size(139, 30);
			this.betterRadioButton9.TabIndex = 0;
			this.betterRadioButton9.TabStop = true;
			this.betterRadioButton9.Text = "None";
			this.betterRadioButton9.UseVisualStyleBackColor = true;
			this.betterRadioButton9.CheckedChanged += new System.EventHandler(this.BetterRadioButton9CheckedChanged);
			// 
			// groupBoxGain
			// 
			this.groupBoxGain.Controls.Add(this.betterRadioButton5);
			this.groupBoxGain.Controls.Add(this.betterRadioButton6);
			this.groupBoxGain.Location = new System.Drawing.Point(447, 23);
			this.groupBoxGain.Margin = new System.Windows.Forms.Padding(4);
			this.groupBoxGain.Name = "groupBoxGain";
			this.groupBoxGain.Padding = new System.Windows.Forms.Padding(4);
			this.groupBoxGain.SelectedRadioButtonIndex = ((uint)(1u));
			this.groupBoxGain.Size = new System.Drawing.Size(240, 65);
			this.groupBoxGain.TabIndex = 1;
			this.groupBoxGain.TabStop = false;
			this.groupBoxGain.Text = "PreAmp Gain";
			// 
			// betterRadioButton5
			// 
			this.betterRadioButton5.Index = ((uint)(0u));
			this.betterRadioButton5.Location = new System.Drawing.Point(33, 21);
			this.betterRadioButton5.Margin = new System.Windows.Forms.Padding(4);
			this.betterRadioButton5.Name = "betterRadioButton5";
			this.betterRadioButton5.Size = new System.Drawing.Size(88, 30);
			this.betterRadioButton5.TabIndex = 0;
			this.betterRadioButton5.Text = "Low";
			this.betterRadioButton5.UseVisualStyleBackColor = true;
			// 
			// betterRadioButton6
			// 
			this.betterRadioButton6.Checked = true;
			this.betterRadioButton6.Index = ((uint)(1u));
			this.betterRadioButton6.Location = new System.Drawing.Point(125, 21);
			this.betterRadioButton6.Margin = new System.Windows.Forms.Padding(4);
			this.betterRadioButton6.Name = "betterRadioButton6";
			this.betterRadioButton6.Size = new System.Drawing.Size(95, 30);
			this.betterRadioButton6.TabIndex = 1;
			this.betterRadioButton6.TabStop = true;
			this.betterRadioButton6.Text = "High";
			this.betterRadioButton6.UseVisualStyleBackColor = true;
			// 
			// gbHighPass
			// 
			this.gbHighPass.Controls.Add(this.radioButton13);
			this.gbHighPass.Controls.Add(this.radioButton12);
			this.gbHighPass.Location = new System.Drawing.Point(21, 23);
			this.gbHighPass.Margin = new System.Windows.Forms.Padding(4);
			this.gbHighPass.Name = "gbHighPass";
			this.gbHighPass.Padding = new System.Windows.Forms.Padding(4);
			this.gbHighPass.SelectedRadioButtonIndex = ((uint)(0u));
			this.gbHighPass.Size = new System.Drawing.Size(397, 65);
			this.gbHighPass.TabIndex = 0;
			this.gbHighPass.TabStop = false;
			this.gbHighPass.Text = "High Pass Filter";
			// 
			// radioButton13
			// 
			this.radioButton13.Checked = true;
			this.radioButton13.Index = ((uint)(0u));
			this.radioButton13.Location = new System.Drawing.Point(93, 21);
			this.radioButton13.Margin = new System.Windows.Forms.Padding(4);
			this.radioButton13.Name = "radioButton13";
			this.radioButton13.Size = new System.Drawing.Size(88, 30);
			this.radioButton13.TabIndex = 0;
			this.radioButton13.TabStop = true;
			this.radioButton13.Text = "Off";
			this.radioButton13.UseVisualStyleBackColor = true;
			// 
			// radioButton12
			// 
			this.radioButton12.Index = ((uint)(1u));
			this.radioButton12.Location = new System.Drawing.Point(189, 21);
			this.radioButton12.Margin = new System.Windows.Forms.Padding(4);
			this.radioButton12.Name = "radioButton12";
			this.radioButton12.Size = new System.Drawing.Size(61, 30);
			this.radioButton12.TabIndex = 1;
			this.radioButton12.Text = "On";
			this.radioButton12.UseVisualStyleBackColor = true;
			// 
			// groupBoxSampleRate
			// 
			this.groupBoxSampleRate.Controls.Add(this.betterRadioButton12);
			this.groupBoxSampleRate.Controls.Add(this.betterRadioButton1);
			this.groupBoxSampleRate.Controls.Add(this.betterRadioButton10);
			this.groupBoxSampleRate.Controls.Add(this.betterRadioButton3);
			this.groupBoxSampleRate.Controls.Add(this.betterRadioButton4);
			this.groupBoxSampleRate.Controls.Add(this.betterRadioButton8);
			this.groupBoxSampleRate.Controls.Add(this.betterRadioButton7);
			this.groupBoxSampleRate.Controls.Add(this.betterRadioButton2);
			this.groupBoxSampleRate.Location = new System.Drawing.Point(21, 96);
			this.groupBoxSampleRate.Margin = new System.Windows.Forms.Padding(4);
			this.groupBoxSampleRate.Name = "groupBoxSampleRate";
			this.groupBoxSampleRate.Padding = new System.Windows.Forms.Padding(4);
			this.groupBoxSampleRate.SelectedRadioButtonIndex = ((uint)(3u));
			this.groupBoxSampleRate.Size = new System.Drawing.Size(665, 71);
			this.groupBoxSampleRate.TabIndex = 2;
			this.groupBoxSampleRate.TabStop = false;
			this.groupBoxSampleRate.Text = "Sample Rate (kHz)";
			// 
			// betterRadioButton12
			// 
			this.betterRadioButton12.Index = ((uint)(0u));
			this.betterRadioButton12.Location = new System.Drawing.Point(508, 20);
			this.betterRadioButton12.Margin = new System.Windows.Forms.Padding(4);
			this.betterRadioButton12.Name = "betterRadioButton12";
			this.betterRadioButton12.Size = new System.Drawing.Size(109, 42);
			this.betterRadioButton12.TabIndex = 7;
			this.betterRadioButton12.Text = "Disable sampling";
			this.betterRadioButton12.UseVisualStyleBackColor = true;
			// 
			// betterRadioButton1
			// 
			this.betterRadioButton1.Index = ((uint)(1u));
			this.betterRadioButton1.Location = new System.Drawing.Point(27, 20);
			this.betterRadioButton1.Margin = new System.Windows.Forms.Padding(4);
			this.betterRadioButton1.Name = "betterRadioButton1";
			this.betterRadioButton1.Size = new System.Drawing.Size(60, 42);
			this.betterRadioButton1.TabIndex = 0;
			this.betterRadioButton1.Text = "288";
			this.betterRadioButton1.UseVisualStyleBackColor = true;
			// 
			// betterRadioButton10
			// 
			this.betterRadioButton10.Index = ((uint)(12u));
			this.betterRadioButton10.Location = new System.Drawing.Point(440, 20);
			this.betterRadioButton10.Margin = new System.Windows.Forms.Padding(4);
			this.betterRadioButton10.Name = "betterRadioButton10";
			this.betterRadioButton10.Size = new System.Drawing.Size(60, 42);
			this.betterRadioButton10.TabIndex = 6;
			this.betterRadioButton10.Text = "24";
			this.betterRadioButton10.UseVisualStyleBackColor = true;
			// 
			// betterRadioButton3
			// 
			this.betterRadioButton3.Checked = true;
			this.betterRadioButton3.Index = ((uint)(3u));
			this.betterRadioButton3.Location = new System.Drawing.Point(168, 20);
			this.betterRadioButton3.Margin = new System.Windows.Forms.Padding(4);
			this.betterRadioButton3.Name = "betterRadioButton3";
			this.betterRadioButton3.Size = new System.Drawing.Size(60, 42);
			this.betterRadioButton3.TabIndex = 2;
			this.betterRadioButton3.TabStop = true;
			this.betterRadioButton3.Text = "96";
			this.betterRadioButton3.UseVisualStyleBackColor = true;
			// 
			// betterRadioButton4
			// 
			this.betterRadioButton4.Index = ((uint)(4u));
			this.betterRadioButton4.Location = new System.Drawing.Point(236, 20);
			this.betterRadioButton4.Margin = new System.Windows.Forms.Padding(4);
			this.betterRadioButton4.Name = "betterRadioButton4";
			this.betterRadioButton4.Size = new System.Drawing.Size(60, 42);
			this.betterRadioButton4.TabIndex = 3;
			this.betterRadioButton4.Text = "72";
			this.betterRadioButton4.UseVisualStyleBackColor = true;
			// 
			// betterRadioButton8
			// 
			this.betterRadioButton8.Index = ((uint)(8u));
			this.betterRadioButton8.Location = new System.Drawing.Point(372, 20);
			this.betterRadioButton8.Margin = new System.Windows.Forms.Padding(4);
			this.betterRadioButton8.Name = "betterRadioButton8";
			this.betterRadioButton8.Size = new System.Drawing.Size(60, 42);
			this.betterRadioButton8.TabIndex = 5;
			this.betterRadioButton8.Text = "36";
			this.betterRadioButton8.UseVisualStyleBackColor = true;
			// 
			// betterRadioButton7
			// 
			this.betterRadioButton7.Index = ((uint)(6u));
			this.betterRadioButton7.Location = new System.Drawing.Point(304, 20);
			this.betterRadioButton7.Margin = new System.Windows.Forms.Padding(4);
			this.betterRadioButton7.Name = "betterRadioButton7";
			this.betterRadioButton7.Size = new System.Drawing.Size(60, 42);
			this.betterRadioButton7.TabIndex = 4;
			this.betterRadioButton7.Text = "48";
			this.betterRadioButton7.UseVisualStyleBackColor = true;
			// 
			// betterRadioButton2
			// 
			this.betterRadioButton2.Index = ((uint)(2u));
			this.betterRadioButton2.Location = new System.Drawing.Point(100, 20);
			this.betterRadioButton2.Margin = new System.Windows.Forms.Padding(4);
			this.betterRadioButton2.Name = "betterRadioButton2";
			this.betterRadioButton2.Size = new System.Drawing.Size(60, 42);
			this.betterRadioButton2.TabIndex = 1;
			this.betterRadioButton2.Text = "144";
			this.betterRadioButton2.UseVisualStyleBackColor = true;
			// 
			// groupBoxRecordSkedule
			// 
			this.groupBoxRecordSkedule.Controls.Add(this.panelRecordTiming);
			this.groupBoxRecordSkedule.Controls.Add(this.checkBoxRunCont);
			this.groupBoxRecordSkedule.Dock = System.Windows.Forms.DockStyle.Top;
			this.groupBoxRecordSkedule.Location = new System.Drawing.Point(0, 102);
			this.groupBoxRecordSkedule.Margin = new System.Windows.Forms.Padding(4);
			this.groupBoxRecordSkedule.Name = "groupBoxRecordSkedule";
			this.groupBoxRecordSkedule.Padding = new System.Windows.Forms.Padding(4);
			this.groupBoxRecordSkedule.SelectedRadioButtonIndex = ((uint)(0u));
			this.groupBoxRecordSkedule.Size = new System.Drawing.Size(717, 127);
			this.groupBoxRecordSkedule.TabIndex = 1;
			this.groupBoxRecordSkedule.TabStop = false;
			this.groupBoxRecordSkedule.Text = "Recording Schedule";
			// 
			// panelRecordTiming
			// 
			this.panelRecordTiming.Controls.Add(this.label12);
			this.panelRecordTiming.Controls.Add(this.textBoxOnceEvery);
			this.panelRecordTiming.Controls.Add(this.labelInvalidTimes);
			this.panelRecordTiming.Controls.Add(this.comboBoxOnceEvery);
			this.panelRecordTiming.Controls.Add(this.label11);
			this.panelRecordTiming.Controls.Add(this.textBoxPeriodOf);
			this.panelRecordTiming.Controls.Add(this.comboBoxPeriodOf);
			this.panelRecordTiming.Location = new System.Drawing.Point(189, 23);
			this.panelRecordTiming.Margin = new System.Windows.Forms.Padding(4);
			this.panelRecordTiming.Name = "panelRecordTiming";
			this.panelRecordTiming.Size = new System.Drawing.Size(497, 95);
			this.panelRecordTiming.TabIndex = 1;
			// 
			// label12
			// 
			this.label12.Location = new System.Drawing.Point(21, 42);
			this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(143, 28);
			this.label12.TabIndex = 3;
			this.label12.Text = "Once every ";
			// 
			// textBoxOnceEvery
			// 
			this.textBoxOnceEvery.Location = new System.Drawing.Point(197, 42);
			this.textBoxOnceEvery.Margin = new System.Windows.Forms.Padding(4);
			this.textBoxOnceEvery.Name = "textBoxOnceEvery";
			this.textBoxOnceEvery.Size = new System.Drawing.Size(80, 22);
			this.textBoxOnceEvery.TabIndex = 4;
			this.textBoxOnceEvery.Text = "6";
			this.textBoxOnceEvery.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// labelInvalidTimes
			// 
			this.labelInvalidTimes.ForeColor = System.Drawing.Color.Red;
			this.labelInvalidTimes.Location = new System.Drawing.Point(5, 74);
			this.labelInvalidTimes.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.labelInvalidTimes.Name = "labelInvalidTimes";
			this.labelInvalidTimes.Size = new System.Drawing.Size(444, 21);
			this.labelInvalidTimes.TabIndex = 6;
			this.labelInvalidTimes.Text = "Record period must be less than record interval!";
			// 
			// comboBoxOnceEvery
			// 
			this.comboBoxOnceEvery.FormattingEnabled = true;
			this.comboBoxOnceEvery.Items.AddRange(new object[] {
			"Seconds",
			"Minutes",
			"Hours",
			"Days"});
			this.comboBoxOnceEvery.Location = new System.Drawing.Point(299, 42);
			this.comboBoxOnceEvery.Margin = new System.Windows.Forms.Padding(4);
			this.comboBoxOnceEvery.Name = "comboBoxOnceEvery";
			this.comboBoxOnceEvery.Size = new System.Drawing.Size(113, 24);
			this.comboBoxOnceEvery.TabIndex = 5;
			this.comboBoxOnceEvery.Text = "Hours";
			this.comboBoxOnceEvery.SelectedIndexChanged += new System.EventHandler(this.ComboBoxOnceEverySelectedIndexChanged);
			// 
			// label11
			// 
			this.label11.Location = new System.Drawing.Point(21, 17);
			this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(153, 28);
			this.label11.TabIndex = 0;
			this.label11.Text = "Record for a period of";
			// 
			// textBoxPeriodOf
			// 
			this.textBoxPeriodOf.Location = new System.Drawing.Point(197, 14);
			this.textBoxPeriodOf.Margin = new System.Windows.Forms.Padding(4);
			this.textBoxPeriodOf.Name = "textBoxPeriodOf";
			this.textBoxPeriodOf.Size = new System.Drawing.Size(80, 22);
			this.textBoxPeriodOf.TabIndex = 1;
			this.textBoxPeriodOf.Text = "30";
			this.textBoxPeriodOf.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// comboBoxPeriodOf
			// 
			this.comboBoxPeriodOf.FormattingEnabled = true;
			this.comboBoxPeriodOf.Items.AddRange(new object[] {
			"Seconds",
			"Minutes",
			"Hours",
			"Days"});
			this.comboBoxPeriodOf.Location = new System.Drawing.Point(299, 14);
			this.comboBoxPeriodOf.Margin = new System.Windows.Forms.Padding(4);
			this.comboBoxPeriodOf.Name = "comboBoxPeriodOf";
			this.comboBoxPeriodOf.Size = new System.Drawing.Size(113, 24);
			this.comboBoxPeriodOf.TabIndex = 2;
			this.comboBoxPeriodOf.Text = "Minutes";
			this.comboBoxPeriodOf.SelectedIndexChanged += new System.EventHandler(this.ComboBoxOnceEverySelectedIndexChanged);
			// 
			// checkBoxRunCont
			// 
			this.checkBoxRunCont.Checked = true;
			this.checkBoxRunCont.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxRunCont.Location = new System.Drawing.Point(28, 36);
			this.checkBoxRunCont.Margin = new System.Windows.Forms.Padding(4);
			this.checkBoxRunCont.Name = "checkBoxRunCont";
			this.checkBoxRunCont.Size = new System.Drawing.Size(185, 30);
			this.checkBoxRunCont.TabIndex = 0;
			this.checkBoxRunCont.Text = "Periodic Recording";
			this.checkBoxRunCont.UseVisualStyleBackColor = true;
			this.checkBoxRunCont.CheckedChanged += new System.EventHandler(this.CheckBoxRunContCheckedChanged);
			// 
			// groupBoxRecStart
			// 
			this.groupBoxRecStart.Controls.Add(this.radioButton1);
			this.groupBoxRecStart.Controls.Add(this.radioButton17);
			this.groupBoxRecStart.Controls.Add(this.dateTimePicker1);
			this.groupBoxRecStart.Controls.Add(this.radioButton14);
			this.groupBoxRecStart.Dock = System.Windows.Forms.DockStyle.Top;
			this.groupBoxRecStart.Location = new System.Drawing.Point(0, 0);
			this.groupBoxRecStart.Margin = new System.Windows.Forms.Padding(4);
			this.groupBoxRecStart.Name = "groupBoxRecStart";
			this.groupBoxRecStart.Padding = new System.Windows.Forms.Padding(4);
			this.groupBoxRecStart.SelectedRadioButtonIndex = ((uint)(1u));
			this.groupBoxRecStart.Size = new System.Drawing.Size(717, 102);
			this.groupBoxRecStart.TabIndex = 0;
			this.groupBoxRecStart.TabStop = false;
			this.groupBoxRecStart.Text = "Recording Starts ";
			// 
			// radioButton1
			// 
			this.radioButton1.Index = ((uint)(0u));
			this.radioButton1.Location = new System.Drawing.Point(20, 63);
			this.radioButton1.Margin = new System.Windows.Forms.Padding(4);
			this.radioButton1.Name = "radioButton1";
			this.radioButton1.Size = new System.Drawing.Size(253, 30);
			this.radioButton1.TabIndex = 1;
			this.radioButton1.Text = "Manually via remote control";
			this.radioButton1.UseVisualStyleBackColor = true;
			this.radioButton1.CheckedChanged += new System.EventHandler(this.RadioButton1CheckedChanged);
			// 
			// radioButton17
			// 
			this.radioButton17.Checked = true;
			this.radioButton17.Index = ((uint)(1u));
			this.radioButton17.Location = new System.Drawing.Point(20, 28);
			this.radioButton17.Margin = new System.Windows.Forms.Padding(4);
			this.radioButton17.Name = "radioButton17";
			this.radioButton17.Size = new System.Drawing.Size(251, 30);
			this.radioButton17.TabIndex = 0;
			this.radioButton17.TabStop = true;
			this.radioButton17.Text = "Immediately upon USB disconect";
			this.radioButton17.UseVisualStyleBackColor = true;
			// 
			// dateTimePicker1
			// 
			this.dateTimePicker1.CustomFormat = "ddddd, MMMM dd,  HH:mm:ss";
			this.dateTimePicker1.Enabled = false;
			this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dateTimePicker1.Location = new System.Drawing.Point(372, 33);
			this.dateTimePicker1.Margin = new System.Windows.Forms.Padding(4);
			this.dateTimePicker1.Name = "dateTimePicker1";
			this.dateTimePicker1.Size = new System.Drawing.Size(293, 22);
			this.dateTimePicker1.TabIndex = 6;
			// 
			// radioButton14
			// 
			this.radioButton14.Index = ((uint)(4u));
			this.radioButton14.Location = new System.Drawing.Point(279, 28);
			this.radioButton14.Margin = new System.Windows.Forms.Padding(4);
			this.radioButton14.Name = "radioButton14";
			this.radioButton14.Size = new System.Drawing.Size(185, 30);
			this.radioButton14.TabIndex = 4;
			this.radioButton14.Text = "At time:";
			this.radioButton14.UseVisualStyleBackColor = true;
			this.radioButton14.CheckedChanged += new System.EventHandler(this.RadioButton14CheckedChanged);
			// 
			// SudarDeployControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoScroll = true;
			this.AutoSize = true;
			this.Controls.Add(this.panel6);
			this.Margin = new System.Windows.Forms.Padding(4);
			this.Name = "SudarDeployControl";
			this.Size = new System.Drawing.Size(717, 598);
			this.Load += new System.EventHandler(this.SudarDeployControlLoad);
			this.panel6.ResumeLayout(false);
			this.groupBoxSync.ResumeLayout(false);
			this.gbAux.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownLogOnceEvery)).EndInit();
			this.groupBox6.ResumeLayout(false);
			this.gbChanSel.ResumeLayout(false);
			this.betterGroupBox1.ResumeLayout(false);
			this.groupBoxGain.ResumeLayout(false);
			this.gbHighPass.ResumeLayout(false);
			this.groupBoxSampleRate.ResumeLayout(false);
			this.groupBoxRecordSkedule.ResumeLayout(false);
			this.panelRecordTiming.ResumeLayout(false);
			this.panelRecordTiming.PerformLayout();
			this.groupBoxRecStart.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		private System.Windows.Forms.CheckBox cbCh1;
		private System.Windows.Forms.CheckBox cbCh2;
		private System.Windows.Forms.CheckBox cbCh3;
		private System.Windows.Forms.CheckBox cbCh4;
		private System.Windows.Forms.GroupBox gbChanSel;
		private SudarHost.BetterRadioButton betterRadioButton12;
		private System.Windows.Forms.CheckBox cblogGps;
		private SudarHost.BetterRadioButton betterRadioButton9;
		private SudarHost.BetterRadioButton betterRadioButton11;
		private System.Windows.Forms.Button button1;
		private SudarHost.BetterGroupBox betterGroupBox1;
		private SudarHost.BetterRadioButton betterRadioButton10;
		private SudarHost.BetterRadioButton betterRadioButton8;
		private SudarHost.BetterRadioButton betterRadioButton7;
		private System.Windows.Forms.NumericUpDown numericUpDownLogOnceEvery;
		private System.Windows.Forms.CheckBox checkBoxDisCalTones;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private SudarHost.BetterGroupBox groupBoxGain;
		private SudarHost.BetterRadioButton betterRadioButton6;
		private SudarHost.BetterRadioButton betterRadioButton5;
		private System.Windows.Forms.Label labelInvalidTimes;
		private System.Windows.Forms.ComboBox comboBoxPeriodOf;
		private System.Windows.Forms.ComboBox comboBoxOnceEvery;
		private System.Windows.Forms.TextBox textBoxPeriodOf;
		private System.Windows.Forms.TextBox textBoxOnceEvery;
		private SudarHost.BetterGroupBox groupBoxSync;
		private System.Windows.Forms.Panel panelRecordTiming;
		private System.Windows.Forms.CheckBox checkBoxTemperature;
		private System.Windows.Forms.CheckBox checkBoxAcceleration;
		private SudarHost.BetterRadioButton betterRadioButton3;
		private SudarHost.BetterRadioButton betterRadioButton2;
		private SudarHost.BetterRadioButton betterRadioButton4;
		private SudarHost.BetterRadioButton betterRadioButton1;
		private SudarHost.BetterGroupBox groupBoxSampleRate;
		private SudarHost.BetterGroupBox gbHighPass;
		private System.Windows.Forms.CheckBox checkBoxRunCont;
		private SudarHost.BetterGroupBox groupBoxRecordSkedule;
		private SudarHost.BetterGroupBox groupBoxRecStart;
		private SudarHost.BetterRadioButton radioButton1;
		private BetterGroupBox gbAux;
		private SudarHost.BetterRadioButton radioButton13;
		private SudarHost.BetterRadioButton radioButton12;
		private SudarHost.BetterGroupBox groupBox6;
		private SudarHost.BetterRadioButton radioButton14;
		private System.Windows.Forms.DateTimePicker dateTimePicker1;
		private SudarHost.BetterRadioButton radioButton17;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Panel panel6;
		private System.Windows.Forms.CheckBox cBstream;
	}
}
