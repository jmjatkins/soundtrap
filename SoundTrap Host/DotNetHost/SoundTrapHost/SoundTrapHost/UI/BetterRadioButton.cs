﻿/* 
* 	SoundTrap Software v1.0
*  
*	Copyright (C) 2011-2014, John Atkins and Mark Johnson
*
*	This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
*
*	This file is part of the SoundTrap software. SoundTrap is an acoustic recording
*	system intended for underwater acoustic measurements. This component of the 
*	SoundTrap project is free software: you can redistribute it and/or modify it 
*	under the terms of the GNU General Public License as published by the Free Software 
*	Foundation, either version 3 of the License, or any later version.
*
*	The SoundTrap software is distributed in the hope that it will be useful, but 
*	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
*	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License along with this 
*	code. If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Windows.Forms;


namespace SudarHost
{
	/// <summary>
	/// Description of BetterRadioButton.
	/// </summary>
	public class BetterRadioButton : RadioButton
	{
		UInt32 index;
		public UInt32 Index {
			get { return index; }
			set { index = value; }
		}
	}
	
	public class BetterGroupBox : GroupBox
	{

		public RadioButton SelectedRadioButton {
			get {
				foreach(Control c in this.Controls) {
					if( c is RadioButton ) {
						if( (c as RadioButton).Checked )
						{
							return c as RadioButton;
						}
					}
				}
				return null;
			}
		}
		public UInt32 SelectedRadioButtonIndex {
			get {
				foreach(Control c in this.Controls) {
					if( c is BetterRadioButton ) {
						if( (c as BetterRadioButton).Checked )
						{
							return (c as BetterRadioButton).Index;
						}
					}
				}
				return 0;
			}
			set {
				foreach(Control c in this.Controls) {
					if( c is BetterRadioButton ) {
						if((c as BetterRadioButton).Index == value) {
							(c as BetterRadioButton).Checked = true;
						}
					}
				}
			}
		}
	}
}


