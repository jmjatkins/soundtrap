﻿/*
 * Created by SharpDevelop.
 * User: jatk009
 * Date: 5/09/2012
 * Time: 12:05 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace SudarHost
{
	partial class FileExtractionControl
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the control.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.button1 = new System.Windows.Forms.Button();
			this.panel4 = new System.Windows.Forms.Panel();
			this.label19 = new System.Windows.Forms.Label();
			this.button4 = new System.Windows.Forms.Button();
			this.label18 = new System.Windows.Forms.Label();
			this.progressBar2 = new System.Windows.Forms.ProgressBar();
			this.panel4.SuspendLayout();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(16, 14);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(117, 23);
			this.button1.TabIndex = 0;
			this.button1.Text = "Select File...";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.Button1Click);
			// 
			// panel4
			// 
			this.panel4.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.panel4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
			this.panel4.Controls.Add(this.label19);
			this.panel4.Controls.Add(this.button4);
			this.panel4.Controls.Add(this.label18);
			this.panel4.Controls.Add(this.progressBar2);
			this.panel4.Location = new System.Drawing.Point(67, 26);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(208, 124);
			this.panel4.TabIndex = 4;
			this.panel4.Visible = false;
			// 
			// label19
			// 
			this.label19.Location = new System.Drawing.Point(21, 22);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(162, 23);
			this.label19.TabIndex = 3;
			this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// button4
			// 
			this.button4.Location = new System.Drawing.Point(108, 79);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(75, 23);
			this.button4.TabIndex = 2;
			this.button4.Text = "Cancel";
			this.button4.UseVisualStyleBackColor = true;
			this.button4.Click += new System.EventHandler(this.Button4Click);
			// 
			// label18
			// 
			this.label18.Location = new System.Drawing.Point(21, 3);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(162, 23);
			this.label18.TabIndex = 1;
			this.label18.Text = "label18";
			this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// progressBar2
			// 
			this.progressBar2.Location = new System.Drawing.Point(21, 48);
			this.progressBar2.Name = "progressBar2";
			this.progressBar2.Size = new System.Drawing.Size(162, 23);
			this.progressBar2.TabIndex = 0;
			// 
			// FileExtractionControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.panel4);
			this.Controls.Add(this.button1);
			this.Name = "FileExtractionControl";
			this.Size = new System.Drawing.Size(342, 177);
			this.panel4.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.ProgressBar progressBar2;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Button button1;
	}
}
