﻿/* 
 * 	SoundTrap Software v1.0
 *
 *	Copyright (C) 2011-2014, John Atkins and Mark Johnson
 *
 *	This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
 *
 *	This file is part of the SoundTrap software. SoundTrap is an acoustic recording
 *	system intended for underwater acoustic measurements. This component of the
 *	SoundTrap project is free software: you can redistribute it and/or modify it
 *	under the terms of the GNU General Public License as published by the Free Software
 *	Foundation, either version 3 of the License, or any later version.
 *
 *	The SoundTrap software is distributed in the hope that it will be useful, but
 *	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License along with this
 *	code. If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SudarHost
{
	/// <summary>
	/// Description of SudarDeployControl.
	/// </summary>
	
	public partial class SudarDeployControl : UserControl
	{
		uint[] timeScale = {1, 60, 60*60, 3600*24};
		
		UInt32 timeOnCount;
		public uint TimeOnCount {
			get { return timeOnCount; }
			set {
				timeOnCount = value;
				labelInvalidTimes.Visible = !TimesValid;
			}
		}

		UInt32 timeOnceEveryCount;
		public UInt32 TimeOnceEveryCount {
			get { return timeOnceEveryCount; }
			set {
				timeOnceEveryCount = value;
				labelInvalidTimes.Visible = !TimesValid;
			}
		}
		
		public UInt32 TimeOn {
			get {
				return TimeOnCount * timeScale[comboBoxPeriodOf.SelectedIndex];
			}
			set {
				TimeOnCount  = value /  timeScale[comboBoxPeriodOf.SelectedIndex];
			}
		}
		
		public UInt32 TimeOnceEvery {
			get {
				return TimeOnceEveryCount * timeScale[comboBoxOnceEvery.SelectedIndex];
			}
			set {
				TimeOnceEveryCount = value / timeScale[comboBoxOnceEvery.SelectedIndex];
			}
		}
		
		public bool TimesValid {
			get {
				Int64 i = TimeOnceEvery;
				Int64 offtime = i - TimeOn;
				return offtime >= 0;
			}
		}

		void ComboBoxOnceEverySelectedIndexChanged(object sender, EventArgs e)
		{
			labelInvalidTimes.Visible = !TimesValid;
		}


		public SudarDeployControl()
		{
			InitializeComponent();
		}
		
		void SudarDeployControlLoad(object sender, EventArgs e)
		{
			textBoxPeriodOf.DataBindings.Add(new Binding("Text", this, "TimeOnCount", true, DataSourceUpdateMode.OnValidation, ""));
			textBoxOnceEvery.DataBindings.Add(new Binding("Text", this, "TimeOnceEveryCount", true, DataSourceUpdateMode.OnValidation, ""));
			//labelInvalidTimes.DataBindings.Add(new Binding("Visible", this, "TimesValid", true, DataSourceUpdateMode.OnValidation, ""));
		}
		
		int baseSampleRate = 0;
		public void UpdateSampleRateOptions(int baseSampleRate)
		{
			if(baseSampleRate != this.baseSampleRate) {
				foreach(Control c in groupBoxSampleRate.Controls)
				{
					if(c is BetterRadioButton) {
						if((c as BetterRadioButton).Index > 0) {
							int sampleRate = (int)(baseSampleRate / (c as BetterRadioButton).Index);
							(c as BetterRadioButton).Text = sampleRate.ToString();
						}
					}
				}
			}
		}
		
		SudarConfig sudarConfig = new SudarConfig();
		
		public SudarConfig GetSudarConfig() {
			sudarConfig.startTriggerMode =  groupBoxRecStart.SelectedRadioButtonIndex;
			sudarConfig.runMode = checkBoxRunCont.Checked ? (UInt32)1 : (UInt32)0;
			if(sudarConfig.runMode == 1) {
				if(!TimesValid) throw new Exception("The periodic recording time settings are invalid\nPlease correct");
			}
			
			sudarConfig.onTime = TimeOn;
			sudarConfig.onceEveryTime = TimeOnceEvery;
			sudarConfig.onTimeScale = (UInt32)comboBoxPeriodOf.SelectedIndex;
			sudarConfig.onceEveryTimeTimeScale = (UInt32)comboBoxOnceEvery.SelectedIndex;
			sudarConfig.decimator = groupBoxSampleRate.SelectedRadioButtonIndex;
			sudarConfig.gain = groupBoxGain.SelectedRadioButtonIndex;
			sudarConfig.hPass = gbHighPass.SelectedRadioButtonIndex;
			sudarConfig.syncMode = groupBoxSync.SelectedRadioButtonIndex;
			sudarConfig.startTime = Properties.Settings.Default.UseUTC ? 
				UnixDateTime.ConvertToUnixTime(dateTimePicker1.Value) : UnixDateTime.ConvertToUnixTime( dateTimePicker1.Value.ToUniversalTime() );
			sudarConfig.auxSensorEnable = 0;
			sudarConfig.compressionMode = 1;
			
			
			if(cBstream.Checked) {
				sudarConfig.serialLogMode = 2;
			}
			else if(cblogGps.Checked) {
				sudarConfig.serialLogMode = 1;
			}
			else {
				sudarConfig.serialLogMode = 0;
			}
			
			sudarConfig.channelEnable = 0;
			if(gbChanSel.Visible) {
				if( cbCh4.Checked ) sudarConfig.channelEnable += 1;
				sudarConfig.channelEnable <<=1;
				if( cbCh3.Checked ) sudarConfig.channelEnable += 1;
				sudarConfig.channelEnable <<=1;
				if( cbCh2.Checked ) sudarConfig.channelEnable += 1;
				sudarConfig.channelEnable <<=1;
				if( cbCh1.Checked ) sudarConfig.channelEnable += 1;
			}
			sudarConfig.DetectParam1 = betterGroupBox1.SelectedRadioButtonIndex;

			if((sudarConfig.DetectParam1 > 0) && (chanCount() >1)) {
				throw new Exception("Click detector cannot be used in conjunction with multiple channels - please correct");
			}
			
			if((sudarConfig.DetectParam1 > 0) && (sudarConfig.decimator < 4)) {
				throw new Exception("Click detector cannot be used in conjunction this sample rate - please correct");
			}
			
			sudarConfig.flags = 0;
			if(checkBoxDisCalTones.Checked) sudarConfig.flags |= (uint) CONFIG_FLAGS.DISABLE_CAL_ROUTINE;
			
			try {
				sudarConfig.auxSensorInterval = (uint)numericUpDownLogOnceEvery.Value;
			}
			catch
			{
				throw new Exception("The sensor log interval is invalid\nPlease correct");
			}
			
			if(checkBoxAcceleration.Checked) sudarConfig.auxSensorEnable += (int)AuxSensor.Accelerometer ;
			if(checkBoxTemperature.Checked) sudarConfig.auxSensorEnable += (int)AuxSensor.Temeprature;
			//if(checkBoxPressure.Checked) sudarConfig.auxSensorEnable += (int)AuxSensor.Pressure;
			sudarConfig.UpdateCrc();

			return sudarConfig;
		}
		
		
		
		public void SetSudarConfig(SudarConfig value, Sudar sudar)
		{
			sudarConfig = value;
			
			gbHighPass.Enabled = sudar.HighPassSettingAvailable;
			gbChanSel.Visible = ((sudar.model == Sudar.Model.ST4300STD) || (sudar.model == Sudar.Model.ST4300) || (sudar.model == Sudar.Model.ST4300HF));
			groupBoxGain.Enabled = sudar.GainSettingAvailable;
			gbAux.Enabled = (sudar.model < Sudar.Model.ST500STD);
			
			groupBoxRecStart.SelectedRadioButtonIndex = value.startTriggerMode;
			checkBoxRunCont.Checked = !(value.runMode == 0);
			groupBoxSampleRate.SelectedRadioButtonIndex = value.decimator;
			groupBoxSync.SelectedRadioButtonIndex = value.syncMode;
			groupBoxGain.SelectedRadioButtonIndex =  value.gain;
			gbHighPass.SelectedRadioButtonIndex = value.hPass;
			dateTimePicker1.Value = Properties.Settings.Default.UseUTC ?
				UnixDateTime.ConvertToDateTime(value.startTime) : UnixDateTime.ConvertToDateTime(value.startTime).ToLocalTime();
			comboBoxPeriodOf.SelectedIndex =  (int)value.onTimeScale;
			comboBoxOnceEvery.SelectedIndex = (int)value.onceEveryTimeTimeScale;
			TimeOn = value.onTime;
			TimeOnceEvery = value.onceEveryTime;
			checkBoxAcceleration.Checked = (value.auxSensorEnable  & (int)AuxSensor.Accelerometer) > 0;
			checkBoxTemperature.Checked = (value.auxSensorEnable  &  (int)AuxSensor.Temeprature) > 0;
			//checkBoxPressure.Checked = (value.auxSensorEnable  & (int)AuxSensor.Pressure) > 0;
			//checkBoxCompress.Checked = value.compressionMode > 0;
			//textBoxAuxLogEvery.Enabled =  value.auxSensorEnable > 0;
			numericUpDownLogOnceEvery.Value = value.auxSensorInterval;
			checkBoxDisCalTones.Checked = (value.flags & (uint)CONFIG_FLAGS.DISABLE_CAL_ROUTINE) > 0;

			cbCh1.Checked = true;//(sudarConfig.channelEnable & 0x01) > 0;
			cbCh2.Checked = (sudarConfig.channelEnable & 0x02) > 0;
			cbCh3.Checked = (sudarConfig.channelEnable & 0x04) > 0;
			cbCh4.Checked = (sudarConfig.channelEnable & 0x08) > 0;
			
			betterGroupBox1.SelectedRadioButtonIndex = sudarConfig.DetectParam1;

			cBstream.Checked = (sudarConfig.serialLogMode == 2);
			cblogGps.Checked = (sudarConfig.serialLogMode == 1);
			
			if(sudar.model >= Sudar.Model.ST500STD) {
				checkBoxAcceleration.Checked = false;
			}
		}
		
		void CheckBoxRunContCheckedChanged(object sender, EventArgs e)
		{
			panelRecordTiming.Enabled = checkBoxRunCont.Checked;
		}
		
		void RadioButton1CheckedChanged(object sender, EventArgs e)
		{
			
		}
		
		void TextBoxAuxLogEveryValidating(object sender, CancelEventArgs e)
		{
			
		}
		
		void Label9Click(object sender, EventArgs e)
		{
			
		}
		
		void BetterRadioButton9CheckedChanged(object sender, System.EventArgs e)
		{
			button1.Enabled =  !betterRadioButton9.Checked;
		}
		
		void Button1Click(object sender, System.EventArgs e)
		{
			Form f = new Form();
			f.FormBorderStyle = FormBorderStyle.FixedDialog;
			f.MaximizeBox = false;
			f.MinimizeBox = false;
			//f.ControlBox = false;
			f.Icon = ParentForm.Icon;
			f.ShowInTaskbar = false;
			BwDetectorControl c = new BwDetectorControl();
			c.Set(sudarConfig);
			f.Controls.Add(c);
			//c.Dock = DockStyle.Fill;
			f.AutoSizeMode = AutoSizeMode.GrowAndShrink;
			f.StartPosition = FormStartPosition.CenterParent;
			f.FormBorderStyle = FormBorderStyle.FixedSingle;
			f.AutoSize = true;
			f.Text = "HF Click Detector Configuration";
			if( f.ShowDialog() == DialogResult.OK) {
				c.Get( ref sudarConfig );
			}
		}
		
		void CbChCheckedChanged(object sender, EventArgs e)
		{
			if(chanCount()>1)
				betterRadioButton9.Checked = true;
		}
		
		
		public int chanCount()
		{
			int i=0;
			if(gbChanSel.Visible) {
				if(cbCh1.Checked) i++;
				if(cbCh2.Checked) i++;
				if(cbCh3.Checked) i++;
				if(cbCh4.Checked) i++;
			}
			return i;
		}
		
		void RadioButton14CheckedChanged(object sender, EventArgs e)
		{
			dateTimePicker1.Enabled = radioButton14.Checked;
		}
		
	}
}
