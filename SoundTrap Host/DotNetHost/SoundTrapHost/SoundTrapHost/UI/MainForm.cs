﻿/* 
 * 	SoundTrap Software v1.0
 *
 *	Copyright (C) 2011-2014, John Atkins and Mark Johnson
 *
 *	This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
 *
 *	This file is part of the SoundTrap software. SoundTrap is an acoustic recording
 *	system intended for underwater acoustic measurements. This component of the
 *	SoundTrap project is free software: you can redistribute it and/or modify it
 *	under the terms of the GNU General Public License as published by the Free Software
 *	Foundation, either version 3 of the License, or any later version.
 *
 *	The SoundTrap software is distributed in the hope that it will be useful, but
 *	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License along with this
 *	code. If not, see <http://www.gnu.org/licenses/>.
 */


using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using LibUsbDotNet.DeviceNotify;
using LibUsbDotNet;
using LibUsbDotNet.Main;

namespace SudarHost
{
	public partial class MainForm : Form
	{
		private List<string> DeviceList = new List<string>();
		
		FileExtractionControl fec;
		
		public MainForm()
		{
			InitializeComponent();
			Sudar.DeviceListChanged += onDeviceAttached;
			Sudar.InitDeviceNotify();
			Text = "SoundTrap Host " + Application.ProductVersion;
			
			//displayInstrumentList();
			
			//Properties.Settings.Default.Reload();
			enableTestModeToolStripMenuItem.Checked = Properties.Settings.Default.AppMode == 1;
			zeroFillDropoutsToolStripMenuItem.Checked = Properties.Settings.Default.ZeroFill;
			useUTCTimeToolStripMenuItem.Checked = Properties.Settings.Default.UseUTC;

			try {
				if(Properties.Settings.Default.FileSaveLocation == "") {
					Properties.Settings.Default.FileSaveLocation = MakeDefaultFileSaveLocation();
				}
				else {
					//check path is valid
					try {
						Path.GetDirectoryName(Properties.Settings.Default.FileSaveLocation);
					}
					catch {
						Properties.Settings.Default.FileSaveLocation = MakeDefaultFileSaveLocation();
						Properties.Settings.Default.Save();
					}
				}
			}
			catch {
				//ignore
			}
		}
		
		void onDeviceAttached(object sender, EventArgs e)
		{
			displayInstrumentList();
		}
		
		private void displayInstrumentList()
		{
			List<Sudar> sudars = Sudar.GetDeviceList();
			panel2.Controls.Clear();
			
			if(sudars.Count ==0) {
				Label l = new Label();
				l.Text = "No Instruments Connected";
				l.TextAlign = ContentAlignment.MiddleCenter;
				l.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
				panel2.Controls.Add(l);
				l.Dock = DockStyle.Fill;
				
			}
			foreach(Sudar s in sudars )
			{
				InstrumentSelectControl sc = new InstrumentSelectControl();
				sc.Sudar = s;
				panel2.Controls.Add(sc);
				sc.Dock = DockStyle.Top;
				sc.Selected += DeviceSelected;
			}
			
		}
		
		Sudar selectedInstrument;
		
		private void DeviceSelected(object sender, EventArgs e)
		{
			panel5.Controls.Clear();
			InstrumentSelectControl sc = sender as InstrumentSelectControl;
			foreach(Control c in panel2.Controls) {
				if(c is InstrumentSelectControl) {
					(c as InstrumentSelectControl).MarkAsSelected(false);
				}
			}
			if(sc !=null) {
				sc.MarkAsSelected(true);
				selectedInstrument = sc.Sudar;
				panel5.Controls.Add(sc.Sudar.Control);
				sc.Sudar.Control.Dock = DockStyle.Fill;
				try {
					sc.Sudar.LedFlash();
				}
				catch {
				}
			}
		}

		private void MainFormLoad(object sender, EventArgs e)
		{
			displayInstrumentList();
		}
		
		private void FileExtractionToolStripMenuItemClick(object sender, EventArgs e)
		{
			if(fec ==null) {
				fec = new FileExtractionControl();
			}
			panel5.Controls.Clear();
			panel5.Controls.Add(fec);
			fec.Dock = DockStyle.Fill;
		}
		
		private void ExitToolStripMenuItemClick(object sender, EventArgs e)
		{
			Application.Exit();
		}
		
		private void AboutToolStripMenuItemClick(object sender, EventArgs e)
		{
			Serializer s = new Serializer(EndianMode.LITTLE);
			UInt16[] t = new UInt16[]{0x1234, 0x2345, 0x3456, 0x4567};
			byte[] tt = s.Serialize(t);
			UInt16 result = Crc.crc.calc(tt,tt.Length);
			
			
			Splash.Status = "";
			Splash.OkButtonIsVisible = true;
			Splash.ShowSplashScreen(true);
		}
		
		public string MakeDefaultFileSaveLocation() {
			string path;
			path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + Path.DirectorySeparatorChar + "SoundTrap";
			return path;
		}

		private void SetFileSaveLocationToolStripMenuItemClick(object sender, EventArgs e)
		{
			FolderBrowserDialog fbd = new FolderBrowserDialog();
			fbd.SelectedPath = Properties.Settings.Default.FileSaveLocation;
			if( fbd.ShowDialog() == DialogResult.OK) {
				Properties.Settings.Default.FileSaveLocation = fbd.SelectedPath;
				Properties.Settings.Default.Save();
			}
		}
		
		
		void EnableTestModeToolStripMenuItemClick(object sender, EventArgs e)
		{
			AppState.instance.mode = enableTestModeToolStripMenuItem.Checked ? AppState.Mode.prodTest : AppState.Mode.normal;
			Properties.Settings.Default.AppMode = (int) AppState.instance.mode;
			Properties.Settings.Default.Save();
		}
		
		void EnableTestModeToolStripMenuItemCheckedChanged(object sender, EventArgs e)
		{
			AppState.instance.mode = enableTestModeToolStripMenuItem.Checked ? AppState.Mode.prodTest : AppState.Mode.normal;
		}
		
		void ServiceToolStripMenuItemClick(object sender, EventArgs e)
		{
			if(selectedInstrument != null) {
				selectedInstrument.Control.DisplayServiceControl();
			}
		}
		
		void SoundTrapUserGuideToolStripMenuItemClick(object sender, EventArgs e)
		{
			string path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
			path += Path.DirectorySeparatorChar + "ST User Guide.pdf";
			System.Diagnostics.Process.Start(path);
		}
		
		void ZeroFillDropoutsToolStripMenuItemClick(object sender, EventArgs e)
		{
			Properties.Settings.Default.ZeroFill = zeroFillDropoutsToolStripMenuItem.Checked;
			Properties.Settings.Default.Save();
		}
		void UseUTCTimeToolStripMenuItemClick(object sender, EventArgs e)
		{
			Properties.Settings.Default.UseUTC = useUTCTimeToolStripMenuItem.Checked;
			Properties.Settings.Default.Save();
		}
		void MenuStrip1ItemClicked(object sender, ToolStripItemClickedEventArgs e)
		{
	
		}
	}
}
