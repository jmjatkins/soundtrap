﻿/*
 * Created by SharpDevelop.
 * User: jatk009
 * Date: 26/08/2014
 * Time: 9:02 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;
using OceanInstruments.ApiProxy2;
using System.Collections.Generic;
using System.Xml;
using System.IO;

namespace SudarHost.UI
{
	/// <summary>
	/// Description of ProdTestControl.
	/// </summary>
	public partial class ProdTestControl : UserControl
	{

		const string TEST_FILE_NAME = "tests.xml";
		
		SudarControl owner;
		public SudarControl Owner {
			get { return owner; }
			set { owner = value; }
		}

		public ProdTestControl()
		{
			InitializeComponent();
		}
		
		void textOut(string t) {
			textBox1.AppendText(t);
		}
		
		
		double CalcNoiseFloor() {
			
			double rms = 0;
			double av = 0;
			
			for(int i=0; i<owner.Sudar.AudioBuf.Length;i++){
				av += owner.Sudar.AudioBuf[i];
			}
			av /= owner.Sudar.AudioBuf.Length;
			
			for(int i=0; i<owner.Sudar.AudioBuf.Length;i++){
				rms +=  Math.Pow(owner.Sudar.AudioBuf[i] - av, 2);
			}
			rms = Math.Sqrt((rms / owner.Sudar.AudioBuf.Length));
			return 20 * Math.Log10(rms);
		}
		
		
		


		void displaySubResult(OiApiProxy.TestSubResult str) {
			textOut(str.name + "\r\n");
			textOut(String.Format("LL:{0:F2} R:{1:F2} HL:{2:F2} ", str.lowLimit, str.testValue, str.highLimit));
			textOut(str.testResult == 0 ? "passed\r\n" : "failed\r\n");
		}
		
		double levelCheck()
		{
			//flush audio buffer
			if( !owner.Sudar.AudioSample(1024) || !(owner.Sudar.AudioBuf.Length > 0) ) {
				throw new Exception("Audio Sample Failed");
			}
			
			if( !owner.Sudar.AudioSample(4096) || !(owner.Sudar.AudioBuf.Length > 0) ) {
				throw new Exception("Audio Sample Failed");
			}
			return Math.Round( CalcNoiseFloor(), 2);
		}
		
		void displayFailed( ) {
			label1.Text = "Failed";
			label1.BackColor = Color.Red;
		}
		
		void pass() {
			label1.Text = "Passed";
			label1.BackColor = Color.GreenYellow;
		}
		

		class TestSequence {
			public TestSequence() {
				tests = new List<ProdTestControl.Test>();
			}
			public string type;
			public string jig;
			public List<Test> tests;
		}

		class Test {
			public string model;
			public string testName;
			public ProdTest test;
			public float low;
			public float high;
			public string param1;
		}
		

		
		Dictionary<string,  TestSequence> testSequences = new Dictionary<string, ProdTestControl.TestSequence>();
		
		//load test sequence from xml file
		void LoadTests()
		{
			try {
				string path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
				path += Path.DirectorySeparatorChar + TEST_FILE_NAME;
				
				if(!File.Exists(path)) throw new Exception("Failed to find file " + path);

				//List<Test> tests = new List<ProdTestControl.Test>();
				XmlDocument reader = new XmlDocument();
				reader.Load(path);
				XmlNodeList e = reader.GetElementsByTagName("TestSet");
				foreach(XmlNode n in e) {
					TestSequence ts = new TestSequence();
					ts.type = n.Attributes["type"].Value;
					ts.jig  = n.Attributes["jig"].Value;
					foreach(XmlNode n1 in n.ChildNodes) {
						var t = new Test() {
							model = n1.Attributes["Model"].Value,
							testName = n1.Attributes["Name"].Value,
							low = Convert.ToSingle(n1.Attributes["LowLimit"].Value),
							high = Convert.ToSingle(n1.Attributes["HighLimit"].Value),
							param1 = n1.Attributes["Param1"].Value
						};
						string funcName = "SudarHost." + n1.Attributes["Function"].Value;
						var ttype = Type.GetType(funcName);
						if(ttype ==null) 
							throw new Exception();
						t.test = (ProdTest)Activator.CreateInstance(ttype);
						ts.tests.Add(t);
					}
					testSequences.Add(ts.type, ts);
				}
			}
			catch(Exception)
			{
			}
		}
		
		
		void RunTestSequence(string type, bool contineOnFail)
		{
			
			try {
				button1.Enabled = false;
				
				label1.Text = "Testing...";
				label1.BackColor = Color.Yellow;
				textBox1.Clear();
				Application.DoEvents();
				
				
				textBox1.ForeColor = Color.Black;
				textOut("Self test begins\r\n");

				int failed = 0;
				OiApiProxy.TestSubResult str;
				List<OiApiProxy.TestSubResult> subResultList  = new List<OiApiProxy.TestSubResult>();

				foreach(Test t in testSequences[type].tests) {
					if(t.model == "all" || t.model == owner.Sudar.modelName)
					{
						if(t.param1 == "serial") {
							var f = new GetParamForm();
							if ( f.ShowDialog(this) == DialogResult.Cancel) {
								break;
							}
							string serial = f.returnValue;
							str = t.test.DoTest(this.owner.Sudar, t.testName, t.low, t.high, serial);
						}
						else str = t.test.DoTest(this.owner.Sudar, t.testName, t.low, t.high, t.param1);
						displaySubResult(str);
						subResultList.Add(str);
						if(str.testResult != 0) {
							textOut("Failed\r\n");
							displayFailed();
							failed = 1;
							if(contineOnFail) 
								continue;
							else break;
						}
					}
				}

				if(failed > 0) {
					displayFailed();
					return;
				}
				
				pass();

				if(!OiApiProxy.IsConfigured) return;
				
				textOut("posting results...");
				try {
					string ver = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
					OiApiProxy.Instance.PostTestResult(owner.Sudar.SerialNumber, owner.Sudar.HwSerial, owner.Sudar.modelName,
					                                   ver, testSequences[type].jig, testSequences[type].type, 0, failed, subResultList);
				}
				catch(Exception ex) {
					textOut("failed.\r\n");
					MessageBox.Show("Failed to post results: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
				textOut("done.\r\n");
			}
			catch(Exception ex) {
				textOut("Failed - unexpected error" + ex.Message + "\r\n");
				displayFailed();
			}
			finally {
				button1.Enabled = true;
				button1.Focus();
			}
		}

		private class CalResult
		{
			public double low;
			public double high;
			public double refLevel;
			public double toneLev;
		}



		void calibrationST500() //cal type 1
		{
			double x;
			float[] fft;
			float[] data;

			int failed = 0;

			List<OiApiProxy.TestSubResult> subResultList  = new List<OiApiProxy.TestSubResult>();
			OiApiProxy.TestSubResult str;
			CalResult  calResult = new CalResult();
			try {
				button1.Enabled = false;
				
				label1.Text = "Testing...";
				label1.BackColor = Color.Yellow;
				textBox1.Clear();
				Application.DoEvents();

				textBox1.ForeColor = Color.Black;
				textOut("Calibration begins\r\n");

				textOut("Enabling Audio...");
				if( !owner.Sudar.AudioEnable(true) ) {
					textOut("Failed\r\n");
					displayFailed();
					return;
				}
				
				textOut("Setting sample rate...");
				if( !owner.Sudar.AudioSetDivider(4) ) {
					textOut("Failed\r\n");
					displayFailed();
					return;
				}
				textOut(String.Format("Sample Rate = {0}\r\n", owner.Sudar.AudioSampleRate));
				textOut("Passed.\r\n");
				
				textOut("Muting Audio...");
				if( !owner.Sudar.AudioSetMute(true) ) {
					textOut("Failed\r\n");
					displayFailed();
					return;
				}
				textOut("Passed.\r\n");

				textOut("Setting cal sig to 1kHz...");
				if( !owner.Sudar.AudioSetCalFrequency(1000) ) {
					textOut("Failed\r\n");
					displayFailed();
					return;
				}
				textOut("Passed.\r\n");

				textOut("enabing cal sig...");
				if( !owner.Sudar.AudioEnableCalSig(true) ) {
					textOut("Failed\r\n");
					displayFailed();
					return;
				}
				textOut("Passed.\r\n");

				textOut("sensitivity test...\r\n");
				Thread.Sleep(200);

				owner.Sudar.AudioSample(4096); //flush;
				owner.Sudar.AudioSample(4096*10);
				Thread.Sleep(200);
				
				fft = sigProc.fft(4096, owner.Sudar.AudioBuf);
				data = sigProc.interpolate(fft,fft.Length/2, 10);
				sigProc.findPeak( data, (float)(1000f / (owner.Sudar.AudioSampleRate / 2)), out x, out calResult.toneLev);

				calResult.refLevel = -37.6;// dBV = 13.2 mV
				calResult.low = 0;
				calResult.high = Math.Round(calResult.toneLev + (-1.0 * calResult.refLevel), 2);
				calResult.toneLev = Math.Round(calResult.toneLev, 2);
				
				str = new OiApiProxy.TestSubResult("sensitivity", 0, -3.0, -1.0, calResult.high);
				subResultList.Add(str);
				displaySubResult(str);
				if(str.testResult != 0) {
					displayFailed();
					failed = 1;
					goto PostResult;
				}
				
				str = new OiApiProxy.TestSubResult("tone level", 0, -41.0, -37.0 , calResult.toneLev);
				subResultList.Add(str);
				displaySubResult(str);
				if(str.testResult != 0) {
					displayFailed();
					failed = 1;
					goto PostResult;
				}
				
				textOut(String.Format("Serial = {0}\r\n", owner.Sudar.SerialNumber));
				textOut(String.Format("Model = {0}\r\n", owner.Sudar.modelName));
				textOut(String.Format("Reference level = {0} dB\r\n", calResult.refLevel));
				textOut(String.Format("High Cal Level = {0:F1} dB\r\n", calResult.high));
				textOut(String.Format("Tone Cal Level = {0:F1} dB\r\n", calResult.toneLev));
				
				pass();

			PostResult:
				
				if(Control.ModifierKeys == Keys.Shift) throw new Exception("Aborted due to shift key held down");
				
				if(failed == 1) return;
				
				if(!OiApiProxy.IsConfigured) return;
				
				if(OiApiProxy.Instance.DeviceExists(owner.Sudar.SerialNumber)) {
					MessageBox.Show("Warning - Device Exists", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}

				textOut("posting test results...");
				try {
					string ver = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
					OiApiProxy.Instance.PostTestResult(owner.Sudar.SerialNumber, owner.Sudar.ToString(), owner.Sudar.modelName,
					                                   ver, "Cal test", "Cal test", 0, failed, subResultList);
				}
				catch(Exception ex) {
					textOut("failed.\r\n");
					MessageBox.Show("Failed to post results: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
				textOut("done.\r\n");


				if(failed == 0) {
					textOut("posting cal results...");
					try {
						OiApiProxy.Instance.PostCalibration(owner.Sudar.SerialNumber, owner.Sudar.HwSerial, owner.Sudar.modelName,
						                                    Math.Round(calResult.low, 1),
						                                    Math.Round(calResult.high, 1),
						                                    Math.Round(calResult.refLevel, 1 ),
						                                    Math.Round(calResult.toneLev, 1),
						                                    1 );
						textOut("done.\r\n");
					}
					catch(Exception ex) {
						MessageBox.Show("Failed to post results: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
					}
				}
			}
			catch(Exception) {
				textOut("Failed - unexpected error\r\n");
				displayFailed();
			}
			finally {
				button1.Enabled = true;
				button1.Focus();
			}
			
		}

		
		void calibration() //cal type 0
		{
			bool useFFT = true;
			double x;
			float[] fft;
			float[] data;

			int failed = 0;
			float[] buf;
			List<OiApiProxy.TestSubResult> subResultList  = new List<OiApiProxy.TestSubResult>();
			OiApiProxy.TestSubResult str;
			CalResult  calResult = new CalResult();
			try {
				button1.Enabled = false;
				
				label1.Text = "Testing...";
				label1.BackColor = Color.Yellow;
				textBox1.Clear();
				Application.DoEvents();

				textBox1.ForeColor = Color.Black;
				textOut("Calibration begins\r\n");

				textOut("Enabling Audio...");
				if( !owner.Sudar.AudioEnable(true) ) {
					textOut("Failed\r\n");
					displayFailed();
					return;
				}
				
				textOut("Setting sample rate...");
				if( !owner.Sudar.AudioSetDivider(4) ) {
					textOut("Failed\r\n");
					displayFailed();
					return;
				}
				textOut(String.Format("Sample Rate = {0}\r\n", owner.Sudar.AudioSampleRate));
				textOut("Passed.\r\n");
				
				textOut("de-muting Audio...");
				if( !owner.Sudar.AudioSetMute(false) ) {
					textOut("Failed\r\n");
					displayFailed();
					return;
				}
				textOut("Passed.\r\n");

				textOut("Setting cal sig to 1kHz...");
				if( !owner.Sudar.AudioSetCalFrequency(1000) ) {
					textOut("Failed\r\n");
					displayFailed();
					return;
				}
				textOut("Passed.\r\n");

				textOut("disabling cal sig...");
				if( !owner.Sudar.AudioEnableCalSig(false) ) {
					textOut("Failed\r\n");
					displayFailed();
					return;
				}
				textOut("Passed.\r\n");

				textOut("Setting low gain...");
				if( !owner.Sudar.AudioSetGain(false) ) {
					textOut("Failed\r\n");
					displayFailed();
					return;
				}
				textOut("Passed.\r\n");

				calResult.refLevel =  Convert.ToDouble( textBox2.Text );

				textOut("Low gain calibration level test...\r\n");
				Thread.Sleep(200);
				
				owner.Sudar.AudioSample(4096); //flush;
				owner.Sudar.AudioSample(4096*10);
				Thread.Sleep(200);
				
				if(useFFT) {
					
					fft = sigProc.fft(4096, owner.Sudar.AudioBuf);
					data = sigProc.interpolate(fft,fft.Length/2, 10);
					sigProc.findPeak( data, (float)(250.0 / (owner.Sudar.AudioSampleRate / 2)), out x, out calResult.low);
				}
				else{
					buf = owner.Sudar.AudioBuf;
					sigProc.detrend(ref buf);
					calResult.low = Math.Log10( sigProc.rms(buf)) * 20;
				}
				
				textOut(String.Format("Low Cal Level = {0:F1} dB\r\n", calResult.low));

				textOut("Setting high gain...");
				if( !owner.Sudar.AudioSetGain(true) ) {
					textOut("Failed\r\n");
					displayFailed();
					return;
				}
				textOut("Passed.\r\n");

				textOut("High gain calibration level test...\r\n");
				Thread.Sleep(200);
				
				owner.Sudar.AudioSample(4096); //flush;
				owner.Sudar.AudioSample(4096*10);
				Thread.Sleep(200);
				
				if(useFFT) {
					fft = sigProc.fft(4096, owner.Sudar.AudioBuf);
					data = sigProc.interpolate(fft,fft.Length/2, 10);
					sigProc.findPeak( data, (float)(250.0 / (owner.Sudar.AudioSampleRate / 2)), out x, out calResult.high);
				}
				else{
					buf = owner.Sudar.AudioBuf;
					sigProc.detrend(ref buf);
					calResult.high = Math.Log10(sigProc.rms(buf))*20;
				}
				
				textOut(String.Format("High Cal Level = {0:F1} dB\r\n", calResult.high));

				textOut("enabling cal sig...");
				if( !owner.Sudar.AudioEnableCalSig(true) ) {
					textOut("Failed\r\n");
					displayFailed();
					return;
				}
				textOut("Passed.\r\n");

				
				textOut("De-muting Audio...");
				if( !owner.Sudar.AudioSetMute(true) ) {
					textOut("Failed\r\n");
					displayFailed();
					return;
				}
				textOut("Passed.\r\n");

				textOut("enabling cal sig...");
				if( !owner.Sudar.AudioEnableCalSig(true) ) {
					textOut("Failed\r\n");
					displayFailed();
					return;
				}
				textOut("Passed.\r\n");

				textOut("tone level test...\r\n");
				Thread.Sleep(200);

				owner.Sudar.AudioSample(4096); //flush;
				owner.Sudar.AudioSample(4096*10);
				Thread.Sleep(200);
				
				if(useFFT) {
					fft = sigProc.fft(4096, owner.Sudar.AudioBuf);
					data = sigProc.interpolate(fft,fft.Length/2, 10);
					sigProc.findPeak( data, (float)(1000f / (owner.Sudar.AudioSampleRate / 2)), out x, out calResult.toneLev);

				}
				else {
					buf = owner.Sudar.AudioBuf;
					sigProc.detrend(ref buf);
					calResult.toneLev = Math.Log10(sigProc.rms(buf))*20;
				}

				calResult.low =  Math.Round((-1.0 * calResult.low) + calResult.refLevel, 2);
				calResult.high = Math.Round((-1.0 *  calResult.high) + calResult.refLevel, 2);
				calResult.toneLev = Math.Round(calResult.high + calResult.toneLev, 2);
				

				str = new OiApiProxy.TestSubResult("high gain calibration", 0, 173.0, 181.0/*176.0*/, calResult.high);
				subResultList.Add(str);
				displaySubResult(str);
				if(str.testResult != 0) {
					displayFailed();
					failed = 1;
					goto PostResult;
				}
				
				if(owner.Sudar.model < Sudar.Model.ST500STD) {
					str = new OiApiProxy.TestSubResult("low gain calibration", 0, 185.0, 193.5 /*188.0*/, calResult.low);
					subResultList.Add(str);
					displaySubResult(str);
					if(str.testResult != 0) {
						displayFailed();
						failed = 1;
						goto PostResult;
					}
				}

				str = new OiApiProxy.TestSubResult("tone level", 0, 133.0, 140.0 /*136.0*/, calResult.toneLev);
				subResultList.Add(str);
				displaySubResult(str);
				if(str.testResult != 0) {
					displayFailed();
					failed = 1;
					goto PostResult;
				}
				
				textOut(String.Format("Serial = {0}\r\n", owner.Sudar.SerialNumber));
				textOut(String.Format("Model = {0}\r\n", owner.Sudar.modelName));
				textOut(String.Format("Reference level = {0} dB\r\n", calResult.refLevel));
				textOut(String.Format("Low Cal Level = {0:F1} dB\r\n", calResult.low));
				textOut(String.Format("High Cal Level = {0:F1} dB\r\n", calResult.high));
				textOut(String.Format("Tone Cal Level = {0:F1} dB\r\n", calResult.toneLev));
				
				pass();

			PostResult:
				
				if(Control.ModifierKeys == Keys.Shift) throw new Exception("Aborted due to shift key held down");
				
				if(failed == 1) return;
				
				if(!OiApiProxy.IsConfigured) return;
				
				if(OiApiProxy.Instance.DeviceExists(owner.Sudar.SerialNumber)) {
					MessageBox.Show("Warning - Device Exists", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}

				textOut("posting test results...");
				try {
					string ver = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
					OiApiProxy.Instance.PostTestResult(owner.Sudar.SerialNumber, owner.Sudar.HwSerial, owner.Sudar.modelName,
					                                   ver, "Cal test", "Cal test", 0, failed, subResultList);
				}
				catch(Exception ex) {
					textOut("failed.\r\n");
					MessageBox.Show("Failed to post results: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
				textOut("done.\r\n");


				if(failed == 0) {
					textOut("posting cal results...");
					try {
						OiApiProxy.Instance.PostCalibration(owner.Sudar.SerialNumber, owner.Sudar.HwSerial, owner.Sudar.modelName,
						                                    Math.Round(calResult.low, 1),
						                                    Math.Round(calResult.high, 1),
						                                    Math.Round(calResult.refLevel, 1 ),
						                                    Math.Round(calResult.toneLev, 1),
						                                    0 );
						textOut("done.\r\n");
					}
					catch(Exception ex) {
						MessageBox.Show("Failed to post results: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
					}
				}


			}
			catch(Exception) {
				textOut("Failed - unexpected error\r\n");
				displayFailed();
			}
			finally {
				button1.Enabled = true;
				button1.Focus();
			}
			
		}

		
		void Button1Click(object sender, EventArgs e)
		{
			if(checkBox1.Checked) {
				calibration();
			}
			else {
				
				RadioButton rb = betterGroupBox1.SelectedRadioButton;
				if(rb != null)
					RunTestSequence(rb.Text, Control.ModifierKeys == Keys.Shift);
			}
		}

		void ProdTestControlLoad(object sender, EventArgs e)
		{
			button1.Focus();
			LoadTests();
			foreach(KeyValuePair<string, TestSequence> ts in testSequences) {
				BetterRadioButton rb = new BetterRadioButton();
				rb.Text = ts.Key;
				rb.Dock = DockStyle.Top;
				betterGroupBox1.Controls.Add(rb);
				rb.Checked = true;
			}
		}
		
		void Button2Click(object sender, EventArgs e)
		{
			string path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
			path += Path.DirectorySeparatorChar + TEST_FILE_NAME;
			System.Diagnostics.Process.Start(path);
		}
		void CheckBox1CheckedChanged(object sender, EventArgs e)
		{
			
		}
	}
}
