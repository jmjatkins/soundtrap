﻿/*
 * Created by SharpDevelop.
 * User: jatk009
 * Date: 10/11/2015
 * Time: 10:50 a.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace SudarHost
{
	partial class BwDetectorControl
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the control.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.cbStoreSnip = new System.Windows.Forms.CheckBox();
			this.label7 = new System.Windows.Forms.Label();
			this.numericUpDown3 = new System.Windows.Forms.NumericUpDown();
			this.label8 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
			this.label6 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
			this.label4 = new System.Windows.Forms.Label();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.label9 = new System.Windows.Forms.Label();
			this.numericUpDown4 = new System.Windows.Forms.NumericUpDown();
			this.label10 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.numericUpDown5 = new System.Windows.Forms.NumericUpDown();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).BeginInit();
			this.SuspendLayout();
			// 
			// cbStoreSnip
			// 
			this.cbStoreSnip.Location = new System.Drawing.Point(-28, 83);
			this.cbStoreSnip.Name = "cbStoreSnip";
			this.cbStoreSnip.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
			this.cbStoreSnip.Size = new System.Drawing.Size(136, 18);
			this.cbStoreSnip.TabIndex = 31;
			this.cbStoreSnip.Text = "Store Snippets";
			this.cbStoreSnip.UseVisualStyleBackColor = true;
			this.cbStoreSnip.CheckedChanged += new System.EventHandler(this.CheckBox2CheckedChanged);
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(166, 46);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(88, 23);
			this.label7.TabIndex = 30;
			this.label7.Text = "Blanking time";
			// 
			// numericUpDown3
			// 
			this.numericUpDown3.Location = new System.Drawing.Point(254, 44);
			this.numericUpDown3.Maximum = new decimal(new int[] {
									65000,
									0,
									0,
									0});
			this.numericUpDown3.Minimum = new decimal(new int[] {
									1000,
									0,
									0,
									0});
			this.numericUpDown3.Name = "numericUpDown3";
			this.numericUpDown3.Size = new System.Drawing.Size(62, 20);
			this.numericUpDown3.TabIndex = 29;
			this.numericUpDown3.Value = new decimal(new int[] {
									1000,
									0,
									0,
									0});
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(335, 46);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(24, 23);
			this.label8.TabIndex = 28;
			this.label8.Text = "us";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(166, 16);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(88, 23);
			this.label5.TabIndex = 27;
			this.label5.Text = "Integration time";
			// 
			// numericUpDown2
			// 
			this.numericUpDown2.Location = new System.Drawing.Point(255, 14);
			this.numericUpDown2.Maximum = new decimal(new int[] {
									65000,
									0,
									0,
									0});
			this.numericUpDown2.Minimum = new decimal(new int[] {
									5,
									0,
									0,
									0});
			this.numericUpDown2.Name = "numericUpDown2";
			this.numericUpDown2.Size = new System.Drawing.Size(61, 20);
			this.numericUpDown2.TabIndex = 26;
			this.numericUpDown2.Value = new decimal(new int[] {
									10,
									0,
									0,
									0});
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(335, 15);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(24, 23);
			this.label6.TabIndex = 25;
			this.label6.Text = "us";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(13, 16);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(62, 23);
			this.label3.TabIndex = 22;
			this.label3.Text = "Threshold";
			// 
			// numericUpDown1
			// 
			this.numericUpDown1.Location = new System.Drawing.Point(76, 14);
			this.numericUpDown1.Maximum = new decimal(new int[] {
									65000,
									0,
									0,
									0});
			this.numericUpDown1.Minimum = new decimal(new int[] {
									10,
									0,
									0,
									0});
			this.numericUpDown1.Name = "numericUpDown1";
			this.numericUpDown1.Size = new System.Drawing.Size(42, 20);
			this.numericUpDown1.TabIndex = 23;
			this.numericUpDown1.Value = new decimal(new int[] {
									10,
									0,
									0,
									0});
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(125, 16);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(24, 23);
			this.label4.TabIndex = 24;
			this.label4.Text = "dB";
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(283, 156);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 35;
			this.button1.Text = "OK";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.Button1Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(202, 156);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(75, 23);
			this.button2.TabIndex = 36;
			this.button2.Text = "Cancel";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.Button2Click);
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(13, 156);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(104, 23);
			this.button3.TabIndex = 37;
			this.button3.Text = "Reset to defaults";
			this.button3.UseVisualStyleBackColor = true;
			this.button3.Click += new System.EventHandler(this.Button3Click);
			// 
			// label9
			// 
			this.label9.Location = new System.Drawing.Point(13, 112);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(82, 23);
			this.label9.TabIndex = 40;
			this.label9.Text = "Snippet pre trig";
			// 
			// numericUpDown4
			// 
			this.numericUpDown4.Enabled = false;
			this.numericUpDown4.Location = new System.Drawing.Point(99, 110);
			this.numericUpDown4.Maximum = new decimal(new int[] {
									750,
									0,
									0,
									0});
			this.numericUpDown4.Minimum = new decimal(new int[] {
									10,
									0,
									0,
									0});
			this.numericUpDown4.Name = "numericUpDown4";
			this.numericUpDown4.Size = new System.Drawing.Size(62, 20);
			this.numericUpDown4.TabIndex = 39;
			this.numericUpDown4.Value = new decimal(new int[] {
									750,
									0,
									0,
									0});
			// 
			// label10
			// 
			this.label10.Location = new System.Drawing.Point(334, 112);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(24, 23);
			this.label10.TabIndex = 38;
			this.label10.Text = "us";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(165, 112);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(88, 23);
			this.label1.TabIndex = 43;
			this.label1.Text = "Snippet post trig";
			// 
			// numericUpDown5
			// 
			this.numericUpDown5.Enabled = false;
			this.numericUpDown5.Location = new System.Drawing.Point(253, 110);
			this.numericUpDown5.Maximum = new decimal(new int[] {
									750,
									0,
									0,
									0});
			this.numericUpDown5.Minimum = new decimal(new int[] {
									10,
									0,
									0,
									0});
			this.numericUpDown5.Name = "numericUpDown5";
			this.numericUpDown5.Size = new System.Drawing.Size(62, 20);
			this.numericUpDown5.TabIndex = 42;
			this.numericUpDown5.Value = new decimal(new int[] {
									750,
									0,
									0,
									0});
			// 
			// BwDetectorControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.label1);
			this.Controls.Add(this.numericUpDown5);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.numericUpDown4);
			this.Controls.Add(this.label10);
			this.Controls.Add(this.button3);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.cbStoreSnip);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.numericUpDown3);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.numericUpDown2);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.numericUpDown1);
			this.Controls.Add(this.label4);
			this.Name = "BwDetectorControl";
			this.Size = new System.Drawing.Size(371, 187);
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).EndInit();
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.NumericUpDown numericUpDown5;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.NumericUpDown numericUpDown1;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.NumericUpDown numericUpDown2;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.NumericUpDown numericUpDown3;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.CheckBox cbStoreSnip;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.NumericUpDown numericUpDown4;
		private System.Windows.Forms.Label label9;
	}
}
