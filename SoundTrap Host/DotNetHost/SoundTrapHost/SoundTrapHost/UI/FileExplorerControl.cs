﻿/* 
 * 	SoundTrap Software v1.0
 *
 *	Copyright (C) 2011-2014, John Atkins and Mark Johnson
 *
 *	This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
 *
 *	This file is part of the SoundTrap software. SoundTrap is an acoustic recording
 *	system intended for underwater acoustic measurements. This component of the
 *	SoundTrap project is free software: you can redistribute it and/or modify it
 *	under the terms of the GNU General Public License as published by the Free Software
 *	Foundation, either version 3 of the License, or any later version.
 *
 *	The SoundTrap software is distributed in the hope that it will be useful, but
 *	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License along with this
 *	code. If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Collections.Generic;
using System.IO;

namespace SudarHost.UI
{
	/// <summary>
	/// Description of FileExplorerControl.
	/// </summary>
	public partial class FileExplorerControl : UserControl
	{
		
		string downloadPath;
		bool doDecompress;
		int downLoadErrorCount;
		int downLoadWarningCount;

		
		Queue<SudarFile> filesToDownload = new Queue<SudarFile>();
		SudFileExpander fileExpander = new SudFileExpander();

		public enum DownloadResult{OK, Cancelled, Failed, Warning};
		
		SudarControl owner;
		public SudarControl Owner {
			get { return owner; }
			set { owner = value; }
		}
		
		List<SudarFile> files = new List<SudarFile>();
		public FileExplorerControl()
		{
			InitializeComponent();
		}

		string BuildFolderSaveName()
		{
			string path = Properties.Settings.Default.FileSaveLocation + Path.DirectorySeparatorChar + owner.Sudar.SerialNumber;
			if(! Directory.Exists(path) ) Directory.CreateDirectory(path);
			return path;
		}
		
		string BuildFileSaveName(string toPath, string description)
		{
			return toPath == "" ?
				BuildFolderSaveName() + Path.DirectorySeparatorChar + description + ".sud"
				: toPath + Path.DirectorySeparatorChar + description + ".sud";
		}
		
		public void RefreashFileList(bool thorough)
		{
			try {
				Owner.ShowProgress(false, "Loading Directory...");
				files = SudarFile.GetFileList(Owner.Sudar, Owner.UpdateProgress,thorough);
				listView1.Items.Clear();
				foreach(SudarFile f in files){
					ListViewItem lvi = listView1.Items.Add(f.description);
					lvi.SubItems.Add(f.date.ToString("yyyy-MM-dd HH:mm:ss" ));
					UInt64 fileSize = (UInt64)f.blocks * f.blockLength;
					lvi.SubItems.Add(String.Format("{0} MB",  fileSize/0x0100000 ));
					bool saved = File.Exists( BuildFileSaveName("", f.description) );
					lvi.SubItems.Add( saved ? "Yes" : "No");
					lvi.ForeColor = saved ? Color.Black : Color.Red;
				}
			}
			catch(IOException) {
				listView1.Items.Clear();
				listView1.Items.Add("No Memory Card");
			}
			finally {
				Owner.HideProgress();
				listView1.Focus();
				if(listView1.Items.Count >0) {
					listView1.Items[ listView1.Items.Count-1 ].EnsureVisible();
				}
			}
		}

		public void RefreashFileStatus()
		{
			for(int i=0; i<files.Count; i++) {
				ListViewItem lvi = listView1.Items[i];
				bool saved = File.Exists( BuildFileSaveName("", files[i].description) );
				lvi.SubItems[3].Text =  saved ? "Yes" : "No";
				lvi.ForeColor = saved ? Color.Black : Color.Red;
			}
		}
		
		void ListView1ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
		{
			//button7.Enabled = (listView1.SelectedItems.Count > 0);
		}
		
		void Button13Click(object sender, EventArgs e)
		{
			RefreashFileList( Control.ModifierKeys == Keys.Shift);
		}
		
		void DeleteLastFile()
		{
			if( MessageBox.Show("Please confirm file deletion", "Delete Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) {
				try {
					Owner.ShowProgress(false, "Deleting File...");
					if(files.Count>0) {
						files[files.Count-1].Delete(Owner.Sudar);
						files.RemoveAt (files.Count-1);
						listView1.Items[ listView1.Items.Count-1 ].Remove();
					}
				}
				finally {
					Owner.HideProgress();
				}
			}
		}
		
		void Button7Click(object sender, EventArgs e)
		{
			downloadPath = "";
			DownLoadSelectedFiles();
		}
		
		void FileExplorerControlLoad(object sender, EventArgs e)
		{
			if(!this.DesignMode && (AppState.instance.mode !=  AppState.Mode.prodTest) ) {

				//if(owner.Sudar.model >= Sudar.Model.ST500STD) {
				owner.Sudar.sdSetCardNo(0);
				CBcardSelect.Visible = true;
				//}

				if(owner.Sudar.BatteryOkForFsRead) {
					if(Control.ModifierKeys != Keys.Shift) {
						RefreashFileList(false);
					}
				}
				else {
					listView1.Items.Clear();
					listView1.Items.Add("Battery low");
					MessageBox.Show("The device battery is very low. To avoid the possibility of an offload error, please wait about 10 minutes for the battery charge to reach 2%, then press 'Refresh'", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				checkBoxDecompress.Checked = Properties.Settings.Default.Decompress;
			}
			this.listView1.Select();
		}
		
		void Button1Click(object sender, EventArgs e)
		{
			if(MessageBox.Show("Are you sure you want to delete all files?", "Delete?", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
			{
				Owner.ShowProgress(true, "Deleting Files...");
				try {
					SudarFile.FormatFS(owner.Sudar, owner.UpdateProgress);
					listView1.Items.Clear();
					RefreashFileList(true);
					owner.Sudar.RefreshCardData((ushort)(CBcardSelect.SelectedIndex < 0 ? 0 : CBcardSelect.SelectedIndex ));
					
				}
				catch
				{
					MessageBox.Show("Failed to delete files.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				}
				finally {
					Owner.HideProgress();
				}
			}
		}
		
		void ListView1KeyPress(object sender, KeyPressEventArgs e)
		{
			e.Handled = true;
		}
		
		void ListView1KeyDown(object sender, KeyEventArgs e)
		{
			/*
			switch(e.KeyCode)
			{
				case Keys.Delete:
					if(listView1.SelectedItems.Count == 1 && listView1.SelectedItems[0] == listView1.Items[listView1.Items.Count - 1]  ) {
						DeleteLastFile();
						e.Handled = true;
					}
					break;
			}
			 */
		}
		
		SudarFile selectedFile;
		void ListView1MouseDown(object sender, MouseEventArgs e)
		{
			selectedFile = null;
			if (e.Button == MouseButtons.Right) //If it's the right button.
			{
				ListViewItem i = listView1.GetItemAt(e.X, e.Y);
				if(i!=null) {
					if(files.Count > 0) {
						selectedFile = files[listView1.Items.IndexOf(i)];
						contextMenuStrip1.Items.Clear();
						
						contextMenuStrip1.Items.Add("Download", null, DownloadToolStripMenuItemClick);
						contextMenuStrip1.Items.Add("Download To...", null, DownloadToToolStripMenuItemClick);
						
						string wavFileName = Path.ChangeExtension(BuildFileSaveName("", selectedFile.description), "wav" );
						string logFileName = Path.ChangeExtension(BuildFileSaveName("", selectedFile.description), ".log.xml" );
						
						if(File.Exists( wavFileName ) ) {
							contextMenuStrip1.Items.Add("Open Wav File", null, OpenFileToolStripMenuItemClick);
						}
						if(File.Exists( logFileName ) ) {
							contextMenuStrip1.Items.Add("Open Log File", null, OpenLogFileToolStripMenuItemClick);
						}
						
						contextMenuStrip1.Show(this.listView1, e.Location);
					}
				}
			}
		}
		
		void ListView1KeyUp(object sender, KeyEventArgs e)
		{
			e.Handled = true;
		}
		
		void ContextMenuStrip1Opening(object sender, CancelEventArgs e)
		{
			
		}
		
		void DownloadToolStripMenuItemClick(object sender, EventArgs e)
		{
			downloadPath = "";
			DownLoadSelectedFiles();
		}
		
		
		void Button2Click(object sender, EventArgs e)
		{
			System.Diagnostics.Process.Start("explorer.exe", "/root," + BuildFolderSaveName());
		}
		
		void DownloadToToolStripMenuItemClick(object sender, EventArgs e)
		{
			FolderBrowserDialog fbd = new FolderBrowserDialog();
			if(fbd.ShowDialog() == DialogResult.OK) {
				downloadPath = fbd.SelectedPath;
				DownLoadSelectedFiles();
			}
		}
		

		void OpenFileToolStripMenuItemClick(object sender, EventArgs e)
		{
			if(selectedFile !=null) {
				string filename = Path.ChangeExtension(BuildFileSaveName("", selectedFile.description), "wav" );
				if(File.Exists(filename)) {
					System.Diagnostics.Process.Start(filename);
				}
			}
		}
		
		void OpenLogFileToolStripMenuItemClick(object sender, EventArgs e)
		{
			if(selectedFile !=null) {
				string filename = Path.ChangeExtension(BuildFileSaveName("", selectedFile.description), "log.xml" );
				if(File.Exists(filename)) {
					System.Diagnostics.Process.Start(filename);
				}
			}
		}

		void DownLoadSelectedFiles()
		{
			Owner.ShowProgress(false, "Initialising download...");
			downLoadErrorCount = 0;
			downLoadWarningCount = 0;
			int inc = Control.ModifierKeys == Keys.Shift ? 10 : 1;
			for(int i=0; i<listView1.Items.Count; i+=inc) {
				if(listView1.Items[i].Selected) {
					filesToDownload.Enqueue( files[i]);
				}
			}
			doDecompress = checkBoxDecompress.Checked;
			RunDownloadMachine();
		}
		
		void RunDownloadMachine()
		{
			if( filesToDownload.Count > 0 ) {
				Owner.ShowProgress(true, String.Format("{0} Files remaining", filesToDownload.Count));
				backgroundWorker1.RunWorkerAsync(filesToDownload.Dequeue());
			}
			else {
				Owner.HideProgress();
				if(downLoadErrorCount > 0) {
					MessageBox.Show(string.Format("{0} error(s) during offload - please check file output", downLoadErrorCount), "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning );
				}
				
				if(downLoadWarningCount > 0) {
					MessageBox.Show(string.Format("{0} file(s) raised a warning during offload - please check log files for more information", downLoadWarningCount), "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning );
				}
				
				RefreashFileStatus();
			}
		}

		void BackgroundWorkerDoWork(object sender, DoWorkEventArgs e)
		{
			// This method runs on the background thread.
			
			DownloadResult downloadResult = DownloadResult.OK;
			SudarFile f = (SudarFile)e.Argument;

			string downLoadFileName = BuildFileSaveName(downloadPath, f.description);
			if(!Directory.Exists(Path.GetDirectoryName(downLoadFileName))) {
				Directory.CreateDirectory(Path.GetDirectoryName(downLoadFileName));
			}


			ProcessResult pr = f.Download(downLoadFileName, owner.Sudar, UpdateProgress);
			if(pr == ProcessResult.Error) downloadResult = DownloadResult.Failed;
			else if(pr == ProcessResult.Cancelled) downloadResult = DownloadResult.Cancelled;
			else if( pr == ProcessResult.Success) {
				if(doDecompress) {
					SudFileExpander.SudFileExpanderResult r = fileExpander.ProcessFile(downLoadFileName, UpdateProgress);
					if ( r == SudFileExpander.SudFileExpanderResult.Error) {
						downloadResult = DownloadResult.Failed;
					}
					if ( r == SudFileExpander.SudFileExpanderResult.Warning) {
						downloadResult = DownloadResult.Warning;
					}
					else if(r == SudFileExpander.SudFileExpanderResult.Cancelled) {
						downloadResult = DownloadResult.Cancelled;
					}
				}
			}
			e.Result = downloadResult;
		}
		
		public bool UpdateProgress(string s, int percentComplete) {
			// This method runs on the background thread.
			backgroundWorker1.ReportProgress(percentComplete, s);
			return backgroundWorker1.CancellationPending;
		}

		void BackgroundWorker1ProgressChanged(object sender, ProgressChangedEventArgs e)
		{
			// This method runs on the main thread.
			if (Owner.UpdateProgress(e.UserState  as string, e.ProgressPercentage))  {
				backgroundWorker1.CancelAsync();
			}
		}
		
		void BackgroundWorker1RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			// This method runs on the main thread.
			DownloadResult dr = (DownloadResult)e.Result;
			if (e.Cancelled || e.Error != null ) {
				filesToDownload.Clear();
			}

			if (dr == DownloadResult.Failed ) {
				downLoadErrorCount++;
			}

			if (dr == DownloadResult.Warning ) {
				downLoadWarningCount++;
			}
			
			if (dr == DownloadResult.Cancelled) {
				filesToDownload.Clear();
			}
			RunDownloadMachine();
		}
		
		void CheckBox1CheckedChanged(object sender, EventArgs e)
		{
			listView1.BeginUpdate();
			foreach (ListViewItem i in listView1.Items) {
				i.Selected = checkBox1.Checked;
			}
			listView1.EndUpdate();
		}
		
		void CheckBoxDecompressCheckedChanged(object sender, EventArgs e)
		{
			Properties.Settings.Default.Decompress = checkBoxDecompress.Checked;
			Properties.Settings.Default.Save();
		}
		
		void ListView1SelectedIndexChanged(object sender, EventArgs e)
		{
			
		}
		void ComboBox1SelectedIndexChanged(object sender, EventArgs e)
		{
			//owner.Sudar.RefreshCardData((ushort)CBcardSelect.SelectedIndex);
			owner.Sudar.sdSetCardNo((ushort)CBcardSelect.SelectedIndex);
			RefreashFileList( false);
		}
	}
}