﻿/*
 * Created by SharpDevelop.
 * User: John
 * Date: 27/07/2018
 * Time: 2:40 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;

namespace SudarHost.UI
{
	/// <summary>
	/// Description of GetParamForm.
	/// </summary>
	public partial class GetParamForm : Form
	{
		public string returnValue {
			get {
				return textBox1.Text;
			}
		}
		
		public GetParamForm()
		{
			InitializeComponent();
		}
	
		void Button2Click(object sender, EventArgs e)
		{
			this.DialogResult = DialogResult.OK;
			this.Close();
		}
		
		void Button1Click(object sender, EventArgs e)
		{
			this.DialogResult = DialogResult.Cancel;
			this.Close();
		}
		
		void GetParamFormKeyPress(object sender, KeyPressEventArgs e)
		{
	
		}
	}
}
