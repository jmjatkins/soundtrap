﻿/* 
* 	SoundTrap Software v1.0
*  
*	Copyright (C) 2011-2014, John Atkins and Mark Johnson
*
*	This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
*
*	This file is part of the SoundTrap software. SoundTrap is an acoustic recording
*	system intended for underwater acoustic measurements. This component of the 
*	SoundTrap project is free software: you can redistribute it and/or modify it 
*	under the terms of the GNU General Public License as published by the Free Software 
*	Foundation, either version 3 of the License, or any later version.
*
*	The SoundTrap software is distributed in the hope that it will be useful, but 
*	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
*	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License along with this 
*	code. If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SudarHost
{
	/// <summary>
	/// Description of SudarControl.
	/// </summary>
	public partial class InstrumentSelectControl : UserControl
	{
		public InstrumentSelectControl()
		{
			InitializeComponent();
		}
		
		Sudar sudar;
		public Sudar Sudar {
			get { return sudar; }
			set {
				sudar = value;
				button1.Text = sudar.InstrumentDescription;
			}
		}
				
		void Button1Click(object sender, EventArgs e)
		{
			OnSelected(EventArgs.Empty);
		}
		
		public event EventHandler Selected;
		protected virtual void OnSelected(EventArgs e)
		{
			if (Selected != null) {
				Selected(this, e);
			}
		}
		
		void Label2Click(object sender, EventArgs e)
		{
			OnSelected(EventArgs.Empty);
		}
		
		public void MarkAsSelected(bool selected) {
			button1.BackColor = selected ? Color.LightSteelBlue : Color.GhostWhite;
			Application.DoEvents();
		}
		
		void Button1Leave(object sender, EventArgs e)
		{
			//button1.BackColor = SystemColors.Window;
		}
		
		void InstrumentSelectControlLoad(object sender, EventArgs e)
		{
			if(AppState.instance.mode == AppState.Mode.prodTest) {
				this.Focus();
			}
		}
	}
}
