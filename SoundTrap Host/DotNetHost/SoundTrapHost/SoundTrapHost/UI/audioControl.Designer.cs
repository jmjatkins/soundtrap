﻿/*
 * Created by SharpDevelop.
 * User: jatk009
 * Date: 23/05/2014
 * Time: 4:47 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace SudarHost.UI
{
	partial class audioForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the control.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(audioForm));
			this.button3 = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.button6 = new System.Windows.Forms.Button();
			this.button5 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.button4 = new System.Windows.Forms.Button();
			this.comboBox3 = new System.Windows.Forms.ComboBox();
			this.checkBox7 = new System.Windows.Forms.CheckBox();
			this.checkBox6 = new System.Windows.Forms.CheckBox();
			this.checkBox5 = new System.Windows.Forms.CheckBox();
			this.comboBox2 = new System.Windows.Forms.ComboBox();
			this.checkBox4 = new System.Windows.Forms.CheckBox();
			this.button1 = new System.Windows.Forms.Button();
			this.comboBox1 = new System.Windows.Forms.ComboBox();
			this.betterGroupBox1 = new SudarHost.BetterGroupBox();
			this.betterRadioButton5 = new SudarHost.BetterRadioButton();
			this.betterRadioButton4 = new SudarHost.BetterRadioButton();
			this.betterRadioButton3 = new SudarHost.BetterRadioButton();
			this.betterRadioButton2 = new SudarHost.BetterRadioButton();
			this.betterRadioButton1 = new SudarHost.BetterRadioButton();
			this.checkBox3 = new System.Windows.Forms.CheckBox();
			this.checkBox1 = new System.Windows.Forms.CheckBox();
			this.checkBox2 = new System.Windows.Forms.CheckBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.radioButton2 = new System.Windows.Forms.RadioButton();
			this.radioButton1 = new System.Windows.Forms.RadioButton();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.label1 = new System.Windows.Forms.Label();
			this.plot1 = new OxyPlot.WindowsForms.Plot();
			this.groupBox1.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.betterGroupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(10, 193);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(75, 23);
			this.button3.TabIndex = 4;
			this.button3.Text = "Sample";
			this.button3.UseVisualStyleBackColor = true;
			this.button3.Click += new System.EventHandler(this.Button3Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.groupBox3);
			this.groupBox1.Controls.Add(this.comboBox3);
			this.groupBox1.Controls.Add(this.checkBox7);
			this.groupBox1.Controls.Add(this.checkBox6);
			this.groupBox1.Controls.Add(this.checkBox5);
			this.groupBox1.Controls.Add(this.comboBox2);
			this.groupBox1.Controls.Add(this.checkBox4);
			this.groupBox1.Controls.Add(this.button1);
			this.groupBox1.Controls.Add(this.comboBox1);
			this.groupBox1.Controls.Add(this.betterGroupBox1);
			this.groupBox1.Controls.Add(this.checkBox3);
			this.groupBox1.Controls.Add(this.button3);
			this.groupBox1.Controls.Add(this.checkBox1);
			this.groupBox1.Controls.Add(this.checkBox2);
			this.groupBox1.Controls.Add(this.groupBox2);
			this.groupBox1.Dock = System.Windows.Forms.DockStyle.Right;
			this.groupBox1.Location = new System.Drawing.Point(502, 0);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(205, 582);
			this.groupBox1.TabIndex = 24;
			this.groupBox1.TabStop = false;
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.button6);
			this.groupBox3.Controls.Add(this.button5);
			this.groupBox3.Controls.Add(this.button2);
			this.groupBox3.Controls.Add(this.button4);
			this.groupBox3.Location = new System.Drawing.Point(22, 470);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(171, 100);
			this.groupBox3.TabIndex = 35;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "REF";
			// 
			// button6
			// 
			this.button6.Location = new System.Drawing.Point(6, 50);
			this.button6.Name = "button6";
			this.button6.Size = new System.Drawing.Size(75, 23);
			this.button6.TabIndex = 36;
			this.button6.Text = "Clear";
			this.button6.UseVisualStyleBackColor = true;
			this.button6.Click += new System.EventHandler(this.Button6Click);
			// 
			// button5
			// 
			this.button5.Location = new System.Drawing.Point(87, 50);
			this.button5.Name = "button5";
			this.button5.Size = new System.Drawing.Size(75, 23);
			this.button5.TabIndex = 35;
			this.button5.Text = "Open";
			this.button5.UseVisualStyleBackColor = true;
			this.button5.Click += new System.EventHandler(this.Button5Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(6, 21);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(75, 23);
			this.button2.TabIndex = 33;
			this.button2.Text = "Capture ";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.Button2Click);
			// 
			// button4
			// 
			this.button4.Location = new System.Drawing.Point(87, 21);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(75, 23);
			this.button4.TabIndex = 34;
			this.button4.Text = "Save";
			this.button4.UseVisualStyleBackColor = true;
			this.button4.Click += new System.EventHandler(this.Button4Click);
			// 
			// comboBox3
			// 
			this.comboBox3.FormattingEnabled = true;
			this.comboBox3.Items.AddRange(new object[] {
			"Chan 0",
			"Chan 1",
			"Chan 2",
			"Chan 3"});
			this.comboBox3.Location = new System.Drawing.Point(111, 121);
			this.comboBox3.Name = "comboBox3";
			this.comboBox3.Size = new System.Drawing.Size(80, 24);
			this.comboBox3.TabIndex = 32;
			this.comboBox3.Text = "Chan 0";
			this.comboBox3.SelectedIndexChanged += new System.EventHandler(this.ComboBox3SelectedIndexChanged);
			// 
			// checkBox7
			// 
			this.checkBox7.Location = new System.Drawing.Point(10, 142);
			this.checkBox7.Name = "checkBox7";
			this.checkBox7.Size = new System.Drawing.Size(104, 24);
			this.checkBox7.TabIndex = 31;
			this.checkBox7.Text = "high pass";
			this.checkBox7.UseVisualStyleBackColor = true;
			this.checkBox7.CheckedChanged += new System.EventHandler(this.CheckBox7CheckedChanged);
			// 
			// checkBox6
			// 
			this.checkBox6.Location = new System.Drawing.Point(113, 214);
			this.checkBox6.Name = "checkBox6";
			this.checkBox6.Size = new System.Drawing.Size(104, 19);
			this.checkBox6.TabIndex = 30;
			this.checkBox6.Text = "oneShot";
			this.checkBox6.UseVisualStyleBackColor = true;
			this.checkBox6.CheckedChanged += new System.EventHandler(this.CheckBox6CheckedChanged);
			// 
			// checkBox5
			// 
			this.checkBox5.Location = new System.Drawing.Point(111, 98);
			this.checkBox5.Name = "checkBox5";
			this.checkBox5.Size = new System.Drawing.Size(104, 24);
			this.checkBox5.TabIndex = 29;
			this.checkBox5.Text = "Show Points";
			this.checkBox5.UseVisualStyleBackColor = true;
			// 
			// comboBox2
			// 
			this.comboBox2.FormattingEnabled = true;
			this.comboBox2.Items.AddRange(new object[] {
			"512",
			"1024",
			"2048",
			"4096",
			"8192"});
			this.comboBox2.Location = new System.Drawing.Point(113, 147);
			this.comboBox2.Name = "comboBox2";
			this.comboBox2.Size = new System.Drawing.Size(72, 24);
			this.comboBox2.TabIndex = 28;
			this.comboBox2.Text = "4096";
			this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.ComboBox2SelectedIndexChanged);
			// 
			// checkBox4
			// 
			this.checkBox4.Location = new System.Drawing.Point(113, 193);
			this.checkBox4.Name = "checkBox4";
			this.checkBox4.Size = new System.Drawing.Size(72, 24);
			this.checkBox4.TabIndex = 27;
			this.checkBox4.Text = "live";
			this.checkBox4.UseVisualStyleBackColor = true;
			this.checkBox4.CheckedChanged += new System.EventHandler(this.CheckBox4CheckedChanged);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(22, 434);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(156, 23);
			this.button1.TabIndex = 26;
			this.button1.Text = "plot f repsonse";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.Button1Click);
			// 
			// comboBox1
			// 
			this.comboBox1.FormattingEnabled = true;
			this.comboBox1.Items.AddRange(new object[] {
			"0",
			"250",
			"500",
			"750",
			"1000",
			"2000",
			"4000",
			"6000",
			"8000",
			"10000",
			"12000",
			"14000",
			"16000",
			"20000",
			"24000",
			"28000",
			"32000",
			"60000",
			"64000",
			"96000",
			"128000",
			"150000",
			"160000",
			"170000"});
			this.comboBox1.Location = new System.Drawing.Point(111, 173);
			this.comboBox1.Name = "comboBox1";
			this.comboBox1.Size = new System.Drawing.Size(74, 24);
			this.comboBox1.TabIndex = 25;
			this.comboBox1.Text = "1000";
			this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.ComboBox1SelectedIndexChanged);
			// 
			// betterGroupBox1
			// 
			this.betterGroupBox1.Controls.Add(this.betterRadioButton5);
			this.betterGroupBox1.Controls.Add(this.betterRadioButton4);
			this.betterGroupBox1.Controls.Add(this.betterRadioButton3);
			this.betterGroupBox1.Controls.Add(this.betterRadioButton2);
			this.betterGroupBox1.Controls.Add(this.betterRadioButton1);
			this.betterGroupBox1.Location = new System.Drawing.Point(22, 239);
			this.betterGroupBox1.Name = "betterGroupBox1";
			this.betterGroupBox1.SelectedRadioButtonIndex = ((uint)(1u));
			this.betterGroupBox1.Size = new System.Drawing.Size(156, 189);
			this.betterGroupBox1.TabIndex = 24;
			this.betterGroupBox1.TabStop = false;
			this.betterGroupBox1.Text = "Sample rate";
			// 
			// betterRadioButton5
			// 
			this.betterRadioButton5.Index = ((uint)(5u));
			this.betterRadioButton5.Location = new System.Drawing.Point(15, 151);
			this.betterRadioButton5.Name = "betterRadioButton5";
			this.betterRadioButton5.Size = new System.Drawing.Size(104, 24);
			this.betterRadioButton5.TabIndex = 4;
			this.betterRadioButton5.Text = "div128";
			this.betterRadioButton5.UseVisualStyleBackColor = true;
			this.betterRadioButton5.Click += new System.EventHandler(this.BetterRadioButton1Click);
			// 
			// betterRadioButton4
			// 
			this.betterRadioButton4.Index = ((uint)(4u));
			this.betterRadioButton4.Location = new System.Drawing.Point(15, 121);
			this.betterRadioButton4.Name = "betterRadioButton4";
			this.betterRadioButton4.Size = new System.Drawing.Size(104, 24);
			this.betterRadioButton4.TabIndex = 3;
			this.betterRadioButton4.Text = "div64";
			this.betterRadioButton4.UseVisualStyleBackColor = true;
			this.betterRadioButton4.Click += new System.EventHandler(this.BetterRadioButton1Click);
			// 
			// betterRadioButton3
			// 
			this.betterRadioButton3.Index = ((uint)(3u));
			this.betterRadioButton3.Location = new System.Drawing.Point(15, 91);
			this.betterRadioButton3.Name = "betterRadioButton3";
			this.betterRadioButton3.Size = new System.Drawing.Size(104, 24);
			this.betterRadioButton3.TabIndex = 2;
			this.betterRadioButton3.Text = "div16";
			this.betterRadioButton3.UseVisualStyleBackColor = true;
			this.betterRadioButton3.Click += new System.EventHandler(this.BetterRadioButton1Click);
			// 
			// betterRadioButton2
			// 
			this.betterRadioButton2.Index = ((uint)(2u));
			this.betterRadioButton2.Location = new System.Drawing.Point(15, 61);
			this.betterRadioButton2.Name = "betterRadioButton2";
			this.betterRadioButton2.Size = new System.Drawing.Size(104, 24);
			this.betterRadioButton2.TabIndex = 1;
			this.betterRadioButton2.Text = "div8";
			this.betterRadioButton2.UseVisualStyleBackColor = true;
			this.betterRadioButton2.CheckedChanged += new System.EventHandler(this.BetterRadioButton2CheckedChanged);
			this.betterRadioButton2.Click += new System.EventHandler(this.BetterRadioButton1Click);
			// 
			// betterRadioButton1
			// 
			this.betterRadioButton1.Checked = true;
			this.betterRadioButton1.Index = ((uint)(1u));
			this.betterRadioButton1.Location = new System.Drawing.Point(15, 31);
			this.betterRadioButton1.Name = "betterRadioButton1";
			this.betterRadioButton1.Size = new System.Drawing.Size(104, 24);
			this.betterRadioButton1.TabIndex = 0;
			this.betterRadioButton1.TabStop = true;
			this.betterRadioButton1.Text = "div4";
			this.betterRadioButton1.UseVisualStyleBackColor = true;
			this.betterRadioButton1.CheckedChanged += new System.EventHandler(this.BetterRadioButton1CheckedChanged);
			this.betterRadioButton1.Click += new System.EventHandler(this.BetterRadioButton1Click);
			// 
			// checkBox3
			// 
			this.checkBox3.Location = new System.Drawing.Point(10, 163);
			this.checkBox3.Name = "checkBox3";
			this.checkBox3.Size = new System.Drawing.Size(118, 24);
			this.checkBox3.TabIndex = 22;
			this.checkBox3.Text = "enable cal sig";
			this.checkBox3.UseVisualStyleBackColor = true;
			this.checkBox3.CheckedChanged += new System.EventHandler(this.CheckBox3CheckedChanged);
			// 
			// checkBox1
			// 
			this.checkBox1.Location = new System.Drawing.Point(10, 121);
			this.checkBox1.Name = "checkBox1";
			this.checkBox1.Size = new System.Drawing.Size(104, 24);
			this.checkBox1.TabIndex = 20;
			this.checkBox1.Text = "high gain";
			this.checkBox1.UseVisualStyleBackColor = true;
			this.checkBox1.CheckedChanged += new System.EventHandler(this.CheckBox1CheckedChanged);
			// 
			// checkBox2
			// 
			this.checkBox2.Location = new System.Drawing.Point(10, 100);
			this.checkBox2.Name = "checkBox2";
			this.checkBox2.Size = new System.Drawing.Size(104, 24);
			this.checkBox2.TabIndex = 21;
			this.checkBox2.Text = "mute";
			this.checkBox2.UseVisualStyleBackColor = true;
			this.checkBox2.CheckedChanged += new System.EventHandler(this.CheckBox2CheckedChanged);
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.radioButton2);
			this.groupBox2.Controls.Add(this.radioButton1);
			this.groupBox2.Location = new System.Drawing.Point(10, 19);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(181, 75);
			this.groupBox2.TabIndex = 23;
			this.groupBox2.TabStop = false;
			// 
			// radioButton2
			// 
			this.radioButton2.Location = new System.Drawing.Point(12, 49);
			this.radioButton2.Name = "radioButton2";
			this.radioButton2.Size = new System.Drawing.Size(86, 24);
			this.radioButton2.TabIndex = 1;
			this.radioButton2.Text = "Wav";
			this.radioButton2.UseVisualStyleBackColor = true;
			this.radioButton2.CheckedChanged += new System.EventHandler(this.RadioButton2CheckedChanged);
			// 
			// radioButton1
			// 
			this.radioButton1.Checked = true;
			this.radioButton1.Location = new System.Drawing.Point(12, 19);
			this.radioButton1.Name = "radioButton1";
			this.radioButton1.Size = new System.Drawing.Size(71, 24);
			this.radioButton1.TabIndex = 0;
			this.radioButton1.TabStop = true;
			this.radioButton1.Text = "Spec";
			this.radioButton1.UseVisualStyleBackColor = true;
			this.radioButton1.CheckedChanged += new System.EventHandler(this.RadioButton1CheckedChanged);
			// 
			// timer1
			// 
			this.timer1.Tick += new System.EventHandler(this.Timer1Tick);
			// 
			// label1
			// 
			this.label1.Dock = System.Windows.Forms.DockStyle.Top;
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(502, 23);
			this.label1.TabIndex = 34;
			// 
			// plot1
			// 
			this.plot1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.plot1.KeyboardPanHorizontalStep = 0.1D;
			this.plot1.KeyboardPanVerticalStep = 0.1D;
			this.plot1.Location = new System.Drawing.Point(0, 23);
			this.plot1.Name = "plot1";
			this.plot1.PanCursor = System.Windows.Forms.Cursors.Hand;
			this.plot1.Size = new System.Drawing.Size(502, 559);
			this.plot1.TabIndex = 35;
			this.plot1.Text = "plot1";
			this.plot1.ZoomHorizontalCursor = System.Windows.Forms.Cursors.SizeWE;
			this.plot1.ZoomRectangleCursor = System.Windows.Forms.Cursors.SizeNWSE;
			this.plot1.ZoomVerticalCursor = System.Windows.Forms.Cursors.SizeNS;
			// 
			// audioForm
			// 
			this.ClientSize = new System.Drawing.Size(707, 582);
			this.Controls.Add(this.plot1);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.groupBox1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "audioForm";
			this.groupBox1.ResumeLayout(false);
			this.groupBox3.ResumeLayout(false);
			this.betterGroupBox1.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		private System.Windows.Forms.ComboBox comboBox3;
		private System.Windows.Forms.CheckBox checkBox7;
		private System.Windows.Forms.CheckBox checkBox6;
		private System.Windows.Forms.CheckBox checkBox5;
		private System.Windows.Forms.ComboBox comboBox2;
		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.CheckBox checkBox4;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.ComboBox comboBox1;
		private SudarHost.BetterRadioButton betterRadioButton4;
		private SudarHost.BetterRadioButton betterRadioButton5;
		private SudarHost.BetterRadioButton betterRadioButton1;
		private SudarHost.BetterRadioButton betterRadioButton2;
		private SudarHost.BetterRadioButton betterRadioButton3;
		private SudarHost.BetterGroupBox betterGroupBox1;
		private System.Windows.Forms.RadioButton radioButton1;
		private System.Windows.Forms.RadioButton radioButton2;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.CheckBox checkBox2;
		private System.Windows.Forms.CheckBox checkBox1;
		private System.Windows.Forms.CheckBox checkBox3;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Label label1;
		private OxyPlot.WindowsForms.Plot plot1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Button button5;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.Button button6;
	}
}
