﻿/*
 * Created by SharpDevelop.
 * User: jatk009
 * Date: 10/11/2015
 * Time: 10:50 a.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;

namespace SudarHost
{
	/// <summary>
	/// Description of BwDetectorControl.
	/// </summary>
	public partial class BwDetectorControl : UserControl
	{
		public BwDetectorControl()
		{
			InitializeComponent();
		}
		
		void SetDefaults()
		{
			numericUpDown1.Value = 14; //thres 14 dB
			numericUpDown2.Value = 200; //intTime 200 us
			numericUpDown3.Value = 5000;//blnkTime 5 ms  
			numericUpDown4.Value = 750; //pre-trig 0.75 ms
			numericUpDown5.Value = 750; //post-trig 0.75 ms
			cbStoreSnip.Checked = true;
		}
		
		public void Set(SudarConfig sc) {
			try {
				numericUpDown1.Value = sc.DetectParam2;
				numericUpDown2.Value = sc.DetectParam3;
				numericUpDown3.Value = sc.DetectParam4;
				numericUpDown4.Value = sc.DetectParam5;
				numericUpDown5.Value = sc.DetectParam6;
				cbStoreSnip.Checked = sc.DetectParam7 > 0;
			}
			catch
			{
				
			}
		}

		public void Get(ref SudarConfig sc) {
			sc.DetectParam2 = (uint)numericUpDown1.Value; //thres 
			sc.DetectParam3 = (uint)numericUpDown2.Value; //intTime 
			sc.DetectParam4 = (uint)numericUpDown3.Value; //blnkTime 
			sc.DetectParam5 = (uint)numericUpDown4.Value; //pretrig 
			sc.DetectParam6 = (uint)numericUpDown5.Value; //posttrig 
			sc.DetectParam7 = cbStoreSnip.Checked ? (uint)1 : (uint)0;
			
		}
		
		void Button1Click(object sender, EventArgs e)
		{
			ParentForm.DialogResult = DialogResult.OK;
			ParentForm.Close();
		}
		
		void Button3Click(object sender, EventArgs e)
		{
			SetDefaults();
		}
		
		void Button2Click(object sender, EventArgs e)
		{
			ParentForm.DialogResult = DialogResult.Cancel;
			ParentForm.Close();
		}
		
		void CheckBox2CheckedChanged(object sender, EventArgs e)
		{
			numericUpDown4.Enabled = cbStoreSnip.Checked;
			numericUpDown5.Enabled = cbStoreSnip.Checked;
		}
	}
}
