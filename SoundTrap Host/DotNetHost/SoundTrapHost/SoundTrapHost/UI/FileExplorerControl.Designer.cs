﻿/*
 * Created by SharpDevelop.
 * User: jatk009
 * Date: 8/10/2012
 * Time: 11:56 a.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace SudarHost.UI
{
	partial class FileExplorerControl
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the control.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.panel3 = new System.Windows.Forms.Panel();
			this.button13 = new System.Windows.Forms.Button();
			this.label17 = new System.Windows.Forms.Label();
			this.button7 = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.CBcardSelect = new System.Windows.Forms.ComboBox();
			this.checkBoxDecompress = new System.Windows.Forms.CheckBox();
			this.button2 = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.panel2 = new System.Windows.Forms.Panel();
			this.checkBox1 = new System.Windows.Forms.CheckBox();
			this.listView1 = new System.Windows.Forms.ListView();
			this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
			this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.downloadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.downloadToToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
			this.panel3.SuspendLayout();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.contextMenuStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.button13);
			this.panel3.Controls.Add(this.label17);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel3.Location = new System.Drawing.Point(0, 0);
			this.panel3.Margin = new System.Windows.Forms.Padding(4);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(997, 64);
			this.panel3.TabIndex = 3;
			// 
			// button13
			// 
			this.button13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.button13.Location = new System.Drawing.Point(713, 21);
			this.button13.Margin = new System.Windows.Forms.Padding(4);
			this.button13.Name = "button13";
			this.button13.Size = new System.Drawing.Size(95, 28);
			this.button13.TabIndex = 7;
			this.button13.Text = "Refresh";
			this.button13.UseVisualStyleBackColor = true;
			this.button13.Click += new System.EventHandler(this.Button13Click);
			// 
			// label17
			// 
			this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label17.Location = new System.Drawing.Point(4, 21);
			this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(268, 28);
			this.label17.TabIndex = 0;
			this.label17.Text = "File Listing:";
			// 
			// button7
			// 
			this.button7.Location = new System.Drawing.Point(8, 7);
			this.button7.Margin = new System.Windows.Forms.Padding(4);
			this.button7.Name = "button7";
			this.button7.Size = new System.Drawing.Size(139, 28);
			this.button7.TabIndex = 5;
			this.button7.Text = "Download";
			this.button7.UseVisualStyleBackColor = true;
			this.button7.Click += new System.EventHandler(this.Button7Click);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.CBcardSelect);
			this.panel1.Controls.Add(this.checkBoxDecompress);
			this.panel1.Controls.Add(this.button2);
			this.panel1.Controls.Add(this.button1);
			this.panel1.Controls.Add(this.button7);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
			this.panel1.Location = new System.Drawing.Point(824, 64);
			this.panel1.Margin = new System.Windows.Forms.Padding(4);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(173, 656);
			this.panel1.TabIndex = 9;
			// 
			// CBcardSelect
			// 
			this.CBcardSelect.FormattingEnabled = true;
			this.CBcardSelect.Items.AddRange(new object[] {
			"SD Card 0 ",
			"SD Card 1",
			"SD Card 2",
			"SD Card 3"});
			this.CBcardSelect.Location = new System.Drawing.Point(8, 151);
			this.CBcardSelect.Name = "CBcardSelect";
			this.CBcardSelect.Size = new System.Drawing.Size(139, 24);
			this.CBcardSelect.TabIndex = 9;
			this.CBcardSelect.Text = "SD Card 0";
			this.CBcardSelect.Visible = false;
			this.CBcardSelect.SelectedIndexChanged += new System.EventHandler(this.ComboBox1SelectedIndexChanged);
			// 
			// checkBoxDecompress
			// 
			this.checkBoxDecompress.Checked = true;
			this.checkBoxDecompress.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxDecompress.Location = new System.Drawing.Point(27, 114);
			this.checkBoxDecompress.Margin = new System.Windows.Forms.Padding(4);
			this.checkBoxDecompress.Name = "checkBoxDecompress";
			this.checkBoxDecompress.Size = new System.Drawing.Size(120, 30);
			this.checkBoxDecompress.TabIndex = 8;
			this.checkBoxDecompress.Text = "Decompress";
			this.checkBoxDecompress.UseVisualStyleBackColor = true;
			this.checkBoxDecompress.CheckedChanged += new System.EventHandler(this.CheckBoxDecompressCheckedChanged);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(8, 79);
			this.button2.Margin = new System.Windows.Forms.Padding(4);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(139, 28);
			this.button2.TabIndex = 7;
			this.button2.Text = "Open Save Folder";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.Button2Click);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(8, 43);
			this.button1.Margin = new System.Windows.Forms.Padding(4);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(139, 28);
			this.button1.TabIndex = 6;
			this.button1.Text = "Delete All";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.Button1Click);
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.checkBox1);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel2.Location = new System.Drawing.Point(0, 639);
			this.panel2.Margin = new System.Windows.Forms.Padding(4);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(824, 81);
			this.panel2.TabIndex = 11;
			// 
			// checkBox1
			// 
			this.checkBox1.Location = new System.Drawing.Point(4, 7);
			this.checkBox1.Margin = new System.Windows.Forms.Padding(4);
			this.checkBox1.Name = "checkBox1";
			this.checkBox1.Size = new System.Drawing.Size(171, 30);
			this.checkBox1.TabIndex = 0;
			this.checkBox1.Text = "Select/Deselect All";
			this.checkBox1.UseVisualStyleBackColor = true;
			this.checkBox1.CheckedChanged += new System.EventHandler(this.CheckBox1CheckedChanged);
			// 
			// listView1
			// 
			this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
			this.columnHeader1,
			this.columnHeader2,
			this.columnHeader3,
			this.columnHeader4});
			this.listView1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.listView1.FullRowSelect = true;
			this.listView1.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
			this.listView1.HideSelection = false;
			this.listView1.Location = new System.Drawing.Point(0, 64);
			this.listView1.Margin = new System.Windows.Forms.Padding(4);
			this.listView1.Name = "listView1";
			this.listView1.Size = new System.Drawing.Size(824, 575);
			this.listView1.TabIndex = 12;
			this.listView1.UseCompatibleStateImageBehavior = false;
			this.listView1.View = System.Windows.Forms.View.Details;
			this.listView1.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.ListView1ItemSelectionChanged);
			this.listView1.SelectedIndexChanged += new System.EventHandler(this.ListView1SelectedIndexChanged);
			this.listView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ListView1KeyDown);
			this.listView1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ListView1KeyPress);
			this.listView1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ListView1KeyUp);
			this.listView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ListView1MouseDown);
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "Name";
			this.columnHeader1.Width = 152;
			// 
			// columnHeader2
			// 
			this.columnHeader2.Text = "Time";
			this.columnHeader2.Width = 146;
			// 
			// columnHeader3
			// 
			this.columnHeader3.Text = "Size";
			this.columnHeader3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// columnHeader4
			// 
			this.columnHeader4.Text = "Saved";
			this.columnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.columnHeader4.Width = 70;
			// 
			// contextMenuStrip1
			// 
			this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
			this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.downloadToolStripMenuItem,
			this.downloadToToolStripMenuItem});
			this.contextMenuStrip1.Name = "contextMenuStrip1";
			this.contextMenuStrip1.Size = new System.Drawing.Size(177, 52);
			this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.ContextMenuStrip1Opening);
			// 
			// downloadToolStripMenuItem
			// 
			this.downloadToolStripMenuItem.Name = "downloadToolStripMenuItem";
			this.downloadToolStripMenuItem.Size = new System.Drawing.Size(176, 24);
			this.downloadToolStripMenuItem.Text = "Download";
			this.downloadToolStripMenuItem.Click += new System.EventHandler(this.DownloadToolStripMenuItemClick);
			// 
			// downloadToToolStripMenuItem
			// 
			this.downloadToToolStripMenuItem.Name = "downloadToToolStripMenuItem";
			this.downloadToToolStripMenuItem.Size = new System.Drawing.Size(176, 24);
			this.downloadToToolStripMenuItem.Text = "Download To...";
			this.downloadToToolStripMenuItem.Click += new System.EventHandler(this.DownloadToToolStripMenuItemClick);
			// 
			// backgroundWorker1
			// 
			this.backgroundWorker1.WorkerReportsProgress = true;
			this.backgroundWorker1.WorkerSupportsCancellation = true;
			this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BackgroundWorkerDoWork);
			this.backgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.BackgroundWorker1ProgressChanged);
			this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.BackgroundWorker1RunWorkerCompleted);
			// 
			// FileExplorerControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.listView1);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.panel3);
			this.Margin = new System.Windows.Forms.Padding(4);
			this.Name = "FileExplorerControl";
			this.Size = new System.Drawing.Size(997, 720);
			this.Load += new System.EventHandler(this.FileExplorerControlLoad);
			this.panel3.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.contextMenuStrip1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		private System.Windows.Forms.CheckBox checkBox1;
		private System.ComponentModel.BackgroundWorker backgroundWorker1;
		private System.Windows.Forms.CheckBox checkBoxDecompress;
		private System.Windows.Forms.ToolStripMenuItem downloadToToolStripMenuItem;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.ToolStripMenuItem downloadToolStripMenuItem;
		private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button button7;
		private System.Windows.Forms.Button button13;
		private System.Windows.Forms.ColumnHeader columnHeader4;
		private System.Windows.Forms.ColumnHeader columnHeader3;
		private System.Windows.Forms.ColumnHeader columnHeader2;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ListView listView1;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.ComboBox CBcardSelect;
		
	}
}
