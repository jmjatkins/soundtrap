﻿/* 
 * 	SoundTrap Software v1.0
 *
 *	Copyright (C) 2011-2014, John Atkins and Mark Johnson
 *
 *	This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
 *
 *	This file is part of the SoundTrap software. SoundTrap is an acoustic recording
 *	system intended for underwater acoustic measurements. This component of the
 *	SoundTrap project is free software: you can redistribute it and/or modify it
 *	under the terms of the GNU General Public License as published by the Free Software
 *	Foundation, either version 3 of the License, or any later version.
 *
 *	The SoundTrap software is distributed in the hope that it will be useful, but
 *	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License along with this
 *	code. If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using SudarHost.Calibration;
using System.Collections.Generic;
using Exocortex.DSP;
using System.Threading;
using OceanInstruments.ApiProxy2;

namespace SudarHost.UI
{
	/// <summary>
	/// Description of SudarServiceControl.
	/// </summary>
	public partial class SudarServiceControl : UserControl
	{
		
		SudarControl owner;
		public SudarControl Owner {
			get { return owner; }
			set { owner = value; }
		}

		public SudarServiceControl()
		{
			InitializeComponent();
		}
		
		
		void Button10Click(object sender, EventArgs e)
		{
			if(owner.Sudar.model >= Sudar.Model.ST600STD) {
				MessageBox.Show("Function not applicable to ST600 series", "Confirmation", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			
			if(Control.ModifierKeys == Keys.Shift)
			{
				OpenFileDialog ofd = new OpenFileDialog();
				ofd.Filter = "(*.bin)|*.bin";
				if(ofd.ShowDialog() == DialogResult.OK) {
					owner.UpdateBootFirmware(ofd.FileName);
				}
			}
			else{
				owner.UpdateBootFirmware("");
			}
		}
		
		void Button9Click(object sender, EventArgs e)
		{
			if(Control.ModifierKeys == Keys.Shift)
			{
				OpenFileDialog ofd = new OpenFileDialog();
				ofd.Filter = "(*.bin)|*.bin";
				if(ofd.ShowDialog() == DialogResult.OK) {
					owner.UpdateFirmware(ofd.FileName);
				}
			}
			else {
				owner.UpdateFirmware("");
			}
			owner.Sudar.GetFirmwareVersion();
		}
		
		void Button14Click(object sender, EventArgs e)
		{
			owner.Sudar.SetRtcTime();
			owner.Sudar.GetStatus();
			MessageBox.Show("Done!", "Confirmation", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}
		
		void Button11Click(object sender, EventArgs e)
		{
			owner.Sudar.GetStatus();
		}
		
		void Button1Click(object sender, EventArgs e)
		{
			owner.Sudar.Reboot();
		}
		
		void Button2Click(object sender, EventArgs e)
		{
			owner.Sudar.GetConfig();
			//owner.Sudar.GetFirmwareVersion();
		}
		
		void Button3Click(object sender, EventArgs e)
		{
			if(Control.ModifierKeys == Keys.Shift)
			{
				OpenFileDialog ofd = new OpenFileDialog();
				ofd.Filter = "(*.txt)|*.txt";
				if(ofd.ShowDialog() == DialogResult.OK) {
					owner.UpdateMspFirmware(ofd.FileName);
				}
			}
			else{
				owner.UpdateMspFirmware("");
			}
		}
		
		void Button4Click(object sender, EventArgs e)
		{
			//owner.Sudar.AudioCaptured += AudioSampleProcess;
			owner.Sudar.AudioEnable(true);
			button4.Enabled = false;
			groupBox1.Enabled = true;
		}
		
		void Button5Click(object sender, EventArgs e)
		{
			const string SerialFileName = "SoundTrapSerialLog.txt";
			//const ushort SerialEepromIdAddress = 31936;
			const uint SerialFlashIdAddress = 0x1FA000;

			
			//log file must exist to enable this function
			if(!File.Exists(SerialFileName)) throw new Exception("Unauthorized");

			if(Control.ModifierKeys == Keys.Shift)
			{
				//owner.Sudar.SerialEepromWrite(SerialEepromIdAddress,  new byte[] {0,0,0,0});
				owner.Sudar.SerialFlashEraseBlock(SerialFlashIdAddress);
			}
			else
			{
				if(owner.Sudar.SerialNumber != owner.Sudar.HwSerial) throw new Exception("Serial Already Assigned");
				
				//find the next available serial number
				var lines = File.ReadAllLines(SerialFileName);
				var lastSerial = lines[ lines.Length-1 ].Split(',')[0];
				uint newSerial = Convert.ToUInt32(lastSerial) + 1;
				
				//log the new serial asignment
				var sr = new StreamWriter(SerialFileName, true);
				sr.WriteLine("{0} , {1}, {2}, {3}", newSerial, owner.Sudar.HwSerial, owner.Sudar.model, DateTime.Now);
				sr.Close();

				//write id and inverse to device's serial flash
				var serializer = new Serializer(EndianMode.BIG_16BIT);
				uint id1 = newSerial;
				uint id2 = (uint)(~id1 & 0xFFFFFFFF);
				uint[] buf = {id1, id2};
				//owner.Sudar.SerialEepromWrite(SerialEepromIdAddress,  serializer.Serialize(buf));
				owner.Sudar.SerialFlashEraseBlock(SerialFlashIdAddress);
				owner.Sudar.SerialFlashWritePage(SerialFlashIdAddress,  serializer.Serialize(buf));
			}
			owner.Sudar.RefreshId();
			MessageBox.Show("Serial Number Set OK", "Set Serial", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}
		
		void Button6Click(object sender, EventArgs e)
		{
			owner.Sudar.AudioSample( (int)owner.Sudar.AudioSampleRate * 5 );
		}
		
		void CheckBox1CheckedChanged(object sender, EventArgs e)
		{
			owner.Sudar.AudioSetGain(checkBox1.Checked);
		}
		
		void CheckBox2CheckedChanged(object sender, EventArgs e)
		{
			owner.Sudar.AudioSetMute(checkBox2.Checked);
		}
		
		void CheckBox3CheckedChanged(object sender, EventArgs e)
		{
			owner.Sudar.AudioEnableCalSig(checkBox3.Checked);
		}
		
		
		public void AudioSampleProcess(object sender, EventArgs e)
		{
			string tempPath = Path.GetTempPath();
			tempPath += Path.DirectorySeparatorChar + "temp.wav";
			WavFileWriter sfr = new WavFileWriter(tempPath, FileMode.Create);
			sfr.Init( owner.Sudar.AudioSampleRate, 1);
			sfr.Write(owner.Sudar.rawAudiobuf.ToArray(), 0, owner.Sudar.rawAudiobuf.Count);
			sfr.Close();
			System.Diagnostics.Process.Start(tempPath);
		}
		
		void Button7Click(object sender, EventArgs e)
		{
			owner.showAudioForm();
		}
		
		void Button8Click(object sender, EventArgs e)
		{
			AudioSampleProcess(null, null);
		}
		
		void CheckBox4CheckedChanged(object sender, EventArgs e)
		{
			owner.Sudar.EnableBatteryCharger(!checkBox4.Checked);
		}
		
		void CheckBox5CheckedChanged(object sender, EventArgs e)
		{
			timer1.Enabled = checkBox5.Checked;
		}


		
		void Timer1Tick(object sender, EventArgs e)
		{
			timer1.Enabled = false;
			try{
				owner.Sudar.PollAdcValues();
				StringBuilder sb = new StringBuilder();
				//foreach(var i in ST600batChannels) {
				for(int i=0;i<16; i++){
					sb.Append(String.Format("{0:0.00} ", owner.Sudar.AdcValue(i) * .00312));
				}
				label1.Text = sb.ToString();
				timer1.Enabled = true;
			}
			catch {
				checkBox5.Checked = false;
			}
		}
		
		void ST600BattTest()
		{
			int noOfbatt = owner.Sudar.ST600batChannelsTestOrder.Length;
			var battOK = new bool[noOfbatt];
			
			for(int j=0; j<noOfbatt; j++)
			{
				label2.Text = j.ToString();
				Application.DoEvents();
				//Thread.Sleep(100);
				int startTime = Environment.TickCount;
				while(Environment.TickCount - startTime < 10000 )
				{
					owner.Sudar.PollAdcValues();
					double v = owner.Sudar.AdcValue(owner.Sudar.ST600batChannelsTestOrder[j]) * .00312;
					if (v > 1.0) {
						battOK[j] = true;
						break;
					}
					Thread.Sleep(100);
				}
				if (battOK[j] != true) {
					label2.Text = "Failed";
					return;
				}
			}
			label2.Text = "Passed";
		}
		void Button12Click(object sender, EventArgs e)
		{
			ST600BattTest();
		}
	}
}