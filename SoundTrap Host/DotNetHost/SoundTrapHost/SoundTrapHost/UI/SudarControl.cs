﻿/* 
 * 	SoundTrap Software v1.0
 *
 *	Copyright (C) 2011-2014, John Atkins and Mark Johnson
 *
 *	This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
 *
 *	This file is part of the SoundTrap software. SoundTrap is an acoustic recording
 *	system intended for underwater acoustic measurements. This component of the
 *	SoundTrap project is free software: you can redistribute it and/or modify it
 *	under the terms of the GNU General Public License as published by the Free Software
 *	Foundation, either version 3 of the License, or any later version.
 *
 *	The SoundTrap software is distributed in the hope that it will be useful, but
 *	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License along with this
 *	code. If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.CodeDom;
using System.ComponentModel;
using System.Drawing;
using System.Media;
using System.Windows.Forms;
using System.Collections.Generic;
using LibUsbDotNet;
using System.IO;
using OceanInstruments.ApiProxy2;
using SudarHost.UI;
using System.Text;

namespace SudarHost
{
	/// <summary>
	/// Description of SudarControl.
	/// </summary>
	public partial class SudarControl : UserControl
	{
		const int statusRefrehInterval = 1000;
		const int statusRefrehAfterErrorInterval = 5000;
		
		audioForm audioForm;
		
		public void showAudioForm()
		{
			if(audioForm == null)
			{
				audioForm = new audioForm();
				audioForm.Owner = this;
			}
			audioForm.ShowDialog();
		}

		
		public SudarControl()
		{
			InitializeComponent();
			fileExplorerControl1.Owner = this;
		}
		
		Sudar sudar;
		public Sudar Sudar {
			get { return sudar; }
			set {
				sudar = value;
				sudar.ConfigChanged += configChanged;
				sudar.StatusChanged += statusChanged;
				sudar.DeviceRemoved += OnRemoved;
				sudar.GetStatus();
				sudar.GetConfig();
				sudar.GetmspReg();
			}
		}
		
		void OnRemoved(object sender, EventArgs e)
		{
			if(this.Parent !=null) {
				this.Parent.Controls.Remove(this);
			}
			timer1.Enabled = false;
			this.Enabled = false;
			this.Visible = false;
		}

		void statusChanged(object sender, EventArgs e)
		{
			if(this.InvokeRequired) return;
			try {
				double volts = (double)sudar.LastStatusPacket.battVoltage / 1000;
				double externBattVolts = (double)sudar.LastStatusPacket.extBattVoltage / 1000;
				double batPercentRemaining = ((volts - 3.6) / 0.45) * 100;
				batPercentRemaining = Math.Min(100, batPercentRemaining);
				batPercentRemaining = Math.Max(0, batPercentRemaining);
				
				listBox1.Items.Clear();
				listBox1.Items.Add(sudar.modelName);
				listBox1.Items.Add("");
				if(Properties.Settings.Default.UseUTC)
					listBox1.Items.Add("Clock: " + sudar.Time.ToString("dd/MM/yy H:mm:ss UTC"));
				else
					listBox1.Items.Add("Clock: " + sudar.Time.ToLocalTime().ToString("dd/MM/yy H:mm:ss"));
				
				listBox1.Items.Add("");
				
				listBox1.Items.Add("");
				listBox1.Items.Add(String.Format("Temperature: {0:F1} deg C", (double)sudar.LastStatusPacket.temperature/100));
				listBox1.Items.Add("");
				if(sudar.model >= Sudar.Model.ST500STD) {
					
					if(sudar.model >= Sudar.Model.ST600STD) {
						listBox1.Items.Add("Battery States");
						for(int i=0;i<sudar.ST600batChannels.GetLength(0);i++) {
							var sb = new StringBuilder();
							for(int j=0;j<sudar.ST600batChannels.GetLength(1);j++) {
								//double v = sudar.AdcValue((i*ST600batChannels.GetLength(0))+j) * .00312;
								double v = sudar.AdcValue(sudar.ST600batChannels[i,j]) * .00312;
								double p = ((v - 3.2)/0.9) * 100; //percentage charge
								p = System.Math.Max(0,p);
								p = System.Math.Min(100,p);
								string s = String.Format("{0:0}%\t",p);
								sb.Append(s);
								
							}
							listBox1.Items.Add(sb.ToString());
						}
						listBox1.Items.Add("");
						listBox1.Items.Add(String.Format("Supply: {0:F2} V", volts + 0.2)); //0.2V diode drop
						listBox1.Items.Add("");
					}
					else {
						listBox1.Items.Add(String.Format("Backup Batt: {0,2:0}%", batPercentRemaining));
						listBox1.Items.Add("");
						listBox1.Items.Add(String.Format("Battery: {0:F2} V", externBattVolts));
						listBox1.Items.Add("");
					}
					listBox1.Items.Add("Memory:");
					if(sudar.CardData.totalSectors0 > 0)
						listBox1.Items.Add(String.Format("Card 0: {0,2:0} GB {1,2:0}% used", (double)sudar.CardData.totalSectors0 / (1E9/512), (double)sudar.CardData.sectorsUsed0*100/sudar.CardData.totalSectors0));
					else listBox1.Items.Add("Card 0: No card" );
					if(sudar.CardData.totalSectors1 > 0)
						listBox1.Items.Add(String.Format("Card 1: {0,2:0} GB {1,2:0}% used", (double)sudar.CardData.totalSectors1 / (1E9/512), (double)sudar.CardData.sectorsUsed1*100/sudar.CardData.totalSectors1 ));
					else listBox1.Items.Add("Card 1: No card" );
					if(sudar.CardData.totalSectors2 > 0)
						listBox1.Items.Add(String.Format("Card 2: {0,2:0} GB {1,2:0}% used", (double)sudar.CardData.totalSectors2 / (1E9/512), (double)sudar.CardData.sectorsUsed2*100/sudar.CardData.totalSectors2 ));
					else listBox1.Items.Add("Card 2: No card" );
					if(sudar.CardData.totalSectors3 > 0)
						listBox1.Items.Add(String.Format("Card 3: {0,2:0} GB {1,2:0}% used", (double)sudar.CardData.totalSectors3 / (1E9/512), (double)sudar.CardData.sectorsUsed3*100/sudar.CardData.totalSectors3 ));
					else listBox1.Items.Add("Card 3: No card" );
					

					
				}
				else{
					listBox1.Items.Add(String.Format("Battery Remaining: {0,2:0}% ({1:F2} V)", batPercentRemaining, volts));
					if(sudar.FsParams != null) {
						listBox1.Items.Add("");
						listBox1.Items.Add(String.Format("Memory Capacity: {0} GB", sudar.FsParams.CapacityGB ));
					}
					if(!sudar.BatteryOkForFsRead && (sudar.PercentFileSystemUsed == 0))
						listBox1.Items.Add("Memory Remaining: ?");
					else
						listBox1.Items.Add(String.Format("Memory Remaining: {0}%", 100-sudar.PercentFileSystemUsed));
					listBox1.Items.Add("");
				}
				
				
				
				//listBox1.Items.Add(String.Format("Pressure: {0}", sudar.LastStatusPacket.pressure));
				listBox1.Items.Add("");
				listBox1.Items.Add("");
				listBox1.Items.Add("");
				
				listBox1.Items.Add(String.Format("Serial {0}", sudar.SerialNumber));
				listBox1.Items.Add(String.Format("Hardware Serial {0}", sudar.HwSerial));
				listBox1.Items.Add(String.Format("Audio Hardware ID {0}", sudar.LastStatusPacket.audioHardwareId));
				
				listBox1.Items.Add(String.Format("Offloader Version: {0:F2}", (double)sudar.LastStatusPacket.swVer/100));
				listBox1.Items.Add(String.Format("MSP Version: {0:F2}", (double)sudar.MspSwVer/100));

				//if(sudar.FlashCardInfo != "")
				//listBox1.Items.Add(String.Format("Memory ID: {0}", sudar.FlashCardInfo));
				
				
				if(sudar.FirmwareVersion !=null) {
					string[] firmwareVersion = sudar.FirmwareVersion.Split('\n');
					if((firmwareVersion != null) && (firmwareVersion.Length >= 3)) {
						listBox1.Items.Add("Firmware Version:");
						listBox1.Items.Add(String.Format("\t{0}",firmwareVersion[0] ));
						listBox1.Items.Add(String.Format("\t{0}",firmwareVersion[1] ));
						listBox1.Items.Add(String.Format("\t{0}",firmwareVersion[2] ));
					}
				}
				sudarDeployControl.UpdateSampleRateOptions(sudar.BaseSampleRate);

//				TimeSpan clockError = sudar.Time.ToLocalTime() - DateTime.Now;
//				label14.ForeColor =  Math.Abs(clockError.TotalSeconds) >  5 ? Color.Red : Color.Black;
				
			}
			catch(Exception) {
				//TODO log error
			}
		}
		
		void configChanged(object sender, EventArgs e)
		{
			try {
				sudarDeployControl.SetSudarConfig(sudar.CurrentConfig, sudar);
			}
			catch(Exception) {
				//TODO log error
			}
		}
		
		
		bool actionCanceled;
		void Button4Click(object sender, EventArgs e)
		{
			actionCanceled = true;
		}
		
		public void ShowProgress(bool showCancel, string processName)
		{
			button4.Visible = showCancel;
			actionCanceled = false;
			label18.Text = processName;
			tabControl1.Enabled = false;
			panel4.Visible = true;
			Application.DoEvents();
		}
		
		public void HideProgress()
		{
			tabControl1.Enabled = true;
			panel4.Visible = false;
		}
		
		public bool UpdateProgress(string s, int percentComplete)
		{
			label19.Text =s;
			progressBar2.Value = Math.Min( percentComplete, progressBar2.Maximum);
			Application.DoEvents();
			return actionCanceled;
		}
		
		
		
		public void UpdateFirmware(string path) {
			if(path == "") {
				path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
				path += Path.DirectorySeparatorChar + "SoundtrapMainApp.bin";
			}
			try {
				ShowProgress(false, "Updating Device Firmware...");
				if(sudar.model < Sudar.Model.ST600STD) {
					Sudar.UpdateApplicationFirmware(path, UpdateProgress);
				}
				else {
					Sudar.UpdateSerialFlashFirmware(path, UpdateProgress);
				}
			}
			finally {
				HideProgress();
			}
		}

		public void UpdateMspFirmware(string path) {
			if(path == "") {
				path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
				path += Path.DirectorySeparatorChar;
				path += sudar.model < Sudar.Model.ST600STD ? "StMsp.v214.txt" : "StMsp.out.txt";
			}
			try {
				ShowProgress(false, "Updating Device Msp Firmware...");
				Sudar.UpdateMspFirmware(path, UpdateProgress);
			}
			finally {
				HideProgress();
			}
		}
		
		public void UpdateBootFirmware(string path) {
			if(path == "") {
				path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
				path += Path.DirectorySeparatorChar;
				path += "SoundTrapReboot.bin";
			}
			try {
				ShowProgress(false, "Updating Bootloader Firmware...");
				Sudar.UpdateBootFirmware(path, UpdateProgress);
			}
			finally {
				HideProgress();
			}
		}

		bool FirmwareIsCurrent(string a)
		{
			string[] curVersion = ("4.0.0.1").Split('.');
			string[] aa = a.Split('.');
			
			for(int i=0; i<4; i++) {
				if( Convert.ToInt16( aa[i] ) != Convert.ToInt16( curVersion[i] )) return false;
			}
			return true;
		}
		
		
		void Button5Click(object sender, EventArgs e)
		{
			SudarConfig sc =  sudarDeployControl.GetSudarConfig();

			if(sc.serialLogMode == 0x02) {
				MessageBox.Show("Warning, you have configured your device to stream data. No audio will be recorded in this mode.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			
			bool result = sudar.SetRtcTime() &&  sudar.SetConfig(sc);
			if(!result) {
				MessageBox.Show("SoundTrap Configure failed - Check Parameters!");
				return;
			}

			try {
				if(sudar.MspSwVer < (sudar.model < Sudar.Model.ST600STD ? 212 : 303)) {
					if( MessageBox.Show("The device's MSP firmware requires updating in order to work with this software. " +
					                    "To proceed you will need to perform a hardware reset which requires a " +
					                    "9 V battery. See user guide for more information. Do you wish to proceed with the MSP update?", "Warning",  MessageBoxButtons.YesNo, MessageBoxIcon.Hand) == DialogResult.Yes) {
						
						UpdateMspFirmware("");
						MessageBox.Show("MSP update complete. The device will now restart. Please re-deploy once it has reconnected...");
						return;
					}
					else{
						throw new Exception("MSP firmware is out of date");
					}
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show("Warning MSP firmware out of date. Deployment severly compromised " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}


			try {
				string swVer = sudar.FirmwareVersion.Split('\n')[0];
				if(!FirmwareIsCurrent(swVer)) {
					UpdateFirmware("");
					sudar.GetFirmwareVersion();
				}
			}
			catch
			{
				MessageBox.Show("Warning - device firmware check failed. Deployment may be compromised");
			}
			
			this.Parent.Controls.Remove(this);
			MessageBox.Show("SoundTrap armed and ready for deployment","Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}
		
		
		void Button6Click(object sender, EventArgs e)
		{
			try {
				sudar.GetConfig();
			}
			catch {
				//TODO - log error
			}
		}
		
		void Timer1Tick(object sender, EventArgs e)
		{
			timer1.Enabled = false;
			try {
				Sudar.GetStatus();
				if((sudar.FirmwareVersion == null || sudar.FirmwareVersion == "" ) && sudar.BatteryOkForFsRead ) {
					sudar.GetFirmwareVersion();
				}
				if(Sudar.model >= Sudar.Model.ST600STD) sudar.PollAdcValues();
			}
			catch {
				//timer1.Enabled = false;
			}
			finally {
				timer1.Enabled = true;
			}
		}
		
		void SudarServiceControl1Load(object sender, EventArgs e)
		{
			if(AppState.instance.mode == AppState.Mode.prodTest){
				DisplayProdTestControl();
			}
		}
		
		SudarServiceControl sudarServiceControl1;
		public void DisplayServiceControl()
		{
			if(sudarServiceControl1 == null ) {
				sudarServiceControl1 = new SudarServiceControl();
				sudarServiceControl1.Owner = this;
				TabPage p = new TabPage("Service");
				p.Controls.Add(sudarServiceControl1);
				sudarServiceControl1.Dock = DockStyle.Fill;
				tabControl1.TabPages.Add(p);
				tabControl1.SelectedTab = p;
			}
		}
		
		
		ProdTestControl prodTestControl1;
		void DisplayProdTestControl()
		{
			if(prodTestControl1 == null) {
				prodTestControl1 = new ProdTestControl();
				prodTestControl1.Owner = this;
				TabPage p = new TabPage("Test");
				p.Controls.Add(prodTestControl1);
				prodTestControl1.Dock = DockStyle.Fill;
				tabControl1.TabPages.Add(p);
				tabControl1.SelectedTab = p;
			}
		}
		
		
		protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
		{
			const int WM_KEYDOWN = 0x100;
			try {
				if (msg.Msg == WM_KEYDOWN && (keyData == Keys.F12)) {
					DisplayServiceControl();
				}
				if (msg.Msg == WM_KEYDOWN && (keyData == Keys.F11)) {
					DisplayProdTestControl();
				}
			}
			catch  {
				//do nothing;
			}
			return base.ProcessCmdKey(ref msg,keyData);
		}
		
		
		void Panel5Click(object sender, EventArgs e)
		{
			System.Diagnostics.Process.Start("www.oceaninstruments.co.nz");
		}
		void FileExplorerControl1Load(object sender, EventArgs e)
		{
			
		}
	}
}
