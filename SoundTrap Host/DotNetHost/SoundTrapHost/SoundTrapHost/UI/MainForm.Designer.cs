﻿/*
 * Created by SharpDevelop.
 * User: jatk009
 * Date: 22/05/2012
 * Time: 12:30 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace SudarHost
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.serviceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.fileExtractionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.setFileSaveLocationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.enableTestModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.zeroFillDropoutsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.useUTCTimeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.soundTrapUserGuideToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.panel3 = new System.Windows.Forms.Panel();
			this.panel5 = new System.Windows.Forms.Panel();
			this.panel4 = new System.Windows.Forms.Panel();
			this.panel2 = new System.Windows.Forms.Panel();
			this.label1 = new System.Windows.Forms.Label();
			this.menuStrip1.SuspendLayout();
			this.panel3.SuspendLayout();
			this.panel4.SuspendLayout();
			this.SuspendLayout();
			// 
			// menuStrip1
			// 
			this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.fileToolStripMenuItem,
			this.viewToolStripMenuItem,
			this.toolsToolStripMenuItem,
			this.helpToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
			this.menuStrip1.Size = new System.Drawing.Size(1312, 28);
			this.menuStrip1.TabIndex = 13;
			this.menuStrip1.Text = "menuStrip1";
			this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.MenuStrip1ItemClicked);
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.exitToolStripMenuItem});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
			this.fileToolStripMenuItem.Text = "File";
			// 
			// exitToolStripMenuItem
			// 
			this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
			this.exitToolStripMenuItem.Size = new System.Drawing.Size(108, 26);
			this.exitToolStripMenuItem.Text = "Exit";
			this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItemClick);
			// 
			// viewToolStripMenuItem
			// 
			this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.serviceToolStripMenuItem});
			this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
			this.viewToolStripMenuItem.Size = new System.Drawing.Size(53, 24);
			this.viewToolStripMenuItem.Text = "View";
			// 
			// serviceToolStripMenuItem
			// 
			this.serviceToolStripMenuItem.Name = "serviceToolStripMenuItem";
			this.serviceToolStripMenuItem.ShortcutKeyDisplayString = "F12";
			this.serviceToolStripMenuItem.Size = new System.Drawing.Size(163, 26);
			this.serviceToolStripMenuItem.Text = "Service";
			this.serviceToolStripMenuItem.Click += new System.EventHandler(this.ServiceToolStripMenuItemClick);
			// 
			// toolsToolStripMenuItem
			// 
			this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.fileExtractionToolStripMenuItem,
			this.setFileSaveLocationToolStripMenuItem,
			this.enableTestModeToolStripMenuItem,
			this.zeroFillDropoutsToolStripMenuItem,
			this.useUTCTimeToolStripMenuItem});
			this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
			this.toolsToolStripMenuItem.Size = new System.Drawing.Size(56, 24);
			this.toolsToolStripMenuItem.Text = "Tools";
			// 
			// fileExtractionToolStripMenuItem
			// 
			this.fileExtractionToolStripMenuItem.Name = "fileExtractionToolStripMenuItem";
			this.fileExtractionToolStripMenuItem.Size = new System.Drawing.Size(281, 26);
			this.fileExtractionToolStripMenuItem.Text = "File Extraction";
			this.fileExtractionToolStripMenuItem.Click += new System.EventHandler(this.FileExtractionToolStripMenuItemClick);
			// 
			// setFileSaveLocationToolStripMenuItem
			// 
			this.setFileSaveLocationToolStripMenuItem.Name = "setFileSaveLocationToolStripMenuItem";
			this.setFileSaveLocationToolStripMenuItem.Size = new System.Drawing.Size(281, 26);
			this.setFileSaveLocationToolStripMenuItem.Text = "Set Default File Save Location";
			this.setFileSaveLocationToolStripMenuItem.Click += new System.EventHandler(this.SetFileSaveLocationToolStripMenuItemClick);
			// 
			// enableTestModeToolStripMenuItem
			// 
			this.enableTestModeToolStripMenuItem.CheckOnClick = true;
			this.enableTestModeToolStripMenuItem.Name = "enableTestModeToolStripMenuItem";
			this.enableTestModeToolStripMenuItem.Size = new System.Drawing.Size(281, 26);
			this.enableTestModeToolStripMenuItem.Text = "Set Test Mode";
			this.enableTestModeToolStripMenuItem.CheckedChanged += new System.EventHandler(this.EnableTestModeToolStripMenuItemCheckedChanged);
			this.enableTestModeToolStripMenuItem.Click += new System.EventHandler(this.EnableTestModeToolStripMenuItemClick);
			// 
			// zeroFillDropoutsToolStripMenuItem
			// 
			this.zeroFillDropoutsToolStripMenuItem.CheckOnClick = true;
			this.zeroFillDropoutsToolStripMenuItem.Name = "zeroFillDropoutsToolStripMenuItem";
			this.zeroFillDropoutsToolStripMenuItem.Size = new System.Drawing.Size(281, 26);
			this.zeroFillDropoutsToolStripMenuItem.Text = "Zero Fill Dropouts";
			this.zeroFillDropoutsToolStripMenuItem.Click += new System.EventHandler(this.ZeroFillDropoutsToolStripMenuItemClick);
			// 
			// useUTCTimeToolStripMenuItem
			// 
			this.useUTCTimeToolStripMenuItem.CheckOnClick = true;
			this.useUTCTimeToolStripMenuItem.Name = "useUTCTimeToolStripMenuItem";
			this.useUTCTimeToolStripMenuItem.Size = new System.Drawing.Size(281, 26);
			this.useUTCTimeToolStripMenuItem.Text = "Use UTC Time";
			this.useUTCTimeToolStripMenuItem.Click += new System.EventHandler(this.UseUTCTimeToolStripMenuItemClick);
			// 
			// helpToolStripMenuItem
			// 
			this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.soundTrapUserGuideToolStripMenuItem,
			this.aboutToolStripMenuItem});
			this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
			this.helpToolStripMenuItem.Size = new System.Drawing.Size(53, 24);
			this.helpToolStripMenuItem.Text = "Help";
			// 
			// soundTrapUserGuideToolStripMenuItem
			// 
			this.soundTrapUserGuideToolStripMenuItem.Name = "soundTrapUserGuideToolStripMenuItem";
			this.soundTrapUserGuideToolStripMenuItem.Size = new System.Drawing.Size(271, 26);
			this.soundTrapUserGuideToolStripMenuItem.Text = "Open SoundTrap User Guide";
			this.soundTrapUserGuideToolStripMenuItem.Click += new System.EventHandler(this.SoundTrapUserGuideToolStripMenuItemClick);
			// 
			// aboutToolStripMenuItem
			// 
			this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
			this.aboutToolStripMenuItem.Size = new System.Drawing.Size(271, 26);
			this.aboutToolStripMenuItem.Text = "About";
			this.aboutToolStripMenuItem.Click += new System.EventHandler(this.AboutToolStripMenuItemClick);
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.panel5);
			this.panel3.Controls.Add(this.panel4);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel3.Location = new System.Drawing.Point(0, 28);
			this.panel3.Margin = new System.Windows.Forms.Padding(4);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(1312, 776);
			this.panel3.TabIndex = 14;
			// 
			// panel5
			// 
			this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel5.Location = new System.Drawing.Point(274, 0);
			this.panel5.Margin = new System.Windows.Forms.Padding(4);
			this.panel5.Name = "panel5";
			this.panel5.Size = new System.Drawing.Size(1038, 776);
			this.panel5.TabIndex = 14;
			// 
			// panel4
			// 
			this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panel4.Controls.Add(this.panel2);
			this.panel4.Controls.Add(this.label1);
			this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
			this.panel4.Location = new System.Drawing.Point(0, 0);
			this.panel4.Margin = new System.Windows.Forms.Padding(4);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(274, 776);
			this.panel4.TabIndex = 13;
			// 
			// panel2
			// 
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(0, 47);
			this.panel2.Margin = new System.Windows.Forms.Padding(4);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(272, 727);
			this.panel2.TabIndex = 26;
			// 
			// label1
			// 
			this.label1.Dock = System.Windows.Forms.DockStyle.Top;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(272, 47);
			this.label1.TabIndex = 19;
			this.label1.Text = "Device List:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1312, 804);
			this.Controls.Add(this.panel3);
			this.Controls.Add(this.menuStrip1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Margin = new System.Windows.Forms.Padding(4);
			this.MinimumSize = new System.Drawing.Size(1327, 850);
			this.Name = "MainForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "SoundTrap Host";
			this.Load += new System.EventHandler(this.MainFormLoad);
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.panel3.ResumeLayout(false);
			this.panel4.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		private System.Windows.Forms.ToolStripMenuItem zeroFillDropoutsToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem soundTrapUserGuideToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem serviceToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem enableTestModeToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem setFileSaveLocationToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem fileExtractionToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.Panel panel5;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ToolStripMenuItem useUTCTimeToolStripMenuItem;
	}
}
