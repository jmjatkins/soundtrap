﻿/*
 * Created by SharpDevelop.
 * User: jatk009
 * Date: 8/08/2012
 * Time: 4:57 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace SudarHost
{
	partial class SudarControl
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the control.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SudarControl));
			this.panel1 = new System.Windows.Forms.Panel();
			this.groupBox11 = new System.Windows.Forms.GroupBox();
			this.panel5 = new System.Windows.Forms.Panel();
			this.listBox1 = new System.Windows.Forms.ListBox();
			this.groupBox8 = new System.Windows.Forms.GroupBox();
			this.radioButton8 = new System.Windows.Forms.RadioButton();
			this.radioButton6 = new System.Windows.Forms.RadioButton();
			this.radioButton7 = new System.Windows.Forms.RadioButton();
			this.groupBox5 = new System.Windows.Forms.GroupBox();
			this.radioButton5 = new System.Windows.Forms.RadioButton();
			this.radioButton4 = new System.Windows.Forms.RadioButton();
			this.comboBox3 = new System.Windows.Forms.ComboBox();
			this.label6 = new System.Windows.Forms.Label();
			this.button3 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.checkBox3 = new System.Windows.Forms.CheckBox();
			this.checkBox2 = new System.Windows.Forms.CheckBox();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.label5 = new System.Windows.Forms.Label();
			this.button1 = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.textBox3 = new System.Windows.Forms.TextBox();
			this.radioButton3 = new System.Windows.Forms.RadioButton();
			this.radioButton2 = new System.Windows.Forms.RadioButton();
			this.radioButton1 = new System.Windows.Forms.RadioButton();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.checkBox1 = new System.Windows.Forms.CheckBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.comboBox2 = new System.Windows.Forms.ComboBox();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.comboBox1 = new System.Windows.Forms.ComboBox();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.panel4 = new System.Windows.Forms.Panel();
			this.label19 = new System.Windows.Forms.Label();
			this.button4 = new System.Windows.Forms.Button();
			this.label18 = new System.Windows.Forms.Label();
			this.progressBar2 = new System.Windows.Forms.ProgressBar();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.panel2 = new System.Windows.Forms.Panel();
			this.panel3 = new System.Windows.Forms.Panel();
			this.button5 = new System.Windows.Forms.Button();
			this.sudarDeployControl = new SudarHost.SudarDeployControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.fileExplorerControl1 = new SudarHost.UI.FileExplorerControl();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.panel1.SuspendLayout();
			this.groupBox11.SuspendLayout();
			this.groupBox8.SuspendLayout();
			this.groupBox5.SuspendLayout();
			this.groupBox4.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.panel4.SuspendLayout();
			this.tabPage2.SuspendLayout();
			this.panel3.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.groupBox11);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
			this.panel1.Location = new System.Drawing.Point(752, 0);
			this.panel1.Margin = new System.Windows.Forms.Padding(4);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(236, 740);
			this.panel1.TabIndex = 1;
			// 
			// groupBox11
			// 
			this.groupBox11.Controls.Add(this.panel5);
			this.groupBox11.Controls.Add(this.listBox1);
			this.groupBox11.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.groupBox11.Location = new System.Drawing.Point(0, 0);
			this.groupBox11.Margin = new System.Windows.Forms.Padding(4);
			this.groupBox11.Name = "groupBox11";
			this.groupBox11.Padding = new System.Windows.Forms.Padding(4);
			this.groupBox11.Size = new System.Drawing.Size(236, 740);
			this.groupBox11.TabIndex = 1;
			this.groupBox11.TabStop = false;
			this.groupBox11.Text = "Status";
			// 
			// panel5
			// 
			this.panel5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.panel5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel5.BackgroundImage")));
			this.panel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.panel5.Cursor = System.Windows.Forms.Cursors.Hand;
			this.panel5.Location = new System.Drawing.Point(32, 565);
			this.panel5.Margin = new System.Windows.Forms.Padding(4);
			this.panel5.Name = "panel5";
			this.panel5.Size = new System.Drawing.Size(171, 76);
			this.panel5.TabIndex = 9;
			this.panel5.Click += new System.EventHandler(this.Panel5Click);
			// 
			// listBox1
			// 
			this.listBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
			| System.Windows.Forms.AnchorStyles.Left) 
			| System.Windows.Forms.AnchorStyles.Right)));
			this.listBox1.BackColor = System.Drawing.SystemColors.Control;
			this.listBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.listBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.listBox1.FormattingEnabled = true;
			this.listBox1.ItemHeight = 17;
			this.listBox1.Location = new System.Drawing.Point(0, 55);
			this.listBox1.Margin = new System.Windows.Forms.Padding(4);
			this.listBox1.Name = "listBox1";
			this.listBox1.Size = new System.Drawing.Size(220, 476);
			this.listBox1.TabIndex = 7;
			// 
			// groupBox8
			// 
			this.groupBox8.Controls.Add(this.radioButton8);
			this.groupBox8.Controls.Add(this.radioButton6);
			this.groupBox8.Controls.Add(this.radioButton7);
			this.groupBox8.Location = new System.Drawing.Point(10, 516);
			this.groupBox8.Name = "groupBox8";
			this.groupBox8.Size = new System.Drawing.Size(457, 56);
			this.groupBox8.TabIndex = 10;
			this.groupBox8.TabStop = false;
			this.groupBox8.Text = "Synchronization";
			// 
			// radioButton8
			// 
			this.radioButton8.Location = new System.Drawing.Point(16, 19);
			this.radioButton8.Name = "radioButton8";
			this.radioButton8.Size = new System.Drawing.Size(77, 24);
			this.radioButton8.TabIndex = 6;
			this.radioButton8.TabStop = true;
			this.radioButton8.Text = "None";
			this.radioButton8.UseVisualStyleBackColor = true;
			// 
			// radioButton6
			// 
			this.radioButton6.Location = new System.Drawing.Point(99, 19);
			this.radioButton6.Name = "radioButton6";
			this.radioButton6.Size = new System.Drawing.Size(56, 24);
			this.radioButton6.TabIndex = 5;
			this.radioButton6.TabStop = true;
			this.radioButton6.Text = "Slave";
			this.radioButton6.UseVisualStyleBackColor = true;
			// 
			// radioButton7
			// 
			this.radioButton7.Location = new System.Drawing.Point(167, 19);
			this.radioButton7.Name = "radioButton7";
			this.radioButton7.Size = new System.Drawing.Size(104, 24);
			this.radioButton7.TabIndex = 4;
			this.radioButton7.TabStop = true;
			this.radioButton7.Text = "Master";
			this.radioButton7.UseVisualStyleBackColor = true;
			// 
			// groupBox5
			// 
			this.groupBox5.Controls.Add(this.radioButton5);
			this.groupBox5.Controls.Add(this.radioButton4);
			this.groupBox5.Controls.Add(this.comboBox3);
			this.groupBox5.Controls.Add(this.label6);
			this.groupBox5.Location = new System.Drawing.Point(10, 338);
			this.groupBox5.Name = "groupBox5";
			this.groupBox5.Size = new System.Drawing.Size(457, 86);
			this.groupBox5.TabIndex = 9;
			this.groupBox5.TabStop = false;
			this.groupBox5.Text = "Audio";
			// 
			// radioButton5
			// 
			this.radioButton5.Location = new System.Drawing.Point(115, 19);
			this.radioButton5.Name = "radioButton5";
			this.radioButton5.Size = new System.Drawing.Size(104, 24);
			this.radioButton5.TabIndex = 3;
			this.radioButton5.TabStop = true;
			this.radioButton5.Text = "Spectral";
			this.radioButton5.UseVisualStyleBackColor = true;
			// 
			// radioButton4
			// 
			this.radioButton4.Location = new System.Drawing.Point(16, 19);
			this.radioButton4.Name = "radioButton4";
			this.radioButton4.Size = new System.Drawing.Size(104, 24);
			this.radioButton4.TabIndex = 2;
			this.radioButton4.TabStop = true;
			this.radioButton4.Text = "Wave";
			this.radioButton4.UseVisualStyleBackColor = true;
			// 
			// comboBox3
			// 
			this.comboBox3.FormattingEnabled = true;
			this.comboBox3.Location = new System.Drawing.Point(102, 55);
			this.comboBox3.Name = "comboBox3";
			this.comboBox3.Size = new System.Drawing.Size(77, 25);
			this.comboBox3.TabIndex = 1;
			this.comboBox3.Text = "256 kHz";
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(16, 58);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(100, 23);
			this.label6.TabIndex = 0;
			this.label6.Text = "Sample Rate";
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(10, 16);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(158, 23);
			this.button3.TabIndex = 8;
			this.button3.Text = "Read Settings From Device";
			this.button3.UseVisualStyleBackColor = true;
			// 
			// button2
			// 
			this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.button2.Location = new System.Drawing.Point(290, 582);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(158, 23);
			this.button2.TabIndex = 7;
			this.button2.Text = "Write Settings To Device";
			this.button2.UseVisualStyleBackColor = true;
			// 
			// groupBox4
			// 
			this.groupBox4.Controls.Add(this.checkBox3);
			this.groupBox4.Controls.Add(this.checkBox2);
			this.groupBox4.Location = new System.Drawing.Point(10, 430);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(457, 80);
			this.groupBox4.TabIndex = 6;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "Ancillary Sensors";
			// 
			// checkBox3
			// 
			this.checkBox3.Location = new System.Drawing.Point(21, 49);
			this.checkBox3.Name = "checkBox3";
			this.checkBox3.Size = new System.Drawing.Size(104, 24);
			this.checkBox3.TabIndex = 1;
			this.checkBox3.Text = "Accleration";
			this.checkBox3.UseVisualStyleBackColor = true;
			// 
			// checkBox2
			// 
			this.checkBox2.Location = new System.Drawing.Point(21, 19);
			this.checkBox2.Name = "checkBox2";
			this.checkBox2.Size = new System.Drawing.Size(104, 24);
			this.checkBox2.TabIndex = 0;
			this.checkBox2.Text = "Pressure";
			this.checkBox2.UseVisualStyleBackColor = true;
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.label5);
			this.groupBox3.Controls.Add(this.button1);
			this.groupBox3.Controls.Add(this.label4);
			this.groupBox3.Controls.Add(this.textBox3);
			this.groupBox3.Controls.Add(this.radioButton3);
			this.groupBox3.Controls.Add(this.radioButton2);
			this.groupBox3.Controls.Add(this.radioButton1);
			this.groupBox3.Location = new System.Drawing.Point(8, 205);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(459, 124);
			this.groupBox3.TabIndex = 5;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Start Trigger";
			// 
			// label5
			// 
			this.label5.Enabled = false;
			this.label5.Location = new System.Drawing.Point(76, 85);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(136, 23);
			this.label5.TabIndex = 6;
			this.label5.Text = "12 August 2011, 10:00:00";
			// 
			// button1
			// 
			this.button1.Enabled = false;
			this.button1.Location = new System.Drawing.Point(218, 80);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(22, 23);
			this.button1.TabIndex = 5;
			this.button1.Text = "...";
			this.button1.UseVisualStyleBackColor = true;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(176, 55);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(100, 23);
			this.label4.TabIndex = 4;
			this.label4.Text = "(m) depth";
			// 
			// textBox3
			// 
			this.textBox3.Location = new System.Drawing.Point(104, 52);
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new System.Drawing.Size(56, 22);
			this.textBox3.TabIndex = 3;
			this.textBox3.Text = "20";
			this.textBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// radioButton3
			// 
			this.radioButton3.Location = new System.Drawing.Point(21, 79);
			this.radioButton3.Name = "radioButton3";
			this.radioButton3.Size = new System.Drawing.Size(139, 24);
			this.radioButton3.TabIndex = 2;
			this.radioButton3.TabStop = true;
			this.radioButton3.Text = "Time";
			this.radioButton3.UseVisualStyleBackColor = true;
			// 
			// radioButton2
			// 
			this.radioButton2.Location = new System.Drawing.Point(21, 49);
			this.radioButton2.Name = "radioButton2";
			this.radioButton2.Size = new System.Drawing.Size(139, 24);
			this.radioButton2.TabIndex = 1;
			this.radioButton2.TabStop = true;
			this.radioButton2.Text = "Pressure";
			this.radioButton2.UseVisualStyleBackColor = true;
			// 
			// radioButton1
			// 
			this.radioButton1.Location = new System.Drawing.Point(21, 19);
			this.radioButton1.Name = "radioButton1";
			this.radioButton1.Size = new System.Drawing.Size(139, 24);
			this.radioButton1.TabIndex = 0;
			this.radioButton1.TabStop = true;
			this.radioButton1.Text = "Salt Water Switch";
			this.radioButton1.UseVisualStyleBackColor = true;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.checkBox1);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.comboBox2);
			this.groupBox1.Controls.Add(this.textBox2);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.comboBox1);
			this.groupBox1.Controls.Add(this.textBox1);
			this.groupBox1.Location = new System.Drawing.Point(8, 45);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(459, 154);
			this.groupBox1.TabIndex = 4;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Timer Schedule";
			// 
			// checkBox1
			// 
			this.checkBox1.Location = new System.Drawing.Point(21, 29);
			this.checkBox1.Name = "checkBox1";
			this.checkBox1.Size = new System.Drawing.Size(139, 24);
			this.checkBox1.TabIndex = 8;
			this.checkBox1.Text = "Record Continously";
			this.checkBox1.UseVisualStyleBackColor = true;
			// 
			// label3
			// 
			this.label3.ForeColor = System.Drawing.SystemColors.HotTrack;
			this.label3.Location = new System.Drawing.Point(18, 124);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(333, 23);
			this.label3.TabIndex = 7;
			this.label3.Text = "Resultant (Estimated) Battery Runtime For This Schedule = 4.5 days";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(18, 101);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(107, 23);
			this.label2.TabIndex = 6;
			this.label2.Text = "For A Period Of";
			// 
			// comboBox2
			// 
			this.comboBox2.FormattingEnabled = true;
			this.comboBox2.Items.AddRange(new object[] {
			"Seconds",
			"Minutes",
			"Hours",
			"Days"});
			this.comboBox2.Location = new System.Drawing.Point(207, 98);
			this.comboBox2.Name = "comboBox2";
			this.comboBox2.Size = new System.Drawing.Size(86, 25);
			this.comboBox2.TabIndex = 5;
			this.comboBox2.Text = "Minutes";
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(131, 98);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(61, 22);
			this.textBox2.TabIndex = 4;
			this.textBox2.Text = "30";
			this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(18, 69);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(107, 23);
			this.label1.TabIndex = 3;
			this.label1.Text = "Record Once Every ";
			// 
			// comboBox1
			// 
			this.comboBox1.FormattingEnabled = true;
			this.comboBox1.Items.AddRange(new object[] {
			"Seconds",
			"Minutes",
			"Hours",
			"Days"});
			this.comboBox1.Location = new System.Drawing.Point(207, 66);
			this.comboBox1.Name = "comboBox1";
			this.comboBox1.Size = new System.Drawing.Size(86, 25);
			this.comboBox1.TabIndex = 2;
			this.comboBox1.Text = "Hours";
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(131, 66);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(61, 22);
			this.textBox1.TabIndex = 0;
			this.textBox1.Text = "6";
			this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// panel4
			// 
			this.panel4.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.panel4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
			this.panel4.Controls.Add(this.label19);
			this.panel4.Controls.Add(this.button4);
			this.panel4.Controls.Add(this.label18);
			this.panel4.Controls.Add(this.progressBar2);
			this.panel4.Location = new System.Drawing.Point(226, 186);
			this.panel4.Margin = new System.Windows.Forms.Padding(4);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(277, 153);
			this.panel4.TabIndex = 3;
			this.panel4.Visible = false;
			// 
			// label19
			// 
			this.label19.Location = new System.Drawing.Point(28, 27);
			this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(216, 28);
			this.label19.TabIndex = 3;
			this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// button4
			// 
			this.button4.Location = new System.Drawing.Point(144, 97);
			this.button4.Margin = new System.Windows.Forms.Padding(4);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(100, 28);
			this.button4.TabIndex = 2;
			this.button4.Text = "Cancel";
			this.button4.UseVisualStyleBackColor = true;
			this.button4.Click += new System.EventHandler(this.Button4Click);
			// 
			// label18
			// 
			this.label18.Location = new System.Drawing.Point(28, 4);
			this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(216, 28);
			this.label18.TabIndex = 1;
			this.label18.Text = "label18";
			this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// progressBar2
			// 
			this.progressBar2.Location = new System.Drawing.Point(28, 59);
			this.progressBar2.Margin = new System.Windows.Forms.Padding(4);
			this.progressBar2.Name = "progressBar2";
			this.progressBar2.Size = new System.Drawing.Size(216, 28);
			this.progressBar2.TabIndex = 0;
			// 
			// timer1
			// 
			this.timer1.Enabled = true;
			this.timer1.Interval = 1000;
			this.timer1.Tick += new System.EventHandler(this.Timer1Tick);
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.panel2);
			this.tabPage2.Controls.Add(this.panel3);
			this.tabPage2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.tabPage2.Location = new System.Drawing.Point(4, 29);
			this.tabPage2.Margin = new System.Windows.Forms.Padding(4);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(4);
			this.tabPage2.Size = new System.Drawing.Size(744, 707);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "Deploy";
			this.tabPage2.UseVisualStyleBackColor = true;
			// 
			// panel2
			// 
			this.panel2.BackColor = System.Drawing.Color.Transparent;
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(4, 639);
			this.panel2.Margin = new System.Windows.Forms.Padding(4);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(736, 64);
			this.panel2.TabIndex = 4;
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.button5);
			this.panel3.Controls.Add(this.sudarDeployControl);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel3.Location = new System.Drawing.Point(4, 4);
			this.panel3.Margin = new System.Windows.Forms.Padding(4);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(736, 635);
			this.panel3.TabIndex = 3;
			// 
			// button5
			// 
			this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button5.Location = new System.Drawing.Point(575, 599);
			this.button5.Margin = new System.Windows.Forms.Padding(4);
			this.button5.Name = "button5";
			this.button5.Size = new System.Drawing.Size(128, 28);
			this.button5.TabIndex = 0;
			this.button5.Text = "Deploy";
			this.button5.UseVisualStyleBackColor = true;
			this.button5.Click += new System.EventHandler(this.Button5Click);
			// 
			// sudarDeployControl
			// 
			this.sudarDeployControl.AutoScroll = true;
			this.sudarDeployControl.AutoSize = true;
			this.sudarDeployControl.Location = new System.Drawing.Point(4, 2);
			this.sudarDeployControl.Margin = new System.Windows.Forms.Padding(5);
			this.sudarDeployControl.Name = "sudarDeployControl";
			this.sudarDeployControl.Size = new System.Drawing.Size(699, 668);
			this.sudarDeployControl.TabIndex = 22;
			this.sudarDeployControl.TimeOn = ((uint)(0u));
			this.sudarDeployControl.TimeOnceEvery = ((uint)(0u));
			this.sudarDeployControl.TimeOnceEveryCount = ((uint)(0u));
			this.sudarDeployControl.TimeOnCount = ((uint)(0u));
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.fileExplorerControl1);
			this.tabPage1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.tabPage1.Location = new System.Drawing.Point(4, 29);
			this.tabPage1.Margin = new System.Windows.Forms.Padding(4);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(4);
			this.tabPage1.Size = new System.Drawing.Size(744, 707);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "Retrieve";
			this.tabPage1.UseVisualStyleBackColor = true;
			// 
			// fileExplorerControl1
			// 
			this.fileExplorerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.fileExplorerControl1.Location = new System.Drawing.Point(4, 4);
			this.fileExplorerControl1.Margin = new System.Windows.Forms.Padding(5);
			this.fileExplorerControl1.Name = "fileExplorerControl1";
			this.fileExplorerControl1.Owner = null;
			this.fileExplorerControl1.Size = new System.Drawing.Size(736, 699);
			this.fileExplorerControl1.TabIndex = 17;
			this.fileExplorerControl1.Load += new System.EventHandler(this.FileExplorerControl1Load);
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.tabControl1.ItemSize = new System.Drawing.Size(100, 25);
			this.tabControl1.Location = new System.Drawing.Point(0, 0);
			this.tabControl1.Margin = new System.Windows.Forms.Padding(4);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(752, 740);
			this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
			this.tabControl1.TabIndex = 4;
			// 
			// SudarControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.panel4);
			this.Controls.Add(this.tabControl1);
			this.Controls.Add(this.panel1);
			this.Margin = new System.Windows.Forms.Padding(4);
			this.Name = "SudarControl";
			this.Size = new System.Drawing.Size(988, 740);
			this.Load += new System.EventHandler(this.SudarServiceControl1Load);
			this.panel1.ResumeLayout(false);
			this.groupBox11.ResumeLayout(false);
			this.groupBox8.ResumeLayout(false);
			this.groupBox5.ResumeLayout(false);
			this.groupBox4.ResumeLayout(false);
			this.groupBox3.ResumeLayout(false);
			this.groupBox3.PerformLayout();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.panel4.ResumeLayout(false);
			this.tabPage2.ResumeLayout(false);
			this.panel3.ResumeLayout(false);
			this.panel3.PerformLayout();
			this.tabPage1.ResumeLayout(false);
			this.tabControl1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		private System.Windows.Forms.Panel panel5;
		private System.Windows.Forms.ListBox listBox1;
		private System.Windows.Forms.Button button5;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Timer timer1;
		private SudarHost.UI.FileExplorerControl fileExplorerControl1;
		private SudarHost.SudarDeployControl sudarDeployControl;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.ProgressBar progressBar2;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.ComboBox comboBox1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.ComboBox comboBox2;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.CheckBox checkBox1;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.RadioButton radioButton1;
		private System.Windows.Forms.RadioButton radioButton2;
		private System.Windows.Forms.RadioButton radioButton3;
		private System.Windows.Forms.TextBox textBox3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.CheckBox checkBox2;
		private System.Windows.Forms.CheckBox checkBox3;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.ComboBox comboBox3;
		private System.Windows.Forms.RadioButton radioButton4;
		private System.Windows.Forms.RadioButton radioButton5;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.RadioButton radioButton7;
		private System.Windows.Forms.RadioButton radioButton6;
		private System.Windows.Forms.RadioButton radioButton8;
		private System.Windows.Forms.GroupBox groupBox8;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.GroupBox groupBox11;
		private System.Windows.Forms.Panel panel1;
	}
}
