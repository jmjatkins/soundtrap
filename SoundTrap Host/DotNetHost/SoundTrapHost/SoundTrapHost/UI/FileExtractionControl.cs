﻿/* 
 * 	SoundTrap Software v1.0
 *
 *	Copyright (C) 2011-2014, John Atkins and Mark Johnson
 *
 *	This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
 *
 *	This file is part of the SoundTrap software. SoundTrap is an acoustic recording
 *	system intended for underwater acoustic measurements. This component of the
 *	SoundTrap project is free software: you can redistribute it and/or modify it
 *	under the terms of the GNU General Public License as published by the Free Software
 *	Foundation, either version 3 of the License, or any later version.
 *
 *	The SoundTrap software is distributed in the hope that it will be useful, but
 *	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License along with this
 *	code. If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.IO;

namespace SudarHost
{
	/// <summary>
	/// Description of FileExtractionControl.
	/// </summary>
	public partial class FileExtractionControl : UserControl
	{
		public FileExtractionControl()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		
		
		bool actionCanceled;
		void ShowProgress(bool showCancel, string processName)
		{
			button4.Visible = showCancel;
			actionCanceled = false;
			label18.Text = processName;
			panel4.Visible = true;
			Application.DoEvents();
		}
		
		void HideProgress()
		{
			panel4.Visible = false;
		}
		
		bool UpdateProgress(string s, int percentComplete)
		{
			label19.Text =s;
			progressBar2.Value = Math.Min( percentComplete, progressBar2.Maximum);
			Application.DoEvents();
			return actionCanceled;
		}

		
		void Button1Click(object sender, EventArgs e)
		{
			actionCanceled = false;
			SudFileExpander fileExpander = new SudFileExpander();
			OpenFileDialog ofd = new OpenFileDialog();
			ofd.Multiselect = true;
			ofd.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
			ofd.Filter = "(*.sud)|*.sud";
			if(ofd.ShowDialog() == DialogResult.OK)
			{
				try {
					for(int i=0;i<ofd.FileNames.Length;i++) {
						ShowProgress(true, String.Format("Processing File {0} of {1}", i, ofd.FileNames.Length));
						SudFileExpander.SudFileExpanderResult r = fileExpander.ProcessFile(ofd.FileNames[i], UpdateProgress);
						if(r == SudFileExpander.SudFileExpanderResult.Cancelled) {
							break;
						}
					}
					if(MessageBox.Show("Would you like to open the file location?", "Open?", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
						System.Diagnostics.Process.Start("explorer.exe", "/select," + ofd.FileNames[0]);
				}
				finally {
					HideProgress();
				}
			}

		}
		
		void Button4Click(object sender, EventArgs e)
		{
			actionCanceled = true;			
		}
	}
}
