﻿/*
 * Created by SharpDevelop.
 * User: jatk009
 * Date: 23/05/2014
 * Time: 4:47 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using SudarHost.Calibration;
using System.Collections.Generic;
using Exocortex.DSP;
using OxyPlot;
using OxyPlot.WindowsForms;
using System.IO;
using System.Text;

namespace SudarHost.UI
{
	/// <summary>
	/// Description of audioControl.
	/// </summary>
	public partial class audioForm : Form
	{
		PlotModel pm = new PlotModel();
		PlotModel specPlotmodel;
		LineSeries refPlotSeries;
		LineSeries fPlotSeries;
		
		public audioForm()
		{
			InitializeComponent();
			plot1.Model = new PlotModel();

			specPlotmodel = new PlotModel("Spectral Density") { LegendTitle = "" };
			//specPlotmodel.Axes.Add(new LogarithmicAxis(AxisPosition.Bottom, "f (Hz)") { Base = 2, MajorGridlineStyle = LineStyle.Solid, TickStyle = OxyPlot.TickStyle.None });
			specPlotmodel.Axes.Add(new LinearAxis(AxisPosition.Bottom, "f (Hz)") { MajorGridlineStyle = LineStyle.Solid, TickStyle = OxyPlot.TickStyle.None });
			specPlotmodel.Axes.Add(new LinearAxis(AxisPosition.Left, -130, 0, 2, 2, "dB re full scale") { MajorGridlineStyle = LineStyle.Solid, TickStyle = OxyPlot.TickStyle.None });
			specPlotmodel.MouseMove += plotMouseMove;

			refPlotSeries = new LineSeries(OxyColor.FromRgb(20,20, 255), 1, "");
			fPlotSeries = new LineSeries(OxyColor.FromRgb(255,20,20), 1, "");

			specPlotmodel.Series.Add(fPlotSeries);
			specPlotmodel.Series.Add(refPlotSeries);
		}

		SudarControl owner;
		public SudarControl Owner {
			get { return owner; }
			set {
				owner = value;
				UpdateSampleRateOptions();
			}
		}
		
		void plotMouseMove(object sender, OxyMouseEventArgs e)
		{
			if(specPlotmodel !=null) {
				float xmax = owner.Sudar.AudioSampleRate / 2;
				DataPoint p =  Axis.InverseTransform(e.Position, specPlotmodel.DefaultXAxis, specPlotmodel.DefaultYAxis);
				DataPoint p2 = findPeak( mProcessed, (float)(p.X / xmax));
				label1.Text = String.Format("{0:F1} hz = {1:F2} dB", p2.X * xmax, p2.Y);
			}
		}
		
		public void UpdateSampleRateOptions()
		{
			foreach(Control c in betterGroupBox1.Controls)
			{
				if(c is BetterRadioButton) {
					int sampleRate = (int)(owner.Sudar.AudioTestModeBaseSampleRate / Math.Pow(2,(c as BetterRadioButton).Index+1));
					(c as BetterRadioButton).Text = sampleRate.ToString();
				}
			}
		}
		
		void Button1Click(object sender, EventArgs e)
		{
			button1.Enabled = false;
			testFrequencyResponse();
			button1.Enabled = true;
		}
		
		
		
		float measureRms(float[] data) {
			float rms = 0;
			if(data.Length == 0) return 0;
			for(int i=0; i<data.Length; i++) {
				rms += data[i] * data[i];
			}
			rms = (float)Math.Sqrt(rms/data.Length);
			return rms;
		}

		int windowLength {
			get {
				return Convert.ToInt32( comboBox2.Text );
			}
		}
		
		
		
		PlotModel frPlotmodel;
		void testFrequencyResponse()
		{
			UInt32 fs = owner.Sudar.AudioSampleRate;

			if( frPlotmodel == null) {
				frPlotmodel = new PlotModel("frequency response") { LegendTitle = "" };
				frPlotmodel.Axes.Add(new LogarithmicAxis(AxisPosition.Bottom, "f (Hz)") { Base = 2, MajorGridlineStyle = LineStyle.Solid, TickStyle = OxyPlot.TickStyle.None });
				frPlotmodel.Axes.Add(new LinearAxis(AxisPosition.Left, -100, 0, 2, 2, "dB re full scale") { MajorGridlineStyle = LineStyle.Solid, TickStyle = OxyPlot.TickStyle.None });
			}
			
			plot1.Model = frPlotmodel;
			
			LineSeries s = new LineSeries();
			frPlotmodel.Series.Clear();
			
			s.Points.Clear();

			float binWidth = ((float)fs/(windowLength));
			float f = 1000;

			//owner.Sudar.AudioSetDivider(2);
			//System.Threading.Thread.Sleep(100);
			float xmax = owner.Sudar.AudioSampleRate / 2;

			while(f < (fs/2) ) {
				owner.Sudar.AudioSetCalFrequency((UInt32)f);
				System.Threading.Thread.Sleep(100);
				owner.Sudar.AudioSample(4096);
				float[] fft = sigProc.fft(2048, owner.Sudar.AudioBuf);
				float[] data = sigProc.interpolate(fft,fft.Length/2, 10);
				DataPoint p2 = findPeak( data, (float)(f / xmax));
				s.Points.Add(new DataPoint(p2.X*xmax, p2.Y));

				//int binCal = (int)Math.Round(f/binWidth);
				//s.Points.Add(new DataPoint(f, fft[binCal]));
				f = f + 4000f;
			}
			frPlotmodel.Series.Add(s);
			frPlotmodel.RefreshPlot(true);
			
		}
		

		
		
		const int interpFactor = 10;
		
		float[] mProcessed;
		void drawSpectrogram()
		{
			float fs = owner.Sudar.AudioSampleRate;
			float[] fft = sigProc.fft(windowLength, owner.Sudar.AudioBuf);
			float binWidth = (fs/(windowLength));
			
			plot1.Model = specPlotmodel;
			
			mProcessed = sigProc.interpolate(fft, fft.Length/2, 10);
			float plotxdiv = (fs / 2) / mProcessed.Length;
			/*
			mProcessed = new float[20000];
			for(int i=0;i<mProcessed.Length;i++) {
				mProcessed[i] = interpolate(fft, binWidth, i*plotxdiv);
			}
			 */
			
			fPlotSeries.Points.Clear();
			
			for(int i=0;i<mProcessed.Length;i++) {
				fPlotSeries.Points.Add(new DataPoint(i*plotxdiv,mProcessed[i]));
			}
			
			if(checkBox5.Checked) {
				ScatterSeries s1 = new ScatterSeries("", OxyColor.FromRgb(255,20,20), 2);
				s1.MarkerType = MarkerType.Circle;
				s1.MarkerSize = 2;
				specPlotmodel.Series.Add(s1);
				for(int i=0;i<windowLength/2;i++) s1.Points.Add(new DataPoint(i*binWidth,fft[i]));
			}

			specPlotmodel.RefreshPlot(true);
			
			float xmax = owner.Sudar.AudioSampleRate / 2;
			float calTome = Convert.ToSingle(comboBox1.SelectedItem.ToString());
			
			DataPoint Lcal = findPeak(mProcessed, calTome/xmax);
			DataPoint L250 = findPeak(mProcessed , 250.0f/xmax);

			label1.Text = String.Format("{0:F1} Hz = {1:F1} dB, {2:F2} kHz = {3:F1} dB", L250.X*xmax, L250.Y, Lcal.X*xmax/1000, Lcal.Y);
		}


		public DataPoint findPeak(float[] dat, float pos) {
			double x;
			double y;
			sigProc.findPeak(dat, pos, out x, out y);
			return new DataPoint(x,y);
		}

		enum Tmode { spectrum, wav };
		Tmode mode = Tmode.spectrum;
		
		void recalc()
		{
			
			mode = radioButton1.Checked ? Tmode.spectrum : Tmode.wav;
			
			switch(mode) {
				case Tmode.spectrum:
					drawSpectrogram();
					break;
				case Tmode.wav:
					drawWav();
					break;
			}
			
		}
		
		
		

		PlotModel wavPlotmodel;
		void drawWav()
		{
			float fs = owner.Sudar.AudioSampleRate;// 288000;//48828;
			float[] wav = owner.Sudar.AudioBuf;
			double T = (double)wav.Length/fs;

			if( wavPlotmodel == null) {
				wavPlotmodel = new PlotModel("Normalised Wave") { LegendTitle = "" };
				wavPlotmodel.Axes.Add(new LinearAxis(AxisPosition.Bottom, "time (s)") { MajorGridlineStyle = LineStyle.Solid, TickStyle = OxyPlot.TickStyle.None });
				wavPlotmodel.Axes.Add(new LinearAxis(AxisPosition.Left, -1, 1, "amplitude") { MajorGridlineStyle = LineStyle.Solid, TickStyle = OxyPlot.TickStyle.None });
			}
			
			plot1.Model = wavPlotmodel;
			LineSeries s = new LineSeries();
			wavPlotmodel.Series.Clear();

			s.Points.Clear();
			
			for(int i=0;i<wav.Length;i++) s.Points.Add(new DataPoint((float)i/fs ,wav[i]));
			
			wavPlotmodel.Series.Add(s);
			plot1.Model.RefreshPlot(true);
			
		}
		
		void Button3Click(object sender, EventArgs e)
		{
			owner.Sudar.AudioSample(checkBox6.Checked ? (512*4) : windowLength * 10 );
			recalc();
		}
		
		void CheckBox2CheckedChanged(object sender, EventArgs e)
		{
			owner.Sudar.AudioSetMute(checkBox2.Checked);
		}
		
		void CheckBox1CheckedChanged(object sender, EventArgs e)
		{
			owner.Sudar.AudioSetGain(checkBox1.Checked);
		}
		
		void CheckBox3CheckedChanged(object sender, EventArgs e)
		{
			owner.Sudar.AudioEnableCalSig(checkBox3.Checked);
		}
		
		void RadioButton1CheckedChanged(object sender, EventArgs e)
		{
			recalc();
			plot1.Model.DefaultXAxis.Reset();
			plot1.Model.DefaultYAxis.Reset();
		}
		
		void RadioButton2CheckedChanged(object sender, EventArgs e)
		{
			recalc();
		}
		
		void BetterRadioButton1Click(object sender, EventArgs e)
		{
			BetterRadioButton b =  sender as BetterRadioButton;
			if(b!=null) {
				owner.Sudar.AudioSetDivider((UInt16)b.Index);
			}
		}
		
		void ComboBox1SelectedIndexChanged(object sender, EventArgs e)
		{
			owner.Sudar.AudioSetCalFrequency(Convert.ToUInt32( comboBox1.SelectedItem.ToString()));
		}
		
		void CheckBox4CheckedChanged(object sender, EventArgs e)
		{
			timer1.Enabled = checkBox4.Checked;
		}
		
		void Timer1Tick(object sender, EventArgs e)
		{
			try {
				timer1.Enabled = false;
				owner.Sudar.AudioSample(checkBox6.Checked ? (512*4) : windowLength);
				recalc();
				timer1.Enabled = true;
			}
			catch {
				checkBox4.Checked = false;
			}
			
		}
		
		void BetterRadioButton2CheckedChanged(object sender, EventArgs e)
		{
			
		}
		
		void ComboBox2SelectedIndexChanged(object sender, EventArgs e)
		{
			
		}
		
		void BetterRadioButton1CheckedChanged(object sender, EventArgs e)
		{
			
		}
		
		void CheckBox6CheckedChanged(object sender, EventArgs e)
		{
			owner.Sudar.AudioEnableTriggerMode(checkBox6.Checked);
		}
		
		void CheckBox7CheckedChanged(object sender, EventArgs e)
		{
			owner.Sudar.AudioSetHighPass(checkBox7.Checked);
		}
		
		void ComboBox3SelectedIndexChanged(object sender, EventArgs e)
		{
			owner.Sudar.AudioSetActiveChannel((ushort)comboBox3.SelectedIndex);
		}
		
		
		//capture ref
		void Button2Click(object sender, EventArgs e)
		{
			refPlotSeries.Points.Clear();
			foreach(DataPoint p in fPlotSeries.Points) refPlotSeries.Points.Add(p);
			specPlotmodel.RefreshPlot(true);
		}

		//save REF
		void Button4Click(object sender, EventArgs e)
		{
			var sfd = new SaveFileDialog();
			sfd.Filter = "(*.ref)|*.ref";
			if(sfd.ShowDialog() == DialogResult.OK) {
				var sw = new StreamWriter(sfd.FileName, false);
				foreach(DataPoint p in refPlotSeries.Points) sw.WriteLine(p.ToString());
				sw.Close();
			}
		}

		//open REF
		void Button5Click(object sender, EventArgs e)
		{
			var ofd = new OpenFileDialog();
			ofd.Filter = "(*.ref)|*.ref";
			if(ofd.ShowDialog() == DialogResult.OK) {
				var sr = new StreamReader(ofd.FileName);
				LineSeries ls = Control.ModifierKeys == Keys.Shift ? fPlotSeries : refPlotSeries;
				ls.Points.Clear();
				while(true) {
					var s = sr.ReadLine();
					if(s==null) break;
					var ss = s.Split(' ');
					var dp = new DataPoint(Convert.ToDouble( ss[0] ), Convert.ToDouble( ss[1] ));
					ls.Points.Add(dp);
				}
				sr.Close();
			}
			specPlotmodel.RefreshPlot(true);
		}
		
		//Clear REF
		void Button6Click(object sender, EventArgs e)
		{
			refPlotSeries.Points.Clear();
			specPlotmodel.RefreshPlot(true);
		}
	}
}
