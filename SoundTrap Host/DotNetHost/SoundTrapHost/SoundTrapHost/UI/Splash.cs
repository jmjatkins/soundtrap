/* 
* 	SoundTrap Software v1.0
*  
*	Copyright (C) 2011-2014, John Atkins and Mark Johnson
*
*	This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
*
*	This file is part of the SoundTrap software. SoundTrap is an acoustic recording
*	system intended for underwater acoustic measurements. This component of the 
*	SoundTrap project is free software: you can redistribute it and/or modify it 
*	under the terms of the GNU General Public License as published by the Free Software 
*	Foundation, either version 3 of the License, or any later version.
*
*	The SoundTrap software is distributed in the hope that it will be useful, but 
*	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
*	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License along with this 
*	code. If not, see <http://www.gnu.org/licenses/>.
*/


using System;
using System.Drawing;
using System.Windows.Forms;

namespace SudarHost
{
	/// <summary>
	/// Description of Splash.
	/// </summary>
	public partial class Splash : Form
	{
		static Splash splashScreen;
		public Splash()
		{
			InitializeComponent();
			#if !DEBUG
			TopMost         = true;
			#endif
			FormBorderStyle = FormBorderStyle.None;
			StartPosition   = FormStartPosition.CenterScreen;
			ShowInTaskbar   = true;
			label3.Text =  System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
		}

		public static Splash SplashScreen {
			get {
				return splashScreen;
			}
			set {
				splashScreen = value;
			}
		}

		public static bool OkButtonIsVisible {
			set {
				splashScreen.button1.Visible = value;
			}
		}
		
		public static void ShowSplashScreen( bool modal )
		{
			if(splashScreen == null)
				splashScreen = new Splash();
			if(modal) {
				splashScreen.ShowDialog();
			} else splashScreen.Show();
			splashScreen.Refresh();
		}

		static public string Status {
			set {
				splashScreen.label2.Text = value;
				splashScreen.Refresh();
			}
		}
		
		void Button1Click(object sender, EventArgs e)
		{
			this.Hide();
		}
	}
}
