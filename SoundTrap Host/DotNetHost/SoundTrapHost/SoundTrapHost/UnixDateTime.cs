﻿/* 
 * 	SoundTrap Software v1.0
 *
 *	Copyright (C) 2011-2014, John Atkins and Mark Johnson
 *
 *	This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
 *
 *	This file is part of the SoundTrap software. SoundTrap is an acoustic recording
 *	system intended for underwater acoustic measurements. This component of the
 *	SoundTrap project is free software: you can redistribute it and/or modify it
 *	under the terms of the GNU General Public License as published by the Free Software
 *	Foundation, either version 3 of the License, or any later version.
 *
 *	The SoundTrap software is distributed in the hope that it will be useful, but
 *	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License along with this
 *	code. If not, see <http://www.gnu.org/licenses/>.
 */

using System;

namespace SudarHost
{
	/// <summary>
	/// Description of UnixDateTime.
	/// </summary>
	public class UnixDateTime
	{
		DateTime epoc = new DateTime(1970, 1, 1);
		UInt32 unixTicks;
		public UnixDateTime(UInt32 unixTicks)
		{
			this.unixTicks = unixTicks;
		}
		public UnixDateTime(DateTime dateTime)
		{
			this.unixTicks = (UInt32)(dateTime - new DateTime(1970, 1, 1)).TotalSeconds;
		}
		
		public DateTime DateTime {
			get { return epoc + TimeSpan.FromSeconds(unixTicks); }
		}
		
		public UInt32 UnixTicks {
			get {
				return unixTicks;
			}
		}
		
		static public UInt32 ConvertToUnixTime(DateTime dateTime)
		{
			return (UInt32)(dateTime - new DateTime(1970, 1, 1)).TotalSeconds;
		}
		
		static public DateTime ConvertToDateTime(UInt32 unixTime)
		{
			DateTime epoch = new DateTime(1970, 1, 1);
			DateTime time;
			try {
				time = epoch + new TimeSpan (0, 0, (int)unixTime);
			}
			catch{
				time = epoch; 
			}
			return 	time;
		}


	}
}
