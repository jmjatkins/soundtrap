﻿
/* 
 * 	SoundTrap Software v1.0
 *
 *	Copyright (C) 2011-2014, John Atkins and Mark Johnson
 *
 *	This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
 *
 *	This file is part of the SoundTrap software. SoundTrap is an acoustic recording
 *	system intended for underwater acoustic measurements. This component of the
 *	SoundTrap project is free software: you can redistribute it and/or modify it
 *	under the terms of the GNU General Public License as published by the Free Software
 *	Foundation, either version 3 of the License, or any later version.
 *
 *	The SoundTrap software is distributed in the hope that it will be useful, but
 *	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License along with this
 *	code. If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using Exocortex.DSP;

namespace SudarHost
{
	/// <summary>
	/// Description of sigProc.
	/// </summary>
	public static class sigProc
	{
		
		static public bool findPeak(float[] dat, float pos, out double x, out double y)
		{
			float index = pos * dat.Length;
			//DataPoint r = new DataPoint();
			float bestpeak = 0.0f;
			//float fs = owner.Sudar.AudioSampleRate;
			//float binWidth = (fs/2/(dat.Length));
			bool up = (dat[1] > dat[0]);
			float bestdist = 1000000;
			float bestValue = 0.0f;
			
			for (int bin = 2; bin < dat.Length; bin++) {
				bool nowUp = dat[bin] > dat[bin - 1];
				if (!nowUp && up) {
					// Local maximum.  Find actual value by cubic interpolation
					int leftbin = bin - 2;
					if (leftbin < 1)
						leftbin = 1;
					float valueAtMax = 0.0f;
					float max = leftbin + sigProc.CubicMaximize(dat[leftbin],
					                                            dat[leftbin + 1],
					                                            dat[leftbin + 2],
					                                            dat[leftbin + 3], ref valueAtMax);

					if (Math.Abs(max - index) < bestdist) {
						bestpeak = max;
						bestdist = Math.Abs(max - index);
						bestValue = valueAtMax;
						if (max > index)
							break;
					}
				}
				up = nowUp;
			}
			
			x = (double)(bestpeak / dat.Length);
			y = bestValue;
			return true;
		}

		
		static public float CubicMaximize(float y0, float y1, float y2, float y3, ref float max)
		{
			// Find coefficients of cubic
			float a, b, c, d;
			a = y0 / -6.0f + y1 / 2.0f - y2 / 2.0f + y3 / 6.0f;
			b = y0 - 5.0f * y1 / 2.0f + 2.0f * y2 - y3 / 2.0f;
			c = -11.0f * y0 / 6.0f + 3.0f * y1 - 3.0f * y2 / 2.0f + y3 / 3.0f;
			d = y0;
			// Take derivative
			float da, db, dc;
			da = 3 * a;
			db = 2 * b;
			dc = c;
			// Find zeroes of derivative using quadratic equation
			float discriminant = db * db - 4 * da * dc;
			if (discriminant < 0.0)
				return -1.0f;              // error

			float x1 = ((-1.0f *db) + (float)Math.Sqrt(discriminant)) / (2.0f * da);
			float x2 = ((-1.0f *db) - (float)Math.Sqrt(discriminant)) / (2.0f * da);

			// The one which corresponds to a local _maximum_ in the
			// cubic is the one we want - the one with a negative
			// second derivative
			float dda = 2 * da;
			float ddb = db;
			if (dda * x1 + ddb < 0)
			{
				max = a*x1*x1*x1+b*x1*x1+c*x1+d;
				return x1;
			}
			else
			{
				max = a*x2*x2*x2+b*x2*x2+c*x2+d;
				return x2;
			}
		}
		
		static public float cubicInterpolate(float y0, float y1, float y2, float y3, float x)
		{
			float a, b, c, d;
			a = y0 / -6.0f + y1 / 2.0f - y2 / 2.0f + y3 / 6.0f;
			b = y0 - 5.0f * y1 / 2.0f + 2.0f * y2 - y3 / 2.0f;
			c = -11.0f * y0 / 6.0f + 3.0f * y1 - 3.0f * y2 / 2.0f + y3 / 3.0f;
			d = y0;

			float xx = x * x;
			float xxx = xx * x;

			return (a * xxx + b * xx + c * x + d);
		}

		static public float interpolate(float[] dat, float binwidth, float freq)
		{
			float bin = freq / binwidth;
			int ibin = (int)(bin) - 1;
			if(ibin<0) ibin = 0;

			return cubicInterpolate(dat[ibin],
			                        dat[ibin + 1],
			                        dat[ibin + 2],
			                        dat[ibin + 3], bin - ibin);

		}
		
		static public float interpolate(float[] dat, float bin)
		{
			int ibin = (int)(bin) - 1;
			if(ibin<0) ibin = 0;

			return cubicInterpolate(dat[ibin],
			                        dat[ibin + 1],
			                        dat[ibin + 2],
			                        dat[ibin + 3], bin - ibin);

		}

		
		
		static public float[] interpolate(float[] dat, int length, int m ) {
			float[] result = new float[dat.Length*m];
			//float plotxdiv = (fs / 2) / mProcessed.Length;
			for(int i=0; i< result.Length; i++) {
				result[i] = interpolate(dat, ((float)i/(float)result.Length) * length);
			}
			return result;
		}
		
		static public float blackmanWindow(int n, int N)
		{
			return (float)(0.42f - (0.5f * (Math.Cos(2 * Math.PI * n / (N - 1)))) +  (0.08f * (Math.Cos(4 * Math.PI * n / (N - 1)))) );
		}


		static public float hammingWindow(int n, int N)
		{
			return (float)(0.54f - (0.46f * (Math.Cos(2 * Math.PI * n / (N - 1)))));
		}

		static public float hanningWindow(int n, int N)
		{
			return (float)(0.50f - (0.50f * (Math.Cos(2 * Math.PI * n / (N - 1)))));
		}
		
		static public void ComputeFFTPolarMag(float[] samples, float[] fft)
		{
			Array.Copy(samples, fft, samples.Length);
			Fourier.RFFT(fft, samples.Length, FourierDirection.Forward);
			
			for (int i = 1; i < samples.Length / 2; i++)
				fft[i] = (fft[i * 2] * fft[i * 2]) +  (fft[i * 2 + 1] * fft[i * 2 + 1]);
			
			fft[0] *= 2;
		}

		
		static public void detrend(ref float[] data)
		{
			float mean = 0;
			for(int i=0; i<data.Length; i++){
				mean += data[i];
			}
			mean /= data.Length;

			for(int i=0; i<data.Length; i++){
				data[i] -= mean;
			}
			
		}

		static public float rms(float[] data)
		{
			double res = 0;
			for(int i=0; i<data.Length; i++){
				res += (data[i]  *data[i]);
			}
			return (float) Math.Sqrt(res/data.Length);
		}

		
		static public float[] fft(int windowLength, float[] data )
		{
			float[] window = new float[windowLength];
			float[] fft = new float[windowLength];
			float[] avFFT = new float[windowLength];
			
			int start = 0;
			int windows = 0;
			while (start + windowLength <= data.Length) {
				for(int i=0;i<windowLength;i++) window[i] = data[start+i] * hammingWindow(i,windowLength);
				//for(int i=0;i<windowLength;i++) window[i] = data[start+i];
				ComputeFFTPolarMag(window,fft);
				for(int i=0;i<windowLength;i++) avFFT[i] += fft[i];
				start += windowLength/2;
				windows++;
			}
			/*
			float wss = 0;
			for(int i=0;i<windowLength;i++) wss +=  hanningWindow(i,windowLength);
			wss = 4.0f / (wss*wss); 
			*/
			float wss = 0.0f;
			
			//correct for window
			for(int i=0;i<windowLength;i++) wss +=  (float)Math.Pow(hammingWindow(i,windowLength), 2);
			wss = (float)( wss/windowLength );
			wss = 1.0f/wss;
			
			float scale = wss/(float)windows;
			
			//+3 dB for single sided spectrum // 
			//Log(WL) correct for nfft
			for(int i=0;i<windowLength;i++) avFFT[i]= 3 - ( 20 * (float)Math.Log10(windowLength) ) + 10 * (float)Math.Log10(avFFT[i]*scale) ;
			
			return avFFT;
		}
		
		static public float[] Chebyshev(float[] inbuf)
		{
			//see Smith Pg 337 - low pass fc = 0.30
			float[] outbuf = new float[inbuf.Length];
			float a0 = 3.849163E-01f;
			float a1 = 7.698326E-01f;
			float a2 = 3.849163E-01f;
			float b1 = -3.249116E-01f;
			float b2 = -2.147536E-01f;
			for(int i=4;i<inbuf.Length;i++) {
				outbuf[i] = (a0*inbuf[i]) + (a1*inbuf[i-1]) + (a2*inbuf[i-2]) + (b1 * outbuf[i-1]) + (b2 * outbuf[i-2]);
			}
			return outbuf;
		}
				
		static public float[] IIR(float[] inbuf)
		{
			//see Smith Pg 326
			float[] outbuf = new float[inbuf.Length];
			float x = 0.8f;
			float a0 = (float)Math.Pow(1-x, 4);
			float b1 = 4 * x;
			float b2 = -6 * (x * x);
			float b3 = 4 * (x*x*x);
			float b4 = (float)Math.Pow(x,4) * -1;
			
			for(int i=4;i<inbuf.Length;i++) {
				outbuf[i] = (a0*inbuf[i]) + (b1 * outbuf[i-1]) + (b2 * outbuf[i-2]) + (b3 * outbuf[i-3]) + (b4 * outbuf[i-4]);
			}
			return outbuf;
		}

	}
}
