﻿/* 
 * 	SoundTrap Software v1.0
 *
 *	Copyright (C) 2011-2014, John Atkins and Mark Johnson
 *
 *	This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
 *
 *	This file is part of the SoundTrap software. SoundTrap is an acoustic recording
 *	system intended for underwater acoustic measurements. This component of the
 *	SoundTrap project is free software: you can redistribute it and/or modify it
 *	under the terms of the GNU General Public License as published by the Free Software
 *	Foundation, either version 3 of the License, or any later version.
 *
 *	The SoundTrap software is distributed in the hope that it will be useful, but
 *	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License along with this
 *	code. If not, see <http://www.gnu.org/licenses/>.
 */

using System;

namespace SudarHost
{
	/// <summary>
	/// Description of SudarConfig.
	/// </summary>
	/// 
	
	enum AuxSensor {
		Accelerometer = 0x01,
		Temeprature = 0x02,
		Pressure = 0x04
	};
	
	
	enum CONFIG_FLAGS {
		DISABLE_CAL_ROUTINE = 0x0001,
	}
	
	public class SudarConfig {
		public UInt32 startTriggerMode;
		public UInt32 runMode;
		public UInt32 startTime;
		public UInt32 onTime;
		public UInt32 onTimeScale;
		public UInt32 onceEveryTime;
		public UInt32 onceEveryTimeTimeScale;
		public UInt32 sampleMode;
		public UInt32 decimator;
		public UInt32 auxSensorEnable;
		public UInt32 syncMode;
		public UInt32 gain;
		public UInt32 compressionMode;
		public UInt32 auxSensorInterval;
		public UInt32 flags;
		public UInt32 hPass;
		public UInt16 serialLogMode; //0 =DateTimeOffset, 1=232, 2=485
		public UInt16 channelEnable; 
		public UInt32 DetectParam1;
		public UInt32 DetectParam2;
		public UInt32 DetectParam3;
		public UInt32 DetectParam4;
		public UInt32 DetectParam5;
		public UInt32 DetectParam6;
		public UInt32 DetectParam7;
		public UInt32 DetectParam8;
		public UInt32 DetectParam9;
		public UInt32 DetectParam10;
		public UInt16 unused10;
		public UInt16 crc;

		public byte[] Serialise()
		{
			Serializer serializer = new Serializer(EndianMode.BIG_16BIT);
			return serializer.Serialize(this);
		}
		
		UInt16 calcCrc()
		{
			Serializer serializer = new Serializer(EndianMode.BIG_16BIT);
			byte[] binData = serializer.Serialize(this);
			return Crc.crc.calc( binData, binData.Length-2);
		}
		
		public void UpdateCrc()
		{
			crc = calcCrc();
		}
		
		public bool CheckCrc()
		{
			return crc == calcCrc();
		}
	}
}
