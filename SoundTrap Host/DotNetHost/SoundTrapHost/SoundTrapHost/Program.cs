﻿/* 
 * 	SoundTrap Software v1.0
 *
 *	Copyright (C) 2011-2014, John Atkins and Mark Johnson
 *
 *	This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
 *
 *	This file is part of the SoundTrap software. SoundTrap is an acoustic recording
 *	system intended for underwater acoustic measurements. This component of the
 *	SoundTrap project is free software: you can redistribute it and/or modify it
 *	under the terms of the GNU General Public License as published by the Free Software
 *	Foundation, either version 3 of the License, or any later version.
 *
 *	The SoundTrap software is distributed in the hope that it will be useful, but
 *	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License along with this
 *	code. If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Windows.Forms;
using System.Threading;

namespace SudarHost
{
	/// <summary>
	/// Class with program entry point.
	/// </summary>

	internal sealed class Program
	{
		/// <summary>
		/// Program entry point.
		/// </summary>
		static DspBootloader bootloader;

		[STAThread]
		private static void Main(string[] args)
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			UnhandledExceptionHandler.UnhandledExceptionHandler.Init("SoundTrapHost");

			try {
				AppState.instance.mode = (AppState.Mode)SudarHost.Properties.Settings.Default.AppMode;
			}
			catch {
				AppState.instance.mode = AppState.Mode.normal;
			}
			
			Splash.ShowSplashScreen(false);
			Splash.Status = "Intialising";
			Thread.Sleep(1000);

			//initialise usb bootloader
			bootloader = new DspBootloader();
			bootloader.Init();

			Splash.SplashScreen.Hide();

			Application.Run(new MainForm());
		}
	}
	
}