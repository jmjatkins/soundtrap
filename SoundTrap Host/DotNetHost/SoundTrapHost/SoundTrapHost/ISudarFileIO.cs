﻿/*
 * Created by SharpDevelop.
 * User: John
 * Date: 21/03/2018
 * Time: 2:04 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace SudarHost
{
	
	public class SudarFsParams{
		public UInt32 pageSize;
		public UInt32 PagesPerBlock;
		public UInt32 BlocksPerFile;
		public UInt32 noOfPages;
		public UInt32 PagesPerFile{ get { return PagesPerBlock * BlocksPerFile;}}
		public UInt32 FileBoundaries{ get { return noOfPages / PagesPerFile;}}
		public UInt32 BytesPerBloc{ get { return PagesPerBlock * pageSize; }}
		public UInt16 CapacityGB{
			get {
				double val = (double)noOfPages * pageSize;
				return (UInt16)Math.Round( val / 1000000000 );
			}
		}
	}
	
	public interface ISudarFileIO
	{
		void Open(string drive);
		bool SetFileReadPageAddress(uint address);
		void SetFileReadPageIncrement(uint inc);
		void FlashRead(out byte[] buf, uint len);
		
		SudarFsParams FsParams {
			get;
		}
		
		DateTime Time {
			get;
		}
		
		string SerialNumber {
			get;
		}
		
		int PercentFileSystemUsed {
			set;
		}
		
		void GetStatus();
		void ResetFsPipe();
	}

}
