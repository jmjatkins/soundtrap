﻿/* 
 * 	SoundTrap Software v1.0
 *
 *	Copyright (C) 2011-2014, John Atkins and Mark Johnson
 *
 *	This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
 *
 *	This file is part of the SoundTrap software. SoundTrap is an acoustic recording
 *	system intended for underwater acoustic measurements. This component of the
 *	SoundTrap project is free software: you can redistribute it and/or modify it
 *	under the terms of the GNU General Public License as published by the Free Software
 *	Foundation, either version 3 of the License, or any later version.
 *
 *	The SoundTrap software is distributed in the hope that it will be useful, but
 *	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License along with this
 *	code. If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Crc;
using System.IO;
using System.ComponentModel;
using LibUsbDotNet;
using LibUsbDotNet.Main;
using System.Text;
using System.Threading;

namespace SudarHost
{
	/// <summary>
	/// Description of Sudar.
	/// </summary>
	public partial class Sudar : SudarUsbIO, ISudarFileIO
	{
		const int defaultTimeout = 500;

		SudarControl control;

		public SudarControl Control {
			get {
				if(control ==null) {
					control = new SudarControl();
					control.Sudar = this;
				}
				return control;
			}
		}

		
		private UsbRegistry usbRegistry;
		public Sudar( UsbRegistry usbRegistry )
		{
			this.usbRegistry = usbRegistry;
		}
		
		
		public bool BatteryOkForFsRead{
			get {
				return (model >= Model.ST600STD) || ( LastStatusPacket !=null)  && (LastStatusPacket.battVoltage > 3600);
			}
		}
		
		public void RefreshId()
		{
			SendCommand(SudarCommands.GET_ID, defaultTimeout);
		}
		
		public void Open(string unused)
		{
			base.Open(usbRegistry);
			try {
				RefreshId();
			}
			catch {
			}
		}
		
		public string InstrumentType {
			get { return "SoundTrap"; }
		}

		public string InstrumentDescription {
			get { return String.Format("{0} {1}", InstrumentType, SerialNumber); }
		}

		private UInt64 hwSerial;
		public string HwSerial
		{
			get { return hwSerial.ToString();}
		}
		
		UInt64 serial;
		public string SerialNumber {
			get { return serial.ToString();}//ToString("X8"); }
		}
		

		internal enum  SudarCommands {
			STATUS_READ = 4,
			STATUS_RESULT = 14,
			FLASH_FORMAT = 5,
			FLASH_WRITE = 6,
			FLASH_SET_READ_PAGE_ADDRESS = 7,
			SET_FLASH_WRITE_PAGE_ADDRESS = 8,
			SERIAL_EEPROM_WRITE = 9,
			SERIAL_EEPROM_READ = 10,
			GET_ID = 13,
			GET_FS_PARAMS = 14,
			SET_RTC_TIME = 15,
			FLASH_SET_READ_PAGE_INCREMENT = 16,
			SET_CONFIG = 17,
			GET_CONFIG = 18,
			GET_SENSORS = 19,
			GET_MSP_REG = 20,
			REBOOT = 21,
			GET_SW_VER = 22,
			CRC_SPEED_TEST = 23,
			MSP_SEND = 24,
			MSP_SET_TEST_VAL = 25,
			MSP_SET_TEST_DIR = 26,
			AUDIO_TEST_ENABLE = 27,
			AUDIO_TEST_SAMPLE = 28,
			AUDIO_SET_GAIN = 29,
			AUDIO_SET_MUTE = 30,
			AUDIO_ENABLE_CAL_SIG = 31,
			AUDIO_READ_HARDWARE_ID = 32,
			AUDIO_SET_DIVIDER = 33,
			AUDIO_SET_CAL_TONE_F = 44,
			FLASH_GET_CARD_INFO = 45,
			AUDIO_ENABLE_TRIGGER = 46,
			MSP_ENABLE_BATT_CHARGE = 47,
			LED_FLASH = 48,
			AUDIO_SET_HPASS = 49,
			UART_TX_TEST = 50,
			UART_RX_TEST = 51,
			UART_SELECT_TRANS = 52,
			UART_ENABLE_TX = 53,
			ACCEL_TEST = 54,
			AUDIO_SET_ACTIVE_CHANNEL = 55,
			ADC_I2C_READ = 56,
			SD_SET_CARD = 57,
			SD_SCAN_CARDS = 58,
			SERIAL_FLASH_ERASE_BLOCK = 59,
			SERIAL_FLASH_WRITE_PAGE = 60
				
		};
		
		public void Reboot()
		{
			SendCommand(SudarCommands.REBOOT, defaultTimeout);
		}
		
		public class TsudarStatus {
			public UInt32 resetRunTime;
			public UInt32 totalRunTime;
			public UInt32 unixTime;
			public UInt16 deviceType;
			public Int16 temperature;
			public UInt16 audioHardwareId;
			public UInt16 commsErrorLog;
			public UInt16 setupSaved;
			public UInt16 nvmInitGood;
			public UInt16 swVer;
			public UInt16 lastCommandStatus;
			public UInt16 battVoltage;
			public UInt16 extBattVoltage;
		}
		
		public enum Model
		{
			ST202STD = 0x0000,
			ST202HF = 0x0001,
			ST300STD = 0x0100,
			ST300HF = 0x0101,
			ST4300STD = 0x0200,
			ST4300HF = 0x0201,
			ST4300 = 0x0202,
			ST500STD = 0x0300,
			ST500HF = 0x0301,
			ST600STD = 0x0400,
			ST600HF = 0x0401,
			Unknown = 0xffff
		}

		public Model model {
			get {
				try {
					return (Model)lastStatusPacket.deviceType;
				}
				catch {
					return Model.Unknown;
				}
			}
		}
		
		public string modelName {
			get {
				return model.ToString();
			}
		}
		
		public class CrcSpeedTestResult {
			public UInt32 et;
			public UInt16 crc;
		}
		
		public class TmspReg
		{
			public byte		command;		// 0
			public byte		mode;			// 1
			public UInt16	flags;			// 2
			public UInt16	unused;			// 4
			public UInt16	sws ;			// 6
			public UInt16	tempr;			// 8
			public UInt16	vbs ;			// 10
			public UInt16	hreg ;			// 12
			public UInt32	tick ;			// 14
			public UInt32	scratch0 ;		// 18
			public UInt16	scratch1 ;		// 22
			public UInt16	swver ;			// 24
			public UInt32	rtime ;			// 26
			public UInt16	rcnt ;			// 30
			public UInt16	sws_thrsh ;		// 32
			public UInt32 	nextStartTime;	// 34
			public UInt16 	irCommand;		// 38
			//public UInt16	scratch[44] ;	// 40
		}
		
		public int[,] ST600batChannels = new int[3,4]{{1,13,9, 5}, {2, 14, 10, 6}, {3, 15, 11, 7}};
		public int[] ST600batChannelsTestOrder = new int[12] {1,2,3, 15,14,13, 9,10,11, 7,6,5};


		public class TsudarSensorData {
			public UInt16 vb ;
			public Int16 tempr ;
			public UInt16 sws ;
			public UInt16 extvb ;
		}
		TsudarSensorData sensorData;
		public Sudar.TsudarSensorData SensorData {
			get { return sensorData; }
			set { sensorData = value; }
		}
		
		TsudarStatus lastStatusPacket;
		public Sudar.TsudarStatus LastStatusPacket {
			get { return lastStatusPacket; }
		}
		
		private string firmwareVersion;
		public string FirmwareVersion {
			get { return firmwareVersion; }
			set { firmwareVersion = value; }
		}
		
		private string flashCardInfo;
		
		private string uartRxBuf;
		
		public string FlashCardInfo {
			get { return flashCardInfo; }
			set { flashCardInfo = value; }
		}
		
		private Int16 audioHardwareId;
		public short AudioHardwareId {
			get { return audioHardwareId; }
			set { audioHardwareId = value; }
		}
		
		
		
		
		private TmspReg mspReg;
		public UInt16 MspSwVer {
			get {
				return mspReg == null ?  (UInt16)0 : mspReg.swver;
			}
		}
		
		public SudarConfig CurrentConfig = new SudarConfig();
		
		bool ProcessPacket(ComsHeader header, byte[] buf )
		{
			Serializer serializer = new Serializer(EndianMode.BIG_16BIT);
			switch((SudarCommands)header.command) {
				case SudarCommands.GET_ID:
					hwSerial = BitConverter.ToUInt32(new byte[] {buf[0], buf[1], buf[2],buf[3]}, 0);
					//serialOldFormat = BitConverter.ToUInt32(new byte[] {buf[0], buf[1], buf[4],buf[5]}, 0);
					
					var id1 = BitConverter.ToUInt32( new byte[] {buf[10], buf[11], buf[8], buf[9]}, 0);
					var id2 = BitConverter.ToUInt32( new byte[] {buf[14], buf[15], buf[12], buf[13]}, 0);
					
					if( id1 == (~id2 &0xffffffff)) {
						serial = id1;
					}
					else serial = hwSerial; //no device serial in eeprom, assume pre 2019 device
					break;
					
				case SudarCommands.GET_FS_PARAMS:
					fsParams = (SudarFsParams)serializer.Deserialize(typeof(SudarFsParams), new MemoryStream(buf));
					break;
				case SudarCommands.SET_RTC_TIME:
					UInt16 success = (UInt16)(buf[0] + (buf[1] << 8));
					break;
				case SudarCommands.GET_CONFIG:
					CurrentConfig = (SudarConfig)serializer.Deserialize(typeof(SudarConfig), new MemoryStream(buf));
					OnconfigChanged(EventArgs.Empty);
					break;
				case SudarCommands.GET_SENSORS:
					sensorData = (TsudarSensorData)serializer.Deserialize(typeof(TsudarSensorData), new MemoryStream(buf));
					break;
				case SudarCommands.GET_MSP_REG:
					mspReg = (TmspReg)serializer.Deserialize(typeof(TmspReg), new MemoryStream(buf));
					break;
				case SudarCommands.GET_SW_VER:
					firmwareVersion = Encoding.Unicode.GetString(buf);
					firmwareVersion = firmwareVersion.Replace('\0','\n');
					break;
					
				case SudarCommands.STATUS_READ:
					lastStatusPacket = (TsudarStatus)serializer.Deserialize(typeof(TsudarStatus), new MemoryStream(buf));
					Time = UnixDateTime.ConvertToDateTime( lastStatusPacket.unixTime );
					OnStatusDataChanged(EventArgs.Empty);
					return lastStatusPacket.lastCommandStatus == 1;
				case SudarCommands.CRC_SPEED_TEST:
					CrcSpeedTestResult r = (CrcSpeedTestResult)serializer.Deserialize(typeof(CrcSpeedTestResult), new MemoryStream(buf));
					break;
				case SudarCommands.AUDIO_TEST_SAMPLE:
					handleAudioCaptured();
					break;
				case SudarCommands.AUDIO_READ_HARDWARE_ID:
					audioHardwareId = BitConverter.ToInt16(new byte[] {buf[0], buf[1]}, 0);
					break;
				case SudarCommands.FLASH_GET_CARD_INFO:
					flashCardInfo = Encoding.Unicode.GetString(buf);
					break;
				case SudarCommands.UART_RX_TEST:
					uartRxBuf = Encoding.Unicode.GetString(buf);
					break;
					
				case SudarCommands.ADC_I2C_READ:
					adcValues = (short[])serializer.Deserialize(adcValues.GetType(), new MemoryStream(buf));
					break;
				case SudarCommands.SD_SCAN_CARDS:
					cardData = ( TcardData )serializer.Deserialize(typeof(TcardData), new MemoryStream(buf));
					break;
			}
			return true;
		}
		
		
		DateTime time;
		public DateTime Time {
			get { return time; }
			set {
				time = value;
			}
		}
		
		public int BaseSampleRate
		{
			get {
				switch(model) {
					case Model.ST202STD:
						return 288;
					case Model.ST202HF:
						return 576;
					case Model.ST300STD:
						return 288;
					case Model.ST300HF:
						return 576;
					case Model.ST500STD:
						return 288;
					case Model.ST500HF:
						return 576;
					case Model.ST4300STD:
						return 288;
					case Model.ST4300HF:
						return 384;
					case Model.ST600STD:
						return 192;
					case Model.ST600HF:
						return 384;
					default:
						return 288;
						
				}
			}
		}
		
		
		bool waitForResponse(out ComsHeader header, out byte[] buf , int timeout )
		{
			Serializer serializer = new Serializer(EndianMode.BIG_16BIT);
			int bytesRead;
			byte[] rxBuf;
			buf = null;
			header = null;
			try {
				Read(out rxBuf, ComsHeader.size, out bytesRead, timeout);
				if(bytesRead == ComsHeader.size) {
					header = serializer.Deserialize( typeof(ComsHeader), new MemoryStream(rxBuf)) as ComsHeader;
					if(header.dataLength > 0) {
						Read(out buf, header.dataLength, out bytesRead, timeout);
					}
					return true;
				} else return false;
			}
			catch {
				return false;
			}
		}
		
		private object semaphore = new object();
		internal bool SendCommand(SudarCommands command, int timeout, params object[] paramlist)
		{
			lock(semaphore) {
				Serializer serializer = new Serializer(EndianMode.BIG_16BIT);
				byte[] buf = serializer.Serialize(paramlist);
				ushort c = Crc.crc.calc(buf, buf.Length);
				ComsHeader cp = new ComsHeader(0, (ushort)command,(ushort)buf.Length, c);
				FlushReadBuf();
				Write((cp.CRCandSerialize()).Concat(buf).ToArray(), timeout);
				while( waitForResponse(out cp, out buf, timeout) ) {
					if(cp.CheckCRC()){
						if(cp.dataLength >0) {
							if( crc.calc(buf, buf.Length) == cp.dataCRC ) {
								return ProcessPacket(cp, buf);
							} else break;
						} else return ProcessPacket(cp, new byte[0]);;
					} else break;
				}
				return false;
			}
		}
		
		public Int16 GetAudioId()
		{
			SendCommand(SudarCommands.AUDIO_READ_HARDWARE_ID, defaultTimeout);
			return audioHardwareId;
		}
		
		public bool LedFlash()
		{
			return SendCommand(SudarCommands.LED_FLASH, defaultTimeout);
		}
		
		
		public void GetStatus()
		{
			SendCommand(SudarCommands.STATUS_READ, defaultTimeout);
		}
		
		public bool mspSend( UInt16[] data )
		{
			return SendCommand(SudarCommands.MSP_SEND, 1000, (UInt16) data.Length, data);
		}
		
		public bool mspSetTestVal( UInt16 val )
		{
			return SendCommand(SudarCommands.MSP_SET_TEST_VAL, defaultTimeout*2, val);
		}
		
		public bool mspSetTestDir( UInt16 val )
		{
			return SendCommand(SudarCommands.MSP_SET_TEST_DIR, defaultTimeout, val);
		}
		
		public bool sdSetCardNo( UInt16 val )
		{
			SendCommand(SudarCommands.SD_SET_CARD, defaultTimeout, val);
			//Thread.Sleep(500);
			return SendCommand(SudarCommands.GET_FS_PARAMS, defaultTimeout);
		}
		
		public void GetmspReg()
		{
			SendCommand(SudarCommands.GET_MSP_REG, defaultTimeout);
		}
		
		public void GetSensors()
		{
			SendCommand(SudarCommands.GET_SENSORS, defaultTimeout);
		}
		
		public string GetFirmwareVersion()
		{
			SendCommand(SudarCommands.GET_SW_VER, defaultTimeout);
			return firmwareVersion;
		}
		
		public void SetFileWritePageAddress(UInt32 address)
		{
			SendCommand(SudarCommands.SET_FLASH_WRITE_PAGE_ADDRESS, defaultTimeout, address);
		}
		
		public bool SetFileReadPageAddress(UInt32 address)
		{
			byte[] buf;
			if(SendCommand(SudarCommands.FLASH_SET_READ_PAGE_ADDRESS, defaultTimeout, address) ) {
				FlashRead(out buf, 1024); //flush buffer
				return true;
			}
			else return false;
		}
		
		public void SetFileReadPageIncrement(UInt32 increment)
		{
			SendCommand(SudarCommands.FLASH_SET_READ_PAGE_INCREMENT, defaultTimeout, increment);
		}
		
		public void FlashWrite(byte[] buf)
		{
			WriteToFs(buf);
		}

		public void FlashRead(out byte[] buf, UInt32 bytesToRead)
		{
			ReadFromFs(out buf, bytesToRead);
		}
		
		public void FlashReset() {
			ResetFsPipe();
		}
		

		public void SerialEepromWrite(ushort address, byte[] buf)
		{
			if(buf.Length > 64) throw new Exception("SerialFlashWrite currenlty limmited to 64 bytes");
			SendCommand(SudarCommands.SERIAL_EEPROM_WRITE, defaultTimeout, address, (ushort)buf.Length, buf);
		}
		
		
		public static void SwapEndian(byte[] data)
		{
			for (int i = 0; i < data.Length; i += 2)
			{
				byte b = data[i];
				data[i] = data[i + 1];
				data[i + 1] = b;
			}
		}

		public void UpdateMspFirmware(string filename, SudarFile.ProgressCallback progressCallback)
		{
			mspProg m = new mspProg(this);
			m.mspprog(filename, progressCallback);
		}
		
		public void UpdateApplicationFirmware(string fileName, SudarFile.ProgressCallback progressCallback) {
			byte[] buf =new byte[512];
			FileInfo fi = new FileInfo(fileName);
			long byesLeftToWrite = fi.Length;
			int pages = (int)(byesLeftToWrite / 512);

			SetFileWritePageAddress(0);

			System.IO.FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			for(int i=0;i<pages;i++) {
				fs.Read(buf, 0, 512);
				SwapEndian(buf);
				FlashWrite(buf);
				byesLeftToWrite -= 512;
				if(pages > 0) progressCallback("", i / (pages/100));
			}
			
			if(byesLeftToWrite > 0) {
				fs.Read(buf, 0, (int)byesLeftToWrite);
				for(int i=(int)byesLeftToWrite; i<512; i++) buf[i] = 0xFF;
				SwapEndian(buf);
				FlashWrite(buf);
			}
			fs.Close();
		}
		
		class PlayBackHeader {
			public UInt32 majic;
			public UInt32 fileLength;
			public UInt16 crc;
			public void SetCrc() {
				Serializer serializer = new Serializer(EndianMode.BIG_16BIT);
				byte[] binData = serializer.Serialize(this);
				crc = Crc.crc.calc( binData, binData.Length-2);
			}
		}
		
		public void UploadPlaybackFile(string fileName, int fileNo, SudarFile.ProgressCallback progressCallback)
		{
			PlayBackHeader pbh = new Sudar.PlayBackHeader();
			
			Serializer s = new Serializer(EndianMode.BIG_16BIT);
			try {
				WavFileReader wfr = new WavFileReader(fileName);
				int maxFileSize = (int)(fsParams.pageSize*fsParams.PagesPerFile);
				/*
				if(wfr.samples * 2 > maxFileSize) {
					throw new Exception(String.Format("DAC File upload failed. Wav file too large. Max sample count is {0}k", maxFileSize/1000));
				}
				 */
				pbh.fileLength = wfr.samples * 2;
				
				//UInt32 fileSize = fsParams.pageSize*fsParams.PagesPerFile;
				//UInt32 padding =  fileSize - (wfr.samples*2);
				
				UInt32 page = (UInt32)(fsParams.PagesPerFile * fileNo);
				
				SetFileWritePageAddress(page); //1st file
				pbh.majic = 0x8888;
				pbh.SetCrc();
				
				byte[] header = new byte[512];
				s.Serialize(pbh).CopyTo(header,0);
				FlashWrite(header);

				int bytesWritten = 0;
				while(bytesWritten/2 < wfr.samples) {
					Int16[] buf = wfr.ReadInt16(fsParams.pageSize/2);
					UInt16[] ibuf = new UInt16[buf.Length];
					for(int i=0;i<buf.Length; i++){
						Int32 t = (Int32)((buf[i]/2) + 16384);
						ibuf[i] = (UInt16)(t);
					}
					FlashWrite(s.Serialize(ibuf));
					bytesWritten += buf.Length*2;
					progressCallback("", (int)((bytesWritten/2) / (wfr.samples/100)) );
				}
				wfr.Close();
			}
			catch(WavFileBadFormatException) {
				throw new Exception("DAC File upload failed. Bad Wav file");
			}
			catch(EndOfStreamException) {
				progressCallback("", 100 );
			}
		}

		List<UInt32> sfBlocks =  new List<uint>	{ 0x0000, 0x2000, 0x4000, 0x6000, 0x8000,
			0x10000, 0x20000, 0x30000, 0x40000, 0x50000, 0x60000, 0x70000, 0x80000,
			0x90000, 0xA0000, 0xB0000, 0xC0000, 0xD0000, 0xE0000, 0xF0000, 0x100000,
			0x110000, 0x120000, 0x130000, 0x140000, 0x150000, 0x160000, 0x170000, 0x180000,
			0x190000, 0x1A0000, 0x1B0000, 0x1C0000, 0x1D0000, 0x1E0000, 0x1F0000, 0x1F8000, 0x1FA000,
			0x1FC000, 0x1FE000 };
		
		
		
		public void UpdateBootFirmware(string fileName, SudarFile.ProgressCallback progressCallback)
		{
			byte[] buf =new byte[64];
			var fi = new FileInfo(fileName);
			long byesLeftToWrite = fi.Length;
			int pages = (int)(byesLeftToWrite / 64);

			System.IO.FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			for(int i=0;i<pages;i++) {
				fs.Read(buf, 0, 64);
				SerialEepromWrite((ushort)(i*64), buf);
				byesLeftToWrite -= 64;
				if(pages > 0) progressCallback("", i / (pages/100));
			}
			
			if(byesLeftToWrite > 0) {
				fs.Read(buf, 0, (int)byesLeftToWrite);
				for(int i=(int)byesLeftToWrite; i<64; i++) buf[i] = 0xFF;
				SerialEepromWrite((ushort)(pages*64), buf);
			}
			fs.Close();
		}
		
		
		
		public void UpdateSerialFlashFirmware(string fileName, SudarFile.ProgressCallback progressCallback)
		{
			const int PAGE_LENGTH = 256; //bytes
			byte[] buf =new byte[PAGE_LENGTH];
			FileInfo fi = new FileInfo(fileName);
			long byesLeftToWrite = fi.Length;
			int pages = (int)(byesLeftToWrite / PAGE_LENGTH);
			UInt32 address;
			
			//if (! SerialFlashEraseChip() ) {
			
			System.IO.FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			for(int i=0;i<pages;i++) {
				fs.Read(buf, 0, PAGE_LENGTH);
				//SerialFlashWrite((ushort)(i*64), buf);
				
				address = (UInt32) i*PAGE_LENGTH;
				
				if(sfBlocks.Contains(address)) {
					if(!SerialFlashEraseBlock(address) ) {
						throw new Exception("Serial Flash Erase Failed");
					}
				}
				
				if (! SerialFlashWritePage(address, buf)) {
					throw new Exception("Serial Flash Write Failed");
				}
				
				byesLeftToWrite -= PAGE_LENGTH;
				if(pages > 0) progressCallback("", i / (pages/100));
			}
			
			if(byesLeftToWrite > 0) {
				fs.Read(buf, 0, (int)byesLeftToWrite);
				for(int i=(int)byesLeftToWrite; i<PAGE_LENGTH; i++) buf[i] = 0xFF;
				//SerialFlashWrite((ushort)(pages*PAGE_LENGTH), buf);
				
				address = (UInt32)pages*PAGE_LENGTH;
				
				if(sfBlocks.Contains(address)) {
					if(!SerialFlashEraseBlock(address) ) {
						throw new Exception("Serial Flash Erase Failed");
					}
				}
				
				if(! SerialFlashWritePage(address, buf)) {
					throw new Exception("Serial Flash Write Failed");
				}
			}
			fs.Close();
		}
		
		public bool SetRtcTime()
		{
			UInt32 unixTime =  UnixDateTime.ConvertToUnixTime(DateTime.UtcNow);
			return SendCommand(SudarCommands.SET_RTC_TIME, defaultTimeout, unixTime);
		}
		
		public bool SetConfig(SudarConfig sc)
		{
			sc.UpdateCrc();
			return SendCommand(SudarCommands.SET_CONFIG, defaultTimeout, sc);
		}
		
		public bool DoCrcSpeedTest()
		{
			return SendCommand(SudarCommands.CRC_SPEED_TEST, 1000);
		}
		
		public bool EnableBatteryCharger( bool enable)
		{
			return SendCommand(SudarCommands.MSP_ENABLE_BATT_CHARGE, defaultTimeout,enable ? (UInt16)1: (UInt16)0);
		}

		public bool GetConfig()
		{
			return SendCommand(SudarCommands.GET_CONFIG, defaultTimeout);
		}
		
		public bool GetFlashCardInfo()
		{
			return SendCommand(SudarCommands.FLASH_GET_CARD_INFO, defaultTimeout);
		}
		
		short[] adcValues = new short[1];
		public void PollAdcValues()
		{
			SendCommand(SudarCommands.ADC_I2C_READ, 2000);
		}
		
		public double AdcValue(int channel) {
			return adcValues[channel];
		}

		
		
		public bool SerialFlashWritePage(UInt32 address, byte[] data)
		{
			UInt16 count = (UInt16)data.Length;
			return SendCommand(SudarCommands.SERIAL_FLASH_WRITE_PAGE, defaultTimeout, address, count, data );
		}
		
		public bool SerialFlashEraseBlock(uint address)
		{
			return SendCommand(SudarCommands.SERIAL_FLASH_ERASE_BLOCK, defaultTimeout, address);
		}
		
		
		
		
		public event EventHandler ConfigChanged;
		protected virtual void OnconfigChanged(EventArgs e)
		{
			if (ConfigChanged != null) {
				ConfigChanged(this, e);
			}
		}

		public event EventHandler StatusChanged;
		protected virtual void OnStatusDataChanged(EventArgs e)
		{
			if (StatusChanged != null) {
				StatusChanged(this, e);
			}
		}

		override public void Close()
		{
			if(deviceRemovedHandler !=null) deviceRemovedHandler(this, HandledEventArgs.Empty);
			base.Close();
		}
		
		
		public string UartRx()
		{
			if( !SendCommand(SudarCommands.UART_RX_TEST,defaultTimeout ) ) return "";
			return uartRxBuf;
		}
		
		private EventHandler deviceRemovedHandler;
		public event EventHandler DeviceRemoved {
			add {
				deviceRemovedHandler += value;
			}
			remove {
				deviceRemovedHandler -=value;
			}
		}
		
		public bool HighPassSettingAvailable {
			get {
				return (model == Model.ST300STD || model == Model.ST300HF || model == Model.ST600STD || model == Model.ST600HF);
			}
		}

		public bool GainSettingAvailable {
			get {
				return (model == Model.ST202STD || model == Model.ST202HF || model == Model.ST300STD || model == Model.ST300HF || model == Model.ST600STD || model == Model.ST600HF);
			}
		}

	}
}
