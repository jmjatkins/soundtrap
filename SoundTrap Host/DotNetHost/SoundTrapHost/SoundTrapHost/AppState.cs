﻿/*
 * Created by SharpDevelop.
 * User: jatk009
 * Date: 26/08/2014
 * Time: 9:19 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace SudarHost
{
	/// <summary>
	/// Description of AppControl.
	/// </summary>
	public class AppState
	{
		private AppState()
		{
			mode = Mode.prodTest;
		}
		public static AppState instance = new AppState();
		public enum Mode {normal, prodTest}; 
		public Mode mode;
	}
}
