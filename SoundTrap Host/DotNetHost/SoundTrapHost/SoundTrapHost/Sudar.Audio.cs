﻿/* 
 * 	SoundTrap Software v1.0
 *
 *	Copyright (C) 2011-2014, John Atkins and Mark Johnson
 *
 *	This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
 *
 *	This file is part of the SoundTrap software. SoundTrap is an acoustic recording
 *	system intended for underwater acoustic measurements. This component of the
 *	SoundTrap project is free software: you can redistribute it and/or modify it
 *	under the terms of the GNU General Public License as published by the Free Software
 *	Foundation, either version 3 of the License, or any later version.
 *
 *	The SoundTrap software is distributed in the hope that it will be useful, but
 *	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License along with this
 *	code. If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using Crc;

namespace SudarHost
{
	/// <summary>
	/// Description of Sudar_Audio.
	/// </summary>
	public partial class Sudar
	{

		public bool AudioEnable(bool enable)
		{
			UInt16 en = (UInt16)(enable ? 1 : 0);
			return SendCommand(SudarCommands.AUDIO_TEST_ENABLE, defaultTimeout, en);
		}
		
		UInt32 audioRawSampleLength = 512;
		public bool AudioSample(int samplelength)
		{
			audioRawSampleLength = (UInt32)samplelength * 2;
			return SendCommand(SudarCommands.AUDIO_TEST_SAMPLE, 1000);
		}
		
		public bool AudioSetGain(bool high)
		{
			return SendCommand(SudarCommands.AUDIO_SET_GAIN, defaultTimeout, (UInt16)(high ? 1 : 0));
		}
		
		public bool AudioSetMute(bool high)
		{
			return SendCommand(SudarCommands.AUDIO_SET_MUTE, defaultTimeout, (UInt16)(high ? 1 : 0));
		}
		
		public bool AudioEnableCalSig(bool enable)
		{
			return SendCommand(SudarCommands.AUDIO_ENABLE_CAL_SIG, defaultTimeout, (UInt16)(enable ? 1 : 0));
		}
		
		
		private const UInt32 clrRate = 73728000;
		
		public UInt32 AudioTestModeBaseSampleRate {
			get {
				return (UInt32)(clrRate / 32);
			}
		}


		public UInt32 AudioSampleRate {
			get {
				return (UInt32)(clrRate / 32 / Math.Pow(2, audioDivider+1));
			}
		}
		
		UInt16 audioDivider = 1;
		public bool AudioSetDivider(UInt16 divider)
		{
			if ( SendCommand(SudarCommands.AUDIO_SET_DIVIDER, defaultTimeout, divider) ) {
				audioDivider = divider;
				return true;
			} else return false;
			
		}
		
		public bool AudioSetCalFrequency(UInt32 frequency)
		{
			return SendCommand(SudarCommands.AUDIO_SET_CAL_TONE_F, defaultTimeout, frequency);
		}
		
		
		
		public float[] AudioBuf {
			get {
				byte[] dat = rawAudiobuf.ToArray();
				float[] xx = new float[dat.Length/2];
				for(int i=0;i<xx.Length;i++) {
					xx[i] = BitConverter.ToInt16(dat, i*2);
					xx[i] = (xx[i]/ 32768);
				}
				//sigProc.detrend(ref xx);
				return xx;
			}
		}
		
		public List<byte> rawAudiobuf = new List<byte>();
		
		private void handleAudioCaptured()
		{
			rawAudiobuf.Clear();
			byte[] buf;
			List<byte> bigbuf = new List<byte>();
			FlashRead(out buf, 1024); //flush buffer
			while(rawAudiobuf.Count < audioRawSampleLength) {
				FlashRead(out buf, 512);
				rawAudiobuf.AddRange(buf);
			}
			OnAudioCaptured(EventArgs.Empty);
		}
		
		
		public event EventHandler AudioCaptured;
		private void OnAudioCaptured(EventArgs e)
		{
			if (AudioCaptured != null) {
				AudioCaptured(this, e);
			}
		}
		
		public bool AudioEnableTriggerMode(bool enable)
		{
			return SendCommand(SudarCommands.AUDIO_ENABLE_TRIGGER, defaultTimeout, enable ? (UInt16)1 : (UInt16)0);
		}
		
		public bool AudioSetHighPass(bool on)
		{
			return SendCommand(SudarCommands.AUDIO_SET_HPASS, defaultTimeout, (UInt16)(on ? 1 : 0));
		}
		
		public bool AudioSetActiveChannel(UInt16 channel)
		{
			return SendCommand(SudarCommands.AUDIO_SET_ACTIVE_CHANNEL, defaultTimeout, channel);
		}
	}
}
