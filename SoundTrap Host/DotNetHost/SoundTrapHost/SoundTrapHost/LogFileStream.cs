﻿/* 
 * 	SoundTrap Software v1.0
 *
 *	Copyright (C) 2011-2014, John Atkins and Mark Johnson
 *
 *	This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
 *
 *	This file is part of the SoundTrap software. SoundTrap is an acoustic recording
 *	system intended for underwater acoustic measurements. This component of the
 *	SoundTrap project is free software: you can redistribute it and/or modify it
 *	under the terms of the GNU General Public License as published by the Free Software
 *	Foundation, either version 3 of the License, or any later version.
 *
 *	The SoundTrap software is distributed in the hope that it will be useful, but
 *	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License along with this
 *	code. If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Xml;
using System.Text;
using System.IO;

namespace SudarHost
{
	/// <summary>
	/// Description of LogFileStream.
	/// </summary>
	public class LogFileStream : FileStream
	{
		StreamWriter t;
		public LogFileStream(string path, FileMode mode) : base(path, mode) {
			/*
			// Create a writer to write XML to the console.
			xtw = new XmlTextWriter(this, Encoding.ASCII);
			xtw.Formatting = Formatting.None;
			xtw.Indentation = 4;
			xtw.WriteStartElement("ST");
			Write("LogFileStream", "Log Created", DateTime.Now.ToLongDateString());
			xtw.Flush();
			*/
			t = new StreamWriter(this);
			t.WriteLine("<ST>");
			t.Flush();
		}
		
		public void Write(int id, string context, string name, string value)
		{
			//t.WriteLine("<PROC_EVENT>");
			t.WriteLine(String.Format("<PROC_EVENT ID=\"{0}\">", id));
			t.WriteLine(String.Format("<{0} {1}=\"{2}\"/>", context, name, value));
			t.WriteLine("</PROC_EVENT>");
			t.Flush();
		}
		
		public override void Close()
		{
			t.WriteLine("</ST>");
			t.Flush();
			base.Close();
		}
	}
}
