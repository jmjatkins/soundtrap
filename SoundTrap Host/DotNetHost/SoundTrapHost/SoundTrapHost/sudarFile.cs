﻿/* 
 * 	SoundTrap Software v1.0
 *
 *	Copyright (C) 2011-2014, John Atkins and Mark Johnson
 *
 *	This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
 *
 *	This file is part of the SoundTrap software. SoundTrap is an acoustic recording
 *	system intended for underwater acoustic measurements. This component of the
 *	SoundTrap project is free software: you can redistribute it and/or modify it
 *	under the terms of the GNU General Public License as published by the Free Software
 *	Foundation, either version 3 of the License, or any later version.
 *
 *	The SoundTrap software is distributed in the hope that it will be useful, but
 *	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License along with this
 *	code. If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Crc;
using System.Xml;
using System.Threading;

namespace SudarHost
{
	/// <summary>
	/// Description of sudarFile.
	/// </summary>
	

	public enum ProcessResult { Success, Error, Cancelled };

	
	public class SudarFile
	{
		private const int NoOfRetries = 10;

		public class SudarOffloadFileHeader
		{
			
			public UInt16 HostCodeVersion;
			public UInt32 HostTime;
			public Byte DeviceType;
			public Byte DeviceCodeVersion;
			public UInt32 DeviceTime;
			public UInt32 DeviceIdentifier;
			public UInt32 BlockLength;
			public UInt16 StartBlock;
			public UInt16 EndBlock;
			public UInt32 NoOfBlocks;
			public UInt16 Crc;

			public SudarOffloadFileHeader ()
			{
			}
			
			public byte[] CRCandSerialize()
			{
				Serializer serializer = new Serializer(EndianMode.BIG_16BIT);
				byte[] binData = serializer.Serialize(this);
				Crc = crc.calc( binData, binData.Length-2);
				return serializer.Serialize(this);
			}
			
			public bool CheckCrc() {
				Serializer serializer = new Serializer(EndianMode.BIG_16BIT);
				byte[] binData = serializer.Serialize(this);
				return  Crc == crc.calc( binData, binData.Length-2);
			}

			static public SudarOffloadFileHeader Deserialise(Stream buf) {
				Serializer serializer = new Serializer(EndianMode.BIG_16BIT);
				SudarOffloadFileHeader result = serializer.Deserialize(typeof(SudarOffloadFileHeader), buf) as SudarOffloadFileHeader;
				if(result!=null && result.CheckCrc())
					return result;
				else return null;
			}
		}
		
		
		public SudarFile(string desciption)
		{
			this.description = desciption;
		}
		
		public DateTime date;
		public UInt32 index;
		public string description;
		public UInt32 startPage;
		public UInt32 startBlock;
		public UInt32 blockLength;
		public UInt16 fileBoundaries;
		public UInt32 blocksPerFile;
		public UInt32 pagesPerBlock;
		
		public UInt32 blocks {
			get {
				return fileBoundaries * blocksPerFile;
			}
		}

		public void Delete(Sudar sudar)
		{
			//FLASH_FORMAT command current works in units of pages TODO - change to blocks
			if(!sudar.SendCommand(Sudar.SudarCommands.FLASH_FORMAT, 2000, startPage, blocks / pagesPerBlock )) {
				throw new Exception("Failed To Delete File");
			}
		}
		
		public delegate bool ProgressCallback(string status, int percentComplete);
		static public void FormatFS(Sudar sudar, ProgressCallback progressCallback)
		{
			UInt32 pagesPerBlock = 1024 * 64;
			for(UInt32 page = sudar.FsParams.PagesPerFile; page < sudar.FsParams.noOfPages; page+= pagesPerBlock) {
				int retries = 0;
				
				UInt32 pagesToDelete = 	page + pagesPerBlock > sudar.FsParams.noOfPages ? sudar.FsParams.noOfPages - page - 1 : pagesPerBlock;
				
				while(true) {
					if(sudar.SendCommand(Sudar.SudarCommands.FLASH_FORMAT, 2000, page, pagesToDelete)) break;
					if(retries++ > 5) throw new Exception("SudarFile format failed");;
				}
				if(progressCallback!=null) {
					if( progressCallback("", (int)(page / (sudar.FsParams.noOfPages/100)) ) ){
						break;
					}
				}
			}
		}

		public class ChunkHeader {
			public UInt16 majicNo;
			public UInt16 ChunkId;
			public UInt16 DataLength;
			public UInt16 SampleCount;
			public UInt32 TimeS;
			public UInt32 TimeOffsetUs;
			public UInt16 DataCrc;
			public UInt16 HeaderCrc;
			
			public bool CheckId() {
				return majicNo == 0xA952;
			}
			
			public static UInt16 Length {
				get {
					return 20;
				}
			}
			
			public bool CheckCrc() {
				Serializer serializer = new Serializer(EndianMode.BIG_16BIT);
				byte[] binData = serializer.Serialize(this);
				return HeaderCrc == crc.calc( binData, binData.Length-2);
			}
			
			static public ChunkHeader Deserialise(Stream buf) {
				Serializer serializer = new Serializer(EndianMode.BIG_16BIT);
				return serializer.Deserialize(typeof(ChunkHeader), buf) as ChunkHeader;
			}
		}
		
		
		public abstract class BlockHeader {

			abstract  public UInt32 BlockLength {
				get;
			}
			
			abstract  public UInt32 FileStartBlock {
				get;
			}

			abstract  public UInt32 TimeUS {
				get;
			}

			abstract  public UInt32 BlockNumber{
				get;
			}

			abstract  public bool IdIsGood {
				get;
			}

			abstract public UInt16 HeaderLength{
				get;
			}

			abstract public bool CheckCrc();
			
			static public BlockHeader Deserialise(byte[] buf) {
				MemoryStream s = new MemoryStream(buf);
				Type t = (buf[1] == 0x6A ? typeof(BlockHeaderV2) : typeof(BlockHeaderV1));
				Serializer serializer = new Serializer(EndianMode.BIG_16BIT);
				BlockHeader result = serializer.Deserialize(t, s) as BlockHeader;
				return result;
			}

		}

		public class BlockHeaderV1 :BlockHeader {
			
			public UInt16 id;
			public UInt16 pack1;
			public UInt32 fileStartBlock;
			public UInt32 blockNumber;
			public UInt32 blockLength;
			public UInt32 timeUS;
			public UInt16 Crc;
			public UInt16 pack2;
			
			override public uint BlockNumber {
				get {
					return blockNumber;
				}
			}
			
			override public uint TimeUS {
				get {
					return timeUS;
				}
			}
			
			override public uint FileStartBlock {
				get {
					return  fileStartBlock ;
				}
			}
			
			override public uint BlockLength {
				get {
					return blockLength;
				}
			}
			
			override public bool IdIsGood {
				get { return id == 0x5abc; }
			}
			
			override public UInt16 HeaderLength{
				get { return 24; }
			}
			
			override public bool CheckCrc() {
				Serializer serializer = new Serializer(EndianMode.BIG_16BIT);
				byte[] binData = serializer.Serialize(this);
				return (Crc == crc.calc( binData, binData.Length-4));
			}
			
			static public BlockHeader Deserialise(Stream buf) {
				Serializer serializer = new Serializer(EndianMode.BIG_16BIT);
				BlockHeader result = serializer.Deserialize(typeof(BlockHeader), buf) as BlockHeader;
				return result;
			}

		}

		public class BlockHeaderV2 : BlockHeader {
			public UInt16 id;
			public UInt16 pack1;
			public UInt32 fileStartBlock;
			public UInt32 blockNumber;
			public UInt32 blockLength;
			public UInt32 timeUS;
			public UInt32 serialNo;
			public UInt32 serialNo2;
			public UInt16 Crc;
			public UInt16 pack2;
			
			override public uint BlockNumber {
				get {
					return blockNumber;
				}
			}
			override public uint TimeUS {
				get {
					return timeUS;
				}
			}
			
			override public uint FileStartBlock {
				get {
					return  fileStartBlock ;
				}
			}
			
			override public bool IdIsGood {
				get { return id == 0x6abc; }
			}
			
			override public UInt16 HeaderLength{
				get { return 32; }
			}
			
			override public uint BlockLength {
				get {
					return blockLength;
				}
			}
			
			override public bool CheckCrc() {
				Serializer serializer = new Serializer(EndianMode.BIG_16BIT);
				byte[] binData = serializer.Serialize(this);
				return (Crc == crc.calc( binData, binData.Length-4));
			}

			static public BlockHeaderV2 Deserialise(Stream buf) {
				Serializer serializer = new Serializer(EndianMode.BIG_16BIT);
				BlockHeaderV2 result = serializer.Deserialize(typeof(BlockHeaderV2), buf) as BlockHeaderV2;
				return result;
			}

		}

		
		
		static public ChunkHeader ReadChunkHeader(MemoryStream s)
		{
			Serializer serializer = new Serializer(EndianMode.BIG_16BIT);
			ChunkHeader result;
			result = serializer.Deserialize(typeof(ChunkHeader), s) as ChunkHeader;
			//TODO - check crc
			return result;
		}
		
		static public List<SudarFile> GetFileList( ISudarFileIO sudar, SudarFile.ProgressCallback progressCallback, bool thorough)
		{
			int emptyBlocks = 0;
			UInt32 fb;
			UInt32 lastFb = 0;
			List<SudarFile> result = new List<SudarFile>();
			byte[] buf;
			SudarFile currentFile = null;
			
			sudar.SetFileReadPageIncrement(sudar.FsParams.PagesPerFile);
			UInt32 page = (UInt32)sudar.FsParams.PagesPerFile;
			
			FlashSetAddress(sudar, page);

			for(fb=1; fb<sudar.FsParams.FileBoundaries; fb++) { //skip first file
				progressCallback("", (int)(page / (sudar.FsParams.noOfPages/100)) );
				page = (UInt32) ( sudar.FsParams.PagesPerFile * fb );
				
				int retries = NoOfRetries;
				while(true)
				{
					try {
						sudar.FlashRead(out buf, 512);
						break;
					}
					catch (ArgumentException) {
						return result;
					}
					catch(Exception e) {
						if(--retries < 0) {
							
							throw(e);
						}
						FlashSetAddress(sudar,  page);
					}
				}
				
				//MemoryStream s = new MemoryStream(buf);
				
				BlockHeader b = BlockHeader.Deserialise(buf);
				
				if(b != null && b.IdIsGood && b.CheckCrc()  ) {
					emptyBlocks = 0;
					lastFb = fb;
					if(b.BlockNumber == b.FileStartBlock) {
						currentFile	 = new SudarFile(String.Format("File {0}", result.Count));
						
						if(Properties.Settings.Default.UseUTC ) {
							currentFile.date = UnixDateTime.ConvertToDateTime( b.TimeUS );
						}
						else
						{
							currentFile.date = UnixDateTime.ConvertToDateTime( b.TimeUS ).ToLocalTime();
						}
						
						if(b is BlockHeaderV2) {
							currentFile.description =  (b as BlockHeaderV2).serialNo.ToString()  + "." + currentFile.date.ToString("yyMMddHHmmss");
						}
						else {
							currentFile.description =  sudar.SerialNumber + "." + currentFile.date.ToString("yyMMddHHmmss");
						}
						
						currentFile.index = fb;
						currentFile.startPage = page;
						currentFile.startBlock = b.FileStartBlock;
						currentFile.blockLength = b.BlockLength;
						currentFile.fileBoundaries = 1;
						currentFile.pagesPerBlock = sudar.FsParams.PagesPerBlock;
						currentFile.blocksPerFile = sudar.FsParams.BlocksPerFile;
						result.Add(currentFile);
					}
					else {
						if(currentFile!=null) {
							currentFile.fileBoundaries++;
						}
					}
				}
				else {
					emptyBlocks ++;
					if(!thorough && (emptyBlocks > 1024)) {
						break;
					}
				}
			}
			if(sudar.FsParams.FileBoundaries >0) {
				sudar.PercentFileSystemUsed = (int)((lastFb * 100) / sudar.FsParams.FileBoundaries);
			}
			return result;
		}

		static public void FlashSetAddress(ISudarFileIO sudar, uint page)
		{
			int retries = NoOfRetries;

			while(retries-- > 0) {
				try {
					if(!sudar.SetFileReadPageAddress(page)) continue;
					//sudar.FlashRead(out buf, 1024); //flush buffer
					//TODO why do we need to do this twice?
					if(!sudar.SetFileReadPageAddress(page)) continue;
					//sudar.FlashRead(out buf, 1024); //flush buffer
					return;
				}
				catch( IOException ) {
					sudar.ResetFsPipe();
					Thread.Sleep(100);
				}
			}
			throw new IOException("Failed to read/reset SoundTrap FS");
		}


		public ProcessResult Download(string fileName, ISudarFileIO sudar, ProgressCallback progressCallback)
		{
			int progress, lastprogress = 0;
			ProcessResult result = ProcessResult.Success;
			byte[] buf;
			UInt32 blocksProcessed = 0;
			FileStream fs = new FileStream(fileName, FileMode.Create, FileAccess.Write);

			try {
				SudarOffloadFileHeader h = new SudarFile.SudarOffloadFileHeader();
				sudar.GetStatus();
				h.DeviceTime = UnixDateTime.ConvertToUnixTime(sudar.Time);
				h.HostTime = UnixDateTime.ConvertToUnixTime(DateTime.Now);
				h.BlockLength = blockLength;
				buf = h.CRCandSerialize();
				fs.Write(buf, 0, buf.Length);
				sudar.SetFileReadPageIncrement(1);
				
				FlashSetAddress(sudar, startPage);
				
				DateTime startTime = DateTime.Now;
				UInt32 bytesRead = 0;
				while(true) {
					
					int retries = NoOfRetries;
					while(true)
					{
						try {
							sudar.FlashRead(out buf, sudar.FsParams.PagesPerBlock * sudar.FsParams.pageSize);
							break;
						}
						catch(IOException e) {
							if(--retries < 0)
								throw(e);
							FlashSetAddress(sudar,  startPage + (blocksProcessed * sudar.FsParams.PagesPerBlock));
						}
					}
					
					//MemoryStream s = new MemoryStream(buf);
					bytesRead += (UInt32)buf.Length;
					BlockHeader b = BlockHeader.Deserialise(buf);
					if((b == null) || !b.IdIsGood || !b.CheckCrc() || (b.FileStartBlock != startBlock)  ) {
						fs.Close();
						break;
					}
					fs.Write(buf, b.HeaderLength, (int)b.BlockLength - b.HeaderLength );
					blocksProcessed++;
					TimeSpan elpasedTime = DateTime.Now - startTime;
					double msb = bytesRead / (( elpasedTime.TotalSeconds) * 1024*1024);
					progress = (int)(blocksProcessed / (blocks / 100) );
					if( progress > lastprogress + 1) {
						lastprogress = progress;
						if( progressCallback(String.Format("Downloading, MBs = {0:0.0}", msb) , progress)) {
							result = ProcessResult.Cancelled;
							break;
						}
					}
				}
				
				sudar.SetFileReadPageIncrement(2); // TODO fix this bodge - these two lines are necessary in order to
				FlashSetAddress(sudar, startPage); // stop the steaming read transaction

				
			}
			catch(Exception) {
				result = ProcessResult.Error;
			}
			finally {
				fs.Close();
			}
			return result;
		}
		
	}

}
