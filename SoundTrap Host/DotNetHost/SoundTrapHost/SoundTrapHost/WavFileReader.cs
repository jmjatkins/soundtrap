﻿/* 
* 	SoundTrap Software v1.0
*  
*	Copyright (C) 2011-2014, John Atkins and Mark Johnson
*
*	This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
*
*	This file is part of the SoundTrap software. SoundTrap is an acoustic recording
*	system intended for underwater acoustic measurements. This component of the 
*	SoundTrap project is free software: you can redistribute it and/or modify it 
*	under the terms of the GNU General Public License as published by the Free Software 
*	Foundation, either version 3 of the License, or any later version.
*
*	The SoundTrap software is distributed in the hope that it will be useful, but 
*	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
*	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License along with this 
*	code. If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.IO;

namespace SudarHost
{
	/// <summary>
	/// Description of WavFileReader.
	/// </summary>
	/// 
	public class WavFileBadFormatException : Exception
	{
		public WavFileBadFormatException(): base() {}
	}

	public class WavFmtBlock{
		public UInt32 subChunckId;
		public UInt16 tag;
		public UInt16 nchan;
		public UInt32 samp_per_sec;
		public UInt32 avg_bytes_per_sec;
		public UInt16 block_align;
		public UInt16 nbits;
	}
	
	public class WavFileReader : FileStream
	{
		public WavFileReader(string fileName) : base(fileName, FileMode.Open)
		{
			ReadHeader();
		}
		
		public UInt32 subChunkSize;
		public WavFmtBlock fmt;
		public UInt32 samples;
		public UInt32 bytesRead;
		
		string ReadString(int count) {
			byte[] buf = new byte[count];
			if( Read(buf,0,count) != count) {
				throw new EndOfStreamException();
			}
			string s = System.Text.Encoding.UTF8.GetString(buf);
			return s;
		}
		
		// see https://ccrma.stanford.edu/courses/422/projects/WaveFormat/
		private void ReadHeader()
		{
			Serializer serializer = new Serializer(EndianMode.LITTLE);
			
			Seek(0, SeekOrigin.Begin);
			StreamReader sr = new StreamReader(this);
			if( ReadString(4) != "RIFF") throw new WavFileBadFormatException();
			
			subChunkSize = (UInt32)serializer.Deserialize(typeof(UInt32), this);

			if( ReadString(4) != "WAVE") throw new WavFileBadFormatException();
			if( ReadString(4) != "fmt ") throw new WavFileBadFormatException();

			fmt  = (WavFmtBlock)serializer.Deserialize(typeof(WavFmtBlock), this);
			if( ReadString(4) != "data") throw new WavFileBadFormatException();
			
			UInt32 subchunksize = (UInt32)serializer.Deserialize(typeof(UInt32), this);
			
			samples =  subchunksize / (UInt32) (fmt.nchan * (fmt.nbits/8) );

			bytesRead = 0;
			headerEndPos = this.Position;
		}
		
		long headerEndPos;
		private byte[] ReadArray(int count) {
			var buf = new byte[count];
			for(UInt32 i=0;i<count;i++) {
				buf[i] = (byte)ReadByte();
			}
			return buf;
		}
		public long SeekSamples(long offset, SeekOrigin origin)
		{
			long pos = base.Seek(offset*2*fmt.nchan, origin);
			if(pos<headerEndPos) {
				pos = base.Seek(headerEndPos, SeekOrigin.Begin);
			}
			return pos;
		}
		
		
		public Int16[] ReadInt16(uint count) //reads left channel only
		{
			var buf = new Int16[count];
			for(int i=0;i<count;i++) {
				try{
					buf[i] = BitConverter.ToInt16(ReadArray(2), 0);
					for(int j =0; j<fmt.nchan-1; j++) { //skip other channels
						ReadArray(2);
					}
				}
				catch(OverflowException) {
					if(i==0)
						throw new EndOfStreamException();
					else {
						return buf.SubArray(0,i);
					}
				}
			}
			return buf;
		}

		public double[] Read(uint count) //reads left channel only
		{
			var buf = new double[count];
			for(int i=0;i<count;i++) {
				try{
					buf[i] = (double)BitConverter.ToInt16(ReadArray(2), 0);
					for(int j =0; j<fmt.nchan-1; j++) { //skip other channels
						ReadArray(2);
					}
				}
				catch(OverflowException) {
					if(i==0)
						throw new EndOfStreamException();
					else {
						return buf.SubArray(0,i);
					}
				}
			}
			return buf;
		}
		
	}
}
