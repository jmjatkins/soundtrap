﻿/*
 * Created by SharpDevelop.
 * User: jatk009
 * Date: 4/04/2013
 * Time: 11:28 a.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Collections.Generic;

namespace SudarHost
{
	/// <summary>
	/// Description of SudFile.
	/// </summary>
	public class SudFileWriter
	{
		XDocument xDoc;
		public SudFileWriter(string deviceType, DateTime hostTime, DateTime deviceTime, string deviceIdentifier)
		{
			xDoc = new XDocument(
				new XDeclaration("1.0", "UTF-8", null),
				new XElement("SUDFILE",
				             new XElement("Header",
				                          new XElement("Version", "1.00"),
				                          new XElement("HostTime", hostTime.ToString() ),
				                          new XElement("DeviceTime", deviceTime.ToString() ),
				                          new XElement("DeviceType", deviceType ),
				                          new XElement("DeviceIdentifier", deviceIdentifier.ToString() )
				                         ),
				             new XElement("DataBlocks", "")
				            ));
		}
		
		public void Write(byte[] data) {
			string hexData = Convert.ToBase64String(data);
			xDoc.Elements("SUDFILE").Elements("DataBlocks").Last().Add(new XElement("Data", hexData));
		}
		
		public void saveToFile(string fileName)
		{
			xDoc.Save(fileName);
		}
	}
	
	public class SudFileReader
	{
		XDocument xDoc;
		string deviceType;
		string hostTime;
		string deviceTime;
		string deviceIdentifier;
		
		public SudFileReader(string filename)
		{
			xDoc = XDocument.Load(filename);
			deviceType = xDoc.Element("SudFile").Element("DeviceType").Value;
			hostTime = xDoc.Element("SudFile").Element("HostTime").Value;
			deviceTime = xDoc.Element("SudFile").Element("DeviceTime").Value;
			deviceIdentifier = xDoc.Element("SudFile").Element("DeviceIdentifier").Value;
			dataBlocks = xDoc.Element("SudFile").Elements("Data"); 
		}
		
		IEnumerable<XElement> dataBlocks;
		
		public byte[] Read()
		{
			byte[] buf = Convert.FromBase64String( dataBlocks.GetEnumerator().Current.Value );
			dataBlocks.GetEnumerator().MoveNext();
			return buf;
		}
	}
}
