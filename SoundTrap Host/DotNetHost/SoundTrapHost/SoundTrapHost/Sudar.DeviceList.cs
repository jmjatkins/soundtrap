﻿/* 
* 	SoundTrap Software v1.0
*  
*	Copyright (C) 2011-2014, John Atkins and Mark Johnson
*
*	This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
*
*	This file is part of the SoundTrap software. SoundTrap is an acoustic recording
*	system intended for underwater acoustic measurements. This component of the 
*	SoundTrap project is free software: you can redistribute it and/or modify it 
*	under the terms of the GNU General Public License as published by the Free Software 
*	Foundation, either version 3 of the License, or any later version.
*
*	The SoundTrap software is distributed in the hope that it will be useful, but 
*	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
*	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License along with this 
*	code. If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Linq;
using System.Collections.Generic;
using Crc;
using System.IO;
using System.ComponentModel;
using LibUsbDotNet;
using LibUsbDotNet.Main;
using LibUsbDotNet.DeviceNotify;
using LibUsbDotNet.LibUsb;

namespace SudarHost
{
	public partial class Sudar 
	{
		static IDeviceNotifier dn;
		static Dictionary<string, Sudar> deviceList = new Dictionary<string, Sudar>();

		static public void InitDeviceNotify()
		{

			dn = DeviceNotifier.OpenDeviceNotifier();
			dn.OnDeviceNotify += onDeviceNotify;
			dn.Enabled = true;
			RefreshList();
		}
		
		static public void RefreshList()
		{
			List<UsbRegistry> uds = SudarUsbIO.GetDeviceList();
			foreach(UsbRegistry usbreg in uds) {
				if(!deviceList.ContainsKey((usbreg.Device as LibUsbDevice).DeviceFilename )) AddDevice(usbreg);
			}
			//check for removals
			foreach(KeyValuePair<string, Sudar> kvp in deviceList.Reverse()) {
				bool found = false;
				foreach(UsbRegistry usbreg in uds) {
					if(kvp.Key == (usbreg.Device as LibUsbDevice).DeviceFilename ){
						found = true;
						break;
					}
				}
				if(!found) {
					try {
						kvp.Value.Close();
					}
					catch {
						
					}
					deviceList.Remove(kvp.Key);
				}
			}
			if(deviceListChangedHandler!= null) deviceListChangedHandler(null, EventArgs.Empty);
		}
		
		static private void AddDevice(UsbRegistry usbreg)
		{
			Sudar s = new Sudar(usbreg);
			s.Open("");
			deviceList.Add((usbreg.Device as LibUsbDevice).DeviceFilename, s);
		}
		
		static new public List<Sudar> GetDeviceList()
		{
			return deviceList.Values.ToList();
		}
		
		static private void onDeviceNotify(object sender, DeviceNotifyEventArgs e)
		{
			if(e.Device.IdProduct == 0x5058) {
				RefreshList();
			}
		}

		static private EventHandler deviceListChangedHandler;
		static public event EventHandler DeviceListChanged {
			add {
				deviceListChangedHandler += value;
			}
			remove {
				deviceListChangedHandler -=value;
			}
		}
	}
}