﻿/* 
 * 	SoundTrap Software v1.0
 *
 *	Copyright (C) 2011-2014, John Atkins and Mark Johnson
 *
 *	This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
 *
 *	This file is part of the SoundTrap software. SoundTrap is an acoustic recording
 *	system intended for underwater acoustic measurements. This component of the
 *	SoundTrap project is free software: you can redistribute it and/or modify it
 *	under the terms of the GNU General Public License as published by the Free Software
 *	Foundation, either version 3 of the License, or any later version.
 *
 *	The SoundTrap software is distributed in the hope that it will be useful, but
 *	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License along with this
 *	code. If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using Crc;
using System.IO;
using System.ComponentModel;

namespace SudarHost
{
	public partial class Sudar
	{

		
		int percentFileSystemUsed = 0;
		public int PercentFileSystemUsed {
			get { return percentFileSystemUsed; }
			set { percentFileSystemUsed = value; }
		}
		
		private SudarFsParams fsParams = null;
		public SudarFsParams FsParams {
			get {
				if(fsParams ==null || fsParams.CapacityGB == 0) {
					if( ! SendCommand(SudarCommands.GET_FS_PARAMS, defaultTimeout) ) throw new Exception("SUDAR - Failed to read FS params");
				}
				return fsParams;
			}
		}
		
		public class TcardData {
			public UInt32 totalSectors0;
			public UInt32 sectorsUsed0;
			public UInt32 totalSectors1;
			public UInt32 sectorsUsed1;
			public UInt32 totalSectors2;
			public UInt32 sectorsUsed2;
			public UInt32 totalSectors3;
			public UInt32 sectorsUsed3;
		}
		
		public bool RefreshCardData(UInt16 val ) 
		{
			return SendCommand(SudarCommands.SD_SCAN_CARDS, 2000, val) 
				&& SendCommand(SudarCommands.GET_FS_PARAMS, defaultTimeout);
		}
		
		private TcardData cardData = null;
		public TcardData CardData {
			get {
				if(cardData==null ) {
					if( ! RefreshCardData(0) ) throw new Exception("SUDAR - Failed to read card data");
					}
				return cardData;
			}
		}
		

		
	}
	
}