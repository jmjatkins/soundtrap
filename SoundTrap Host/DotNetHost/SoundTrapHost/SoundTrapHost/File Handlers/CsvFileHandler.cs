﻿/* 
 * 	SoundTrap Software v1.0
 *
 *	Copyright (C) 2011-2014, John Atkins and Mark Johnson
 *
 *	This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
 *
 *	This file is part of the SoundTrap software. SoundTrap is an acoustic recording
 *	system intended for underwater acoustic measurements. This component of the
 *	SoundTrap project is free software: you can redistribute it and/or modify it
 *	under the terms of the GNU General Public License as published by the Free Software
 *	Foundation, either version 3 of the License, or any later version.
 *
 *	The SoundTrap software is distributed in the hope that it will be useful, but
 *	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License along with this
 *	code. If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Xml;


namespace SudarHost
{
	/// <summary>
	/// Description of TxtFileHandler.
	/// </summary>
	public class CsvFileHandler : ISudarDataHandler
	{
		StreamWriter sw;
		string fileName;
		string fileSuffix = "csv";
		int id;
		string header;

		public CsvFileHandler(string sourceFileName)
		{
			fileName = Path.GetDirectoryName(sourceFileName) + Path.DirectorySeparatorChar + Path.GetFileNameWithoutExtension(sourceFileName) + ".";
		}
		
		public string FileType {
			get {
				return "csv";
			}
		}

		public void ProcessChunk(SudarFile.ChunkHeader ch, ref byte[] buf)
		{
			if(sw == null) {
				sw = new StreamWriter(fileName + fileSuffix + ".csv", false);
				sw.WriteLine(header);
			}
			string s = Encoding.Unicode.GetString(buf);
			sw.Write( s );
			sw.Flush();
		}

		/*
		
		public void ProcessChunk(SudarFile.ChunkHeader ch, ref byte[] buf)
		{
			if(sw == null) {
				sw = new StreamWriter(fileName + fileSuffix, false);
			}
			
			int i = 0;
			while(i+1 < buf.Length) {
				UInt32 rtime = swapWords(BitConverter.ToUInt32(buf, i));
				i+=4;
				UInt32 mticks = swapWords(BitConverter.ToUInt32(buf, i));
				i+=4;
				UInt16 n = BitConverter.ToUInt16(buf, i);
				i+=2;
				
				byte[] b = buf.SubArray(i,(n % 2 == 1) ? n+1 : n);
				Sudar.SwapEndian(b);
				string s = Encoding.ASCII.GetString(b, 0, n);
				//string s = Encoding.ASCII.GetString(buf,i,n);
				i+=n;
				if(n % 2 == 1) i++;
				if((rtime ==0) && (mticks == 0)) {
					sw.Write( String.Format("{0}\n", s));
				}
				else sw.Write( String.Format("{0},{1},{2}\n", rtime, mticks, s));
			}
			sw.Flush();
		}
		 */
		
		public void Close()
		{
			if(sw != null) {
				sw.Close();
			}
		}
		
		
		public void Init(LogFileStream log, string innerXml, int id)
		{
			this.id = id;
			XmlDocument reader = new XmlDocument();
			reader.LoadXml(innerXml);
			XmlNodeList e = reader.GetElementsByTagName("CFG");
			foreach(XmlNode n in e) {
				foreach(XmlNode n2 in n) {
					if( n2.Name == "SUFFIX")
						fileSuffix = n2.InnerXml.Trim();
					else if( n2.Name == "HEADER")
						header = n2.InnerXml.Trim();
				}
			}
		}
	}
}
