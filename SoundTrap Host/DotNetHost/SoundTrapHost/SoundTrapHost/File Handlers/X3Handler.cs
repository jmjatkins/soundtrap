﻿/* 
 * 	SoundTrap Software v1.0
 *
 *	Copyright (C) 2011-2014, John Atkins and Mark Johnson
 *
 *	This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
 *
 *	This file is part of the SoundTrap software. SoundTrap is an acoustic recording
 *	system intended for underwater acoustic measurements. This component of the
 *	SoundTrap project is free software: you can redistribute it and/or modify it
 *	under the terms of the GNU General Public License as published by the Free Software
 *	Foundation, either version 3 of the License, or any later version.
 *
 *	The SoundTrap software is distributed in the hope that it will be useful, but
 *	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License along with this
 *	code. If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using System.Text;
using System.Collections.Generic;

namespace SudarHost
{
	/// <summary>
	/// Description of X3Handler.
	/// </summary>
	/*
	public static class StreamExtension
	{
		public static void WriteInt( this Stream s, Int16 val)
		{
			s.Write(BitConverter.GetBytes(val), 0, 2);
		}
	}
	 */

	public class X3Handler : ISudarDataHandler
	{
		int blockLength;
		ushort nchan = 1;

		BitStream2 bs;

		public X3Handler(string sourceFileName)
		{
		}
		
		Int16 FixSign(UInt16 d, int nbits)
		{
			UInt32 half = (UInt32)(1<<(nbits-1));
			UInt32 offs = (UInt32)(half<<1);
			return (Int16)((d >= half) ? (Int32)d-offs : d);
		}
		
		ushort[] RSUFFS = {0,1,3} ;
		short[]   IRT = {0,-1,1,-2,2,-3,3,-4,4,-5,5,-6,6,-7,7,-8,8,-9,9,-10,10,
			-11,11,-12,12,-13,13,-14,14,-15,15,-16,16,-17,17,-18,18,
			-19,19,-20,20,-21,21,-22,22,-23,23,-24,24,-25,25,-26,26};

		void unpackr(BitStream2 b, Int16[] bufOut, int n, int code)
		{
			// unpacker for variable length Rice codes
			// Returns 0 if ok, 1 if there are not enough bits in the stream.
			long ow = 0;
			long msk ;
			int ntogo = 0;
			int ns;
			int suff;
			int nsuffix = RSUFFS[code];
			int lev = 1<<nsuffix ;
			
			for(int k=0; k<n; k++) {      // Do for n words
				// First find the end of the variable length section.
				// If there is an end and a complete suffix in the current word, it will
				// have a value of at least 1<<nsuffix. If not, append the next word from
				// the stream
				
				
				ntogo = b.ReadIntLargerThan(lev, out ow);
				
				// ow is now guaranteed to have a start and a suffix.
				// Find the start (i.e., the first 1 bit from the left)
				for(ns=1, msk = 1<<(ntogo-1); ns<=ntogo && (ow & msk)==0; ns++, msk>>=1);
				if(ns>ntogo) {
					throw new Exception("rice decode error"); //error
				}
				ntogo -= ns+nsuffix ;
				suff = (int)((ow >> ntogo) & (lev-1));
				ow &= (1<<ntogo)-1 ;
				bufOut[k] = (Int16) IRT[lev*(ns-1)+suff];
			}
			//TODO: do we need to return any unused bits to the input stream?
			if(ntogo > 0)
				throw new Exception("ntogo > 0 !");
		}

		
		void integrate(Int16[] op, Int16 last, int count )
		{
			// De-emphasis filter to reverse the diff in the compressor.
			// Filters operates in-place.
			int    k ;
			for(k=0; k<count; k++) {
				last += op[k];
				op[k] = last ;
			}
		}

		
		void unpack(BitStream2 bufIn, Int16[] bufOut, int nb, int count)
		{
			for(int i=0; i<count; i++) {
				bufOut[i] = FixSign(bufIn.ReadInt(nb), nb);
			}
		}
		
		int BlockDecode(BitStream2 bufIn, out Int16[] buf, Int16 last, int count) {
			int nb = 0;
			int code = bufIn.ReadInt(2);
			if(code==0) {
				//bfp or pass thru block
				nb = bufIn.ReadInt(4); //E
				if(nb>0) {
					nb++;
				}
				else {
					int nn = bufIn.ReadInt(6) + 1;
					if (nn > blockLength) throw new Exception("bad block length");
					count = nn;
					code = bufIn.ReadInt(2);
					if(code == 0) {
						nb = bufIn.ReadInt(4) + 1;
					}
				}
			}
			buf = new Int16[count];
			if(code > 0) {
				unpackr(bufIn, buf, count, code-1);
			}
			else {
				unpack(bufIn, buf, nb, count);
				if(nb == 16) return count;
			}
			
			integrate(buf, last, buf.Length);
			return buf.Length;
		}
		
		public void ProcessChunk(SudarFile.ChunkHeader ch, ref byte[] buf)
		{
			Int16[] b;
			int[] bOutPos = new int[] {0,1,2,3};
			Int16[] bOut  = new Int16[ch.SampleCount * nchan];
			
			Int16[] last = new Int16[4];

			bs = new BitStream2(buf);

			for(int c=0; c<nchan;c++) {
				UInt16 j =bs.ReadInt(16);
				bOut[bOutPos[c]] = unchecked((short)j);
				//Int16 j = FixSign(bs.ReadInt(16), 16);
				//bOut[bOutPos[c]] = j;
				last[c] = bOut[bOutPos[c]] ;
				bOutPos[c]+=nchan;
			}


			int samplesToGo = (ch.SampleCount-1);
			while(samplesToGo > 0) {
				//int c = 0;
				int n=0;
				n = Math.Min(blockLength, samplesToGo);
				for(int c=0; c<nchan;c++) {
					n = BlockDecode(bs, out b, last[c], n);
					last[c] = b[b.Length-1];
					//b.CopyTo(bOut, bOutPos);
					for(int i=0; i<b.Length; i++) {
						bOut[bOutPos[c]] = b[i];
						bOutPos[c] += nchan;
					}
				}
				samplesToGo -= n;
			}
			
			buf = new byte[bOut.Length*2];
			for(int i=0; i< bOut.Length; i++) {
				buf[i*2] = (byte)(bOut[i] & 0x00ff);
				buf[(i*2)+1] = (byte)((bOut[i] >> 8)& 0x00ff);
			}
			
		}
		
		public void Close()
		{
		}
		
		int id;
		
		public void Init(LogFileStream log, string innerXml, int id)
		{
			this.id = id;
			XmlDocument reader = new XmlDocument();
			reader.LoadXml(innerXml);
			XmlNodeList e = reader.GetElementsByTagName("CFG");
			foreach(XmlNode n in e) {
				foreach(XmlNode n2 in n) {
					if( n2.Name == "BLKLEN")
						blockLength = Convert.ToInt32(n2.InnerXml);
					if( n2.Name == "NCHS" )
						nchan = Convert.ToUInt16(n2.InnerXml) ;

				}
			}
		}
		
		public string FileType {
			get {
				return "x3v2";
			}
		}
		
	}
}
