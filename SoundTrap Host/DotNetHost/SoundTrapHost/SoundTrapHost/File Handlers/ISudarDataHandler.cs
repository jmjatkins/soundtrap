﻿/* 
 * 	SoundTrap Software v1.0
 *
 *	Copyright (C) 2011-2014, John Atkins and Mark Johnson
 *
 *	This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
 *
 *	This file is part of the SoundTrap software. SoundTrap is an acoustic recording
 *	system intended for underwater acoustic measurements. This component of the
 *	SoundTrap project is free software: you can redistribute it and/or modify it
 *	under the terms of the GNU General Public License as published by the Free Software
 *	Foundation, either version 3 of the License, or any later version.
 *
 *	The SoundTrap software is distributed in the hope that it will be useful, but
 *	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License along with this
 *	code. If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.IO;
using System.Collections.Generic;


namespace SudarHost
{
	
	public class FileFormatNotSupportedException : Exception
	{
		public FileFormatNotSupportedException(string message) : base(message) {;}
	}
	
	/// <summary>
	/// Description of SudarDataHandler.
	/// </summary>
	public interface ISudarDataHandler
	{
		void ProcessChunk(SudarFile.ChunkHeader ch, ref byte[] buf);
		string FileType {get;}
		void Close();
		void Init(LogFileStream log, string innerXml, int id);
	}
	
	public class SudarDataHandlerHelper
	{
		static public ISudarDataHandler CreateHandler(string ftype, string filePath) {
			switch(ftype.ToLower()) {
					//case "xml": return new XmlFileHandler(filePath);
					case "x3v2": return new X3Handler(filePath);
					case "wav": return new WavFileHandler(filePath);
					case "txt": return new TxtFileHandler(filePath);
					case "csv": return new CsvFileHandler(filePath);
					default : throw new FileFormatNotSupportedException(String.Format("FType {0} not supported", ftype));
			}
		}
	}
	/*
	public class SudarDataHandlers {
		public SudarDataHandlers() {;}
		public Dictionary<int, ISudarDataHandler> handlers = new Dictionary<int, ISudarDataHandler>();
		public void clear() {
			handlers.Clear();
		}
		
		private ISudarDataHandler GetHandler(string ftype, string filePath) {
			switch(ftype.ToLower()) {
					case "xml": return new XmlFileHandler(filePath);
					case "x3v2": return new X3Handler(filePath);
					case "wav": return new WavFileHandler(filePath);
					case "txt": return new TxtFileHandler(filePath);
					default : throw new FileFormatNotSupportedException(String.Format("FType {0} not supported", ftype));
			}
		}
		
		public void Register(int id, string ftype, string filePath, string innerXml ) {
			ISudarDataHandler h = GetHandler(ftype, filePath); 
			h.Init(innerXml);
			handlers.Add( id, h);
		}
		
		public ISudarDataHandler FindHandler(int id) {
			return  handlers[id] ;
		}
		
		public void Close()
		{
			foreach(ISudarDataHandler sdh in handlers.Values) {
				sdh.Close();
			}
			handlers.Clear();
		}
	}
    */
	
}
