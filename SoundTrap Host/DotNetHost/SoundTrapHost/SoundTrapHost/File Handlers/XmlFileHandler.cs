﻿/* 
 * 	SoundTrap Software v1.0
 *
 *	Copyright (C) 2011-2014, John Atkins and Mark Johnson
 *
 *	This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
 *
 *	This file is part of the SoundTrap software. SoundTrap is an acoustic recording
 *	system intended for underwater acoustic measurements. This component of the
 *	SoundTrap project is free software: you can redistribute it and/or modify it
 *	under the terms of the GNU General Public License as published by the Free Software
 *	Foundation, either version 3 of the License, or any later version.
 *
 *	The SoundTrap software is distributed in the hope that it will be useful, but
 *	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License along with this
 *	code. If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Collections.Generic;

namespace SudarHost
{

	public class XmlFileHandler : ISudarDataHandler
	{
		LogFileStream log;
		string fileName;
		int id;
		
		public void Init(LogFileStream log, string innerXml, int id)
		{
			this.id = id;
			this.log = log;
		}
		
		System.Collections.Generic.Dictionary<string, string> paramaters = new Dictionary<string, string>();
		public Dictionary<string, string> Paramaters {
			get { return paramaters; }
		}

		Dictionary<int, DataConfig> configs;
		
		public XmlFileHandler(string sourceFileName,  Dictionary<int, DataConfig> d)
		{
			fileName = Path.GetDirectoryName(sourceFileName) + Path.DirectorySeparatorChar + Path.GetFileNameWithoutExtension(sourceFileName) + ".xml";
			configs = d;
		}
		
		public string FileType {
			get {
				return "xml";
			}
		}
		
		public void ProcessChunk(SudarFile.ChunkHeader ch, ref byte[] buf)
		{
			StreamWriter logSw = new StreamWriter(log);
			Sudar.SwapEndian(buf);

			for(int i=0;i<buf.Length; i++) {
				if(buf[i] == 0x00) buf[i] = 13; //replace nulls with CR
			}

			XmlDocument reader = new XmlDocument();
			reader.Load(new MemoryStream(buf));
			
			StreamReader sr = new StreamReader( new MemoryStream( buf ));
			logSw.Write(sr.ReadToEnd());
			logSw.Flush();
			
			XmlNodeList l = reader.GetElementsByTagName("CFG");
			if(l.Count > 0) {
				foreach(XmlNode n in l) {
					XmlAttribute ftype = n.Attributes["FTYPE"];
					XmlAttribute id = n.Attributes["ID"];
					XmlAttribute srcid = n.Attributes["CODEC"];
					XmlAttribute suffix = n.Attributes["SUFFIX"];
					if(ftype != null && id != null && Convert.ToInt32(id.Value) != 0) {
						DataConfig d = new DataConfig();
						d.id = Convert.ToInt32(id.Value);
						d.ftype = ftype.Value;
						if(srcid != null)
							d.srcId = Convert.ToInt32(srcid.Value);
						d.handler = SudarDataHandlerHelper.CreateHandler(d.ftype, fileName);
						d.handler.Init(log, n.OuterXml, d.id);
						configs.Add(d.id, d);
					}
				}
			}
		}
		
		public void Close()
		{
		}
	}
}

