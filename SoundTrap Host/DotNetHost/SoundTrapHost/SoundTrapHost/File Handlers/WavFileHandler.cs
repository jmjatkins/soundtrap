﻿/* 
 * 	SoundTrap Software v1.0
 *
 *	Copyright (C) 2011-2014, John Atkins and Mark Johnson
 *
 *	This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
 *
 *	This file is part of the SoundTrap software. SoundTrap is an acoustic recording
 *	system intended for underwater acoustic measurements. This component of the
 *	SoundTrap project is free software: you can redistribute it and/or modify it
 *	under the terms of the GNU General Public License as published by the Free Software
 *	Foundation, either version 3 of the License, or any later version.
 *
 *	The SoundTrap software is distributed in the hope that it will be useful, but
 *	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License along with this
 *	code. If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Xml;
using System.IO;
using System.Text;
using System.Collections.Generic;


namespace SudarHost
{
	/// <summary>
	/// Decorder for SUDAR wav data.
	/// </summary>
	public class WavFileHandler : ISudarDataHandler
	{
		string fileName;
		string fileSuffix = "wav";

		LogFileStream log;
		
		const double timeErrorWarningThreshold = 0.04;
		//5%
		
		WavFileWriter fsWavOut;

		UInt32 fs = 0;
		UInt32 timeCheck = 1;
		int channel;
		ushort nchan = 1;
		Int64 cumulativeTimeErrorUs;
		Int64 cumulativeSamples;
		SudarFile.ChunkHeader firstChunk;
		SudarFile.ChunkHeader lastChunk;
		Int32 chunkCount;
		int lastError = 0;
		int id;
		
		public void Init(LogFileStream log, string innerXml, int id)
		{
			this.id = id;
			this.log = log;
			channel = -1;
			nchan = 1;
			cumulativeTimeErrorUs = 0;
			cumulativeSamples = 0;
			chunkCount = 0;
			lastChunk = null;

			XmlDocument reader = new XmlDocument();
			reader.LoadXml(innerXml);
			XmlNodeList e = reader.GetElementsByTagName("CFG");
			foreach (XmlNode n in e) {
				foreach (XmlNode n2 in n) {
					if (n2.Name == "FS")
						fs = Convert.ToUInt32(n2.InnerXml);
					if (n2.Name == "SUFFIX")
						fileSuffix = n2.InnerXml.Trim();
					if (n2.Name == "TIMECHK")
						timeCheck = Convert.ToUInt32(n2.InnerXml);
					if( n2.Name == "CHANNEL" )
						channel = Convert.ToInt32(n2.InnerXml) ;
					if (n2.Name == "NCHS")
						nchan = Convert.ToUInt16(n2.InnerXml);
					
				}
			}
		}
		
		public WavFileHandler(string sourceFileName)
		{
			fileName = Path.GetDirectoryName(sourceFileName) + Path.DirectorySeparatorChar + Path.GetFileNameWithoutExtension(sourceFileName);
		}
		
		public string FileType {
			get {
				return "wav";
			}
		}
		
		
		int ToNextNearest(int x)
		{
			if (x < 0) {
				return 0;
			}
			--x;
			x |= x >> 1;
			x |= x >> 2;
			x |= x >> 4;
			x |= x >> 8;
			x |= x >> 16;
			return x + 1;
		}
		
		int ToNearest(int x)
		{
			int next = ToNextNearest(x);
			int prev = next >> 1;
			return next - x < x - prev ? next : prev;
		}
		
		bool prevChunkWasNeg = false;
		
		public void ProcessChunk(SudarFile.ChunkHeader ch, ref byte[] buf)
		{
			if (fsWavOut == null) {
				if(channel == -1) {
					fsWavOut = new WavFileWriter(fileName + "." + fileSuffix, FileMode.Create);
				}
				else fsWavOut = new WavFileWriter(fileName +  "." + channel.ToString() + "." + fileSuffix, FileMode.Create);
				fsWavOut.Init(fs, nchan);
			}

			chunkCount++;
			
			
			if (lastChunk == null) {
				//first chunk
				firstChunk = ch;
			} else {
				UInt32 elapsedTimeS = (ch.TimeS - lastChunk.TimeS);
				long elapsedTimeUs = (long)((elapsedTimeS * 1000000) + ((Int64)ch.TimeOffsetUs - lastChunk.TimeOffsetUs));
				long calculatedTime = (long)((long)lastChunk.SampleCount * 1000000 / fs);
				int error = (Int32)(elapsedTimeUs - calculatedTime);
				cumulativeTimeErrorUs += error;
				lastChunk = ch;
				if ((timeCheck > 0) && (Math.Abs(error) > (calculatedTime * timeErrorWarningThreshold))) {
					//log warning
					double t = (double)(ch.TimeS - firstChunk.TimeS) + ((double)((long)ch.TimeOffsetUs - firstChunk.TimeOffsetUs) / 1000000);
					if (error > 0) {
						if (!prevChunkWasNeg) {
							log.Write(id, "WavFileHandler", "Info", String.Format("Sampling Gap {0} us at sample {1} ({2} s), chunk {3}", error, cumulativeSamples, t, chunkCount));
							if (Properties.Settings.Default.ZeroFill) {
								
								int samplesToAdd = (int)(error * fs / 1000000);
								//samplesToAdd = ToNearest(samplesToAdd);
								byte[] fill = new byte[samplesToAdd * 2 * nchan];
								fsWavOut.Write(fill, 0, fill.Length);
								error = 0;
								cumulativeSamples += samplesToAdd;
								log.Write(id, "WavFileHandler", "Info", String.Format("added {0} zeros", samplesToAdd));
							}
						}
					} else
						prevChunkWasNeg = true;
					
				} else
					prevChunkWasNeg = false;
				lastError = error;
			}
			cumulativeSamples += ch.SampleCount;
			fsWavOut.Write(buf, 0, buf.Length);
			lastChunk = ch;
		}
		
		public void Close()
		{
			if (lastChunk != null) {
				Int64 recordPeriod = lastChunk.TimeS - firstChunk.TimeS;
				recordPeriod *= 1000000;
				recordPeriod += (Int64)lastChunk.TimeOffsetUs - firstChunk.TimeOffsetUs;
				log.Write(id, "WavFileHandler", "OffloaderTimeZone", TimeZone.CurrentTimeZone.StandardName);
				log.Write(id, "WavFileHandler", "SamplingStartTimeLocal", String.Format("{0:s}", UnixDateTime.ConvertToDateTime((UInt32)firstChunk.TimeS).ToLocalTime()));
				log.Write(id, "WavFileHandler", "SamplingStopTimeLocal", String.Format("{0:s}", UnixDateTime.ConvertToDateTime((UInt32)lastChunk.TimeS).ToLocalTime()));
				log.Write(id, "WavFileHandler", "SamplingStartTimeUTC", String.Format("{0:s}", UnixDateTime.ConvertToDateTime((UInt32)firstChunk.TimeS)));
				log.Write(id, "WavFileHandler", "SamplingStopTimeUTC", String.Format("{0:s}", UnixDateTime.ConvertToDateTime((UInt32)lastChunk.TimeS)));
				log.Write(id, "WavFileHandler", "SamplingStartTimeSubS", String.Format("{0} us", firstChunk.TimeOffsetUs));
				log.Write(id, "WavFileHandler", "SamplingTimePeriod", String.Format("{0} us", recordPeriod));
				log.Write(id, "WavFileHandler", "CumulativeSamplingGap", String.Format("{0} us", cumulativeTimeErrorUs));
				log.Write(id, "WavFileHandler", "SampleCount", String.Format("{0}", cumulativeSamples));
				if (fsWavOut != null) {
					fsWavOut.Close();
					fsWavOut = null;
				}
			}
		}
	}
}