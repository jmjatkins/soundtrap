﻿/*
 * Created by SharpDevelop.
 * User: jatk009
 * Date: 30/10/2013
 * Time: 1:40 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.IO;
using BKSystem.IO;

namespace SudarHost.File_Handlers
{
	/// <summary>
	/// Description of x3.
	/// </summary>
	public class x3
	{
		public x3()
		{
		}
		

	
		private int	x3BlkDecode(short op, BitStream s, short last, int n, int nch)
		{
			// unpack and decode one block from one channel
			short	d ;
			int	err, nb, nn;

			// unpack the n samples in the block
			d = s.Read(d,0,2); // first get the code select header (2 bits)
			if(d == 0)	{					   // it is a bfp or pass thru block
				s.Read(nb, 0,4);
				if(nb>0)                  // it is a valid bfp header
					++nb ;                       // number of bits/word is one more than the exponent
				else {
					s.Read(nn,6)+1;
					if(nn>n) {
						printf(" Bad blklen field in X3: %d (%d)\n",nn,n) ;
						return(-1) ;
					}
					
					n = nn ;
					// now get the block type bit
					s.Read(d, 0,2); // first get the code select header (2 bits)
					if(d == 0)				    // if it is a bfp or pass thru block
						s.Read(nb,0,4)+1; // get the 4-bit bfp exponent
				}
			}
			
			// now we have the code type, number of bits and block length
			//printf("     :%d, %d, %d\n",n,d,nb);
			if(d>0)           	          // the block is rice encoded
				err = unpackr(op,b,n,d-1,nch) ;

			else {										// the block is bfp encoded with word length of nb
				err = unpackn(op,b,n,nb,nch) ;
				if(nb==16)                // no filtering needed if pass through coded
					return(err ? -1 : n) ;          // no sign fix needed either IF short is 16 bits
				// If there are portability concerns, do the fixsign call
				fixsign(op,n,nb,nch) ;
			}

			integrate(op,last,n,nch) ;       // run deemphasis filter
			
			if(err)
				printf("     last:%d, %d, %d, this: %d, %d, %d, useage %d %d %d %d\n",lchnk[0],lchnk[1],lchnk[2],n,d,nb,tchnk[0],tchnk[1],tchnk[2],tchnk[3]);
			else {
				lchnk[0] = n ;
				lchnk[1] = d ;
				lchnk[2] = nb ;
				if(d>=0 && d<=3)
					++tchnk[d] ;
			}

			return(err ? -n : n) ;
		}

	}
}
