﻿/*
 * Created by SharpDevelop.
 * User: jatk009
 * Date: 23/08/2014
 * Time: 10:37 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;

namespace SudarHost
{
	public class DataConfig
	{
		public int id;
		public int srcId;
		public string ftype;
		public object context;
		public object innerXml;
		public ISudarDataHandler handler;
		/*
		public Dictionary<int, DataConfig> Configs = new Dictionary<int, DataConfig>();
		public void CloseAll() {
			foreach(KeyValuePair<int, DataConfig> kvp in Configs) {
				kvp.Value.handler.Close();
			}
		}
		*/
	}
}
