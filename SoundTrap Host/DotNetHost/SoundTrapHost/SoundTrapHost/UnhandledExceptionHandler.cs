/* 
* 	SoundTrap Software v1.0
*  
*	Copyright (C) 2011-2014, John Atkins and Mark Johnson
*
*	This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
*
*	This file is part of the SoundTrap software. SoundTrap is an acoustic recording
*	system intended for underwater acoustic measurements. This component of the 
*	SoundTrap project is free software: you can redistribute it and/or modify it 
*	under the terms of the GNU General Public License as published by the Free Software 
*	Foundation, either version 3 of the License, or any later version.
*
*	The SoundTrap software is distributed in the hope that it will be useful, but 
*	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
*	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License along with this 
*	code. If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Threading;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Diagnostics;

namespace UnhandledExceptionHandler
{
	/// <summary>
	/// Handles Unhandled Exceptions On All Threads
	/// Writes To Log
	/// </summary>
	public class UnhandledExceptionHandler
	{
		static UnhandledExceptionHandler instance;
		public static void Init(string logName)
		{
			instance = new UnhandledExceptionHandler();
			instance.eventLogSourcename = logName;
		}

		UnhandledExceptionHandler()
		{
			Application.ThreadException += new ThreadExceptionEventHandler(ExceptionHandler);
			Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
			AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(ExceptionHandler2);
		}
		
		string eventLogSourcename;
		private void ExceptionHandler( object sender, ThreadExceptionEventArgs e)
		{
			ShowErrorMessage(e.Exception);
			//EventLog.WriteEntry( eventLogSourcename, "Exception thrown : " + e.Exception.Message );
		}

		private void ExceptionHandler2( object sender, UnhandledExceptionEventArgs e )
		{
			try {
				if(e.ExceptionObject is Exception){
					Exception ex = e.ExceptionObject as Exception;
					ShowErrorMessage(ex);
					//EventLog.WriteEntry( eventLogSourcename, "Exception 2 thrown : " + ex.Message );
				}
				else MessageBox.Show("Unknown Error", "Eror", MessageBoxButtons.OK, MessageBoxIcon.Error);
				
			} 
			catch
			{
				
			}
		}
		
		
		void ShowErrorMessage(Exception ex)
		{
//			MessageBox.Show(String.Format("Error:\n{0}\n At:\n{1}", ex.Message, ex.TargetSite), "Error", 
//			                MessageBoxButtons.OK, MessageBoxIcon.Error);
			MessageBox.Show(ex.Message, "Warning - Unhandled Error", 
			                MessageBoxButtons.OK, MessageBoxIcon.Warning);
		}
		
		
	}
}
