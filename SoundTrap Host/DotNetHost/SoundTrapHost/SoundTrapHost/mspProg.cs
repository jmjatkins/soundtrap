﻿/* 
* 	SoundTrap Software v1.0
*  
*	Copyright (C) 2011-2014, John Atkins and Mark Johnson
*
*	This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
*
*	This file is part of the SoundTrap software. SoundTrap is an acoustic recording
*	system intended for underwater acoustic measurements. This component of the 
*	SoundTrap project is free software: you can redistribute it and/or modify it 
*	under the terms of the GNU General Public License as published by the Free Software 
*	Foundation, either version 3 of the License, or any later version.
*
*	The SoundTrap software is distributed in the hope that it will be useful, but 
*	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
*	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License along with this 
*	code. If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace SudarHost
{
	/// <summary>
	/// Description of mspProg.
	/// </summary>
	public class mspProg
	{
		Sudar s;
		const  UInt16 MSP_FLASH_INFO_ADDR  = 0x1000;
		public mspProg(Sudar s)
		{
			this.s = s;
		}
		
		UInt16[] hexStringtoArray(string hexString)
		{
			hexString = hexString.TrimEnd(' ');
			string[] ss = hexString.Split(' ');
			UInt16[] v = new UInt16[ss.Length];
			for(int i=0;i<ss.Length;i++) v[i] = Convert.ToUInt16(ss[i], 16);
			return v;
		}

		public void mspprog(string filename, SudarFile.ProgressCallback progressCallback)
		{
			if(!File.Exists(filename)) throw new Exception("Failed to find file " + filename);
			StreamReader f = File.OpenText(filename);
			try {
				UInt32	addr = 0;
				UInt16 cinit = 0;
				
				FileInfo fi = new FileInfo(filename);
				long fileLength = fi.Length;
				
				EnterProgmode();

				if(!erase() ) throw new Exception("MSP Erase Failed");

				Thread.Sleep(60);

				if(!unlock()) throw new Exception("MSP unlock failed");

				if(!sendhdr(filename)) throw new Exception("MSP send header failed");

				
				while(true) {
					
					string s = f.ReadLine();
					
					progressCallback("", (int)(f.BaseStream.Position / (fileLength/100)) );
					
					if(s == "" || s[0]=='q')
						break ;

					if(s[0]=='@') {
						addr = Convert.ToUInt16(s.Substring(1), 16);
						continue;
					}
					
					//data line
					UInt16[] d = hexStringtoArray(s);
					
					if(addr==0x0fffe)             // catch the reset interrupt vector
						cinit = (UInt16)(d[0]+(d[1]<<8)) ;

					//printf("  writing %d bytes to 0x%x\n", k-1, addr) ;

					if(!write((UInt16)addr,d) ) throw new Exception("MSP write failed");

					addr += (UInt16) d.Length;
				}

				start(cinit); //causes a usb reset
				//if(!start(cinit)) throw new Exception("MSP start failed");
			}
			finally {
				f.Close();
				progressCallback("", 100 );
			}
		}

		bool sendhdr(string fileName)
		{
			UInt16[] hdr = new UInt16[6];
			int nname = (fileName.Length +1); 
			if(nname % 2 > 0) nname++;// must be even
			UInt16[] d = new UInt16[nname + 12];
			// block header contains:
			// HOST CODE VERSION 16 bit
			// DATE LOADED 32 bit
			// DATE MODIFIED 32 bit
			// FNAME LENGTH 16 bit
			// FNAME (8 bit characters, 0 filled to an even number, maximum 62 characters)
			hdr[0] = 123 ;
			hdr[1] = 456 ;
			hdr[2] = 789 ;
			hdr[3] = 0xabc ;
			hdr[4] = 0xdef ;
			hdr[5] = (UInt16)nname ;

			// unpack hdr to d+8 in little-endian order
			int j = 0; //should this be 8??
			for(int i=0; i<6; i++) {
				d[j++] = (UInt16) (hdr[i] & 0x0ff);
				d[j++] = (UInt16) (hdr[i]>>8);
			}

			for(int i =0; i < fileName.Length; i++)
				d[j++] = fileName[i] ;

			return(write(MSP_FLASH_INFO_ADDR, d)) ;
		}
		
		private bool erase()
		{
			UInt16[]  d = {0x80,24,4,4,0,0,0,0} ;  // erase instruction
			return s.mspSend(d) ;
		}
		
		private bool unlock()
		{
			UInt16[] d = new UInt16[40];
			d[0] = 0x80;
			d[1] = 16;
			d[2] = 36;
			d[3] = 36;
			d[4] = 0;
			d[5] = 0;
			d[6] = 0;
			d[7] = 0;
			// unlock instruction

			for(int i=8; i<40; i++) d[i] = 0xff;
			return s.mspSend(d) ;
		}

		
		private bool write(UInt16 addr, UInt16[] d)
		{
			UInt16[] txbuf = new UInt16[d.Length+8];

			txbuf[0] = 0x80 ;
			txbuf[1] = 18;
			txbuf[2] = txbuf[3] = (UInt16)(d.Length+4);
			txbuf[4] = (UInt16)(addr&0x0ff);
			txbuf[5] = (UInt16)(addr>>8);
			txbuf[6] = (UInt16)d.Length;
			txbuf[7] = 0;
			for(int i=0; i<d.Length; i++) txbuf[i+8] = d[i];
			return s.mspSend(txbuf) ;
		}

		
		bool EnterProgmode()
		{
			s.mspSetTestVal(0);
			s.mspSetTestDir(1);
			MessageBox.Show("Press the reset button (for ST300 connect 9V), then click OK...", "", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
			s.mspSetTestVal(1);
			Thread.Sleep(10);
			s.mspSetTestVal(0);
			Thread.Sleep(10);
			s.mspSetTestVal(1);
			Thread.Sleep(10);
			MessageBox.Show("Release the reset button (disconnect 9V), then click OK...", "", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
			Thread.Sleep(10);
			s.mspSetTestVal(0);
			s.mspSetTestDir(0);
			return true;
		}

		bool start(UInt16 addr)
		{
			UInt16[]  d = {0x80,26,4,4,0,0,0,0} ;  // start instruction
			d[4] = (UInt16)(addr & 0x0ff);
			d[5] = (UInt16)(addr >> 8);
			return s.mspSend(d);
		}

	}
}
