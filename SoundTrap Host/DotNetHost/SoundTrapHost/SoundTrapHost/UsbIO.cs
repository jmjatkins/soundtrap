﻿/* 
 * 	SoundTrap Software v1.0
 *
 *	Copyright (C) 2011-2014, John Atkins and Mark Johnson
 *
 *	This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
 *
 *	This file is part of the SoundTrap software. SoundTrap is an acoustic recording
 *	system intended for underwater acoustic measurements. This component of the
 *	SoundTrap project is free software: you can redistribute it and/or modify it
 *	under the terms of the GNU General Public License as published by the Free Software
 *	Foundation, either version 3 of the License, or any later version.
 *
 *	The SoundTrap software is distributed in the hope that it will be useful, but
 *	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License along with this
 *	code. If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.IO;
using System.Threading;
using System.Text;
using LibUsbDotNet;
using LibUsbDotNet.Main;
using System.Collections.Generic;

namespace SudarHost
{

	/// <summary>
	/// Sudar USB Interface class. Implements read/write for SUDAR.
	/// </summary>
	public class SudarUsbIO
	{
		const int vid = 0x451; 		//vendor ID
		const int pid = 0x5058; 	//product id
		const int fsTimeout	= 2000; //filesystem io timeout ms
		const int packetSize = 512; //usb packet size bytes
		
		UsbDevice usbDevice;
		UsbEndpointWriter epWriterCommand;	//command write endpoint
		UsbEndpointReader epReaderCommand;	//command response read endpoint
		UsbEndpointWriter epWriterFs;		//filesystem write endpoint
		UsbEndpointReader epReaderFs;		//filesystem read endpoint

		bool isOpen = false;
		Queue<byte> rxQueue = new Queue<byte>();
		
		public SudarUsbIO()
		{
		}

		
		virtual public void Open(UsbRegistry usbreg)
		{
			usbreg.Open(out usbDevice);
			
			IUsbDevice wholeUsbDevice = usbDevice as IUsbDevice;
			if (!ReferenceEquals(wholeUsbDevice, null))
			{
				wholeUsbDevice.SetConfiguration(1);
				wholeUsbDevice.ClaimInterface(0);
			}
			epWriterCommand = usbDevice.OpenEndpointWriter(WriteEndpointID.Ep02);
			epReaderCommand = usbDevice.OpenEndpointReader(ReadEndpointID.Ep01);
			
			epWriterFs = usbDevice.OpenEndpointWriter(WriteEndpointID.Ep04);
			epReaderFs = usbDevice.OpenEndpointReader(ReadEndpointID.Ep03);

			isOpen = true;
		}
		
		
		public bool IsConnected
		{
			get {
				return isOpen;
			}
		}
		
		virtual public void Close()
		{
			isOpen = false;
			IUsbDevice wholeUsbDevice = usbDevice as IUsbDevice;
			if (!ReferenceEquals(wholeUsbDevice, null))
			{
				wholeUsbDevice.ReleaseInterface(0);
			}
			if(usbDevice.IsOpen) {
				usbDevice.Close();
			}
		}
		
		public static List<UsbRegistry> GetDeviceList()
		{
			UsbDeviceFinder usbFinder = new UsbDeviceFinder(vid, pid);
			List<UsbRegistry> dl = new List<UsbRegistry>();
			UsbRegDeviceList list = UsbDevice.AllLibUsbDevices.FindAll(usbFinder);
			foreach(UsbRegistry ur in list )
			{
				dl.Add(ur);
			}
			return dl;
		}
		
		public void FlushReadBuf()
		{
			rxQueue.Clear();
		}
		
		
		public void Read(out byte[] buf, int length, out int bytesRead, int timeout)
		{
			byte[] packetBuffer = new byte[packetSize];
			if(rxQueue.Count < length) {
				ErrorCode ec = epReaderCommand.Read(packetBuffer, 0, packetSize,  timeout, out bytesRead);
				for(int i=0;i<bytesRead; i++) rxQueue.Enqueue(packetBuffer[i]);
			}
			bytesRead = Math.Min(length, rxQueue.Count);
			buf = new byte[bytesRead];
			for(int i=0;i<bytesRead; i++) buf[i] = rxQueue.Dequeue();
		}
		
		public void Write(byte[] buf, int timeout)
		{
			int bytesWritten;
			ErrorCode ec = epWriterCommand.Write(buf, timeout, out bytesWritten);
			if (ec != ErrorCode.None) {
				throw new Exception(ec.ToString() +  "\n" + UsbDevice.LastErrorString);
			}
		}
		
		public void ResetFsPipe()
		{
			epReaderFs.Reset();
			Thread.Sleep(100);
			epReaderFs.Flush();
		}
		
		public void ReadFromFs(out byte[] buf, UInt32 bytesToRead)
		{
			int count;
			buf = new byte[bytesToRead];
			ErrorCode ec = epReaderFs.Read(buf, fsTimeout, out count);
			if (ec != ErrorCode.None)
				throw new IOException("Failed to read from USB, check connection");
		}

		public void WriteToFs(byte[] buf)
		{
			int bytesWritten;
			ErrorCode ec = epWriterFs.Write(buf, fsTimeout, out bytesWritten);
			if (ec != ErrorCode.None) {
				throw new Exception("FS " + ec.ToString() +  "\n" + UsbDevice.LastErrorString);
			}
		}
		
	}
}
