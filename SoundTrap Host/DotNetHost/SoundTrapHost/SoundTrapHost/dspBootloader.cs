﻿/* 
 * 	SoundTrap Software v1.0
 *
 *	Copyright (C) 2011-2014, John Atkins and Mark Johnson
 *
 *	This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
 *
 *	This file is part of the SoundTrap software. SoundTrap is an acoustic recording
 *	system intended for underwater acoustic measurements. This component of the
 *	SoundTrap project is free software: you can redistribute it and/or modify it
 *	under the terms of the GNU General Public License as published by the Free Software
 *	Foundation, either version 3 of the License, or any later version.
 *
 *	The SoundTrap software is distributed in the hope that it will be useful, but
 *	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License along with this
 *	code. If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.IO;
using System.Threading;
using System.Text;
using LibUsbDotNet;
using LibUsbDotNet.Main;
using LibUsbDotNet.DeviceNotify;
using System.Collections.Generic;
using System.Windows.Forms;

namespace SudarHost
{
	/// <summary>
	/// Implements code to bootload SUDAR when connected to USB. Monitors USB device connections and responds
	/// to appropriate vendor/product ID by sending the SUDAR offloader binary.
	/// </summary>
	public class DspBootloader
	{
		const int usbTimeout = 1000;
		IDeviceNotifier dn;
		
		public void Init()
		{
			dn = DeviceNotifier.OpenDeviceNotifier();
			dn.OnDeviceNotify += onDeviceNotify;
			dn.Enabled = true;
			BootLoadAllDevices();
		}
		
		string bootloadDevFilePath = @"C:\Users\John\Documents\SoundTrap Source\soundtrap\SoundTrap Offloader\Debug\SoundTrapOffloader.bin";
		public void BootLoadAllDevices()
		{
			string path;
			UsbDeviceFinder usbFinder = new UsbDeviceFinder(0x451, 0x9010);
			UsbRegDeviceList list = UsbDevice.AllLibUsbDevices.FindAll(usbFinder);

			if( System.Diagnostics.Debugger.IsAttached && File.Exists(bootloadDevFilePath) ) {
				path = bootloadDevFilePath;
			}
			else {
				path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
				path += Path.DirectorySeparatorChar +  "SoundTrapOffloader.bin";
			}
			if(!File.Exists(path)) {
				MessageBox.Show("Failed to find SUDAR bootloader file", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			else foreach(UsbRegistry ur in list )
			{
				BootLoad(ur, path);
			}
		}

		void onDeviceNotify(object sender, DeviceNotifyEventArgs e)
		{
			try {
				if( e.Device.IdVendor == 0x451) {
					if((e.EventType== EventType.DeviceArrival) &&  (e.Device.IdProduct == 0x9010)) {
						BootLoadAllDevices();
					}
				}
			}
			catch {
				//ignore
			}
		}
		
		public  void Swap(byte[] data)
		{
			for (int i = 0; i < data.Length; i += 2)
			{
				byte b = data[i];
				data[i] = data[i + 1];
				data[i + 1] = b;
			}
		}

		void BootLoad(UsbRegistry usbreg, string fileName)
		{
			UsbEndpointWriter writer1;
			UsbDevice usbDevice;
			int bytesWritten;
			byte[] buf =new byte[64];
			FileInfo fi = new FileInfo(fileName);
			long byesLeftToWrite = fi.Length;
			int pages = (int)(byesLeftToWrite / 64);

			
			usbreg.Open(out usbDevice);
			IUsbDevice wholeUsbDevice = usbDevice as IUsbDevice;
			if (!ReferenceEquals(wholeUsbDevice, null))
			{
				wholeUsbDevice.SetConfiguration(1);
				wholeUsbDevice.ClaimInterface(0);
			}
			
			writer1 = usbDevice.OpenEndpointWriter(WriteEndpointID.Ep01);

			System.IO.FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			for(int i=0;i<pages;i++) {
				fs.Read(buf, 0, 64);
				Swap(buf);
				ErrorCode ec = writer1.Write(buf, usbTimeout, out bytesWritten);
				if (ec != ErrorCode.None) return;
				byesLeftToWrite -= 64;
			}
			
			if(byesLeftToWrite > 0) {
				buf =new byte[byesLeftToWrite];
				fs.Read(buf, 0, (int)byesLeftToWrite);
				//for(int i=(int)byesLeftToWrite; i<512; i++) buf[i] = 0xFF;
				Swap(buf);
				ErrorCode ec = writer1.Write(buf, usbTimeout, out bytesWritten);
			}
			
			fs.Close();
			wholeUsbDevice.ReleaseInterface(0);
		}
		
	}
}
