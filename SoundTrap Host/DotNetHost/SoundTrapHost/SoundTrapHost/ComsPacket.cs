﻿/* 
* 	SoundTrap Software v1.0
*  
*	Copyright (C) 2011-2014, John Atkins and Mark Johnson
*
*	This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
*
*	This file is part of the SoundTrap software. SoundTrap is an acoustic recording
*	system intended for underwater acoustic measurements. This component of the 
*	SoundTrap project is free software: you can redistribute it and/or modify it 
*	under the terms of the GNU General Public License as published by the Free Software 
*	Foundation, either version 3 of the License, or any later version.
*
*	The SoundTrap software is distributed in the hope that it will be useful, but 
*	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
*	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License along with this 
*	code. If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using Crc;

namespace SudarHost
{
	/// <summary>
	/// Description of ComsPacket.
	/// </summary>
	public class ComsHeader
	{
		public ushort        sop;
		public ushort        id;
		public ushort        command;
		public ushort        dataLength;
		public ushort        dataCRC;

		public ushort      	 padding0;
		public ushort        padding1;
		public ushort        padding2;
		public ushort        padding3;
		public ushort        padding4;
		public ushort        padding5;
		public ushort        padding6;
		public ushort        padding7;
		public ushort        padding8;
		public ushort        padding9;
		public ushort        padding10;
		public ushort        padding11;
		public ushort        padding12;
		public ushort        padding13;
		public ushort        padding14;
		public ushort        padding15;
		public ushort        padding16;
		public ushort        padding17;
		public ushort        padding18;
		public ushort        padding19;
		public ushort        padding20;
		public ushort        padding21;
		public ushort        padding22;
		public ushort        padding23;
		public ushort        padding24;
		public ushort        padding25;
		public ushort        headerCRC;

		public ComsHeader( )
		{
			sop = 0x1234;
		}
		
		public ComsHeader( UInt16 id, ushort command, UInt16 dataLength, UInt16 dataCRC )
		{
			sop = 0x1234;
			this.id = id;
			this.command = (ushort)command;
			this.dataLength = dataLength;
			this.dataCRC = dataCRC;
			this.headerCRC = 0;
			
		}
		
		static public int size {
			get { return 64; } //padded to usb FIFO size
		}
		
		public bool CheckCRC()
		{
			Serializer serializer = new Serializer(EndianMode.BIG_16BIT);
			byte[] binData = serializer.Serialize(this);
			return  ( crc.calc( binData, binData.Length-2) == headerCRC);
		}
		
		public byte[] CRCandSerialize()
		{
			Serializer serializer = new Serializer(EndianMode.BIG_16BIT);
			byte[] binData = serializer.Serialize(this);
			headerCRC = crc.calc( binData, binData.Length-2);
			//headerCRC = crc.calc2( binData, binData.Length-2);
			
			binData = serializer.Serialize(this);
			return serializer.Serialize(this);
		}
		
		
		
	}


}
