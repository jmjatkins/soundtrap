﻿/* 
* 	SoundTrap Software v1.0
*  
*	Copyright (C) 2011-2014, John Atkins and Mark Johnson
*
*	This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
*
*	This file is part of the SoundTrap software. SoundTrap is an acoustic recording
*	system intended for underwater acoustic measurements. This component of the 
*	SoundTrap project is free software: you can redistribute it and/or modify it 
*	under the terms of the GNU General Public License as published by the Free Software 
*	Foundation, either version 3 of the License, or any later version.
*
*	The SoundTrap software is distributed in the hope that it will be useful, but 
*	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
*	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License along with this 
*	code. If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.IO;
using System.Text;

namespace SudarHost
{
	/// <summary>
	/// Description of WavFileWriter.
	/// </summary>
	public class WavFileWriter : FileStream
	{
		public WavFileWriter(string fileName, FileMode mode) : base(fileName, mode)
		{
		}
		
		long bytesWritten;
		UInt32 sampleRate;
		ushort channels;
		
		public class WavFmtBlock{
			public UInt32 subChunckId;
			public UInt16 tag;
			public UInt16 nchan;
			public UInt32 samp_per_sec;
			public UInt32 avg_bytes_per_sec;
			public UInt16 block_align;
			public UInt16 nbits;
		};
		
		public override void Write(byte[] array, int offset, int count)
		{
			base.Write(array, offset, count);
			bytesWritten+=count;
		}
		
		public override void WriteByte(byte value)
		{
			base.WriteByte(value);
			bytesWritten++;
		}
		
		public void Init(UInt32 sampleRate, ushort channels)
		{
			this.sampleRate = sampleRate;
			this.channels = channels;
			WriteHeader(0);
		}

		// see https://ccrma.stanford.edu/courses/422/projects/WaveFormat/
		private void WriteHeader(UInt32 samples)
		{
			byte[] buf;
			
			Serializer serializer = new Serializer(EndianMode.LITTLE);
			Seek(0, SeekOrigin.Begin);
			StreamWriter sw = new StreamWriter(this);
			sw.Write("RIFF");
			sw.Flush();
			
			UInt32 subchunckSize = (samples * 2) + 36;
			buf = serializer.Serialize(subchunckSize);
			Write(buf, 0, buf.Length);
			
			sw.Write("WAVE");
			sw.Write("fmt ");
			sw.Flush();

			WavFmtBlock fmt  = new WavFileWriter.WavFmtBlock();
			fmt.nchan = channels;
			fmt.samp_per_sec = sampleRate;
			fmt.subChunckId = 16;
			fmt.tag = 1;
			fmt.avg_bytes_per_sec =  (UInt32) (sampleRate * channels * 2);
			fmt.block_align = (ushort)(2 * channels);
			fmt.nbits = 16;
			buf = serializer.Serialize(fmt);
			Write(buf, 0, buf.Length);
			
			sw.Write("data");
			sw.Flush();
			buf = serializer.Serialize(samples * 2);
			Write(buf, 0, buf.Length);
			//doReverseEndian = true;
			bytesWritten = 0;
		}
		
		public override void Close()
		{
			WriteHeader((UInt32)bytesWritten / 2); //Rewrite now we have the correct sample count
			base.Close();
		}
	}
}
