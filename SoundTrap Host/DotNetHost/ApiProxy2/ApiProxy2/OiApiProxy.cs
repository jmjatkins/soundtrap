﻿/* 
 * 	SoundTrap Software v1.0
 *
 *	Copyright (C) 2011-2014, John Atkins and Mark Johnson
 *
 *	This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
 *
 *	This file is part of the SoundTrap software. SoundTrap is an acoustic recording
 *	system intended for underwater acoustic measurements. This component of the
 *	SoundTrap project is free software: you can redistribute it and/or modify it
 *	under the terms of the GNU General Public License as published by the Free Software
 *	Foundation, either version 3 of the License, or any later version.
 *
 *	The SoundTrap software is distributed in the hope that it will be useful, but
 *	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License along with this
 *	code. If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Text;
using Newtonsoft.Json;


namespace OceanInstruments.ApiProxy2
{
	/// <summary>
	/// Interface to Ocean Instruments DB Web interface including Auth0 Logon
	/// </summary>
	
	public class OiApiProxy
	{
		private const int requestTimeout = 20000;
		private const string ModelUrl = "/api/Models";
		private const string DeviceUrl = "/api/Devices";
		private const string CalibrationUrl = "/api/Calibrations";
		private const string ProdtestResultUrl = "/api/ProdTestResult";
		private const string ProdtestSubResultUrl = "/api/ProdTestSubResult";
		
		public class Model
		{
			public virtual Int32 ModelId { get; set; }
			public virtual String Code { get; set; }
			public virtual String FullDesc { get; set; }
			public virtual ICollection<Device> Devices { get; set; }
		}
		
		public class Calibration
		{
			public virtual Int32 CalibrationId { get; set; }
			public virtual Int32 DeviceId { get; set; }
			public virtual Double LowFreq { get; set; }
			public virtual Double HighFreq { get; set; }
			public virtual Double Tone { get; set; }
			public virtual Double RefLevel { get; set; }
			public virtual DateTime DateCreated { get; set; }
			public virtual Device Device { get; set; }
			public virtual Int32 CalType { get; set; }
		}
		
		public class Device
		{
			public virtual Int32 DeviceId { get; set; }
			public virtual String SerialNo { get; set; }
			public virtual Int32 ModelId { get; set; }
			public virtual DateTime DateCreated { get; set; }
			public virtual Nullable<DateTime> DateUpdated { get; set; }
			public virtual String HardwareSerial { get; set; }
			public virtual ICollection<Calibration> Calibrations { get; set; }
			public virtual Model Model { get; set; }
		}
		
		public partial class ProdTestResult
		{
			public virtual int TestResultId { get; set; }
			public virtual int TestVersion { get; set; }
			public virtual System.DateTime DateCreated { get; set; }
			public virtual string TestJig { get; set; }
			public virtual string TestName { get; set; }
			public virtual string SoftwareVer { get; set; }
			public virtual int TestResultCode { get; set; }
			public virtual int DeviceId { get; set; }
			public virtual Device Device { get; set; }
			public virtual ICollection<ProdTestSubResult> ProdTestSubResults { get; set; }
		}
		
		public partial class ProdTestSubResult
		{
			public virtual int SubTestResultId { get; set; }
			public virtual int TestResultId { get; set; }
			public virtual int TestVersion { get; set; }
			public virtual string SubTestName { get; set; }
			public virtual Nullable<double> LowLimit { get; set; }
			public virtual Nullable<double> HighLimit { get; set; }
			public virtual Nullable<double> TestValue { get; set; }
			public virtual int TestResult { get; set; }
			public virtual ProdTestResult ProdTestResult { get; set; }
		}

		
		static private OiApiProxy instance;
		public static OiApiProxy Instance {
			get {
				if(instance ==null) instance = new OiApiProxy();
				return instance;
			}
		}

		string id_token = "";
		
		string clientId = "";
		string userName = "";
		string url = "";
		string passWord = "";
		
		static string configFileName = "oiDb.txt";
		
		static public bool IsConfigured {
			get {
				return System.IO.File.Exists(configFileName);
			}
		}
		
		private OiApiProxy()
		{
			using (StreamReader sr = new StreamReader(configFileName)) {
				userName = sr.ReadLine();
				passWord = sr.ReadLine();
				url = sr.ReadLine();
				clientId = sr.ReadLine();
			}
		}
		
		private string GetToken()
		{
			var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://oceaninstruments.auth0.com/oauth/ro" );
			httpWebRequest.Proxy = null;
			httpWebRequest.Method = "POST";
			httpWebRequest.ContentType = "application/json; charset=utf-8";
			httpWebRequest.Timeout = requestTimeout;
			
			var parameters = new Dictionary<string, string>
			{
				{ "client_id", clientId },
				{ "connection", "Username-Password-Authentication" },
				{ "username", userName },
				{ "password", passWord },
				{ "grant_type", "password" },
				{ "scope", "openid name given_name email role" }
			};
			
			string d = Newtonsoft.Json.JsonConvert.SerializeObject( parameters );
			byte[]  buffer = Encoding.ASCII.GetBytes(d);

			httpWebRequest.ContentLength = buffer.Length;
			Stream PostData = httpWebRequest.GetRequestStream();
			
			PostData.Write(buffer, 0, buffer.Length);
			PostData.Close();

			var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
			
			StreamReader _Answer = new StreamReader(httpResponse.GetResponseStream());
			string ss = _Answer.ReadToEnd();
			
			var data = Newtonsoft.Json.JsonConvert.DeserializeObject <Dictionary<string, string>>(ss);
			
			if(data.ContainsKey("id_token")) {
				string t = data["id_token"];
				return t;
			}
			else {
				throw new Exception("Bad autho id token return");
			}
		}
		
		private bool loggedIn {
			get { return id_token != "";}
		}
		
		private void Login()
		{
			id_token = GetToken();
		}
		
		private int GetModelId(string code) {
			var httpWebRequest = (HttpWebRequest)WebRequest.Create(url + ModelUrl + "?" );
			httpWebRequest.Proxy = null;
			httpWebRequest.ContentType = "application/json; charset=utf-8";
			httpWebRequest.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + id_token);
			httpWebRequest.Method = "GET";
			httpWebRequest.Timeout = requestTimeout;
			httpWebRequest.ContentType = "text/json";

			var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
			
			StreamReader _Answer = new StreamReader(httpResponse.GetResponseStream());
			string s = _Answer.ReadToEnd();
			
			var d = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Model>>(s);

			if(d.Count == 0) {
				throw new Exception("Failed to get model codes from OiDb");
			}
			
			foreach(Model m in d) {
				if( m.Code == code) return m.ModelId;
			}
			
			return -1;
		}
		
		private  int FindDevice(string serialNo)
		{
			var httpWebRequest = (HttpWebRequest)WebRequest.Create(url + DeviceUrl + "/search/" + serialNo );
			httpWebRequest.Proxy = null;
			httpWebRequest.Method = "GET";
			httpWebRequest.Timeout = requestTimeout;
			httpWebRequest.ContentType = "text/json";
			var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
			
			StreamReader _Answer = new StreamReader(httpResponse.GetResponseStream());
			string s = _Answer.ReadToEnd();
			
			var d = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Device>>(s);
			
			foreach (var r in d) {
				if(r.SerialNo == serialNo) return r.DeviceId;
			}
			
			return -1;
		}

		private int PostDevice(string serialNo, string modelCode, string hardwareSerial)
		{
			if(!loggedIn) Login();
			
			int modelId = GetModelId(modelCode);
			if(modelId == -1) {
				throw new Exception("Unknown Model Code");
			}
			
			var httpWebRequest = (HttpWebRequest)WebRequest.Create(url + DeviceUrl + "?" );
			httpWebRequest.Proxy = null;
			httpWebRequest.Method = "POST";
			httpWebRequest.ContentType = "application/json; charset=utf-8";
			httpWebRequest.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + id_token);
			httpWebRequest.Timeout = requestTimeout;
			
			Device device = new Device();
			device.SerialNo = serialNo;
			device.DateCreated = DateTime.Now;
			device.ModelId = modelId;
			device.HardwareSerial = hardwareSerial;
			string d = Newtonsoft.Json.JsonConvert.SerializeObject( device );
			byte[]  buffer = Encoding.ASCII.GetBytes(d);

			httpWebRequest.ContentLength = buffer.Length;
			Stream PostData = httpWebRequest.GetRequestStream();
			
			PostData.Write(buffer, 0, buffer.Length);
			PostData.Close();

			var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
			
			StreamReader reply = new StreamReader(((HttpWebResponse)httpWebRequest.GetResponse()).GetResponseStream());
			string s = reply.ReadToEnd();
			device = Newtonsoft.Json.JsonConvert.DeserializeObject<Device>(s);
			return device.DeviceId;
			
		}
		
		public int PostCalibration(string serialNo, string hardwareSerial, string modelCode, double low, double high, double reference, double tone, int calType = 0)
		{
			if(!loggedIn) Login();
			
			int deviceId = FindDevice(serialNo);
			if(deviceId ==-1) {
				PostDevice(serialNo, modelCode, hardwareSerial);
				deviceId = FindDevice(serialNo);
				if(deviceId ==-1) {
					throw new Exception("Failed to post new device to OiDb");
				}
			}
			
			var httpWebRequest = (HttpWebRequest)WebRequest.Create(url + CalibrationUrl  + "?" );
			httpWebRequest.Proxy = null;
			httpWebRequest.Method = "POST";
			httpWebRequest.ContentType = "application/json; charset=utf-8";
			httpWebRequest.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + id_token);
			httpWebRequest.Timeout = requestTimeout;

			Calibration cal = new Calibration();
			cal.DeviceId = deviceId;
			cal.LowFreq = low;
			cal.HighFreq = high;
			cal.RefLevel = reference;
			cal.Tone = tone;
			cal.DateCreated = DateTime.Now; //necessary?
			cal.CalType = calType;
			
			string d = Newtonsoft.Json.JsonConvert.SerializeObject( cal );
			byte[]  buffer = Encoding.ASCII.GetBytes(d);

			httpWebRequest.ContentLength = buffer.Length;
			Stream PostData = httpWebRequest.GetRequestStream();
			
			PostData.Write(buffer, 0, buffer.Length);
			PostData.Close();

			StreamReader reply = new StreamReader(((HttpWebResponse)httpWebRequest.GetResponse()).GetResponseStream());
			string s = reply.ReadToEnd();
			cal = Newtonsoft.Json.JsonConvert.DeserializeObject<Calibration>(s);
			return cal.CalibrationId;
		}


		
		public class TestSubResult {
			public TestSubResult(string name, int version, double lowLimit, double highLimit, double testValue, int testResult) {
				this.name = name;
				this.lowLimit = lowLimit;
				this.highLimit = highLimit;
				this.testValue = testValue;
				this.testResult = testResult;
			}
			public TestSubResult(string name, int version, double lowLimit, double highLimit, double testValue ) {
				this.name = name;
				this.version = version;
				this.lowLimit = lowLimit;
				this.highLimit = highLimit;
				this.testValue = testValue;
				this.testResult = (testValue > lowLimit && testValue <= highLimit) ? 0 : 1;
			}
			public string name;
			public int version;
			public double lowLimit;
			public double highLimit;
			public double testValue;
			public int testResult;
		}

		
		public bool DeviceExists(string serialNo)
		{
			return (FindDevice(serialNo) != -1);
		}
		
		public int PostTestResult(string serialNo, string hardwareSerial,string modelCode, string softwareVersion, string jigName, string testName, int testVersion, int testResultCode, List<TestSubResult> subResults)
		{
			if(!loggedIn) Login();
			
			int deviceId = FindDevice(serialNo);
			
			if(deviceId ==-1) {
				PostDevice(serialNo, modelCode, hardwareSerial);
				deviceId = FindDevice(serialNo);
				if(deviceId ==-1) {
					throw new Exception("Failed to post new device to OiDb");
				}
			}
			
			var httpWebRequest = (HttpWebRequest)WebRequest.Create(url + ProdtestResultUrl  + "?" );
			httpWebRequest.Proxy = null;
			httpWebRequest.Method = "POST";
			httpWebRequest.ContentType = "application/json; charset=utf-8";
			httpWebRequest.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + id_token);
			httpWebRequest.Timeout = requestTimeout;

			ProdTestResult ptr = new ProdTestResult();
			ptr.DeviceId = deviceId;
			ptr.TestJig = jigName;
			ptr.TestName = testName;
			ptr.TestResultCode = testResultCode;
			ptr.DateCreated = DateTime.Now; //necessary?
			ptr.SoftwareVer = softwareVersion;
			ptr.TestVersion = testVersion;
			
			string d = Newtonsoft.Json.JsonConvert.SerializeObject( ptr );
			byte[]  buffer = Encoding.ASCII.GetBytes(d);

			httpWebRequest.ContentLength = buffer.Length;
			Stream PostData = httpWebRequest.GetRequestStream();
			
			PostData.Write(buffer, 0, buffer.Length);
			PostData.Close();

			StreamReader reply = new StreamReader(((HttpWebResponse)httpWebRequest.GetResponse()).GetResponseStream());
			string s = reply.ReadToEnd();
			ptr = Newtonsoft.Json.JsonConvert.DeserializeObject<ProdTestResult>(s);
			
			foreach(TestSubResult tsr in subResults) {
				PostTestSubResult(ptr.TestResultId, tsr);
			}
			
			return ptr.TestResultId;
		}

		
		public int PostTestSubResult(int testResultId, TestSubResult subresult)
		{
			if(!loggedIn) Login();
			
			
			var httpWebRequest = (HttpWebRequest)WebRequest.Create(url + ProdtestSubResultUrl  + "?" );
			httpWebRequest.Proxy = null;
			httpWebRequest.Method = "POST";
			httpWebRequest.ContentType = "application/json; charset=utf-8";
			httpWebRequest.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + id_token);
			httpWebRequest.Timeout = requestTimeout;

			ProdTestSubResult ptr = new ProdTestSubResult();
			ptr.TestResultId = testResultId;
			ptr.SubTestName = subresult.name;
			ptr.TestVersion = subresult.version;
			ptr.LowLimit = subresult.lowLimit;
			ptr.HighLimit = subresult.highLimit;
			ptr.TestValue  = subresult.testValue;
			ptr.TestResult = subresult.testResult;
			
			string d = Newtonsoft.Json.JsonConvert.SerializeObject( ptr );
			byte[]  buffer = Encoding.ASCII.GetBytes(d);

			httpWebRequest.ContentLength = buffer.Length;
			Stream PostData = httpWebRequest.GetRequestStream();
			
			PostData.Write(buffer, 0, buffer.Length);
			PostData.Close();

			StreamReader reply = new StreamReader(((HttpWebResponse)httpWebRequest.GetResponse()).GetResponseStream());
			string s = reply.ReadToEnd();
			ptr = Newtonsoft.Json.JsonConvert.DeserializeObject<ProdTestSubResult>(s);
			return ptr.SubTestResultId;
		}


		
		
		
	}
}