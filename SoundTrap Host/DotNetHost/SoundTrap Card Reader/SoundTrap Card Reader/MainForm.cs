﻿/* 
 * 	SoundTrap Software v1.0
 *
 *	Copyright (C) 2011-2014, John Atkins and Mark Johnson
 *
 *	This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
 *
 *	This file is part of the SoundTrap software. SoundTrap is an acoustic recording
 *	system intended for underwater acoustic measurements. This component of the
 *	SoundTrap project is free software: you can redistribute it and/or modify it
 *	under the terms of the GNU General Public License as published by the Free Software
 *	Foundation, either version 3 of the License, or any later version.
 *
 *	The SoundTrap software is distributed in the hope that it will be useful, but
 *	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License along with this
 *	code. If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using SudarHost;

namespace ReadRawSd
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		List<SudarFile> files = new List<SudarFile>();
		int downLoadErrorCount = 0;
		int downLoadWarningCount = 0;
		bool doDecompress;
		string downloadPath;

		FileExtractionControl fec;
		
		Queue<SudarFile> filesToDownload = new Queue<SudarFile>();
		SudFileExpander fileExpander = new SudFileExpander();

		public MainForm()
		{
			InitializeComponent();
			
			Text = "SoundTrap Memory Card Reader " + Application.ProductVersion;
			
			try {
				if(Settings1.Default.FileSaveLocation == "") {
					Settings1.Default.FileSaveLocation = MakeDefaultFileSaveLocation();
				}
				else {
					//check path is valid
					try {
						Path.GetDirectoryName(Settings1.Default.FileSaveLocation);
					}
					catch {
						Settings1.Default.FileSaveLocation = MakeDefaultFileSaveLocation();
						Settings1.Default.Save();
					}
				}
			}
			catch {
				//ignore
			}

		}
		
		public void HideProgress()
		{
			panel5.Enabled = true;
			panel4.Visible = false;
		}

		public void ShowProgress(bool showCancel, string processName)
		{
			button2.Visible = showCancel;
			actionCanceled = false;
			label2.Text = processName;
			panel5.Enabled = false;
			panel4.Visible = true;
			Application.DoEvents();
		}
		
		bool actionCanceled = false;
		public bool ReportProgress(string s, int percentComplete)
		{
			label2.Text = s;
			progressBar1.Value = Math.Min( percentComplete, progressBar1.Maximum);
			Application.DoEvents();
			return actionCanceled;
		}


		
		public bool progressCallback(string status, int percentComplete)
		{
			progressBar1.Value = percentComplete;
			return false;
		}

		
		public string MakeDefaultFileSaveLocation() {
			string path;
			path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + Path.DirectorySeparatorChar + "SoundTrap";
			return path;
		}

		
		string BuildFolderSaveName()
		{
			string path = Settings1.Default.FileSaveLocation + Path.DirectorySeparatorChar;// + owner.Sudar.SerialNumber;
			if(! Directory.Exists(path) ) Directory.CreateDirectory(path);
			return path;
		}
		
		string BuildFileSaveName(string toPath, string description)
		{
			return toPath == "" ?
				BuildFolderSaveName() + Path.DirectorySeparatorChar + description + ".sud"
				: toPath + Path.DirectorySeparatorChar + description + ".sud";
		}
		
		public uint FindCardPageCount(uint start = 0, uint inc = 0)
		{
			byte[] buf = new byte[512];
			if(inc == 0) {
				inc = sudarDiskIo.FsParams.BlocksPerFile * sudarDiskIo.FsParams.PagesPerBlock * 512;
			}
			uint page = start;
			
			while(true) {
				sudarDiskIo.SetFileReadPageAddress(page);
				try {
					sudarDiskIo.FlashRead(out buf, 512);
				}
				catch
				{
					if(inc > 1) {
						return FindCardPageCount(page-inc, inc/2);
					} else return (uint)(page - 1);
				}
				page += inc;
			}
		}

		public void RefreashFileList(bool thorough)
		{
			ulong totalFileSize = 0;
			try {
				ShowProgress(false, "Loading Directory...");
				files = SudarFile.GetFileList(sudarDiskIo, progressCallback, thorough);
				listView1.Items.Clear();
				foreach(SudarFile f in files){
					ListViewItem lvi = listView1.Items.Add(f.description);
					lvi.SubItems.Add(f.date.ToString("yyyy-MM-dd HH:mm:ss" ));
					UInt64 fileSize = (UInt64)f.blocks * f.blockLength;
					lvi.SubItems.Add(String.Format("{0} MB",  fileSize/0x0100000 ));
					bool saved = File.Exists( BuildFileSaveName("", f.description) );
					lvi.SubItems.Add( saved ? "Yes" : "No");
					lvi.ForeColor = saved ? Color.Black : Color.Red;
					totalFileSize += fileSize/0x0100000;
				}
			}
			finally {
				HideProgress();
				listView1.Focus();
				if(listView1.Items.Count >0) {
					listView1.Items[ listView1.Items.Count-1 ].EnsureVisible();
					label1.Text = String.Format("{0} Files, {1:0.0} GB Used", listView1.Items.Count, totalFileSize/1000);
				}
			}
		}

		public void RefreashFileStatus()
		{
			for(int i=0; i<files.Count; i++) {
				ListViewItem lvi = listView1.Items[i];
				bool saved = File.Exists( BuildFileSaveName("", files[i].description) );
				lvi.SubItems[3].Text =  saved ? "Yes" : "No";
				lvi.ForeColor = saved ? Color.Black : Color.Red;
			}
		}
		
		
		SudarFile selectedFile;
		void ListView1MouseDown(object sender, MouseEventArgs e)
		{
			selectedFile = null;
			if (e.Button == MouseButtons.Right) //If it's the right button.
			{
				ListViewItem i = listView1.GetItemAt(e.X, e.Y);
				if(i!=null) {
					selectedFile = files[listView1.Items.IndexOf(i)];
					contextMenuStrip1.Items.Clear();
					
					contextMenuStrip1.Items.Add("Download", null, DownloadToolStripMenuItemClick);
					
					string wavFileName = Path.ChangeExtension(BuildFileSaveName("", selectedFile.description), "wav" );
					string logFileName = Path.ChangeExtension(BuildFileSaveName("", selectedFile.description), ".log.xml" );
					
					if(File.Exists( wavFileName ) ) {
						contextMenuStrip1.Items.Add("Open Wav File", null, OpenFileToolStripMenuItemClick);
					}
					if(File.Exists( logFileName ) ) {
						contextMenuStrip1.Items.Add("Open Log File", null, OpenLogFileToolStripMenuItemClick);
					}
					
					contextMenuStrip1.Show(this.listView1, e.Location);
				}
			}
		}
		
		void DownloadToolStripMenuItemClick(object sender, EventArgs e)
		{
			downloadPath = Settings1.Default.FileSaveLocation;
			DownLoadSelectedFiles();
		}
		
		
		void Button2Click(object sender, EventArgs e)
		{
			actionCanceled = true;
			
		}
		
		void OpenFileToolStripMenuItemClick(object sender, EventArgs e)
		{
			if(selectedFile !=null) {
				string filename = Path.ChangeExtension(BuildFileSaveName("", selectedFile.description), "wav" );
				if(File.Exists(filename)) {
					System.Diagnostics.Process.Start(filename);
				}
			}
		}
		
		void OpenLogFileToolStripMenuItemClick(object sender, EventArgs e)
		{
			if(selectedFile !=null) {
				string filename = Path.ChangeExtension(BuildFileSaveName("", selectedFile.description), "log.xml" );
				if(File.Exists(filename)) {
					System.Diagnostics.Process.Start(filename);
				}
			}
		}


		void DownLoadSelectedFiles()
		{
			ShowProgress(false, "Initialising download...");
			downLoadErrorCount = 0;
			downLoadWarningCount = 0;
			for(int i=0; i<listView1.Items.Count; i++) {
				if(listView1.Items[i].Selected) {
					filesToDownload.Enqueue( files[i]);
				}
			}
			doDecompress = checkBoxDecompress.Checked;
			RunDownloadMachine();
		}

		void RunDownloadMachine()
		{
			if( filesToDownload.Count > 0 ) {
				ShowProgress(true, String.Format("{0} Files remaining", filesToDownload.Count));
				backgroundWorker1.RunWorkerAsync(filesToDownload.Dequeue());
			}
			else {
				HideProgress();
				if(downLoadErrorCount > 0) {
					MessageBox.Show(string.Format("{0} error(s) during offload - please check file output", downLoadErrorCount), "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning );
				}
				
				if(downLoadWarningCount > 0) {
					MessageBox.Show(string.Format("{0} file(s) raised a warning during offload - please check log files for more information", downLoadWarningCount), "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning );
				}
				
				RefreashFileStatus();
			}
		}

		void BackgroundWorkerDoWork(object sender, DoWorkEventArgs e)
		{
			// This method runs on the background thread.
			
			DownloadResult downloadResult = DownloadResult.OK;
			SudarFile f = (SudarFile)e.Argument;

			string downLoadFileName = BuildFileSaveName(downloadPath, f.description);
			if(!Directory.Exists(Path.GetDirectoryName(downLoadFileName))) {
				Directory.CreateDirectory(Path.GetDirectoryName(downLoadFileName));
			}

			ProcessResult pr = f.Download(downLoadFileName, sudarDiskIo, UpdateProgress );
			if(pr == ProcessResult.Error) downloadResult = DownloadResult.Failed;
			else if(pr == ProcessResult.Cancelled) downloadResult = DownloadResult.Cancelled;
			else if( pr == ProcessResult.Success) {
				if(doDecompress) {
					SudFileExpander.SudFileExpanderResult r = fileExpander.ProcessFile(downLoadFileName, UpdateProgress);
					if ( r == SudFileExpander.SudFileExpanderResult.Error) {
						downloadResult = DownloadResult.Failed;
					}
					if ( r == SudFileExpander.SudFileExpanderResult.Warning) {
						downloadResult = DownloadResult.Warning;
					}
					else if(r == SudFileExpander.SudFileExpanderResult.Cancelled) {
						downloadResult = DownloadResult.Cancelled;
					}
				}
			}
			e.Result = downloadResult;
		}
		
		public bool UpdateProgress(string s, int percentComplete) {
			// This method runs on the background thread.
			backgroundWorker1.ReportProgress(percentComplete, s);
			return backgroundWorker1.CancellationPending;
		}


		
		void BackgroundWorker1ProgressChanged(object sender, ProgressChangedEventArgs e)
		{
			// This method runs on the main thread.
			if (ReportProgress(e.UserState  as string, e.ProgressPercentage))  {
				backgroundWorker1.CancelAsync();
			}
		}
		
		public enum DownloadResult{OK, Cancelled, Failed, Warning};

		void BackgroundWorker1RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			// This method runs on the main thread.
			DownloadResult dr = (DownloadResult)e.Result;
			if (e.Cancelled || e.Error != null ) {
				filesToDownload.Clear();
			}

			if (dr == DownloadResult.Failed ) {
				downLoadErrorCount++;
			}

			if (dr == DownloadResult.Warning ) {
				downLoadWarningCount++;
			}
			
			if (dr == DownloadResult.Cancelled) {
				filesToDownload.Clear();
			}
			RunDownloadMachine();
		}
		
		void CheckBox1CheckedChanged(object sender, EventArgs e)
		{
			listView1.BeginUpdate();
			foreach (ListViewItem i in listView1.Items) {
				i.Selected = checkBox1.Checked;
			}
			listView1.EndUpdate();
		}
		
		void MainFormLoad(object sender, EventArgs e)
		{
			checkBoxDecompress.Checked = Settings1.Default.Decompress;

		}
		
		void Button3Click(object sender, EventArgs e)
		{
			//FolderBrowserDialog fbd = new FolderBrowserDialog();
			//fbd.SelectedPath = downloadPath;
			//if(fbd.ShowDialog() == DialogResult.OK) {
			downloadPath = Settings1.Default.FileSaveLocation;
			DownLoadSelectedFiles();
			//}
		}
		
		
		void CheckBoxDecompressCheckedChanged(object sender, EventArgs e)
		{
			Settings1.Default.Decompress = checkBoxDecompress.Checked;
			Settings1.Default.Save();
		}
		
		void Panel6Click(object sender, EventArgs e)
		{
			System.Diagnostics.Process.Start("www.oceaninstruments.co.nz");

		}
		void ComboBox1DropDown(object sender, EventArgs e)
		{
			comboBox1.Items.Clear();
			drives = DriveInfo.GetDrives();
			comboBox1.Items.Clear();
			foreach(DriveInfo d in drives) {
				if(d.DriveType == DriveType.Removable) comboBox1.Items.Add(d.Name);
			}

		}
		
		
		void FormatCard() {
			
			int inc = (int)sudarDiskIo.FsParams.PagesPerFile * 64;
			long sector = sudarDiskIo.FsParams.noOfPages - inc;
			while(true) {
				
				ShowProgress(true, String.Format("{0} Sectors remaining", sector));
				try {
					sudarDiskIo.FormatSectors( sector, inc );
					sector -= inc;
				}
				catch {
					HideProgress();
					break;
				}
			}
			
		}
		
		void Button4Click(object sender, EventArgs e)
		{
			var r = MessageBox.Show("Are you sure you want to formatt the card?", "Warning!", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
			if(r == DialogResult.OK) {
				r = MessageBox.Show("All data will be permanently deleted", "Warning!", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
				if(r == DialogResult.OK) {
					FormatCard();
				}
			}
		}
		
		void ComboBox1SelectedIndexChanged(object sender, EventArgs e)
		{
			uint pages;
			char[] charsToTrim = {'\\'};
			if((comboBox1.SelectedItem != null) && (comboBox1.SelectedItem.ToString() != "")) {
				try {
					sudarDiskIo.Close();
					sudarDiskIo.Open(comboBox1.SelectedItem.ToString().Trim(charsToTrim));
					try {
						
						
						try {
							pages = FindCardPageCount();
							label3.Text = String.Format("Card: {0:0.0} GB", pages / ( 1E9 / 512) );
							Application.DoEvents();
							sudarDiskIo.FsParams.noOfPages = pages;
						}
						catch {
							MessageBox.Show("Warning - bad card format. See SoundTrap user guide for card formatting information", "Info", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
						}
						
					}
					catch(Exception ee) {
						label3.Text = ee.Message;
						
					}
					button4.Enabled = true;
					
				}
				catch {
					
				}
			}
		}
		
		SudarDiskIo sudarDiskIo = new SudarDiskIo();
		DriveInfo[] drives;

		void Button1Click(object sender, EventArgs e)
		{
			RefreashFileList(true);
		}
		void ToolsToolStripMenuItemClick(object sender, EventArgs e)
		{
			if(fec ==null) {
				fec = new FileExtractionControl();
			}

			var form1 = new Form();
			form1.Icon = this.Icon;
			form1.Text = " Soundtrap File Extraction";
			form1.Controls.Add(fec);
			fec.Dock = DockStyle.Fill;
			form1.ShowDialog(this);
			
		}
		void ExitToolStripMenuItemClick(object sender, EventArgs e)
		{
			Application.Exit();
		}
		void SetDefaultDownloadLocationToolStripMenuItemClick(object sender, EventArgs e)
		{
			FolderBrowserDialog fbd = new FolderBrowserDialog();
			fbd.SelectedPath = Settings1.Default.FileSaveLocation;
			if( fbd.ShowDialog() == DialogResult.OK) {
				Settings1.Default.FileSaveLocation = fbd.SelectedPath;
				Settings1.Default.Save();
			}
		}
		
		void DownloadToToolStripMenuItemClick(object sender, EventArgs e)
		{
			FolderBrowserDialog fbd = new FolderBrowserDialog();
			fbd.SelectedPath = downloadPath;
			if(fbd.ShowDialog() == DialogResult.OK) {
				downloadPath = fbd.SelectedPath;
				DownLoadSelectedFiles();
			}
		}
		void ListView1SelectedIndexChanged(object sender, EventArgs e)
		{
	
		}
		

		
		
	}
}
