/* 
* 	SoundTrap Software v1.0
*  
*	Copyright (C) 2011-2014, John Atkins and Mark Johnson
*
*	This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
*
*	This file is part of the SoundTrap software. SoundTrap is an acoustic recording
*	system intended for underwater acoustic measurements. This component of the 
*	SoundTrap project is free software: you can redistribute it and/or modify it 
*	under the terms of the GNU General Public License as published by the Free Software 
*	Foundation, either version 3 of the License, or any later version.
*
*	The SoundTrap software is distributed in the hope that it will be useful, but 
*	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
*	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
*	
*	You should have received a copy of the GNU General Public License along with this 
*	code. If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.Reflection;
using System.Runtime.Serialization;
using System.IO;
using System.Collections.Generic;

namespace SudarHost
{
	/// <summary>
	/// Description of binarySerializer.
	/// </summary>
	
	public enum EndianMode { LITTLE, BIG_16BIT };

	public class Serializer
	{
		EndianMode endianMode;
		
		public Serializer(EndianMode endianMode)
		{
			this.endianMode = endianMode;
		}
		
		private byte[] correctEndian(byte[] buf)
		{
			if(endianMode == EndianMode.LITTLE) {
				return buf;
			}
			else {
				byte temp1;
				byte temp2;
				for(int i=0; i + 3 < buf.Length ; i+=4) {
					temp1 = buf[i];
					temp2 = buf[i+1];

					buf[i] = buf[i+2];
					buf[i+1] = buf[i+3];
					buf[i+2] = temp1;
					buf[i+3] = temp2;
				}
				return buf;
			}
		}
		
		private byte[] streamToArray(Stream buf, int count)
		{
			byte[] result = new byte[count];
			for(int i=0;i<count;i++) {
				int j = buf.ReadByte();
				if(j==-1) throw new EndOfStreamException();
				result[i] = (byte)j;
			}
			return result;
		}
		
		public byte[] Serialize( object val )
		{
			MemoryStream buf = new MemoryStream();
			Type T = val.GetType();
			if(T.IsArray) {
				//T is an array
				IEnumerator i = (val as Array).GetEnumerator();
				while (i.MoveNext()) {
					byte[] b = Serialize(i.Current); //recursive
					buf.Write(b,0,b.Length);
				}
			}
			else if(!T.IsValueType) {
				//T is a class
				FieldInfo[] feilds = T.GetFields();
				foreach(FieldInfo fi in feilds)
				{
					if(!fi.IsNotSerialized) {
						byte[] b = Serialize(fi.GetValue(val)); //recursive
						buf.Write(b,0,b.Length);
					}
				}
			}
			else {
				//T is a value
				if(T == typeof(byte)) {
					buf.WriteByte((byte)val);
				}
				else if(T == typeof(bool)) {
					buf.WriteByte( Convert.ToByte( (bool)val));
				}
				else if(T == typeof(ushort)) {
					byte[] byteBuf = BitConverter.GetBytes((ushort)val);
					buf.Write(byteBuf,0,byteBuf.Length);
				}
				else if(T == typeof(short)) {
					byte[] byteBuf = BitConverter.GetBytes((short)val);
					buf.Write(byteBuf,0,byteBuf.Length);
				}
				else if(T == typeof(uint)) {
					byte[] byteBuf = BitConverter.GetBytes((uint)val);
					correctEndian(byteBuf);
					buf.Write(byteBuf,0,byteBuf.Length);
				}
				else if(T == typeof(int)) {
					byte[] byteBuf = BitConverter.GetBytes((int)val);
					correctEndian(byteBuf);
					buf.Write(byteBuf,0,byteBuf.Length);
				}
				else if(T == typeof(ulong)) {
					byte[] byteBuf = BitConverter.GetBytes((ulong)val);
					correctEndian(byteBuf);
					buf.Write(byteBuf,0,byteBuf.Length);
				}
				else if(T == typeof(long)) {
					byte[] byteBuf = BitConverter.GetBytes((long)val);
					correctEndian(byteBuf);
					buf.Write(byteBuf,0,byteBuf.Length);
				}
			}
			return buf.ToArray();;
		}

		public object Deserialize(Type T, Stream buf )
		{
			object o = Activator.CreateInstance(T);
			if(!T.IsValueType) {
				//T is a class
				FieldInfo[] feilds = o.GetType().GetFields();
				foreach(FieldInfo fi in feilds) {
					fi.SetValue(o, Deserialize(fi.FieldType, buf)); //recursive
				}
				return o;
			}
			else {
				//T is a value
				if(T == typeof(byte)) {
					o = (byte)buf.ReadByte();
				}
				else if(T == typeof(short)) {
					o = BitConverter.ToInt16(streamToArray(buf,2), 0);
				}
				else if(T == typeof(ushort)) {
					o = BitConverter.ToUInt16(streamToArray(buf,2), 0);
				}
				else if(T == typeof(int)) {
					o = (int )BitConverter.ToInt32(correctEndian(streamToArray(buf, 4)), 0);
				}
				else if(T == typeof(uint)) {
					o = (uint )BitConverter.ToUInt32(correctEndian(streamToArray(buf, 4)), 0);
				}
				else if(T == typeof(ulong)) {
					o = (ulong )BitConverter.ToUInt64(correctEndian(streamToArray(buf, 8)), 0);
				}
				else if(T == typeof(long)) {
					o = (long )BitConverter.ToInt64(correctEndian(streamToArray(buf, 8)), 0);
				}
				else throw new Exception("Invalid Type For Deserialization: " + T.Name);
				return o;
			}
		}
	}
}
