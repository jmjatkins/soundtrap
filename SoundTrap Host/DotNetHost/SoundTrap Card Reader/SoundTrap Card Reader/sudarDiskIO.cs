﻿/* 
 * 	SoundTrap Software v1.0
 *
 *	Copyright (C) 2011-2014, John Atkins and Mark Johnson
 *
 *	This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
 *
 *	This file is part of the SoundTrap software. SoundTrap is an acoustic recording
 *	system intended for underwater acoustic measurements. This component of the
 *	SoundTrap project is free software: you can redistribute it and/or modify it
 *	under the terms of the GNU General Public License as published by the Free Software
 *	Foundation, either version 3 of the License, or any later version.
 *
 *	The SoundTrap software is distributed in the hope that it will be useful, but
 *	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License along with this
 *	code. If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Crc;
using System.Xml;
using System.Threading;
using ReadRawSd;

namespace SudarHost
{
	//public enum ProcessResult { Success, Error, Cancelled };

	public class SudarDiskIo : ISudarFileIO
	{
		RawDiskIo.StreamStruct ioStream;
		UInt64 page = 0;
		uint pageinc = 1;
		
		public void Open(string drive)
		{
			//"\\.\E:\"`
			ioStream = RawDiskIo.CreateStream("\\\\.\\" + drive, FileAccess.Read);
			fsParams.BlocksPerFile = 256;
			fsParams.PagesPerBlock = 32;
			fsParams.pageSize = 512;
			fsParams.noOfPages = (UInt32)(256E9 / 512); //assume 256GB
		}
		
		public void Close()
		{
			RawDiskIo.DropStream(ioStream);
		}
		
		
		SudarFsParams fsParams = new SudarFsParams();
		public SudarFsParams FsParams {
			get {
				return fsParams;
			}
		}
		
		public bool SetFileReadPageAddress(uint address)
		{
			page = address;
			return true;
		}
		
		public void SetFileReadPageIncrement(uint inc)
		{
			pageinc = inc;
		}
		
		static byte[] correctEndian(byte[] buf)
		{
			byte temp1;
			byte temp2;
			for(int i=0; i + 3 < buf.Length ; i+=4) {
				temp1 = buf[i];
				temp2 = buf[i+1];

				buf[i] = buf[i+2];
				buf[i+1] = buf[i+3];
				buf[i+2] = temp1;
				buf[i+3] = temp2;
			}
			return buf;
		}

		static byte[] correctEndian2(byte[] buf)
		{
			byte temp1;
			for(int i=0; i+1 < buf.Length ; i+=2) {
				temp1 = buf[i];

				buf[i] = buf[i+1];
				buf[i+1] = temp1;
			}
			return buf;
		}
		
		public void FormatSectors(long sector, int count)
		{
			int bytes = count * 512;
			var buf = new byte[bytes];
			
			for(int i=0;i<bytes;i++) {
				buf[i] = 0xff;
			}
			
			RawDiskIo.WriteSector(sector*512, bytes, buf, ioStream);
		}
		
		public void FlashWrite(byte[] buf, uint len) {
			buf = correctEndian2(buf);
			RawDiskIo.WriteSector((long)page*512, (int)len, buf, ioStream);
			if(pageinc==1){
				page += len/512;
			}
			else{
				page += pageinc;
			}
		}
		
		public void FlashRead(out byte[] buf, uint len){
			buf = RawDiskIo.ReadSector((long)page*512, (int)len, ioStream);
			buf = correctEndian2(buf);
			if(pageinc==1){
				page += len/512;
			}
			else{
				page += pageinc;
			}
		}
		
		public DateTime Time {
			get {
				return DateTime.Now;
			}
		}
		
		public string SerialNumber {
			get {
				return "unknown";
			}
		}
		
		int percentFileSystemUsed;
		public int PercentFileSystemUsed {
			set {
				percentFileSystemUsed = value;
			}
		}
		
		public void GetStatus() {
			
		}
		
		public void ResetFsPipe() {
			
		}
		
	}
	
}
