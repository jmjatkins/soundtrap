﻿/* 
 * 	SoundTrap Software v1.0
 *
 *	Copyright (C) 2011-2014, John Atkins and Mark Johnson
 *
 *	This work is a derivative of the D3-API Copyright (C) 2008-2010, Mark Johnson
 *
 *	This file is part of the SoundTrap software. SoundTrap is an acoustic recording
 *	system intended for underwater acoustic measurements. This component of the
 *	SoundTrap project is free software: you can redistribute it and/or modify it
 *	under the terms of the GNU General Public License as published by the Free Software
 *	Foundation, either version 3 of the License, or any later version.
 *
 *	The SoundTrap software is distributed in the hope that it will be useful, but
 *	WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License along with this
 *	code. If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.IO;
using System.Runtime.InteropServices;

namespace SudarHost
{
	public static class RawDiskIo
	{
		public struct StreamStruct
		{
			public System.IO.Stream STR;
			public Microsoft.Win32.SafeHandles.SafeFileHandle SH;
			public bool isERROR;
		}
		
		[DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		internal static extern Microsoft.Win32.SafeHandles.SafeFileHandle CreateFile(string lpFileName,
		                                                                             FileAccess dwDesiredAccess, FileShare dwShareMode,
		                                                                             uint lpSecurityAttributes, FileMode dwCreationDisposition, uint
		                                                                             dwFlagsAndAttributes,
		                                                                             uint hTemplateFile);
		
		public static StreamStruct CreateStream(string drive, FileAccess type)
		{
			var retVal = new StreamStruct();
			Microsoft.Win32.SafeHandles.SafeFileHandle h;
			
			h = CreateFile(/* @"\\.\"+ */drive + "", type, FileShare.None, 0, FileMode.Open, 0, 0);
			if (h.IsInvalid) {
				retVal.isERROR = true;
				return retVal;
			}
			
			
			System.IO.Stream inPutFile = new FileStream(h, type);
			retVal.SH = h;
			retVal.STR = inPutFile;
			retVal.isERROR = false;
			return retVal;
		}
		
		public static byte[] ReadSector(long startingAddress, int length, StreamStruct iface)
		{
			byte[] b = new byte[length];
			if (!iface.SH.IsInvalid)
			{
				if (iface.STR.CanRead)
				{
					//b = 0; // [numberofsectors];
					iface.STR.Seek(startingAddress, SeekOrigin.Begin);
					iface.STR.Read(b, 0, length);
					return b;
				}
			}
			return null;
		}
		
		public static  int WriteSector(long startingsector, int length, byte[] data, StreamStruct iface)
		{
			if (!iface.SH.IsInvalid)
			{
				if (iface.STR.CanRead)
				{
					iface.STR.Seek(startingsector, SeekOrigin.Begin);
					iface.STR.Write(data, 0, length);
					iface.STR.Flush();
					return 0;
				}
			}
			return -1;
		}
		
		public static  bool DropStream(StreamStruct iface)
		{
			try
			{
				iface.STR.Close();
				iface.SH.Close();
				return true;
			}
			catch (Exception)
			{
				return false;
			}
		}
	}
}
