﻿/*
 * Created by SharpDevelop.
 * User: John
 * Date: 24/10/2017
 * Time: 12:46 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Windows.Forms;

namespace ReadRawSd
{
	/// <summary>
	/// Class with program entry point.
	/// </summary>
	internal sealed class Program
	{
		/// <summary>
		/// Program entry point.
		/// </summary>
		[STAThread]
		private static void Main(string[] args)
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			UnhandledExceptionHandler.UnhandledExceptionHandler.Init("SoundTrapCardReader");
			Application.Run(new MainForm());
		}
		
	}
}
